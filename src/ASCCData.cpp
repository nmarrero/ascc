/*	ASCCData.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/ASCCData.h"
#include "../include/Utils.h"

Taxon *TaxonArray::Find(unsigned int ExportID) const
{
	for(unsigned int index = 0, size = this->size(); index < size; ++index)
	{
		if (this->Item(index)->ExportID() == ExportID){return(this->Item(index));}
	}
	return(NULL);
}
int TaxonArray::Compare(Taxon** first, Taxon** second)
{
	double difference = (*first)->CurrentOKR() - (*second)->CurrentOKR();
	if (difference > 0.00000000){return(1);}
	else if (difference < 0.0000000){return(-1);}
	return(0);
}
	
Agebin::Agebin():m_Index(Agebin::sm_AgebinArray.size()),m_CurrentSet(false),m_MaxValue(0.0),m_MinValue(0.0),m_CurrentValue(0.0){Agebin::sm_AgebinArray.Add(this); }
void Agebin::ResetArray()
{
	for(unsigned int index = 0, size = Agebin::sm_AgebinArray.size(); index < size; ++index)
	{
		delete Agebin::sm_AgebinArray[index];
	}
	Agebin::sm_AgebinArray.Empty();
}

void Agebin::OlderThan(int linkindex)
{
	for(unsigned int index = 0, size = this->m_OlderThan.size(); index < size; ++index){if (this->m_OlderThan[index] == linkindex){return;}}
	this->m_OlderThan.Add(linkindex);
}
void Agebin::YoungerThan(int linkindex)
{
	for(unsigned int index = 0, size = this->m_YoungerThan.size(); index < size; ++index){if (this->m_YoungerThan[index] == linkindex){return;}}
	this->m_YoungerThan.Add(linkindex);
}
bool Agebin::GenerateValue()
{
	double MinValue = this->m_MinValue, MaxValue = this->m_MaxValue;
	AgebinArray& Array = Agebin::GetArray();
	unsigned int arrayindex = 0;
	bool RepeatGeneration = false;
	for(unsigned int index = 0, size = this->m_YoungerThan.size(); index < size; ++index)
	{
		arrayindex = this->m_YoungerThan[index];
		Agebin *bin = Array.Find(arrayindex);
		if (this->m_Index != arrayindex && bin->CurrentSet() && bin->CurrentValue() < MaxValue)// && bin->CurrentValue() >= MinValue)
		{
			//wxMessageBox(wxString::Format("Setting younger than (max): %f (before %f,%f)",Array[arrayindex]->CurrentValue(),MinValue,MaxValue));
			MaxValue = bin->CurrentValue();
		}
		else if (this->m_Index != arrayindex && !bin->CurrentSet()){RepeatGeneration = true;}
	}
	for(unsigned int index = 0, size = this->m_OlderThan.size(); index < size; ++index)
	{
		arrayindex = this->m_OlderThan[index];
		Agebin *bin = Array.Find(arrayindex);
		if (this->m_Index != arrayindex && bin->CurrentSet() && bin->CurrentValue() > MinValue)// && bin->CurrentValue() <= MaxValue)
		{
			//wxMessageBox(wxString::Format("Setting older than (min): %f (before %f,%f)",Array[arrayindex]->CurrentValue(),MinValue,MaxValue));
			MinValue = bin->CurrentValue();
		}
		else if (this->m_Index != arrayindex && !bin->CurrentSet()){RepeatGeneration = true;}
	}
	//wxASSERT_MSG(MaxValue >= MinValue,"Max Age is less than Min Age!");
	double Range = MaxValue - MinValue; if (Range < 0.00){Range = 0.00;}
		this->m_CurrentValue = Range*double(rand())/double(RAND_MAX) + MinValue;
		this->m_CurrentSet = true;
	return(RepeatGeneration);
}

int CompareAgebinRange(Agebin **first, Agebin **second)
{
	double Range1 = (*first)->MaxValue() - (*first)->MinValue();
	double Range2 = (*second)->MaxValue() - (*second)->MinValue();
	return(Range1>Range2?1:(Range1<Range2?-1:0));
}
AgebinArray Agebin::sm_AgebinArray;
AgebinArray::~AgebinArray()
{
	for(unsigned int index = 0, size = this->size(); index < size; ++index)
	{
		delete this->Item(index);
	}
	this->Empty();
}
Agebin *AgebinArray::Find(unsigned int Index)
{
	for(unsigned int index = 0, size = this->size(); index < size; ++index)
	{
		if (this->Item(index)->Index() == Index){return(this->Item(index));}
	}
	return(NULL);
}
void GenerateAgebinArray(TaxonArray& Taxa)
{
	//Reset the Taxa export IDs
	Taxon::ResetExportIDs();
	Agebin::ResetArray();

	for(unsigned int index = 0, size = Taxa.size(); index < size; ++index)
	{
		//Create an agebin for this taxon
		Agebin *bin = new Agebin();
		bin->Index(Taxa[index]->ExportID());
		bin->MinValue(Taxa[index]->MinOKR());
		bin->MaxValue(Taxa[index]->MaxOKR());
	}
	AgebinArray &Ages = Agebin::GetArray();
		
	//The agebins exist, now to set up the links
	for(unsigned int index = 0, size = Taxa.size(); index < size; ++index)
	{
		for(unsigned int link = 0, links = Taxa[index]->OKRLinks(); link < links; ++link)
		{
			OKRLink& CurrentLink = Taxa[index]->Link(link);
			if (CurrentLink.LinkedOKR()->Owner())
			{
				if (CurrentLink.Type() == OLDER_THAN)
				{
					Ages[index]->OlderThan(CurrentLink.LinkedOKR()->Owner()->ExportID());
					Ages.Find(CurrentLink.LinkedOKR()->Owner()->ExportID())->YoungerThan(Ages[index]->Index());
				}
				else 
				{
					Ages[index]->YoungerThan(CurrentLink.LinkedOKR()->Owner()->ExportID());
					Ages.Find(CurrentLink.LinkedOKR()->Owner()->ExportID())->OlderThan(Ages[index]->Index());
				}
			}
			else
			{
				for(unsigned int age = 0, totalages = Ages.size(); age < totalages; ++age)
				{
					//Restricting age based on time scale only matters if there is another taxon 
					//	that shares the dates of the time scale, so figure out if that's true and
					//	set the link to that taxon
					if (index != age &&
						Ages[age]->MaxValue() == CurrentLink.LinkedOKR()->MaxOKR() &&
						Ages[age]->MinValue() == CurrentLink.LinkedOKR()->MinOKR())
					{
						if (CurrentLink.Type() == OLDER_THAN)
						{
							Ages[index]->OlderThan(Ages[age]->Index()); 
							Ages[age]->YoungerThan(Ages[index]->Index());
						}
						else 
						{
							Ages[index]->YoungerThan(Ages[age]->Index()); 
							Ages[age]->OlderThan(Ages[index]->Index());
						}
					}
				}
			}
		}
	}
	//Go through the age bins and reorder them based on their age ranges
	Ages.Sort(&CompareAgebinRange);
}
ASCCDataAnalysisResult::ASCCDataAnalysisResult(unsigned int trees, unsigned int replicates):m_Size(trees),m_Replicates(replicates),m_Taxa(0),
	m_MIG(new double[replicates*trees]),m_GER(new double[replicates*trees]),
	m_MSM(new double[replicates*trees]),m_dAGL(new double[replicates*trees]),
	m_FAD(NULL),m_FADAges(NULL),m_MinGhostLineages(NULL),m_MaxGhostLineages(NULL),
	m_Lo(new double[replicates]),m_Lm(new double[replicates])
{}
ASCCDataAnalysisResult::ASCCDataAnalysisResult(ASCCDataAnalysisResult &other):m_Size(other.m_Size),m_Replicates(other.m_Replicates),
	m_MIG(new double[this->m_Replicates*this->m_Size]),m_GER(new double[this->m_Replicates*this->m_Size]),
	m_MSM(new double[this->m_Replicates*this->m_Size]),m_dAGL(new double[this->m_Replicates*this->m_Size]),
	m_FAD(new double[this->m_Replicates*this->m_Size*this->m_Taxa]),m_FADAges(new double[this->m_Replicates*this->m_Taxa]),
	m_Lo(new double[this->m_Replicates]),m_Lm(new double[this->m_Replicates])
{
	this->Taxa(other.m_Taxa);
	memcpy(this->m_MIG,other.m_MIG,this->m_Replicates*this->m_Size*sizeof(double));
	memcpy(this->m_GER,other.m_GER,this->m_Replicates*this->m_Size*sizeof(double));
	memcpy(this->m_MSM,other.m_MSM,this->m_Replicates*this->m_Size*sizeof(double));
	memcpy(this->m_dAGL,other.m_dAGL,this->m_Replicates*this->m_Size*sizeof(double));
	memcpy(this->m_FAD,other.m_FAD,this->m_Replicates*this->m_Size*this->m_Taxa*sizeof(double));
	memcpy(this->m_FADAges,other.m_FADAges,this->m_Replicates*this->m_Taxa*sizeof(double));
	memcpy(this->m_MinGhostLineages,other.m_MinGhostLineages,this->m_Size*this->m_Taxa*sizeof(double));
	memcpy(this->m_MaxGhostLineages,other.m_MaxGhostLineages,this->m_Size*this->m_Taxa*sizeof(double));
	memcpy(this->m_Lo,other.m_Lo,this->m_Replicates*sizeof(double));
	memcpy(this->m_Lm,other.m_Lm,this->m_Replicates*sizeof(double));
}
ASCCDataAnalysisResult::~ASCCDataAnalysisResult()
{
	wxDELETEA(this->m_MIG); 
	wxDELETEA(this->m_GER); 
	wxDELETEA(this->m_MSM); 
	wxDELETEA(this->m_dAGL);
	wxDELETEA(this->m_FAD);
	wxDELETEA(this->m_FADAges);
	wxDELETEA(this->m_MinGhostLineages);
	wxDELETEA(this->m_MaxGhostLineages);
	wxDELETEA(this->m_Lo); 
	wxDELETEA(this->m_Lm);
}

ASCCDataAnalysis::ASCCDataAnalysis():
	m_Replicates(1000),m_Seed(time(NULL)),m_Result(NULL),m_MIG(true),m_MSM(true),m_GER(true),m_dAGL(true){}
ASCCDataAnalysis::~ASCCDataAnalysis(){wxDELETE(this->m_Result);}
bool ASCCDataAnalysis::CalculateScores(PTreeCtrl *ctrl)
{
	if (!this->m_MIG && !this->m_MSM && !this->m_GER && !this->m_dAGL){return(false);}
	unsigned int ReplicatePercent = min<unsigned long>(this->m_Replicates/100,50);
	if (ReplicatePercent == 0){ReplicatePercent = 1;}
	wxProgressDialog progress("Calculating", "Calculating Scores...", this->m_Replicates/ReplicatePercent + 1, NULL, wxPD_APP_MODAL|wxPD_SMOOTH|wxPD_CAN_ABORT|wxPD_ELAPSED_TIME|wxPD_ESTIMATED_TIME);
	progress.Show();
	if (this->m_Result && this->m_Result->Size() != ctrl->Trees()){wxDELETE(this->m_Result);} 
	if (!this->m_Result){this->m_Result = new ASCCDataAnalysisResult(ctrl->Trees(), this->m_Replicates);}
	this->m_TreeNames.Empty(); this->m_TreeNames = ctrl->TreeNames(); ctrl->UpdateLeafHashes(); ctrl->DistributeGhostLineages();

	//(Re)create Taxon Array and Agebin Array
		this->m_TaxonArray.Empty(); 
		Agebin::ResetArray();
		wxArrayString TaxaNames, TreeTaxaNames;
		ctrl->GetLeaves(TaxaNames);
		TaxaNames.Sort();

		Taxon *Current = NULL;
		wxString OldName = wxEmptyString;
		for(unsigned int index = 0; index < TaxaNames.size(); ++index)
		{
			if (TaxaNames[index] == OldName){TaxaNames.RemoveAt(index); index--; continue;}
			Current = Taxon::FindTaxonByName(TaxaNames[index]);
			wxASSERT_MSG(Current,"Unable to find taxon '" + TaxaNames[index] + "' in current taxa");
			if (Current){this->m_TaxonArray.Add(Current);}
			OldName = TaxaNames[index];
		}
		GenerateAgebinArray(this->m_TaxonArray);
		this->m_Result->Taxa(this->m_TaxonArray.size());
		this->m_TreeData.Empty(); this->m_TreeData = ctrl->GetTreeNameData();

	//PTreeNode *TaxonNode = NULL, *TaxonParentNode = NULL;
	for(unsigned int replicate = 0; replicate < this->m_Replicates; ++replicate)
	{
		if (replicate % ReplicatePercent == 0 && !progress.Update(replicate/ReplicatePercent)){return(false);}
		this->NewReplicate();
		double Lo = this->m_Result->Lo(replicate,this->CalculateLo()), Lm = this->m_Result->Lm(replicate,this->CalculateLm()), CurrentMIG = 0.0;
		for(unsigned int tree = 0, trees = ctrl->Trees(); tree < trees; ++tree)
		{
			if (this->m_MIG || this->m_GER || this->m_MSM || this->m_dAGL)
				{CurrentMIG = this->m_Result->MIG(replicate,tree,this->CalculateMIG(ctrl,tree));}
			if (this->m_MSM){this->m_Result->MSM(replicate,tree,this->CalculateMSM(Lo,CurrentMIG));}
			if (this->m_GER){this->m_Result->GER(replicate,tree,this->CalculateGER(Lo,Lm,CurrentMIG));}
			if (this->m_dAGL){this->m_Result->dAGL(replicate,tree,this->CalculatedAGL(CurrentMIG));}
			for(unsigned int taxon = 0, taxa = this->m_Result->Taxa(); taxon < taxa; taxon++)
			{
				int ExportID = this->m_TaxonArray[taxon]->ExportID();
				PTreeNode* TheTaxon = ctrl->GetTree(tree)->FindLeaf(this->m_TaxonArray[taxon]->Name());
				//TaxonNode = TaxonParentNode = NULL;
				if (TheTaxon)
				{
					this->m_Result->MinGhostLineage(tree,ExportID-1,TheTaxon->MinGhostLineage());
					this->m_Result->MaxGhostLineage(tree,ExportID-1,TheTaxon->MaxGhostLineage());
					this->m_Result->FADAge(replicate,ExportID-1,this->m_TaxonArray[taxon]->CurrentOKR());
					this->m_Result->FAD(replicate, tree, (ExportID-1), TheTaxon->FAD());
					//TaxonNode = (PTreeNode *)TheTaxon->GetParent();
					//if (TaxonNode)
					//{
					//	TaxonParentNode = (PTreeNode *)TaxonNode->GetParent();
					//	if (TaxonParentNode){this->m_Result->FAD(replicate,tree,(ExportID-1),TaxonParentNode->FAD());}
					//	else {this->m_Result->FAD(replicate,tree, ExportID-1, TaxonNode->FAD());}
					//}
				}
				if (!TheTaxon)// || !TaxonNode)
				{
					this->m_Result->FAD(replicate,tree,ExportID-1,0.00);
					this->m_Result->FADAge(replicate,ExportID-1,0.00);
				}
			}
		}
	}
	for(unsigned int tree = 0, trees = ctrl->Trees(); tree < trees; ++tree)
	{
		for(unsigned int taxon = 0, taxa = this->m_Result->Taxa(); taxon < taxa; taxon++)
		{
			int ExportID = this->m_TaxonArray[taxon]->ExportID();
			PTreeNode* TheTaxon = ctrl->GetTree(tree)->FindLeaf(this->m_TaxonArray[taxon]->Name());
			if (TheTaxon)
			{
				this->m_Result->MinGhostLineage(tree,ExportID-1,TheTaxon->CalculatedMinGhostLineage());
				this->m_Result->MaxGhostLineage(tree,ExportID-1,TheTaxon->CalculatedMaxGhostLineage());
			}
			else
			{
				this->m_Result->MinGhostLineage(tree,ExportID-1,0.00);
				this->m_Result->MaxGhostLineage(tree,ExportID-1,0.00);
			}
		}
	}
	return(true);
}
void ASCCDataAnalysis::Save(wxString Filename)
{
	if (Filename.IsEmpty())
	{
		Filename = wxFileSelector("Select save file:",wxEmptyString,wxEmptyString,"ASCC Data Files|*.asccdata","ASCC Data Files (*.asccdata)|*.asccdata|All Files (*.*)|*.*",wxFD_SAVE|wxFD_OVERWRITE_PROMPT,NULL);
		if (Filename.IsEmpty()){return;}
	}
	wxFileOutputStream OutputStream(Filename); if (!OutputStream.IsOk()){return;}
	wxDataOutputStream DataStream(OutputStream);
			
	DataStream << wxInt8(this->m_MIG);
	DataStream << wxInt8(this->m_MSM);
	DataStream << wxInt8(this->m_GER);
	DataStream << wxInt8(this->m_dAGL);
	DataStream << wxInt32(this->m_Replicates);
	DataStream << wxInt32(this->m_Seed);
	DataStream << wxInt32(this->m_Result->Size());
	DataStream << wxInt32(this->m_TreeNames.size());
	for(unsigned int index = 0, size = this->m_TreeNames.size(); index < size; index++)
	{
		DataStream << this->m_TreeNames[index];
	}
	for(unsigned int replicate = 0, size = this->m_Replicates; replicate < size; ++replicate)
	{
		DataStream << this->m_Result->Lo(replicate);
		DataStream << this->m_Result->Lm(replicate);
		for(unsigned int tree = 0, trees = this->m_Result->Size(); tree < trees; ++tree)
		{
			DataStream << this->m_Result->MIG(replicate,tree);
			DataStream << this->m_Result->GER(replicate,tree);
			DataStream << this->m_Result->MSM(replicate,tree);
			DataStream << this->m_Result->dAGL(replicate,tree);
		}
	}
}
ASCCDataAnalysis *ASCCDataAnalysis::Load(wxString Filename)
{
	if (Filename.IsEmpty())
	{
		Filename = wxFileSelector("Select file to load:",wxEmptyString,wxEmptyString,"ASCC Data Files|*.asccdata","ASCC Data Files (*.asccdata)|*.asccdata|All Files (*.*)|*.*",wxFD_OPEN,NULL);
		if (Filename.IsEmpty()){return(NULL);}
	}
	wxFileInputStream InputStream(Filename);
	if (!InputStream.IsOk()){return(NULL);}

	//Ok, continue...
	wxDataInputStream DataStream(InputStream);
	wxInt32 replicates, seed, trees, treenames;
	wxInt8 MIG, MSM, GER, dAGL;
	double Data;
	wxString str;

	DataStream >> MIG >> MSM >> GER >> dAGL;
	DataStream >> replicates;
	DataStream >> seed;
	DataStream >> trees;
	DataStream >> treenames;

	ASCCDataAnalysis* data = new ASCCDataAnalysis();
	data->SetScoreMIG(MIG == 1);
	data->SetScoreMSM(MSM == 1);
	data->SetScoreGER(GER == 1);
	data->SetScoredAGL(dAGL == 1);
	data->Replicates(replicates);
	data->Seed(seed);
	ASCCDataAnalysisResult* result = new ASCCDataAnalysisResult(trees, replicates);
	for(unsigned int index = 0, size = treenames; index < size; ++index)
	{
		DataStream >> str;
		data->m_TreeNames.Add(str);
	}
	for(unsigned int replicate = 0, size = replicates; replicate < size; ++replicate)
	{
		DataStream >> Data; result->Lo(replicate,Data);
		DataStream >> Data; result->Lm(replicate,Data);
		for(int tree = 0; tree < treenames; ++tree)
		{
			DataStream >> Data; result->MIG(replicate,tree,Data);
			DataStream >> Data; result->GER(replicate,tree,Data);
			DataStream >> Data; result->MSM(replicate,tree,Data);
			DataStream >> Data; result->dAGL(replicate,tree,Data);
		}
	}
	data->m_Result = result;
	return(data);
}
void ASCCDataAnalysis::NewReplicate()
{
	//Inc currentreplicate or reset to 0
	if (this->m_CurrentReplicate++ >= this->m_Replicates){this->m_CurrentReplicate = 0;}
	//Go through the taxa and set each one's age
	Taxon *Current = NULL;
	for (unsigned int index = 0, size = this->m_TaxonArray.size(); index < size; ++index)
	{
		this->m_TaxonArray[index]->CurrentOKR(0.00);
	}
	for(unsigned int index = 0, size = Agebin::GetArray().size(); index < size; ++index)
	{
		Agebin::GetArray()[index]->Reset();
	}

	wxString Results;
	bool RepeatGeneration = true;
	unsigned int RepeatCounter = 1;
	while(RepeatGeneration && RepeatCounter++ < 5)
	{
		RepeatGeneration = false;
		for (unsigned int index = 0, size = Agebin::GetArray().size(); index < size; ++index)
		{
			unsigned int TaxonExportID = Agebin::GetArray()[index]->Index();
			Current = this->m_TaxonArray.Find(TaxonExportID);
			RepeatGeneration |= Agebin::GetArray()[index]->GenerateValue();
			//Results += wxString::Format("(%d,%d) %s = %f\n",index,TaxonIndex,Current->Name(),Agebin::GetArray()[index]->CurrentValue());
			//wxASSERT_MSG(CurrentOKR > 0.0,"Current OKR for Taxon '" + Current->Name() + wxString::Format("' is outside range (%f)",CurrentOKR));
			Current->CurrentOKR(Agebin::GetArray()[index]->CurrentValue());
		}
	}
	//wxMessageBox(Results);
	this->m_TaxonArray.Sort(&TaxonArray::Compare);

}
double ASCCDataAnalysis::CalculateMIG(PTreeCtrl *ctrl, unsigned int TreeIndex){return(ctrl->CalculateMIG(TreeIndex));}
double ASCCDataAnalysis::CalculateLo()
{
	//double Oldest = this->m_TaxonArray[this->m_TaxonArray.size()-1]->CurrentOKR(), Youngest = this->m_TaxonArray[0]->CurrentOKR();
	double Lo = this->m_TaxonArray[this->m_TaxonArray.size()-1]->CurrentOKR() - this->m_TaxonArray[0]->CurrentOKR();
	//wxASSERT(Lo > 0.00);
	return Lo;
}
double ASCCDataAnalysis::CalculateLm()
{
	double Lm = 0.0;
	double MaxAge = 0.0;
	for(unsigned int index = 0, size = this->m_TaxonArray.size(); index < size; ++index)
	{
		if (this->m_TaxonArray[index]->CurrentOKR() > MaxAge){MaxAge = this->m_TaxonArray[index]->CurrentOKR();}
		Lm += this->m_TaxonArray[index]->CurrentOKR();
	}
	Lm = MaxAge*this->m_TaxonArray.size() - Lm;
	//wxASSERT(Lm > 0.00);
	return Lm;
}
double ASCCDataAnalysis::CalculateMSM(double Lo, double MIG)
{
	return(Lo/MIG);
}
double ASCCDataAnalysis::CalculateGER(double Lo, double Lm, double MIG)
{
	return(1.0 - ((MIG - Lo)/(Lm - Lo)));
}
double ASCCDataAnalysis::CalculatedAGL(double MIG)
{
	double taxa = double(this->m_TaxonArray.size());
	return(MIG/(2.0*taxa-2.0));
}
	

ExposureString UIntToStr(unsigned int val){return(ExposureString(wxString::Format("%d",val)));}
unsigned int StrToUInt(ExposureString val){unsigned long ret = 0; wxString valstr = val; valstr.Replace(".",""); valstr.Replace(",",""); valstr.ToULong(&ret); return(ret);}
DECLARE_EXPOSURE_TEXT_TYPE(unsigned int, UIntToStr, StrToUInt)

BEGIN_EXPOSURE_TABLE(ASCCDataAnalysis,"Analyzing Data")
	EXPOSURE_GROUP("Calculation Options")->Expand(false)

		EXPOSE_TEXT("Seed","RNG Seed",&ASCCDataAnalysis::Seed,&ASCCDataAnalysis::Seed,"The random seed to use for this data analysis")->AlignLeft()
		EXPOSE_STATIC_TEXT("Using the same random seed on the same tree "
						   "data with the same number of replicates "
						   "will give the same results, \n     so "
						   "choosing an easy to remember seed or saving "
						   "the one entered here can be useful.")->AlignLeft()
		EXPOSURE_ROW()
		EXPOSURE_SPACE(10,10)
		EXPOSURE_ROW()
		EXPOSE_TEXT("Replicates","How many replicates?\n(1 - 1,000,000)",&ASCCDataAnalysis::Replicates,&ASCCDataAnalysis::Replicates,"The number of replicates to use for this data analysis")->AlignLeft()
		EXPOSE_STATIC_TEXT("Note: For older ages and/or broader "
						   "age ranges, more replicates are "
						   "required for thorough exploration \n    "
						   "of the age range")->AlignLeft()
		EXPOSURE_ROW()
		EXPOSE_CHECKBOX("MaxAgeWarnings","Show Max Age Warnings",&ASCCDataAnalysis::ShowMaxAgeWarning,&ASCCDataAnalysis::ShowMaxAgeWarning,"Whether or not to show warnings if taxa have a max age <= 0.00")->AlignLeft()
	EXPOSURE_END_GROUP()
	EXPOSURE_ROW()
	EXPOSURE_GROUP("Metrics to Calculate")
		EXPOSE_CHECKBOX("MIG","MIG (Minimum Implied Gap)",&ASCCDataAnalysis::GetScoreMIG,&ASCCDataAnalysis::SetScoreMIG,"Select this to include MIG calculation output in the scores")->AlignRight()
		EXPOSE_STATIC_TEXT(" - The sum of all of the ghost lineages implied by the tree topology and FADs of the taxa under study.")->AlignLeft()
		EXPOSURE_ROW()
		EXPOSE_CHECKBOX("MSM","MSM* (modified Manhattan Stratigraphic Measure)",&ASCCDataAnalysis::GetScoreMSM,&ASCCDataAnalysis::SetScoreMSM,"Select this to include MSM* calculation output in the scores")->AlignRight()
		EXPOSE_STATIC_TEXT(" - Calculates the distance the MIG value for the tree is from the minimum possible value.")->AlignLeft()
		EXPOSURE_ROW()
		EXPOSE_CHECKBOX("GER","GER (Gap Excess Ratio)",&ASCCDataAnalysis::GetScoreGER,&ASCCDataAnalysis::SetScoreGER,"Select this to include GER calculation output in the scores")->AlignRight()
		EXPOSE_STATIC_TEXT(" - Calculates the distance the MIG value is from the minimum and maximum possible value. Values closer to 1.0 preferred.")->AlignLeft()
		EXPOSURE_ROW()
		EXPOSE_CHECKBOX("dAGL",L"\u25B3AGL (Average Ghost Lineage)",&ASCCDataAnalysis::GetScoredAGL,&ASCCDataAnalysis::SetScoredAGL,L"Select this to include \u25B3AGL calculation output in the scores")->AlignRight()
		EXPOSE_STATIC_TEXT(" - (unpublished statistic) Calculates the average length of the ghost lineages in the tree.\n   Calculated by dividing the MIG value by the total number of branches and internodes within the tree topology.")->AlignLeft()
	EXPOSURE_END_GROUP()
END_EXPOSURE_TABLE()

wxString AnalyzeTNTScores(wxString ScoreLocation)
{
	if (ScoreLocation.IsEmpty())
	{
		wxDirDialog dlg(wxTheApp->GetTopWindow(),"Enter the location of the TNT files:");
		if (dlg.ShowModal() == wxID_CANCEL){return(wxEmptyString);}
		ScoreLocation = dlg.GetPath();
	}
	wxFileName fname;
	if (wxDirExists(ScoreLocation)){fname.AssignDir(ScoreLocation);}
	else {fname.Assign(ScoreLocation);}

	wxFileInputStream GerInputStream(fname.GetPath(wxPATH_GET_VOLUME|wxPATH_GET_SEPARATOR).Append("ger.out")); wxTextInputStream Ger(GerInputStream);
	wxFileInputStream MigInputStream(fname.GetPath(wxPATH_GET_VOLUME|wxPATH_GET_SEPARATOR).Append("MIG.out")); wxTextInputStream Mig(MigInputStream);
	wxFileInputStream MsmInputStream(fname.GetPath(wxPATH_GET_VOLUME|wxPATH_GET_SEPARATOR).Append("msm.out")); wxTextInputStream Msm(MsmInputStream);

	wxString ReturnString;
	if (!GerInputStream.IsOk()){ReturnString += "Unable to open ger.out\n";}
	if (!MigInputStream.IsOk()){ReturnString += "Unable to open MIG.out\n";}
	if (!MsmInputStream.IsOk()){ReturnString += "Unable to open msm.out\n";}
	if (!ReturnString.IsEmpty()){ReturnString.Prepend("Error: "); wxLogError(ReturnString); return(ReturnString);}

	unsigned int TreesToAnalyze = (unsigned int)::wxGetNumberFromUser("","Enter number of trees:","TNT Analysis",1,1,4,NULL);

	double GerMin[4], GerMax[4], MigMin[4], MigMax[4], MsmMin[4], MsmMax[4];
	GerMin[0] = GerMin[1] = GerMin[2] = GerMin[3] = 10000.0;
	GerMax[0] = GerMax[1] = GerMax[2] = GerMax[3] = -10000.0;
	MigMin[0] = MigMin[1] = MigMin[2] = MigMin[3] = 10000.0;
	MigMax[0] = MigMax[1] = MigMax[2] = MigMax[3] = -10000.0;
	MsmMin[0] = MsmMin[1] = MsmMin[2] = MsmMin[3] = 10000.0; 
	MsmMax[0] = MsmMax[1] = MsmMax[2] = MsmMax[3] = -10000.0;

	wxWindowDisabler disableAll;
	wxBusyInfo *wait = new wxBusyInfo("Analyzing ger.out...",wxTheApp->GetTopWindow());


	wxString Token;
	double temp;
	bool first[4]; first[0] = first[1] = first[2] = first[3] = true;
	Ger.ReadLine();
	unsigned int line=0;
	while(!GerInputStream.Eof())
	{
		Ger.ReadWord();
		Token = Ger.ReadWord();
		for(unsigned int index = 0; index < TreesToAnalyze; ++index)
		{
			if (Token.ToDouble(&temp))
			{
				if (first[index]){GerMin[index] = GerMax[index] = temp; first[index] = false;}
				if (temp < GerMin[index]){GerMin[index] = temp;}
				if (temp > GerMax[index]){GerMax[index] = temp;}
			}
			Token = Ger.ReadWord();
			Ger.ReadLine();
			line++;
		}
	}
		
	wxDELETE(wait);
	wait = new wxBusyInfo("Analyzing MIG.out...",wxTheApp->GetTopWindow());

	first[0] = first[1] = first[2] = first[3] = true;
	Mig.ReadLine(); //Discard first line
	long ltemp;
	while(!MigInputStream.Eof())
	{
		Mig.ReadWord();
		Token = Mig.ReadWord();
		for(unsigned int index = 0; index < TreesToAnalyze; ++index)
		{
			if (Token.ToLong(&ltemp))
			{
				if (first[index]){MigMin[index] = MigMax[index] = ltemp; first[index] = false;}
				if (ltemp < MigMin[index]){MigMin[index] = ltemp;}
				if (ltemp > MigMax[index]){MigMax[index] = ltemp;}
			}
			Token = Mig.ReadWord();
			Mig.ReadLine();
		}
	}

	wxDELETE(wait);
	wait = new wxBusyInfo("Analyzing msm.out...",wxTheApp->GetTopWindow());

	first[0] = first[1] = first[2] = first[3] = true;
	Msm.ReadLine(); //Discard first line
	while(!MsmInputStream.Eof())
	{
		Msm.ReadWord();
		Token = Msm.ReadWord();
		for(unsigned int index = 0; index < TreesToAnalyze; ++index)
		{
			if (Token.ToDouble(&temp))
			{
				if (first[index]){MsmMin[index] = MsmMax[index] = temp; first[index] = false;}
				if (temp < MsmMin[index]){MsmMin[index] = temp;}
				if (temp > MsmMax[index]){MsmMax[index] = temp;}
			}
			Token = Msm.ReadWord();
			Msm.ReadLine();
		}
	}
		
	wxDELETE(wait);

	wxString Result = "TNT Output Scores:\n\n ";
	for(unsigned int index = 0, size = TreesToAnalyze; index < size; ++index)
	{
		Result += wxString::Format("Tree: %d\n", index+1);
		Result += wxString::Format("\tMIG: %.0f-%.0f\n ",MigMin[index],MigMax[index]);
		Result += wxString::Format("\tGER: %.4f-%.4f\n ",GerMin[index],GerMax[index]);
		Result += wxString::Format("\tMSM: %.4f-%.4f\n\n ",MsmMin[index],MsmMax[index]);
	}
	return(Result);
}
wxArrayString ASCCImport(wxString Filename, bool ShowResults)
{
	wxArrayString TreeData;

	//Get the filename, open it, and read the contents
	wxString Contents;
	if (Filename.IsEmpty())
	{
		Filename = wxFileSelector("Select file to parse:",wxEmptyString,wxEmptyString,"TRE, NEX, TNT files (*.tre;*.nex;*.tnt)|*.tre;*.nex;*.tnt","TRE, NEX, TNT files (*.tre;*.nex;*.tnt)|*.tre;*.nex;*.tnt|All Files(*.*)|*.*",wxFD_OPEN);
		if (Filename.IsEmpty()){return(TreeData);}
	}

	wxFileInputStream InputStream(Filename); if (!InputStream.IsOk()){wxLogMessage("Error: Unable to open file '" + Filename + "'");return(TreeData);}
	wxTextInputStream File(InputStream);
	wxArrayString ContentsArray;
	wxArrayString TaxaContentsArray;

	bool RecordingTree = false;
	bool RecordingTaxa = false;
	while(!InputStream.Eof())
	{
		Contents = File.ReadLine();
		if (Contents.Lower().Find("begin tree") != wxNOT_FOUND){RecordingTree = true;}
		if (Contents.Lower().Find("begin taxa") != wxNOT_FOUND){RecordingTaxa = true;}
		if (Contents.Lower().Find("end") != wxNOT_FOUND){RecordingTree = RecordingTaxa = false;}

		if (RecordingTree){ContentsArray.Add(Contents);}
		if (RecordingTaxa){TaxaContentsArray.Add(Contents);}
	}

	if (ContentsArray.IsEmpty())
	{
		InputStream.SeekI(0);
		while(!InputStream.Eof())
		{
			ContentsArray.Add(File.ReadLine());
		}
	}
		
	bool FoundTaxa = false;
	Taxon *Taxa = NULL;
	if (!TaxaContentsArray.IsEmpty())
	{
		while(TaxaContentsArray.size() > 0)
		{
			if (TaxaContentsArray[0].Lower().Find("taxlabels") == wxNOT_FOUND){TaxaContentsArray.RemoveAt(0);}
			else {TaxaContentsArray.RemoveAt(0); break;}
		}
		//TaxaContentsArray now starts at the taxa line
		wxStringTokenizer tok;
		wxString Token;
		while(TaxaContentsArray.size() > 0)
		{
			tok.SetString(TaxaContentsArray[0]," \r\n\t;",wxTOKEN_STRTOK);
			while (tok.HasMoreTokens() && !(Token = tok.GetNextToken()).IsEmpty() && Token.CmpNoCase("end") != 0)
			{
				if (!Taxon::FindTaxonByName(Token))
				{
					Taxa = new Taxon();
					Taxa->ChangeName(Token);
				}
				FoundTaxa = true;
			}
			TaxaContentsArray.RemoveAt(0);
		}
	}

	//ContentsArray has the text split by line
	wxString Token;
	wxArrayString TreeContents;
	bool InRecord = false;
	wxRegEx TaxaRE; bool TaxaREResult = TaxaRE.Compile("\\s*(\\d*)\\s*([^;,\\s]+)[,;]?\\s*.*$",wxRE_ADVANCED|wxRE_ICASE);
	if (!TaxaREResult){wxMessageBox("TaxaRE Compilation failed");}
	size_t size = ContentsArray.size();
	int TaxaCounter = 0;
	for(size_t index = 0; index < size; ++index)
	{
		Token = ContentsArray[index];
		if (InRecord)
		{
			if (TaxaRE.Matches(Token))
			{
				wxString TaxaName = TaxaRE.GetMatch(Token,2);
				Taxa = Taxon::FindTaxonByName(TaxaName);
				if (!Taxa)
				{
					Taxa = new Taxon();
					Taxa->ChangeName(TaxaRE.GetMatch(Token,2));
				}
				long num = TaxaCounter++;
				TaxaRE.GetMatch(Token,1).ToLong(&num);
				Taxa->ImportID((int)num);
				FoundTaxa = true;
			}
			if (Token.Find(';') != wxNOT_FOUND){InRecord = false;}
		}
		else if (Token.Lower().Find("translate") != wxNOT_FOUND){InRecord = true;}
		else if (Token.Lower().Find("xread") != wxNOT_FOUND){InRecord = true; index++;}
		else {TreeContents.Add(Token);}
	}

	//Now we look for tree data
	wxRegEx RemoveBracketsRE; bool RemoveBracketsREResult = RemoveBracketsRE.Compile("\\[.*?\\]",wxRE_ADVANCED|wxRE_ICASE);
	wxRegEx RemoveDataRE; bool RemoveDataREResult = RemoveDataRE.Compile(":.*?([,\\(\\)])",wxRE_ADVANCED|wxRE_ICASE);
	wxRegEx TreeRE; bool TreeREResult = TreeRE.Compile("\\s*tree[\\s\\*]+(\\S*).*?\\(",wxRE_ADVANCED|wxRE_ICASE);
	wxRegEx DataRE; bool DataREResult = DataRE.Compile("[^\\(]*(\\([\\(\\d\\w, ]+[^\\;\\*]+)",wxRE_ADVANCED|wxRE_ICASE);
	wxRegEx CharRE; bool CharREResult = CharRE.Compile("[^\\d\\w\\(\\)\\,\\;\\s]",wxRE_ADVANCED|wxRE_ICASE);
	if (!RemoveBracketsREResult){wxMessageBox("RemoveBracketsRE Compilation failed");}
	if (!RemoveDataREResult){wxMessageBox("RemoveDataRE Compilation failed");}
	if (!TreeREResult){wxMessageBox("TreeRE Compilation failed");} 
	if (!DataREResult){wxMessageBox("DataRE Compilation failed");}
	if (!CharREResult){wxMessageBox("CharRE Compilation failed");}
	wxString TreeName;
	bool FoundTreeName = false;
	bool FoundTreeData = false;
	size = TreeContents.size();
	for(size_t index = 0; index < size; ++index)
	{
		wxString TreeName = wxEmptyString;
		Token = TreeContents[index];
		RemoveBracketsRE.Replace(&Token,"");
		RemoveDataRE.Replace(&Token,"\\1");
		Token.Replace(","," ");
		if (TreeRE.Matches(Token)){TreeName = TreeRE.GetMatch(Token,1); FoundTreeName = true;}
		if (DataRE.Matches(Token))
		{
			//Test the tree data for correctness
			wxString tree = DataRE.GetMatch(Token,1);
			int ParenCount = 0; wxString ParenRemainder = tree; tree += " ";
			while(!(ParenRemainder = ParenRemainder.AfterFirst('(')).IsEmpty()){ParenCount++;}
			ParenRemainder = tree;
			while(!(ParenRemainder = ParenRemainder.AfterFirst(')')).IsEmpty()){ParenCount--;}
			if (!CharRE.Matches(tree) && ParenCount == 0)
			{
				TreeData.Add(TreeName); TreeData.Add(tree); FoundTreeData = true;
			}
		}
	}

	//Update the user with results
	wxString Results = "Searched file: " + Filename + "\nResults:\n";
	
	if (FoundTaxa){Results += "\tTaxa Information Found\n";}
	else {Results += "\tTaxa Information Not Found\n";}
	
	if (FoundTreeData && FoundTreeName){Results += "\tTree Data Found (with names)\n";}
	else if (FoundTreeData){Results += "\tTree Data Found (without names)\n";}
	else {Results += "\tTree Data Not Found\n";}

	if (ShowResults){wxMessageBox(Results,"File Import Results");}
	return(TreeData);
}

	ASCCRestructureDialog::ASCCRestructureDialog():wxDialog() {}
	ASCCRestructureDialog::ASCCRestructureDialog(wxWindow *parent, ComPolyOption AllowComPolyMethod, 
		wxString Caption, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
		: wxDialog()
	{
		this->Create(parent, AllowComPolyMethod, Caption, id, pos, size, style, name);
	}
	bool ASCCRestructureDialog::Create(wxWindow *parent, ComPolyOption AllowComPolyMethod, 
		wxString Caption, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
	{
		bool Result = wxDialog::Create(parent, id, "Restructure Polytomies", pos, size, style, name);
		this->CenterOnParent();
		this->m_AllowComPoly = AllowComPolyMethod;
		wxBoxSizer *TopSizer = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *ButtonSizer = new wxBoxSizer(wxHORIZONTAL);

		TopSizer->Add(new wxStaticText(this, wxID_ANY, Caption),0,wxALL,10);
		
		ButtonSizer->Add(new wxButton(this, RESTRUCTURE_CHRONOLOGICALLY, "Chronologically"));
		ButtonSizer->Add(new wxButton(this, RESTRUCTURE_REVERSE_CHRONOLOGICALLY, "Reverse Chronologically"));
		if (this->m_AllowComPoly == ALLOW_COMPOLY){ButtonSizer->Add(new wxButton(this, NO_RESTRUCTURE, "Use ComPoly (both)"));}
		ButtonSizer->Add(new wxButton(this, wxID_CANCEL, "Cancel"));

		TopSizer->Add(ButtonSizer, 0, wxALL, 10);
		this->SetSizerAndFit(TopSizer);
		return(Result);
	}
	void ASCCRestructureDialog::OnButton(wxCommandEvent& event)
	{
		//We have something other than cancel
		if (this->IsModal()){this->EndModal(event.GetId());}
		else {this->SetReturnCode(event.GetId()); this->Show(false);}
	}
IMPLEMENT_CLASS(ASCCRestructureDialog,wxDialog);
BEGIN_EVENT_TABLE(ASCCRestructureDialog,wxDialog)
	EVT_BUTTON(RESTRUCTURE_CHRONOLOGICALLY, ASCCRestructureDialog::OnButton)
	EVT_BUTTON(RESTRUCTURE_REVERSE_CHRONOLOGICALLY, ASCCRestructureDialog::OnButton)
	EVT_BUTTON(NO_RESTRUCTURE, ASCCRestructureDialog::OnButton)
END_EVENT_TABLE()

	