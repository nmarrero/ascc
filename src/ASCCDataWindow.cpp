/*	ASCCDataWindow.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../include/ASCCDataWindow.h"
#include "../include/Utils.h"

wxString LayerNames[] = {"MIG (1)", "GER (2)", "MSM* (3)", L"\u25B3AGL (4)","Right click for help and options"};

ASCCHistogramWindow::ASCCHistogramWindow( wxWindow *parent, wxWindowID id,const wxPoint &pos,const wxSize &size,long flags):
mpWindow(parent,id,pos,size,flags)
{
	this->LockAspect(false);
}
void ASCCHistogramWindow::OnChar(wxKeyEvent& event)
{
	mpLayer *layer = NULL;
	switch(event.GetKeyCode())
	{
		case('1'):{layer = this->GetLayerByName(LayerNames[0]); break;}
		case('2'):{layer = this->GetLayerByName(LayerNames[1]); break;}
		case('3'):{layer = this->GetLayerByName(LayerNames[2]); break;}
		case('4'):{layer = this->GetLayerByName(LayerNames[3]); break;}
	}
	if (layer){layer->SetVisible(!layer->IsVisible()); this->UpdateAll(); this->m_changed = true;}
	else {event.Skip();}
}
BEGIN_EVENT_TABLE(ASCCHistogramWindow,mpWindow)
	EVT_CHAR(ASCCHistogramWindow::OnChar)
END_EVENT_TABLE()

ASCCInfoLegend::ASCCInfoLegend():mpInfoLegend(){}
ASCCInfoLegend::ASCCInfoLegend(wxRect rect, const wxBrush* brush, wxString HelpString):mpInfoLegend(rect,brush),m_HelpString(HelpString){}
ASCCInfoLegend::~ASCCInfoLegend(){}
void ASCCInfoLegend::Plot(wxDC & dc, mpWindow & w)
{
	if (m_visible) {
		// Adjust relative position inside the window
		int scrx = w.GetScrX();
		int scry = w.GetScrY();
		if ((m_winX != scrx) || (m_winY != scry)) {
#ifdef MATHPLOT_DO_LOGGING
			// wxLogMessage(_("mpInfoLayer::Plot() screen size has changed from %d x %d to %d x %d"), m_winX, m_winY, scrx, scry);
#endif
			if (m_winX != 1) m_dim.x = (int) floor((double)(m_dim.x*scrx/m_winX));
			if (m_winY != 1) {
				m_dim.y = (int) floor((double)(m_dim.y*scry/m_winY));
				UpdateReference();
			}
			// Finally update window size
			m_winX = scrx;
			m_winY = scry;
		}
//     wxImage image0(wxT("pixel.png"), wxBITMAP_TYPE_PNG);
//     wxBitmap image1(image0);
//     wxBrush semiWhite(image1);
		dc.SetBrush(m_brush);
		dc.SetFont(m_font);
		const int baseWidth = (mpLEGEND_MARGIN*2 + mpLEGEND_LINEWIDTH);
		int textX = baseWidth, textY = mpLEGEND_MARGIN;
		int plotCount = 0;
		int posY = 0;
		int tmpX = 0, tmpY = 0;
		mpLayer* ly = NULL;
		wxPen lpen;
		wxString label;
		for (unsigned int p = 0; p < w.CountAllLayers(); p++) {
			ly = w.GetLayer(p);
			if (ly->GetLayerType() == mpLAYER_PLOT) {
				label = ly->GetName();
				dc.GetMultiLineTextExtent(label, &tmpX, &tmpY);
				textX = (textX > (tmpX + baseWidth)) ? textX : (tmpX + baseWidth + mpLEGEND_MARGIN);
				textY += (tmpY);
			}
		}
		//Add the help string
		label = this->m_HelpString;
		dc.GetMultiLineTextExtent(label,&tmpX,&tmpY);
		textX = (textX > (tmpX + baseWidth)) ? textX : (tmpX + baseWidth + mpLEGEND_MARGIN);
		textY += (tmpY);


		dc.SetPen(m_pen);
		dc.SetBrush(m_brush);
		m_dim.width = textX;
		if (textY != mpLEGEND_MARGIN) { // Don't draw any thing if there are no visible layers
			textY += mpLEGEND_MARGIN;
			m_dim.height = textY;
			dc.DrawRectangle(m_dim.x, m_dim.y, m_dim.width, m_dim.height);
			for (unsigned int p2 = 0; p2 < w.CountAllLayers(); p2++) {
				ly = w.GetLayer(p2);
				if (ly->GetLayerType() == mpLAYER_PLOT) {
					label = ly->GetName();
					lpen = ly->GetPen();
					dc.GetTextExtent(label, &tmpX, &tmpY);
					dc.SetPen(lpen);
					//textX = (textX > (tmpX + baseWidth)) ? textX : (tmpX + baseWidth);
					//textY += (tmpY + mpLEGEND_MARGIN);
					posY = m_dim.y + mpLEGEND_MARGIN + plotCount*tmpY + (tmpY>>1);
					if (ly->IsVisible())
					{
						dc.DrawLine(m_dim.x + mpLEGEND_MARGIN,   // X start coord
								posY,                        // Y start coord
								m_dim.x + mpLEGEND_LINEWIDTH + mpLEGEND_MARGIN, // X end coord
								posY);
					}
					//dc.DrawRectangle(m_dim.x + 5, m_dim.y + 5 + plotCount*tmpY, 5, 5);
					dc.DrawText(label, m_dim.x + baseWidth, m_dim.y + mpLEGEND_MARGIN + plotCount*tmpY);
					plotCount++;
				}
			}
			label = this->m_HelpString;
			dc.GetTextExtent(label, &tmpX, &tmpY);
			dc.SetPen(lpen);
			posY = m_dim.y + mpLEGEND_MARGIN + plotCount*tmpY + (tmpY>>1);
			dc.DrawText(label, m_dim.x + baseWidth, m_dim.y + mpLEGEND_MARGIN + plotCount*tmpY);
			plotCount++;
		}
	}
}

ASCCFXYVector::ASCCFXYVector(wxString Name):mpFXYVector(Name){}
void ASCCFXYVector::Plot(wxDC & dc, mpWindow & w)
{
	if (m_visible)
	{
		//wxGCDC gcdc((wxWindowDC &)dc);
			
		double x, y;
		// Do this to reset the counters to evaluate bounding box for label positioning
		Rewind(); GetNextXY(x, y);
		maxDrawX = x; minDrawX = x; maxDrawY = y; minDrawY = y;
		//drawnPoints = 0;
		Rewind();
		wxCoord startPx = m_drawOutsideMargins ? 0 : w.GetMarginLeft();
		wxCoord endPx   = m_drawOutsideMargins ? w.GetScrX() : w.GetScrX() - w.GetMarginRight();
		wxCoord minYpx  = m_drawOutsideMargins ? 0 : w.GetMarginTop();
		wxCoord maxYpx  = m_drawOutsideMargins ? w.GetScrY() : w.GetScrY() - w.GetMarginBottom();


		dc.SetPen( m_pen);
		dc.SetBrush( m_brush );
		wxCoord ix = 0, iy = 0;
		wxCoord zero_y=w.y2p(0.0);
		wxCoord *data = new wxCoord[endPx - startPx];
		for(wxCoord index = 0, size = endPx - startPx; index < size; index++){data[index] = zero_y;}

		while (GetNextXY(x, y))
		{
			ix = w.x2p(x);
			iy = w.y2p(y);
			if (iy < minYpx){iy = minYpx;}
			if (m_drawOutsideMargins || ((ix >= startPx) && (ix <= endPx) && (iy >= minYpx) && (iy <= maxYpx))) 
			{
				if (iy < data[ix-startPx]){data[ix-startPx] = iy;}
			}
		}
		for(wxCoord index = 0, size = endPx - startPx; index < size; ++index)
		{
			if (data[index] != zero_y)
			{
				dc.DrawLine(startPx+index,zero_y,startPx+index,data[index]);
				UpdateViewBoundary(startPx+index, data[index]);
			}
		}
		wxDELETEA(data);
	}
}

ASCCDataAnalysisWindow::ASCCDataAnalysisWindow():wxFrame(NULL,wxID_ANY,"ASCC Data Analysis",wxDefaultPosition, wxSize(1024,768),
	wxBORDER_THEME | wxCLIP_CHILDREN | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxRESIZE_BORDER | wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX),
m_Grid(NULL), m_Data(NULL), m_MinMIG(NULL), m_MinGER(NULL), m_MinMSM(NULL), m_MindAGL(NULL), m_MinFAD(NULL), m_MaxMIG(NULL), 
m_MaxGER(NULL), m_MaxMSM(NULL), m_MaxdAGL(NULL), m_MaxFAD(NULL), m_FADMinChosenAges(NULL), m_FADMaxChosenAges(NULL), m_Notebook(NULL)
{
	this->CreateStatusBar(1);
}
ASCCDataAnalysisWindow::ASCCDataAnalysisWindow(ASCCDataAnalysis *data):wxFrame(NULL,wxID_ANY,"ASCC Data Analysis",wxDefaultPosition, wxSize(1024,768),
	wxBORDER_THEME | wxCLIP_CHILDREN | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxRESIZE_BORDER | wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX),
m_Grid(NULL), m_Data(NULL), m_MinMIG(NULL), m_MinGER(NULL), m_MinMSM(NULL), m_MindAGL(NULL), m_MinFAD(NULL), m_MaxMIG(NULL), 
m_MaxGER(NULL), m_MaxMSM(NULL), m_MaxdAGL(NULL), m_MaxFAD(NULL), m_FADMinChosenAges(NULL), m_FADMaxChosenAges(NULL), m_Notebook(NULL)
{
	this->CreateStatusBar(1);
	this->LoadData(data);
}

void ASCCDataAnalysisWindow::LoadData(ASCCDataAnalysis *data)
{
	this->Show(false);
	this->UnloadData();

	this->m_TreeNames = data->TreeNames();
	this->m_Grid = NULL;
	this->m_MIG = data->GetScoreMIG();
	this->m_MSM = data->GetScoreMSM();
	this->m_GER = data->GetScoreGER();
	this->m_dAGL = data->GetScoredAGL();
	this->m_Replicates = data->Replicates();
	this->m_Data = data;
	this->m_Notebook = new wxNotebook(this,wxID_ANY);
		
	this->m_Seed = data->Seed();
	this->m_MinMIG = new double[this->m_TreeNames.size()];
	this->m_MinGER = new double[this->m_TreeNames.size()]; 
	this->m_ShowReplicates = false;
		
	this->m_MinMSM = new double[this->m_TreeNames.size()];
	this->m_MindAGL = new double[this->m_TreeNames.size()];
		
	this->m_MaxMIG = new double[this->m_TreeNames.size()];
	this->m_MaxGER = new double[this->m_TreeNames.size()];
		
	this->m_MaxMSM = new double[this->m_TreeNames.size()];
	this->m_MaxdAGL = new double[this->m_TreeNames.size()];

	this->m_MaxFAD = new double[data->Result()->Taxa()*this->m_TreeNames.size()];
	this->m_MinFAD = new double[data->Result()->Taxa()*this->m_TreeNames.size()];

	this->m_FADMaxChosenAges = new double[data->Result()->Taxa()*this->m_TreeNames.size()];
	this->m_FADMinChosenAges = new double[data->Result()->Taxa()*this->m_TreeNames.size()];

	this->GenerateMenu();

	for(unsigned int index = 0, size = this->m_TreeNames.size(); index < size; ++index)
	{
		this->m_MinMIG[index] = this->m_MinGER[index] = this->m_MinMSM[index] = this->m_MindAGL[index] = 1.0;
		this->m_MaxMIG[index] = this->m_MaxGER[index] = this->m_MaxMSM[index] = this->m_MaxdAGL[index] = 1.0;
		for(unsigned int taxon = 0, taxa = data->Result()->Taxa(); taxon < taxa; ++taxon)
		{
			this->m_MinFAD[taxon*size+index] = 1.0;
			this->m_MaxFAD[taxon*size+index] = 1.0;

			this->m_FADMinChosenAges[taxon*size+index] = 1.0;
			this->m_FADMaxChosenAges[taxon*size+index] = 1.0;
		}
	}

	double temp, tempAge;
	ASCCDataAnalysisResult *result = data->Result();
	for(unsigned int tree = 0, trees = this->m_TreeNames.size(); tree < trees; ++tree)
	{
		for(unsigned int replicate = 0, size = this->m_Replicates; replicate < size; ++replicate)
		{
			temp = result->MIG(replicate,tree); if (replicate == 0){this->m_MinMIG[tree] = temp; this->m_MaxMIG[tree] = temp;}
												if (temp < this->m_MinMIG[tree]){this->m_MinMIG[tree] = temp;} 
												if (temp > this->m_MaxMIG[tree]){this->m_MaxMIG[tree] = temp;}
			temp = result->GER(replicate,tree); if (replicate == 0){this->m_MinGER[tree] = temp; this->m_MaxGER[tree] = temp;}
												if (temp < this->m_MinGER[tree]){this->m_MinGER[tree] = temp;}
												if (temp > this->m_MaxGER[tree]){this->m_MaxGER[tree] = temp;}
			temp = result->MSM(replicate,tree); if (replicate == 0){this->m_MinMSM[tree] = temp; this->m_MaxMSM[tree] = temp;}
												if (temp < this->m_MinMSM[tree]){this->m_MinMSM[tree] = temp;}
												if (temp > this->m_MaxMSM[tree]){this->m_MaxMSM[tree] = temp;}
			temp = result->dAGL(replicate,tree); if (replicate == 0){this->m_MindAGL[tree] = temp; this->m_MaxdAGL[tree] = temp;}
												if (temp < this->m_MindAGL[tree]){this->m_MindAGL[tree] = temp;}
												if (temp > this->m_MaxdAGL[tree]){this->m_MaxdAGL[tree] = temp;}
			for(unsigned int taxon = 0, taxa = result->Taxa(); taxon < taxa; taxon++)
			{
				int ExportID = data->Taxa()[taxon]->ExportID();
				temp = result->FAD(replicate,tree,(ExportID-1)); 
				tempAge = result->FADAge(replicate,(ExportID-1));
				if (replicate == 0)
				{
					this->m_MinFAD[(ExportID-1)*trees+tree] = temp; this->m_FADMinChosenAges[(ExportID-1)*trees+tree] = tempAge;
					this->m_MaxFAD[(ExportID-1)*trees+tree] = temp; this->m_FADMaxChosenAges[(ExportID-1)*trees+tree] = tempAge;
				}
				if (temp < this->m_MinFAD[(ExportID-1)*trees+tree]){this->m_MinFAD[(ExportID-1)*trees+tree] = temp; this->m_FADMinChosenAges[(ExportID-1)*trees+tree] = tempAge;}
				if (temp > this->m_MaxFAD[(ExportID-1)*trees+tree]){this->m_MaxFAD[(ExportID-1)*trees+tree] = temp; this->m_FADMaxChosenAges[(ExportID-1)*trees+tree] = tempAge;}
			}
		}
	}
	//Add Ghost Lineages to FAD Data
	for(unsigned int tree = 0, trees = this->m_TreeNames.size(); tree < trees; ++tree)
	{
		for(unsigned int taxon = 0, taxa = result->Taxa(); taxon < taxa; taxon++)
		{
			int ExportID = data->Taxa()[taxon]->ExportID();
			this->m_MinFAD[(ExportID-1)*trees+tree] += result->MinGhostLineage(tree,(ExportID-1));
			this->m_MaxFAD[(ExportID-1)*trees+tree] += result->MaxGhostLineage(tree,(ExportID-1));
		}
	}
	
	this->BuildGrid();
	this->BuildHistograms();
	wxBoxSizer *TopSizer = new wxBoxSizer(wxVERTICAL);
	TopSizer->Add(new wxStaticText(this,wxID_ANY,wxString::Format("Seed: %d",this->m_Seed)));
	TopSizer->Add(new wxStaticText(this,wxID_ANY,wxString::Format("Replicates: %d",this->m_Replicates)));
	TopSizer->Add(this->m_Notebook,1,wxEXPAND);
	this->SetSizer(TopSizer);
	this->Layout();
	this->Show(true);
}
void ASCCDataAnalysisWindow::UnloadData()
{
	this->m_TreeNames.Clear();
	Agebin::ResetArray();
	wxDELETE(this->m_Data);
	wxDELETE(this->m_Grid);
	this->m_Grid = NULL;
	wxDELETE(this->m_Notebook);
	this->m_Notebook = NULL;
	wxDELETEA(this->m_MinMIG); wxDELETEA(this->m_MinGER); wxDELETEA(this->m_MinMSM); wxDELETEA(this->m_MindAGL); wxDELETEA(this->m_MinFAD); wxDELETEA(this->m_FADMinChosenAges);
	wxDELETEA(this->m_MaxMIG); wxDELETEA(this->m_MaxGER); wxDELETEA(this->m_MaxMSM); wxDELETEA(this->m_MaxdAGL); wxDELETEA(this->m_MaxFAD); wxDELETEA(this->m_FADMaxChosenAges);
}
ASCCDataAnalysisWindow::~ASCCDataAnalysisWindow()
{
	this->UnloadData();	
}
void ASCCDataAnalysisWindow::BuildGrid()
{
	if (this->m_Grid){wxBusyInfo wait_delete("Deleting current grid..."); this->m_Notebook->DeletePage(0); this->m_Grid = NULL;}
	if (!this->m_Grid){this->m_Grid = new wxGrid(this->m_Notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize);}
	this->m_Notebook->InsertPage(0,this->m_Grid,"Data",true);
		
	wxBusyInfo *wait = new wxBusyInfo("Generating data grid...");
	//Determine columns
	int scores = 0, currentrow = 0;
	bool UseMIG = false, UseGER = false, UseMSM = false, UsedAGL = false;
			
	if (this->m_MIG){UseMIG = true; ++scores;}
	if (this->m_GER){UseGER = true; ++scores;}
	if (this->m_MSM){UseMSM = true; ++scores;}
	if (this->m_dAGL){UsedAGL = true; ++scores;}
	if (this->m_ShowReplicates)
	{
		//add two to cols to include Lo and Lm
		this->m_Grid->CreateGrid(scores+1+1+this->m_Replicates, (scores+2)*this->m_TreeNames.size());
	}
	else
	{
		unsigned int TotalTrees = 0;
		for(unsigned int index = 0, size = this->m_TreeNames.size(); index < size; ++index)
		{
			if (!this->m_TreeNames[index].StartsWith("__compoly_chron")){TotalTrees++;}
		}
		this->m_Grid->CreateGrid(scores+1, 2*TotalTrees);
	}
	this->m_Grid->EnableEditing(false);
	scores = 0;
	this->m_Grid->SetRowLabelValue(0,""); this->m_Grid->SetRowLabelValue(1,"");
	if (UseMIG){this->m_Grid->SetRowLabelValue(++scores,"MIG");}
	if (UseGER){this->m_Grid->SetRowLabelValue(++scores,"GER");}
	if (UseMSM){this->m_Grid->SetRowLabelValue(++scores,"MSM*");}
	if (UsedAGL){this->m_Grid->SetRowLabelValue(++scores,"dAGL");}
	int IndexMultiplier = (this->m_ShowReplicates?scores+2:2);
	wxString TreeName;
	double MIG, GER, MSM, dAGL;
	for(unsigned int index = 0, treeindex = 0, size = this->m_TreeNames.size(); index < size; ++index,++treeindex)
	{
		//index is the tree data to use
		//treeindex is where to place it on the grid (multiplied by IndexMultipliter)
		TreeName = this->m_TreeNames[index];
		if (!this->m_ShowReplicates)
		{
			this->m_Grid->SetColLabelValue(treeindex*IndexMultiplier,(TreeName.StartsWith("__compoly_chron")?"Compoly " + TreeName.Mid(16):TreeName));
		}
		else
		{
			this->m_Grid->SetColLabelValue(treeindex*IndexMultiplier,(TreeName.StartsWith("__compoly_chron")?"Chronological " + TreeName.Mid(16):(TreeName.StartsWith("__compoly_reverse_chron")?"Reverse Chronological " + TreeName.Mid(24):TreeName)));
		}
		for(unsigned int col = 1, size = IndexMultiplier; col < size; ++col){this->m_Grid->SetColLabelValue(treeindex*IndexMultiplier+col,"");}
		
		this->m_Grid->SetCellValue(0,treeindex*IndexMultiplier,"min"); 
			if (TreeName.StartsWith("__compoly_chron") && !this->m_ShowReplicates)
			{ //Combine the min scores for the two compoly trees
				MIG = min(this->m_MinMIG[index],this->m_MinMIG[index+1]);
				GER = min(this->m_MinGER[index],this->m_MinGER[index+1]);
				MSM = min(this->m_MinMSM[index],this->m_MinMSM[index+1]);
				dAGL = min(this->m_MindAGL[index],this->m_MindAGL[index+1]);
			}
			else {MIG = this->m_MinMIG[index]; GER = this->m_MinGER[index]; MSM = this->m_MinMSM[index]; dAGL = this->m_MindAGL[index];}

			currentrow = 1;
			if (UseMIG){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier,wxString::Format("%.4f mya",MIG));}
			if (UseGER){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier,wxString::Format("%.4f",GER));}
			if (UseMSM){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier,wxString::Format("%.4f",MSM));}
			if (UsedAGL){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier,wxString::Format("%.4f mya/branch",dAGL));}
		this->m_Grid->SetCellValue(0,treeindex*IndexMultiplier+1,"max");
			if (TreeName.StartsWith("__compoly_chron") && !this->m_ShowReplicates)
			{ //Combine the max scores for the two compoly trees
				MIG = max(this->m_MaxMIG[index],this->m_MaxMIG[index+1]);
				GER = max(this->m_MaxGER[index],this->m_MaxGER[index+1]);
				MSM = max(this->m_MaxMSM[index],this->m_MaxMSM[index+1]);
				dAGL = max(this->m_MaxdAGL[index],this->m_MaxdAGL[index+1]);
			}
			else {MIG = this->m_MaxMIG[index]; GER = this->m_MaxGER[index]; MSM = this->m_MaxMSM[index]; dAGL = this->m_MaxdAGL[index];}
			currentrow = 1;
			if (UseMIG){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier+1,wxString::Format("%.4f mya",MIG));}
			if (UseGER){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier+1,wxString::Format("%.4f",GER));}
			if (UseMSM){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier+1,wxString::Format("%.4f",MSM));}
			if (UsedAGL){this->m_Grid->SetCellValue(currentrow++,treeindex*IndexMultiplier+1,wxString::Format("%.4f mya/branch",dAGL));}
			
		if (TreeName.StartsWith("__compoly_chron") && !this->m_ShowReplicates){index++; /* Skip the reverse tree, we've already incorporated it */}
	}
	if (!this->m_ShowReplicates)
	{
		this->m_Grid->AutoSize();
	}
	wxDELETE(wait);
	if (this->m_ShowReplicates)
	{
		unsigned int ReplicatePercent = min<unsigned long>(this->m_Replicates/100,50);
		if (ReplicatePercent == 0){ReplicatePercent = 1;}
		wxProgressDialog progress("Adding", "Adding Replicates...", this->m_Replicates/ReplicatePercent + 1, NULL, wxPD_APP_MODAL|wxPD_SMOOTH|wxPD_CAN_ABORT|wxPD_ELAPSED_TIME|wxPD_ESTIMATED_TIME);
		progress.Show();
		ASCCDataAnalysisResult *result = this->m_Data->Result();
		this->m_Grid->SetRowLabelValue(currentrow,"Replicates");
		for(unsigned int index = 0, size = this->m_TreeNames.size(); index < size; ++index)
		{
			scores = 0;
			if (UseMIG){this->m_Grid->SetCellValue(currentrow,index*IndexMultiplier+scores++,"MIG");}
			if (UseGER){this->m_Grid->SetCellValue(currentrow,index*IndexMultiplier+scores++,"GER");}
			if (UseMSM){this->m_Grid->SetCellValue(currentrow,index*IndexMultiplier+scores++,"MSM*");}
			if (UsedAGL){this->m_Grid->SetCellValue(currentrow,index*IndexMultiplier+scores++,"dAGL");}
			this->m_Grid->SetCellValue(currentrow,index*IndexMultiplier+scores++,"Lo");
			this->m_Grid->SetCellValue(currentrow,index*IndexMultiplier+scores++,"Lm");
		}
		++currentrow;
		for(unsigned int replicate = 0, size = this->m_Replicates; replicate < size; ++replicate)
		{
			if (replicate % ReplicatePercent == 0 && !progress.Update(replicate/ReplicatePercent)){break;}
			this->m_Grid->SetRowLabelValue(currentrow,wxString::Format("%d",replicate+1));
			for(unsigned int tree = 0, trees = this->m_TreeNames.size(); tree < trees; ++tree)
			{
				scores = 0;						
				if (UseMIG){this->m_Grid->SetCellValue(currentrow,tree*IndexMultiplier+scores++,wxString::Format("%.4f",result->MIG(replicate,tree)));}
				if (UseGER){this->m_Grid->SetCellValue(currentrow,tree*IndexMultiplier+scores++,wxString::Format("%.4f",result->GER(replicate,tree)));}
				if (UseMSM){this->m_Grid->SetCellValue(currentrow,tree*IndexMultiplier+scores++,wxString::Format("%.4f",result->MSM(replicate,tree)));}
				if (UsedAGL){this->m_Grid->SetCellValue(currentrow,tree*IndexMultiplier+scores++,wxString::Format("%.4f",result->dAGL(replicate,tree)));}
				this->m_Grid->SetCellValue(currentrow,tree*IndexMultiplier+scores++,wxString::Format("%.4f",result->Lo(replicate)));
				this->m_Grid->SetCellValue(currentrow,tree*IndexMultiplier+scores++,wxString::Format("%.4f",result->Lm(replicate)));
			}
			++currentrow;
		}
	}
	this->SendSizeEvent();
}
void ASCCDataAnalysisWindow::BuildHistograms()
{
	if ((!this->m_MIG  && !this->m_MSM && !this->m_GER && !this->m_dAGL) || this->m_TreeNames.size() < 2){return;}
	while (this->m_Notebook->GetPageCount() > 1){this->m_Notebook->DeletePage(1);}
	//wxScrolledWindow *HistogramPage = new wxScrolledWindow(this->m_Notebook,wxID_ANY);
	//HistogramPage->SetScrollRate(0,10);
	//wxSizer *HistogramSizer = new wxBoxSizer(wxVERTICAL);
	//Set how precise we want the differences to be (each difference is multiplied by this and rounded down)
	double precision = 1000.0;
	double one_over_precision = 1.0/precision;

	//Determine what to find
	bool UseMIG = false, UseGER = false, UseMSM = false, UsedAGL = false;
			
	if (this->m_MIG){UseMIG = true;}
	if (this->m_GER){UseGER = true;}
	if (this->m_MSM){UseMSM = true;}
	if (this->m_dAGL){UsedAGL = true;}

	DifferenceHash MIGHash, GERHash, MSMHash, dAGLHash;
	ASCCDataAnalysisResult *result = this->m_Data->Result();
	//Go through each pair of trees and generate differences for each replicate
	unsigned int totaltrees = this->m_TreeNames.size();
	wxProgressDialog progress("Generating", "Generating Histograms...", (totaltrees)*(totaltrees), NULL, wxPD_APP_MODAL|wxPD_SMOOTH|wxPD_CAN_ABORT|wxPD_ELAPSED_TIME|wxPD_ESTIMATED_TIME);
	progress.Show();
	wxString Tree1Name,Tree2Name;
	for(unsigned int tree1 = 0, size = totaltrees - 1; tree1 < size; ++tree1)
	{
		unsigned int tree2 = tree1 + 1;
		Tree1Name = this->m_TreeNames[tree1];
		if (this->m_TreeNames[tree1].StartsWith("__compoly_chron")){tree2 = tree1 + 2; Tree1Name = "Compoly " + Tree1Name.Mid(16);}
		else if (this->m_TreeNames[tree1].StartsWith("__compoly_reverse_chron")){continue;}
			
		for(;tree2 <= size; ++tree2)
		{
			if (this->m_TreeNames[tree2].StartsWith("__compoly_chron")){Tree2Name = "Compoly " + this->m_TreeNames[tree2].Mid(16);}
			else if (this->m_TreeNames[tree2].StartsWith("__compoly_reverse_chron")){}
			else {
				Tree2Name = this->m_TreeNames[tree2];
			}

			if (!progress.Update(tree1*size+tree2,"Generating Histograms... \n\tCollecting Data for " + Tree1Name + " vs " + Tree2Name)){return;}
			//win->EnableDoubleBuffer(true);
			//Clear the hashes
			for(unsigned int replicate=0, replicates = this->m_Replicates; replicate < replicates; ++replicate)
			{
				if (UseMIG)
				{
					MIGHash[long(precision * (result->MIG(replicate,tree1)-result->MIG(replicate,tree2)))] += 1; 
				}
				if (UseGER){GERHash[long(precision * (result->GER(replicate,tree1)-result->GER(replicate,tree2)))] += 1;}
				if (UseMSM){MSMHash[long(precision * (result->MSM(replicate,tree1)-result->MSM(replicate,tree2)))] += 1;}
				if (UsedAGL){dAGLHash[long(precision * (result->dAGL(replicate,tree1)-result->dAGL(replicate,tree2)))] += 1;}
			}
			//If we have added the first half of a compoly, don't bother making a histogram yet
			//chron1 & chron2 -> stay on chron1, go on to rev_chron2
			//chron1 & rev_chron2 -> go on to rev_chron1, back up to chron2
			//rev_chron1 & chron2 -> stay on rev_chron1, go on to rev_chron2
			//rev_chron1 & rev_chron2 -> proceed normally
			//chron1 & non -> go on to rev_chron1, keep non
			//rev_chron1 & non -> proceed normally
			//non & chron2 -> stay on non, go on to rev_chron2
			//non & rev_chron2 -> proceed normally
			//non & non -> proceed normally
			if (this->m_TreeNames[tree1].StartsWith("__compoly_chron"))
			{
				if (this->m_TreeNames[tree2].StartsWith("__compoly_chron")){continue;}
				if (this->m_TreeNames[tree2].StartsWith("__compoly_reverse_chron")){tree1++; tree2 -= 2; continue;}
				else {tree1++; tree2--; continue;}
			}
			else if (this->m_TreeNames[tree1].StartsWith("__compoly_reverse_chron"))
			{
				if (this->m_TreeNames[tree2].StartsWith("__compoly_chron")){continue;}
				//if (this->m_TreeNames[tree2].StartsWith("__compoly_reverse_chron")){do nothing - proceed normally}
				//else {do nothing - proceed normally}
			}
			else
			{
				if (this->m_TreeNames[tree2].StartsWith("__compoly_chron")){continue;}
				//if (this->m_TreeNames[tree2].StartsWith("__compoly_reverse_chron")){do nothing - proceed normally}
				//else {do nothing - proceed normally}
			}
			if (!progress.Update(tree1*size+tree2,"Generating Histograms... \n\tBuilding " + Tree1Name + " vs " + Tree2Name)){return;}

			//Create histograms from the differences between these two trees
			std::vector<double> differences, amounts;
			mpWindow *win = new ASCCHistogramWindow(this->m_Notebook,wxID_ANY);
				
			for(unsigned int type = 0, types = 4; type < types; ++type)
			{
				wxString Name;
				DifferenceHash *TheHash = NULL;
				bool Use = false;
					
				wxColour LayerColor;
				unsigned char alpha = 128;
				switch(type)
				{
					case(0): {Name = LayerNames[type]; TheHash = &MIGHash; Use = UseMIG; LayerColor.Set(wxBLUE->Red(), wxBLUE->Green(), wxBLUE->Blue(), alpha); break;}
					case(1): {Name = LayerNames[type]; TheHash = &GERHash; Use = UseGER; LayerColor.Set(wxRED->Red(), wxRED->Green(), wxRED->Blue(), alpha); break;}
					case(2): {Name = LayerNames[type]; TheHash = &MSMHash; Use = UseMSM; LayerColor.Set(wxGREEN->Red(), wxGREEN->Green(), wxGREEN->Blue(), alpha); break;}
					default: {Name = LayerNames[type]; TheHash = &dAGLHash; Use = UsedAGL; LayerColor.Set(wxBLACK->Red(), wxBLACK->Green(), wxBLACK->Blue(), alpha); break;}
				}
				if (Use)
				{
					//Set Transparency
					wxPen vectorpen(LayerColor, 2, wxPENSTYLE_SOLID);
					wxBrush vectorbrush(LayerColor, wxBRUSHSTYLE_SOLID);
					ASCCFXYVector *layer = new ASCCFXYVector(Name);
					layer->SetContinuity(false);
					layer->SetPen(vectorpen);
					layer->SetBrush(vectorbrush);
					layer->SetDrawOutsideMargins(false);
					layer->ShowName(false);
					differences.clear(); amounts.clear();
					differences.reserve(TheHash->size()); amounts.reserve(TheHash->size());
					//wxString Results;
					//double debugmin = 10000.0,debugmax = -10000.0;
					for(DifferenceHash::iterator it=TheHash->begin(), end=TheHash->end(); it != end; ++it)
					{
						differences.push_back(double(it->first)*one_over_precision);
						amounts.push_back(log10(double(it->second+1)));
						//if (double(it->first)*one_over_precision > debugmax){debugmax = double(it->first)*one_over_precision;}
						//if (double(it->first)*one_over_precision < debugmin){debugmin = double(it->first)*one_over_precision;}
						//Results += wxString::Format("%d\n",it->second);
					}
					//Results = wxString::Format("%s -> Min: %f, Max: %f",Name,debugmin,debugmax);
					//wxMessageBox(Results);
					layer->SetData(differences,amounts);
					win->AddLayer(layer,false);
				}
			}
			//HistogramSizer->Add(win,1,wxEXPAND);
			wxFont graphFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
			mpScaleX* xaxis = new mpScaleX(wxT("Pairwise Differences"), mpALIGN_BOTTOM, true, mpX_NORMAL);
			mpScaleY* yaxis = new mpScaleLogY(wxT("Number of Replicates"), mpALIGN_LEFT);
			xaxis->SetFont(graphFont);
			yaxis->SetFont(graphFont);
			xaxis->SetDrawOutsideMargins(false);
			yaxis->SetDrawOutsideMargins(false);
			// Fake axes formatting to test arbitrary format string
				xaxis->SetLabelFormat(wxT("%.3f"));
			yaxis->SetLabelFormat(wxT("%.0f"));
			win->SetMargins(30, 30, 50, 100);
		//     m_plot->SetMargins(50, 50, 200, 150);
			win->AddLayer(     xaxis );
			win->AddLayer(     yaxis );
			ASCCInfoLegend* leg;
			win->AddLayer( leg = new ASCCInfoLegend(wxRect(200,20,40,40), wxWHITE_BRUSH,LayerNames[4])); //&hatch2));
			leg->SetVisible(true);
			win->EnableDoubleBuffer(true);
			win->SetMPScrollbars(true);
			win->Fit();
			this->m_Notebook->AddPage(win,Tree1Name + " vs " + Tree2Name);
			//Clear the hashes
			MIGHash.clear();
			GERHash.clear();
			MSMHash.clear();
			dAGLHash.clear();
		}
	}
	//HistogramPage->SetSizer(HistogramSizer);
	//HistogramPage->Layout();
	//this->m_Notebook->InsertPage(1,HistogramPage,"Histograms",false);

}
void ASCCDataAnalysisWindow::ExportFAD(wxString Filename)
{
	if (Filename.IsEmpty())
	{
		Filename = wxFileSelector("Select .CSV file:",wxEmptyString,wxEmptyString,"ASCC CSV Files|*.csv","ASCC CSV Files (*.csv)|*.csv|All Files (*.*)|*.*",wxFD_SAVE|wxFD_OVERWRITE_PROMPT,NULL);
		if (Filename.IsEmpty()){return;}
	}
	wxFileOutputStream OutputStream(Filename); if (!OutputStream.IsOk()){return;}
	wxTextOutputStream Stream(OutputStream);

	const TaxonArray& Taxa = this->m_Data->Taxa();
	wxString TreeName;
	bool HasCompoly = false;
	for (unsigned int tree = 0, trees = this->m_TreeNames.size(); tree < trees; tree++)
	{
		if (this->m_TreeNames[tree].StartsWith("__compoly_chron")){HasCompoly = true;}
	}
	Stream << "Tree, Taxon ID, Taxon, Observed FAD Min, Observed FAD Max, Computed FAD Min, Taxon Age Selection (Min), Computed FAD Max, Taxon Age Selection (Max)";
	if (HasCompoly){Stream << ", Reverse Computed FAD Min, Reverse Taxon Age Selection (Min), Reverse Computed FAD Max, Reverse Taxon Age Selection (Max)";}
	Stream << "\n";

	for (unsigned int tree = 0, trees = this->m_TreeNames.size(); tree < trees; tree++)
	{
		TreeName = this->m_TreeNames[tree];
		Stream << (TreeName.StartsWith("__compoly_chron")?"Compoly " + TreeName.Mid(16):TreeName) << "," << this->m_Data->TreeData()[tree].AfterFirst('=') << "\n";
	
		for (unsigned int taxon = 0, taxa = Taxa.size(); taxon < taxa; taxon++)
		{
			Taxon *TheTaxon = Taxa.Find(taxon+1);
			int ExportID = TheTaxon->ExportID();
			Stream << ","; //To leave an empty column so we know which tree this belongs to
			//TaxonID, Taxon, Observed FAD Min, Observed FAD Max
			Stream << ExportID << "," << TheTaxon->Name() << "," << wxString::Format("%f,%f,",TheTaxon->MinOKR(),TheTaxon->MaxOKR());
			//Computed FAD Min, Taxon Age Selection (Min)
			Stream << wxString::Format("%f,%f,", this->m_MinFAD[(ExportID-1)*trees+tree], this->m_FADMinChosenAges[(ExportID-1)*trees+tree]); 
			//Computed FAD Max, Taxon Age Selection (Max)
			Stream << wxString::Format("%f,%f,", this->m_MaxFAD[(ExportID-1)*trees+tree], this->m_FADMaxChosenAges[(ExportID-1)*trees+tree]); 

			if (TreeName.StartsWith("__compoly_chron"))
			{
				//Reverse Computed FAD Min, Reverse Taxon Age Selection (Min)
				Stream << wxString::Format("%f,%f,", this->m_MinFAD[(ExportID-1)*trees+tree+1], this->m_FADMinChosenAges[(ExportID-1)*trees+tree+1]);\
				//Reverse Computed FAD Max, Reverse Taxon Age Selection (Max)
				Stream << wxString::Format("%f,%f,", this->m_MaxFAD[(ExportID-1)*trees+tree+1], this->m_FADMaxChosenAges[(ExportID-1)*trees+tree+1]);\
			}
			Stream << "\n";
		}
		if (TreeName.StartsWith("__compoly_chron")){tree++; /* Skip the reverse tree, we've already incorporated it */}
	}

	if (this->m_ShowReplicates)
	{
		for (unsigned int tree = 0, trees = this->m_TreeNames.size(); tree < trees; tree++)
		{
			TreeName = this->m_TreeNames[tree];
			Stream << TreeName << ",";
			for (unsigned int taxon = 0, taxa = Taxa.size(); taxon < taxa; taxon++)
			{
				Stream << Taxa[taxon]->Name() << ",";
			}
			Stream << "\n";

			for (unsigned int replicate = 0, replicates = this->m_Replicates; replicate < replicates; replicate++)
			{
				Stream << wxString::Format("%d,",replicate);
				for (unsigned int taxon = 0, taxa = Taxa.size(); taxon < taxa; taxon++)
				{
					int ExportID = Taxa[taxon]->ExportID();
					Stream << wxString::Format("%f,",this->m_Data->Result()->FAD(replicate,tree,(ExportID-1)));
				}
				Stream << "\n";
			}
		}
	}
}
void ASCCDataAnalysisWindow::ExportGrid(wxString Filename)
{
	if (!this->m_Data || !this->m_Grid)
	{
		wxMessageBox("No data to export");
		return;
	}
	if (Filename.IsEmpty())
	{
		Filename = wxFileSelector("Select .CSV file:",wxEmptyString,wxEmptyString,"ASCC CSV Files|*.csv","ASCC CSV Files (*.csv)|*.csv|All Files (*.*)|*.*",wxFD_SAVE|wxFD_OVERWRITE_PROMPT,NULL);
		if (Filename.IsEmpty()){return;}
	}
	wxFileOutputStream OutputStream(Filename); if (!OutputStream.IsOk()){return;}
	wxTextOutputStream Stream(OutputStream);
	
	Stream << ","; //This is the "column header" for the row headers
	for(unsigned int col = 0, cols = this->m_Grid->GetCols(); col < cols; col++)
	{
		Stream << this->m_Grid->GetColLabelValue(col) << ",";
	}
	Stream << "\n";
	for(int row = 0, rows = this->m_Grid->GetRows(); row < rows; row++)
	{
		Stream << this->m_Grid->GetRowLabelValue(row) << ",";
		for(unsigned int col = 0, cols = this->m_Grid->GetCols(); col < cols; col++)
		{
			Stream << this->m_Grid->GetCellValue(row,col).BeforeFirst(' ') << ",";
		}
		Stream << "\n";
	}
}
void ASCCDataAnalysisWindow::GenerateMenu()
{
	wxMenuBar *Menu = new wxMenuBar();
	wxMenu *FileMenu = new wxMenu();
	if (this->m_Data)
	{
		if (this->m_ShowReplicates)
		{
			FileMenu->Append(wxID_VIEW_DETAILS,"&Hide Replicates\tCtrl+H","Hides the replicate results");
		}
		else
		{
			FileMenu->Append(wxID_VIEW_DETAILS,"S&how Replicates\tCtrl+H","Shows the replicate results");
		}
		FileMenu->Append(wxID_SAVE,"&Save\tCtrl+S","Saves the current data");
		FileMenu->Append(wxID_SAVEAS,"&Export Current Data Tab\tCtrl+E","Exports the currently shown data into a .csv file");
		FileMenu->Append(wxID_FIND,"Export FAD &Information\tCtrl+I","Exports the FAD data into a .csv file");
	}
	FileMenu->Append(wxID_OPEN,"&Load\tCtrl+O","Loads new data");
	Menu->Append(FileMenu,"&File");
	if (this->m_Data)
	{
		wxMenu *EditMenu = new wxMenu();
		EditMenu->Append(wxID_SELECTALL,"Select &All\tCtrl+A","Selects every cell");
		EditMenu->Append(wxID_COPY,"&Copy\tCtrl+C","Copies the selected data onto the clipboard");
		Menu->Append(EditMenu,"&Edit");
	}
	if (this->GetMenuBar()){wxMenuBar *Old = this->GetMenuBar(); this->SetMenuBar(Menu); wxDELETE(Old);}
	else {this->SetMenuBar(Menu);}
}
void ASCCDataAnalysisWindow::OnContextMenu(wxContextMenuEvent& event)
{
	this->ShowContextMenu();
}
void ASCCDataAnalysisWindow::ShowContextMenu()
{
	wxMenu *ContextMenu = new wxMenu();
		ContextMenu->Append(wxID_SELECTALL,"Select All","Selects every cell");
		ContextMenu->Append(wxID_COPY,"Copy","Copies the selected data onto the clipboard");
	this->PopupMenu(ContextMenu);
	wxDELETE(ContextMenu);
}
void ASCCDataAnalysisWindow::OnCopy(wxCommandEvent& event)
{
	if (!wxTheClipboard->Open() && !wxTheClipboard->Open() )
	{
		wxLogError(wxT("Can't open clipboard."));
		return;
	}
	wxString TheData;
	unsigned int TopCol, TopRow, BottomCol, BottomRow;
	for(unsigned int index = 0, size = this->m_Grid->GetSelectionBlockTopLeft().size(); index < size; ++index)
	{
		TopCol = this->m_Grid->GetSelectionBlockTopLeft()[index].GetCol();
		TopRow = this->m_Grid->GetSelectionBlockTopLeft()[index].GetRow();
		BottomCol = this->m_Grid->GetSelectionBlockBottomRight()[index].GetCol();
		BottomRow = this->m_Grid->GetSelectionBlockBottomRight()[index].GetRow();

		for(unsigned int col = TopCol; col <= BottomCol; ++col)
		{
			TheData += this->m_Grid->GetColLabelValue(col);
			if (col != BottomCol){TheData += "\t";}
		}
		TheData += "\n";
		for(unsigned int row = TopRow; row <= BottomRow; ++row)
		{
			TheData += this->m_Grid->GetRowLabelValue(row) + "\t";
			for(unsigned int col = TopCol; col <= BottomCol; ++col)
			{
				TheData += this->m_Grid->GetCellValue(row,col);
				if (col != BottomCol){TheData += "\t";}
			}
			TheData += "\n";
		}
	}
	if ( !wxTheClipboard->AddData(new wxTextDataObject(TheData)) )
	{
		wxLogError(wxT("Can't copy data to the clipboard"));
	}
	wxTheClipboard->Flush();
	wxTheClipboard->Close();
}
void ASCCDataAnalysisWindow::OnGridRightClick(wxGridEvent& event)
{
	this->ShowContextMenu();
}
void ASCCDataAnalysisWindow::OnSelectAll(wxCommandEvent& event)
{
	this->m_Grid->SelectAll();
}
void ASCCDataAnalysisWindow::OnViewDetails(wxCommandEvent& event)
{
	this->Show(false);
	this->m_ShowReplicates = !this->m_ShowReplicates;
	this->GenerateMenu();
	this->BuildGrid();
	this->Show(true);
}
void ASCCDataAnalysisWindow::OnSave(wxCommandEvent& event)
{
	if (this->m_Data)
	{
		this->m_Data->Save();
	}
}
void ASCCDataAnalysisWindow::OnExportGrid(wxCommandEvent& event)
{
	this->ExportGrid();
}
void ASCCDataAnalysisWindow::OnExportFAD(wxCommandEvent& event)
{
	this->ExportFAD();
}
void ASCCDataAnalysisWindow::OnLoad(wxCommandEvent& event)
{
	ASCCDataAnalysis *data = ASCCDataAnalysis::Load();
	if (data){ASCCDataAnalysisWindow *win = new ASCCDataAnalysisWindow(data);}
}
	
BEGIN_EVENT_TABLE(ASCCDataAnalysisWindow, wxFrame)
	EVT_GRID_CELL_RIGHT_CLICK(ASCCDataAnalysisWindow::OnGridRightClick)
	EVT_GRID_LABEL_RIGHT_CLICK(ASCCDataAnalysisWindow::OnGridRightClick)
	EVT_MENU(wxID_COPY, ASCCDataAnalysisWindow::OnCopy)
	EVT_MENU(wxID_SELECTALL, ASCCDataAnalysisWindow::OnSelectAll)
	EVT_MENU(wxID_VIEW_DETAILS, ASCCDataAnalysisWindow::OnViewDetails)
	EVT_MENU(wxID_SAVE,ASCCDataAnalysisWindow::OnSave)
	EVT_MENU(wxID_SAVEAS,ASCCDataAnalysisWindow::OnExportGrid)
	EVT_MENU(wxID_FIND,ASCCDataAnalysisWindow::OnExportFAD)
	EVT_MENU(wxID_OPEN,ASCCDataAnalysisWindow::OnLoad)
	EVT_CONTEXT_MENU(ASCCDataAnalysisWindow::OnContextMenu)
END_EVENT_TABLE()