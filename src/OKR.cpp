/*	OKR.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file OKR.cpp
 * @author Nico Marrero
 * @date September, 2012
 * @brief This file contains the code for dynamic and predefined Oldest Known Records
 *
 */
#include "../include/OKR.h"
#include "../include/Taxa.h"
#include "../include/Utils.h"
#include <wx/arrimpl.cpp>

#define TEXT_BORDER 3

DEFINE_EVENT_TYPE(EVT_OKR_PREDEFINED_SELECTION);

using namespace PredefinedOKR;

PredefinedOKRHash OKR::sm_PredefinedOKRHash;

WX_DEFINE_OBJARRAY(_OKRLinkArray);

IMPLEMENT_CLASS(OKR,wxObject);
IMPLEMENT_CLASS(OKRNode,wxControl);
IMPLEMENT_CLASS(OKRSelectableNode,OKRNode);
IMPLEMENT_CLASS(OKRSelectionTree,wxScrolledWindow);

BEGIN_EXPOSURE_TABLE(OKRLink,"A link that declares one OKR estimation to be always younger than another")
END_EXPOSURE_TABLE()

BEGIN_EXPOSURE_TABLE(OKR,"An Oldest Known Record estimation")
END_EXPOSURE_TABLE()

BEGIN_EVENT_TABLE(OKRNode,wxControl)
	EVT_PAINT(OKRNode::OnPaint)
END_EVENT_TABLE()

BEGIN_EVENT_TABLE(OKRSelectableNode, OKRNode)
	EVT_LEFT_UP(OKRSelectableNode::OnMouseLeftUp)
	EVT_PAINT(OKRSelectableNode::OnPaint)
END_EVENT_TABLE()

BEGIN_EVENT_TABLE(OKRSelectionTree, wxScrolledWindow)
	EVT_SIZE(OKRSelectionTree::OnSize)
END_EVENT_TABLE()


bool OKRLink::operator==(const OKRLink& other)
{
	bool Result = this->m_LinkType == other.m_LinkType;
	if (!this->m_LinkedOKR){Result &= this->m_LinkedOKR == other.m_LinkedOKR;}
	else {Result &= this->m_LinkedOKR->operator==(*other.m_LinkedOKR);}
	return(Result);
}
wxDataInputStream& OKRLink::Load(wxDataInputStream& strm)
{
	wxInt16 LinkType, LinkedOKRType;
	wxString LinkName;

	strm >> LinkType;
	if (LinkType == 1){this->m_LinkType = YOUNGER_THAN;}
	else {this->m_LinkType = OLDER_THAN;}

	strm >> LinkedOKRType;
	if (LinkedOKRType == 2)
	{
		strm >> LinkName;
		Taxon *LinkTaxon = Taxon::FindTaxonByName(LinkName);
		if (!LinkTaxon){LinkTaxon = new Taxon(); LinkTaxon->ChangeName(LinkName);}
		this->m_LinkedOKR = (OKR*)LinkTaxon;
		wxASSERT(this->m_LinkedOKR);
	}
	else if (LinkedOKRType == 1)
	{
		strm >> LinkName;
		this->m_LinkedOKR = OKR::FindPredefinedOKR(LinkName);
		wxASSERT(this->m_LinkedOKR);
	}
	return(strm);
}
//wxDataOutputStream& OKRLink::Save(wxDataOutputStream& strm)
//{
//	if (this->m_LinkType == YOUNGER_THAN){strm << (wxInt16)1;}
//	else {strm << (wxInt16)0;}
//
//	if (this->m_LinkedOKR->Owner()){strm << (wxInt8)2; strm << this->m_LinkedOKR->Owner()->Name();}
//	else if (!this->m_LinkedOKR->PredefinedName().IsEmpty()){strm << (wxInt16)1; strm << this->m_LinkedOKR->PredefinedName();}
//	else {strm << (wxInt16)0;}
//	return(strm);
//}
//wxTextInputStream& OKRLink::Load(wxTextInputStream& strm)
//{
//	wxString LinkType, LinkedOKRType;
//	wxString LinkName;
//
//	strm >> LinkType;
//	if (LinkType.Contains("Younger")){this->m_LinkType = YOUNGER_THAN;}
//	else {this->m_LinkType = OLDER_THAN;}
//
//	strm >> LinkedOKRType;
//	if (LinkedOKRType.Contains("Owner"))
//	{
//		strm >> LinkName; LinkName.Trim(true).Trim(false);
//		Taxon *LinkTaxon = Taxon::FindTaxonByName(LinkName);
//		if (!LinkTaxon){LinkTaxon = new Taxon(); LinkTaxon->ChangeName(LinkName);}
//		this->m_LinkedOKR = (OKR*)LinkTaxon;
//	}
//	else if (LinkedOKRType.Contains("Predefined"))
//	{
//		strm >> LinkName; LinkName.Trim(true).Trim(false);
//		this->m_LinkedOKR = OKR::FindPredefinedOKR(LinkName);
//	}
//	return(strm);
//}
//wxTextOutputStream& OKRLink::Save(wxTextOutputStream& strm)
//{
//	if (this->m_LinkType == YOUNGER_THAN){strm << "\t\tYounger\n";}
//	else {strm << "\t\tOlder\n";}
//
//	if (this->m_LinkedOKR->Owner()){strm << "\t\tType = Owner\n"; strm << "\t\t" << this->m_LinkedOKR->Owner()->Name() << "\n";}
//	else if (!this->m_LinkedOKR->PredefinedName().IsEmpty()){strm << "\t\tType = Predefined\n"; strm << "\t\t" << this->m_LinkedOKR->PredefinedName() << "\n";}
//	else {strm << "\t\tType = Other\n";}
//	return(strm);
//}
wxXmlNode* OKRLink::Save()
{
	wxXmlNode *LinkNode = new wxXmlNode(wxXML_ELEMENT_NODE,"AgeLink");
	LinkNode->AddChild(CreateXmlContentNode("Relation",(this->m_LinkType==YOUNGER_THAN?"Younger":"Older")));
	if (this->m_LinkedOKR->Owner()){LinkNode->AddChild(CreateXmlContentNode("Owner",this->m_LinkedOKR->Owner()->Name()));}
	else if (!this->m_LinkedOKR->PredefinedName().IsEmpty()){LinkNode->AddChild(CreateXmlContentNode("Predefined",this->m_LinkedOKR->PredefinedName()));}

	return(LinkNode);
}
bool OKRLink::Load(wxXmlNode* CurrentNode)
{
	if (!CurrentNode || CurrentNode->GetName() != "AgeLink"){return(false);}
	bool Result = true;
	bool HasRelation = false, HasOwner = false, HasPredefined = false;
	for(wxXmlNode *Current = CurrentNode->GetChildren(); Current != NULL; Current = Current->GetNext())
	{
		if (Current->GetName() == "Relation"){this->m_LinkType = (Current->GetNodeContent()=="Younger"?YOUNGER_THAN:OLDER_THAN); HasRelation = true;}
		if (Current->GetName() == "Owner")
		{
			wxString LinkName = Current->GetNodeContent();
			Taxon *LinkTaxon = Taxon::FindTaxonByName(LinkName);
			if (!LinkTaxon)
			{
				LinkTaxon = new Taxon(); 
				LinkTaxon->ChangeName(LinkName);
			}
			this->LinkedOKR((OKR*)LinkTaxon);
			HasOwner = true;
		}
		if (Current->GetName() == "Predefined")
		{
			wxString LinkName = Current->GetNodeContent();
			this->m_LinkedOKR = OKR::FindPredefinedOKR(LinkName);
			HasPredefined = true;
		}
	}
	if (!HasRelation){Result = false; wxMessageBox("No relation");}
	if (!HasOwner && !HasPredefined){Result = false; wxMessageBox("No owner or predefined");}
	return(Result);
}
	
wxString OKR::SameAsTaxon()
{
	if (this->m_IsSameAs && this->m_SameAsLink && this->m_SameAsLink->Owner()){return(this->m_SameAsLink->Owner()->Name());} 
	return(wxEmptyString);
}

void OKR::SameAsTaxon(wxString TaxonName)
{
	if (this->m_Owner && this->m_Owner->Name() != TaxonName)
	{
		Taxon *FoundTaxon = Taxon::FindTaxonByName(TaxonName);
		if (FoundTaxon && FoundTaxon->Name() != this->m_Owner->Name())
		{
			this->SameAs(FoundTaxon);
		}
	}
}

wxDataInputStream& OKR::Load(wxDataInputStream& strm)
{
	wxInt16 SameAs;
	wxString SameAsTaxonName;
	Taxon *SameAsTaxon;
	wxUint32 size;

	strm >> this->m_MinOKR >> this->m_MaxOKR >> SameAs;
	if (SameAs == 1)
	{
		this->m_IsSameAs = true;
		strm >> SameAsTaxonName;
		SameAsTaxon = Taxon::FindTaxonByName(SameAsTaxonName);
		if (!SameAsTaxon){SameAsTaxon = new Taxon(); SameAsTaxon->ChangeName(SameAsTaxonName);}
		this->m_SameAsLink = (OKR*)SameAsTaxon;
	}
	else {this->m_IsSameAs = false;}

	
	strm >> size;
	OKRLink Link;
	for(unsigned int index = 0; index < size; ++index)
	{
		this->m_OKRLinkArray.Add(Link);
		this->m_OKRLinkArray[index].Load(strm);
	}
	return(strm);
}
//wxDataOutputStream& OKR::Save(wxDataOutputStream& strm)
//{
//	strm << this->m_MinOKR << this->m_MaxOKR;
//	if (this->m_IsSameAs && this->m_SameAsLink && this->m_SameAsLink->Owner())
//	{
//		strm << (wxInt16)1;
//		strm << this->m_SameAsLink->Owner()->Name();
//	}
//	else {strm << (wxInt16)0;}
//
//	strm << (wxUint32)this->m_OKRLinkArray.size();
//	for(unsigned int index = 0, size = this->m_OKRLinkArray.size(); index < size; ++index)
//	{
//		this->m_OKRLinkArray[index].Save(strm);
//	}
//	return(strm);
//}
//wxTextInputStream& OKR::Load(wxTextInputStream& strm)
//{
//	unsigned long SameAs, size;
//	wxString SameAsTaxonName;
//	Taxon *SameAsTaxon;
//	wxString line;
//
//	strm >> line; line.AfterFirst('=').ToDouble(&this->m_MinOKR);
//	strm >> line; line.AfterFirst('=').ToDouble(&this->m_MaxOKR);
//	strm >> line; line.AfterFirst('=').ToULong(&SameAs);
//	
//	if (SameAs == 1)
//	{
//		this->m_IsSameAs = true;
//		strm >> line; SameAsTaxonName = line.AfterFirst('=').Trim(false).Trim(true);
//		SameAsTaxon = Taxon::FindTaxonByName(SameAsTaxonName);
//		if (!SameAsTaxon){SameAsTaxon = new Taxon(); SameAsTaxon->ChangeName(SameAsTaxonName);}
//		this->m_SameAsLink = (OKR*)SameAsTaxon;
//	}
//	else {this->m_IsSameAs = false;}
//
//	
//	strm >> line; line.AfterFirst('=').ToULong(&size);
//	OKRLink Link;
//	for(unsigned int index = 0; index < size; ++index)
//	{
//		this->m_OKRLinkArray.Add(Link);
//		this->m_OKRLinkArray[index].Load(strm);
//	}
//	return(strm);
//}
//wxTextOutputStream& OKR::Save(wxTextOutputStream& strm)
//{
//	strm << "\tMinOKR = " << this->m_MinOKR << "\n";
//	strm << "\tMaxOKR = " << this->m_MaxOKR << "\n";
//	if (this->m_IsSameAs && this->m_SameAsLink && this->m_SameAsLink->Owner())
//	{
//		strm << "\tGrouped = 1\n";
//		strm << "\tSameAs = " << this->m_SameAsLink->Owner()->Name() << "\n";
//	}
//	else {strm << "\tGrouped = 0\n";}
//
//	strm << "\tLinkArrayCount = " << (wxUint32)this->m_OKRLinkArray.size() << "\n";
//	for(unsigned int index = 0, size = this->m_OKRLinkArray.size(); index < size; ++index)
//	{
//		this->m_OKRLinkArray[index].Save(strm);
//	}
//	return(strm);
//}
wxXmlNode* OKR::Save()
{
	wxXmlNode *OKRNode = new wxXmlNode(wxXML_ELEMENT_NODE, "OKR");
	OKRNode->AddChild(CreateXmlContentNode("Min",wxString::Format("%f",this->m_MinOKR)));
	OKRNode->AddChild(CreateXmlContentNode("Max",wxString::Format("%f",this->m_MaxOKR)));
	if (this->m_AcceptableOKR){OKRNode->AddChild(CreateXmlContentNode("AcceptableOKR","True"));}
	if (this->m_IsSameAs && this->m_SameAsLink && this->m_SameAsLink->Owner())
	{
		OKRNode->AddChild(CreateXmlContentNode("SameAs",this->m_SameAsLink->Owner()->Name()));
	}

	for(unsigned int index = 0, size = this->m_OKRLinkArray.size(); index < size; ++index)
	{
		OKRNode->AddChild(this->m_OKRLinkArray[index].Save());
	}
	return(OKRNode);
}
bool OKR::Load(wxXmlNode *CurrentNode)
{
	if (!CurrentNode || CurrentNode->GetName() != "OKR"){return(false);}
	bool Result = true;
	for(wxXmlNode *Current = CurrentNode->GetChildren(); Current != NULL; Current = Current->GetNext())
	{
		if (Current->GetName() == "Min"){Current->GetNodeContent().ToDouble(&this->m_MinOKR);}
		if (Current->GetName() == "Max"){Current->GetNodeContent().ToDouble(&this->m_MaxOKR);}
		if (Current->GetName() == "AcceptableOKR"){this->m_AcceptableOKR = (Current->GetNodeContent()=="True")?true:false;}
		if (Current->GetName() == "SameAs")
		{
			this->m_IsSameAs = true;
			wxString SameAsTaxonName = Current->GetNodeContent();
			Taxon *SameAsTaxon = Taxon::FindTaxonByName(SameAsTaxonName);
			if (!SameAsTaxon){SameAsTaxon = new Taxon(); SameAsTaxon->ChangeName(SameAsTaxonName);}
			this->m_SameAsLink = (OKR*)SameAsTaxon;
		}
		if (Current->GetName() == "AgeLink")
		{
			OKRLink Link;
			Result &= Link.Load(Current);
			this->m_OKRLinkArray.Add(Link);
		}
	}
	return(Result);
}

void OKRNode::ColourChildren(wxColour Colour1, wxColour Colour2)
{
	if (!this->m_Colour.IsOk()){this->m_Colour = Colour2;}
	wxControl::SetBackgroundColour(this->m_Colour);

	float interp = 0.0f;
	unsigned char Red1, Blue1, Green1;
	unsigned char Red2, Blue2, Green2;

	Red1 = Colour1.Red(); Green1 = Colour1.Green(); Blue1 = Colour1.Blue();
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		interp = (float)(index+1)/(float)size;
		Red2 = (1.0f - interp)*((float)Colour1.Red()) + (interp)*((float)Colour2.Red());
		Green2 = (1.0f - interp)*((float)Colour1.Green()) + (interp)*((float)Colour2.Green());
		Blue2 = (1.0f - interp)*((float)Colour1.Blue()) + (interp)*((float)Colour2.Blue());

		this->m_Children[index]->ColourChildren(wxColour(Red1,Green1,Blue1),wxColour(Red2,Green2,Blue2));

		Red1 = Red2; Green1 = Green2; Blue1 = Blue2;
	}
}
void OKRNode::LayoutChildren()
{
	wxSize MySize(0,0);
	if (this->m_Children.size() == 0)
	{
		MySize = this->GetMinSize(); MySize.IncBy(1,0);
		this->SetSize(MySize);
		return;
	}
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		this->m_Children[index]->LayoutChildren();
		this->m_Children[index]->SetPosition(wxPoint(this->GetMinSize().GetWidth(),MySize.GetHeight()));
		MySize.IncBy(0,this->m_Children[index]->GetSize().GetHeight());
		MySize.SetWidth(max(MySize.GetWidth(),this->GetMinSize().GetWidth()+this->m_Children[index]->GetSize().GetWidth()));
	}
	this->SetSize(MySize);
}
void OKRSelectableNode::DoPaint(wxDC& dc)
{
	OKRNode::DoPaint(dc);

	wxSize MySize = this->GetSize();
	dc.SetPen(wxPen(*wxBLACK,3));

		
	if (this->m_HasAgeMin)
	{
		int Height = max(1,(int)(MySize.GetHeight()*this->m_AgeMinLocation));
		dc.DrawLine(0,Height,MySize.GetWidth(),Height);
	}
	if (this->m_HasAgeMax)
	{
		int Height = min(MySize.GetHeight()-2,(int)(MySize.GetHeight()*this->m_AgeMaxLocation));
		dc.DrawLine(0,Height,MySize.GetWidth(),Height);
	}
}

void OKRNode::DoPaint(wxDC& dc)
{
	dc.SetFont(wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT));
	dc.SetBackground(wxBrush(this->GetBackgroundColour()));
	dc.Clear();
	dc.SetBrush(*wxTRANSPARENT_BRUSH);
	wxSize TextExtent = dc.GetTextExtent(this->m_Name);
	wxSize sz = this->GetMinSize();
	wxPoint Pos = wxPoint(max(TEXT_BORDER,(sz.GetWidth()-TextExtent.GetWidth()-5)),this->GetSize().GetHeight()/2 - TextExtent.GetHeight()/2);

	sz = this->GetSize(); sz.DecBy(1,0);
	wxRect Rect(wxPoint(0,0),sz);
	
	if (!this->m_Name.IsEmpty()){dc.DrawRectangle(Rect);}

	dc.DrawText(this->m_Name,Pos);
	if (!this->m_SecondaryName.IsEmpty()){dc.DrawText(this->m_SecondaryName,Pos.x+TEXT_BORDER,Pos.y+TextExtent.GetHeight());}
}

void OKRNode::RemoveChild(wxWindowBase *child)
{
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		if (this->m_Children[index] == child)
		{
			this->m_Children.RemoveAt(index);
			break;
		}
	}
	wxControl::RemoveChild(child);
}

bool OKRSelectionTree::Create(wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
	if (!wxScrolledWindow::Create(parent, id, pos, size, style)){return(false);}
	wxScrolledWindow::SetScrollRate(10,10);
	wxScrolledWindow::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	wxScrolledWindow::SetInitialSize(size);

	//Create the entire tree
	this->m_Tree = new OKRNode("","",wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW),this,wxID_ANY);
	OKRNode *CurrentEon = NULL, *CurrentEra = NULL, *CurrentSystem = NULL, *CurrentSeries = NULL;
	int CurrentEonIndex = -1, CurrentEraIndex = -1, CurrentSystemIndex = -1, CurrentSeriesIndex = -1;	
	wxSize LargestEonExtent(0,0), LargestEraExtent(0,0), LargestSystemExtent(0,0), LargestSeriesExtent(0,0), LargestStageExtent(0,0);

	OKRNode *CurrentNode = NULL, *ParentNode = NULL;
	wxSize CurrentExtent;

	unsigned int index = 0;
	wxClientDC dc(this);
	while(GeologicScale[index].Name != "End of Data")
	{
		GeologicScaleData& Data = GeologicScale[index++];
		wxString Name = Data.Name, SecondaryName;
		if (Data.Name.Contains("/"))
		{
			Name = Data.Name.BeforeFirst('/') + "/";
			SecondaryName = Data.Name.AfterFirst('/');
			if (dc.GetTextExtent(Name).GetWidth() < dc.GetTextExtent(SecondaryName).GetWidth()){CurrentExtent = dc.GetTextExtent(SecondaryName);}
			else {CurrentExtent = dc.GetTextExtent(Name);}
		}
		else {CurrentExtent = dc.GetTextExtent(Data.Name);}
		CurrentExtent.IncBy(TEXT_BORDER,CurrentExtent.GetHeight());
		switch(Data.Type)
		{
		case(EON):
			{
				LargestEonExtent.SetHeight(max(LargestEonExtent.GetHeight(),CurrentExtent.GetHeight()));
				LargestEonExtent.SetWidth(max(LargestEonExtent.GetWidth(),CurrentExtent.GetWidth()));
				break;
			}
		case(ERA):
			{
				LargestEraExtent.SetHeight(max(LargestEraExtent.GetHeight(),CurrentExtent.GetHeight()));
				LargestEraExtent.SetWidth(max(LargestEraExtent.GetWidth(),CurrentExtent.GetWidth()));
				break;
			}
		case(SYSTEM):
			{
				LargestSystemExtent.SetHeight(max(LargestSystemExtent.GetHeight(),CurrentExtent.GetHeight()));
				LargestSystemExtent.SetWidth(max(LargestSystemExtent.GetWidth(),CurrentExtent.GetWidth()));
				break;
			}
		case(SERIES):
			{
				LargestSeriesExtent.SetHeight(max(LargestSeriesExtent.GetHeight(),CurrentExtent.GetHeight()));
				LargestSeriesExtent.SetWidth(max(LargestSeriesExtent.GetWidth(),CurrentExtent.GetWidth()));
				break;
			}
		case(STAGE):
			{
				LargestStageExtent.SetHeight(max(LargestStageExtent.GetHeight(),CurrentExtent.GetHeight()));
				LargestStageExtent.SetWidth(max(LargestStageExtent.GetWidth(),CurrentExtent.GetWidth()));
				break;
			}
		}
		
	}
	index = 0;
	while(GeologicScale[index].Name != "End of Data")
	{
		GeologicScaleData& Data = GeologicScale[index++];
		//Set the parent
		switch(Data.Type)
		{
		case(EON):{ParentNode = this->m_Tree; break;}
		case(ERA):{ParentNode = CurrentEon; ParentNode->SetMinSize(LargestEonExtent); break;}
		case(SYSTEM):{ParentNode = CurrentEra; ParentNode->SetMinSize(LargestEraExtent); break;}
		case(SERIES):{ParentNode = CurrentSystem; ParentNode->SetMinSize(LargestSystemExtent); break;}
		case(STAGE):{ParentNode = CurrentSeries; ParentNode->SetMinSize(LargestSeriesExtent);break;}
		}

		//Make the node
		wxString Name = Data.Name, SecondaryName = wxEmptyString;
		wxColour NodeColour;
		if (Data.Name.Contains("/"))
		{
			Name = Data.Name.BeforeFirst('/') + "/";
			SecondaryName = Data.Name.AfterFirst('/');
		}
		if (Data.Colour.IsOk() && !Data.EndColour.IsOk()){NodeColour = Data.Colour;}
		if (Data.PredefinedOKR){CurrentNode = new OKRSelectableNode(Name,SecondaryName,NodeColour,Data.PredefinedOKR,ParentNode);}
		else {CurrentNode = new OKRNode(Name, SecondaryName, NodeColour, ParentNode);}
		ParentNode->AddChild(CurrentNode);
		
		//Update the "Current" to be a future parent
		switch(Data.Type)
		{
		case(EON):
			{
				if (CurrentEonIndex != -1 && GeologicScale[CurrentEonIndex].Colour.IsOk() && GeologicScale[CurrentEonIndex].EndColour.IsOk())
				{
					CurrentEon->ColourChildren(GeologicScale[CurrentEonIndex].Colour,GeologicScale[CurrentEonIndex].EndColour);
				}
				CurrentEon = CurrentNode; CurrentEonIndex = index-1;
				CurrentNode->SetMinSize(wxSize(LargestEonExtent.GetWidth()+LargestEraExtent.GetWidth()+LargestSystemExtent.GetWidth()+LargestSeriesExtent.GetWidth()+LargestStageExtent.GetWidth(),LargestEonExtent.GetHeight()));
				break;
			}
		case(ERA):
			{
				if (CurrentEraIndex != -1 && GeologicScale[CurrentEraIndex].Colour.IsOk() && GeologicScale[CurrentEraIndex].EndColour.IsOk())
				{
					CurrentEra->ColourChildren(GeologicScale[CurrentEraIndex].Colour,GeologicScale[CurrentEraIndex].EndColour);
				}
				CurrentEra = CurrentNode; CurrentEraIndex = index-1; 
				CurrentNode->SetMinSize(wxSize(LargestEraExtent.GetWidth()+LargestSystemExtent.GetWidth()+LargestSeriesExtent.GetWidth()+LargestStageExtent.GetWidth(),LargestEraExtent.GetHeight()));
				break;
			}
		case(SYSTEM):
			{
				if (CurrentSystemIndex != -1 && GeologicScale[CurrentSystemIndex].Colour.IsOk() && GeologicScale[CurrentSystemIndex].EndColour.IsOk())
				{
					CurrentSystem->ColourChildren(GeologicScale[CurrentSystemIndex].Colour,GeologicScale[CurrentSystemIndex].EndColour);
				}
				CurrentSystem = CurrentNode; CurrentSystemIndex = index-1;
				CurrentNode->SetMinSize(wxSize(LargestSystemExtent.GetWidth()+LargestSeriesExtent.GetWidth()+LargestStageExtent.GetWidth(),LargestSystemExtent.GetHeight()));			
				break;
			}
		case(SERIES):
			{
				if (CurrentSeriesIndex != -1 && GeologicScale[CurrentSeriesIndex].Colour.IsOk() && GeologicScale[CurrentSeriesIndex].EndColour.IsOk())
				{
					CurrentSeries->ColourChildren(GeologicScale[CurrentSeriesIndex].Colour,GeologicScale[CurrentSeriesIndex].EndColour);
				}
				CurrentSeries = CurrentNode; CurrentSeriesIndex = index-1; 
				CurrentNode->SetMinSize(wxSize(LargestSeriesExtent.GetWidth()+LargestStageExtent.GetWidth(),LargestSeriesExtent.GetHeight()));
				break;
			}
		case(STAGE):{CurrentNode->SetMinSize(LargestStageExtent); break;}
		}
	}
	this->LayoutChildren();
	return(true);
}

GeologicScaleData GeologicScale[] = {
	{"No Fossil Record", EON, &NoFossilRecord, *wxWHITE},
	{"Phanerozoic", EON, NULL, wxColour(60,179,113)},
		{"Cenozoic", ERA, NULL, wxColour(255,255,255), wxColour(255,255,0)}, 
			{"Quaternary", SYSTEM, NULL}, 
				{"Holocene", SERIES, NULL}, 
					{"Holocene", STAGE, &HoloceneStage},
				{"Pleistocene", SERIES, NULL, }, 
					{"Tarantian", STAGE, &TarantianStage, },
					{"Ionian", STAGE, &IonianStage, },
					{"Calabrian", STAGE, &CalabrianStage, },
					{"Gelasian", STAGE, &GelasianStage, },
			{"Neogene", SYSTEM, NULL, }, 
				{"Pliocene", SERIES, NULL, }, 
					{"Piacenzian", STAGE, &PiacenzianStage, },
					{"Zanclean", STAGE, &ZancleanStage, },
				{"Miocene", SERIES, NULL, }, 
					{"Messinian", STAGE, &MessinianStage, },
					{"Tortonian", STAGE, &TortonianStage, },
					{"Serravallian", STAGE, &SerravallianStage, },
					{"Langhian", STAGE, &LanghianStage, },
					{"Burdigalian", STAGE, &BurdigalianStage, },
					{"Aquitanian", STAGE, &AquitanianStage, },
			{"Paloegene", SYSTEM, NULL, }, 
				{"Oligocene", SERIES, NULL, }, 
					{"Chattian", STAGE, &ChattianStage, },
					{"Rupelian", STAGE, &RupelianStage, },
				{"Eocene", SERIES, NULL, }, 
					{"Priabonian", STAGE, &PriabonianStage, },
					{"Bartonian", STAGE, &BartonianStage, },
					{"Lutetian", STAGE, &LutetianStage, },
					{"Ypresian", STAGE, &YpresianStage, },
				{"Paleocene", SERIES, NULL, }, 
					{"Thanetian", STAGE, &ThanetianStage, },
					{"Selandian", STAGE, &SelandianStage, },
					{"Danian", STAGE, &DanianStage, },
		{"Mesozoic", ERA, NULL, wxColour(255,255,255), wxColour(0,255,0)}, 
			{"Cretaceous", SYSTEM, NULL}, 
				{"Upper Cretaceous", SERIES, NULL}, 
					{"Maastrichtian", STAGE, &MaastrichtianStage},
					{"Campanian", STAGE, &CampanianStage},
					{"Santonian", STAGE, &SantonianStage},
					{"Coniacian", STAGE, &ConiacianStage},
					{"Turonian", STAGE, &TuronianStage},
					{"Cenomanian", STAGE, &CenomanianStage},
				{"Lower Cretaceous", SERIES, NULL}, 
					{"Albian", STAGE, &AlbianStage},
					{"Aptian", STAGE, &AptianStage},
					{"Barremian", STAGE, &BarremianStage},
					{"Hauterivian", STAGE, &HauterivianStage},
					{"Valanginian", STAGE, &ValanginianStage},
					{"Berriasian", STAGE, &BerriasianStage},
			{"Jurassic", SYSTEM, NULL}, 
				{"Upper Jurassic", SERIES, NULL}, 
					{"Tithonian", STAGE, &TithonianStage},
					{"Kimmeridgian", STAGE, &KimmeridgianStage},
					{"Oxfordian", STAGE, &OxfordianStage},
				{"Middle Jurassic", SERIES, NULL}, 
					{"Callovian", STAGE, &CallovianStage},
					{"Bathonian", STAGE, &BathonianStage},
					{"Bajocian", STAGE, &BajocianStage},
					{"Aalenian", STAGE, &AalenianStage},
				{"Lower Jurassic", SERIES, NULL}, 
					{"Toarcian", STAGE, &ToarcianStage},
					{"Pliensbachian", STAGE, &PliensbachianStage},
					{"Sinemurian", STAGE, &SinemurianStage},
					{"Hettangian", STAGE, &HettangianStage},
			{"Triassic", SYSTEM, NULL}, 
				{"Upper Triassic", SERIES, NULL}, 
					{"Rhaetian", STAGE, &RhaetianStage},
					{"Norian", STAGE, &NorianStage},
					{"Carnian", STAGE, &CarnianStage},
				{"Middle Triassic", SERIES, NULL}, 
					{"Ladinian", STAGE, &LadinianStage},
					{"Anisian", STAGE, &AnisianStage},
				{"Lower Triassic", SERIES, NULL}, 
					{"Olenekian", STAGE, &OlenekianStage},
					{"Induan", STAGE, &InduanStage},
		{"Paleosoic", ERA, NULL, wxColour(255,255,255), wxColour(100,0,255)}, 
			{"Permian", SYSTEM, NULL}, 
				{"Lopingian", SERIES, NULL}, 
					{"Changhsingian", STAGE, &ChanghsingianStage},
					{"Wuchiapingian", STAGE, &WuchiapingianStage},
				{"Guadalupian", SERIES, NULL}, 
					{"Capitanian", STAGE, &CapitanianStage},
					{"Wordian", STAGE, &WordianStage},
					{"Roadian", STAGE, &RoadianStage},
				{"Cisuralian", SERIES, NULL}, 
					{"Kungurian", STAGE, &KungurianStage},
					{"Artinskian", STAGE, &ArtinskianStage},
					{"Sakmarian", STAGE, &SakmarianStage},
					{"Asselian", STAGE, &AsselianStage},
			{"Carboniferous/Pennsylvania", SYSTEM, NULL}, 
					{"Upper", SERIES, NULL}, 
						{"Gzhelian", STAGE, &GzhelianStage},
						{"Kasimovian", STAGE, &KasimovianStage},
					{"Middle", SERIES, NULL},
						{"Moscovian", STAGE, &MoscovianStage},
					{"Lower", SERIES, NULL},
						{"Bashkirian", STAGE, &BashkirianStage},
			{"Corboniferous/Mississippi", SYSTEM, NULL}, 
					{"Upper", SERIES, NULL},
						{"Serpukhovian", STAGE, &SerpukhovianStage},
					{"Middle", SERIES, NULL},
						{"Visean", STAGE, &ViseanStage},
					{"Lower", SERIES, NULL},
						{"Tournaisian", STAGE, &TournaisianStage},
			{"Devian", SYSTEM, NULL}, 
					{"Upper", SERIES, NULL},
						{"Famennian", STAGE, &FamennianStage},
						{"Frasnian", STAGE, &FrasnianStage},
					{"Middle", SERIES, NULL},
						{"Givetian", STAGE, &GivetianStage},
						{"Eifelian", STAGE, &EifelianStage},
					{"Lower", SERIES, NULL},
						{"Emsian", STAGE, &EmsianStage},
						{"Pragian", STAGE, &PragianStage},
						{"Lochkovian", STAGE, &LochkovianStage},
			{"Silurian", SYSTEM, NULL}, 
					{"Pridoli", SERIES, &PridoliSeries},
					{"Ludlow", SERIES, NULL}, 
						{"Ludfordian", STAGE, &LudfordianStage},
						{"Gorstian", STAGE, &GorstianStage},
					{"Wenlock", SERIES, NULL}, 
						{"Homerian", STAGE, &HomerianStage},
						{"Sheinwoodian", STAGE, &SheinwoodianStage},
					{"Llandovery", SERIES, NULL}, 
						{"Telychian", STAGE, &TelychianStage},
						{"Aeronian", STAGE, &AeronianStage},
						{"Rhuddanian", STAGE, &RhuddanianStage},	
			{"Ordovician", SYSTEM, NULL}, 
					{"Upper", SERIES, NULL},
						{"Hirnantian", STAGE, &HirnantianStage},
						{"Katian", STAGE, &KatianStage},
						{"Sandbian", STAGE, &SandbianStage},
					{"Middle", SERIES, NULL},
						{"Darriwilian", STAGE, &DarriwilianStage},
						{"Dapingian", STAGE, &DapingianStage},
					{"Lower", SERIES, NULL},
						{"Floian", STAGE, &FloianStage},
						{"Tremadocian", STAGE, &TremadocianStage},
			{"Cambrian", SYSTEM, NULL}, 
					{"Furongian", SERIES, NULL}, 
						{"Stage 10", STAGE, &Stage10},
						{"Stage 9", STAGE, &Stage9},
						{"Paibian", STAGE, &PaibianStage},
					{"Series 3", SERIES, NULL},
						{"Guzhangian", STAGE, &GuzhangianStage},
						{"Drumian", STAGE, &DrumianStage},
						{"Stage 5", STAGE, &Stage5},
					{"Series 2", SERIES, NULL},
						{"Stage 4", STAGE, &Stage4},
						{"Stage 3", STAGE, &Stage3},
					{"Terreneuvian", SERIES, NULL}, 
						{"Stage 2", STAGE, &Stage2},
						{"Fortunian", STAGE, &FortunianStage},
	{"Proterozoic", EON, NULL, wxColour(255,228,225), wxColour(255,0,0)},
		{"Neoproterozoic", ERA, NULL}, 
			{"Ediacaran", SYSTEM, &EdiacaranSystem},
			{"Cryogenian", SYSTEM, &CryogenianSystem},
			{"Tonian", SYSTEM, &TonianSystem},
		{"Mesoproterozoic", ERA, NULL}, 
			{"Stenian", SYSTEM, &StenianSystem},
			{"Ectasian", SYSTEM, &EctasianSystem},
			{"Calymmian", SYSTEM, &CalymmianSystem},
		{"Paleoproterozoic", ERA, NULL}, 
			{"Statherian", SYSTEM, &StatherianSystem},
			{"Orosirian", SYSTEM, &OrosirianSystem},
			{"Rhyacian", SYSTEM, &RhyacianSystem},
			{"Siderian", SYSTEM, &SiderianSystem},
	{"Archean", EON, NULL, wxColour(200,200,200), wxColour(100,100,100)},
		{"Neoarchean", ERA, &NeoarcheanEra},
		{"Mesoarchean", ERA, &MesoarcheanEra},
		{"Paleoarchean", ERA, &PaleoarcheanEra},
		{"Eoarchean", ERA, &EoarcheanEra},
	{"Headan", EON, &HeadanEon, *wxWHITE},
{"End of Data", EON, NULL, *wxWHITE}
};
	const OKR PredefinedOKR::NoFossilRecord		(0.0,		0.0,		"NoFossilRecord",		OKRLinkArray()																);
	const OKR PredefinedOKR::HoloceneStage		(0.0,		0.011784,	"HoloceneStage",		OKRLinkArray()									.YoungerThan(TarantianStage));
	const OKR PredefinedOKR::TarantianStage		(0.011784,	0.126,		"TarantianStage",		OKRLinkArray().OlderThan(HoloceneStage)			.YoungerThan(IonianStage));
	const OKR PredefinedOKR::IonianStage		(0.126,		0.781,		"IonianStage",			OKRLinkArray().OlderThan(TarantianStage)		.YoungerThan(CalabrianStage));
	const OKR PredefinedOKR::CalabrianStage		(0.781,		1.806,		"CalabrianStage",		OKRLinkArray().OlderThan(IonianStage)			.YoungerThan(GelasianStage));
	const OKR PredefinedOKR::GelasianStage		(1.806,		2.588,		"GelasianStage",		OKRLinkArray().OlderThan(CalabrianStage)		.YoungerThan(PiacenzianStage));
	const OKR PredefinedOKR::PiacenzianStage	(2.588,		3.6,		"PiacenzianStage",		OKRLinkArray().OlderThan(GelasianStage)			.YoungerThan(ZancleanStage));
	const OKR PredefinedOKR::ZancleanStage		(3.6,		5.332,		"ZancleanStage",		OKRLinkArray().OlderThan(PiacenzianStage)		.YoungerThan(MessinianStage));
	const OKR PredefinedOKR::MessinianStage		(5.332,		7.246,		"MessinianStage",		OKRLinkArray().OlderThan(ZancleanStage)			.YoungerThan(TortonianStage));
	const OKR PredefinedOKR::TortonianStage		(7.246,		11.608,		"TortonianStage",		OKRLinkArray().OlderThan(MessinianStage)		.YoungerThan(SerravallianStage));
	const OKR PredefinedOKR::SerravallianStage	(11.608,	13.82,		"SerravallianStage",	OKRLinkArray().OlderThan(TortonianStage)		.YoungerThan(LanghianStage));
	const OKR PredefinedOKR::LanghianStage		(13.82,		15.97,		"LanghianStage",		OKRLinkArray().OlderThan(SerravallianStage)		.YoungerThan(BurdigalianStage));
	const OKR PredefinedOKR::BurdigalianStage	(15.97,		20.43,		"BurdigalianStage",		OKRLinkArray().OlderThan(LanghianStage)			.YoungerThan(AquitanianStage));
	const OKR PredefinedOKR::AquitanianStage	(20.43,		23.03,		"AquitanianStage",		OKRLinkArray().OlderThan(BurdigalianStage)		.YoungerThan(ChattianStage));
	const OKR PredefinedOKR::ChattianStage		(23.03,		28.4+0.1,	"ChattianStage",		OKRLinkArray().OlderThan(AquitanianStage)		.YoungerThan(RupelianStage));
	const OKR PredefinedOKR::RupelianStage		(28.4-0.1,	33.9+0.1,	"RupelianStage",		OKRLinkArray().OlderThan(ChattianStage)			.YoungerThan(PriabonianStage));
	const OKR PredefinedOKR::PriabonianStage	(33.9-0.1,	37.2+0.1,	"PriabonianStage",		OKRLinkArray().OlderThan(RupelianStage)			.YoungerThan(BartonianStage));
	const OKR PredefinedOKR::BartonianStage		(37.2-0.1,	40.4+0.2,	"BartonianStage",		OKRLinkArray().OlderThan(PriabonianStage)		.YoungerThan(LutetianStage));
	const OKR PredefinedOKR::LutetianStage		(40.4-0.2,	48.6+0.2,	"LutetianStage",		OKRLinkArray().OlderThan(BartonianStage)		.YoungerThan(YpresianStage));
	const OKR PredefinedOKR::YpresianStage		(48.6-0.2,	55.8+0.2,	"YpresianStage",		OKRLinkArray().OlderThan(LutetianStage)			.YoungerThan(ThanetianStage));
	const OKR PredefinedOKR::ThanetianStage		(55.8-0.2,	58.7+0.2,	"ThanetianStage",		OKRLinkArray().OlderThan(YpresianStage)			.YoungerThan(SelandianStage));
	const OKR PredefinedOKR::SelandianStage		(58.7-0.2,	61.1+0.2,	"SelandianStage",		OKRLinkArray().OlderThan(ThanetianStage)		.YoungerThan(DanianStage));
	const OKR PredefinedOKR::DanianStage		(61.1-0.2,	65.5+0.3,	"DanianStage",			OKRLinkArray().OlderThan(SelandianStage)		.YoungerThan(MaastrichtianStage));
	const OKR PredefinedOKR::MaastrichtianStage(65.5-0.3,	70.6+0.6,	"MaastrichtianStage",	OKRLinkArray().OlderThan(DanianStage)			.YoungerThan(CampanianStage));
	const OKR PredefinedOKR::CampanianStage		(70.6-0.6,	83.5+0.7,	"CampanianStage",		OKRLinkArray().OlderThan(MaastrichtianStage)	.YoungerThan(SantonianStage));
	const OKR PredefinedOKR::SantonianStage		(83.5-0.7,	85.8+0.7,	"SantonianStage",		OKRLinkArray().OlderThan(CampanianStage)		.YoungerThan(ConiacianStage));
	const OKR PredefinedOKR::ConiacianStage		(85.8-0.7,	89.3+1.0,	"ConiacianStage",		OKRLinkArray().OlderThan(SantonianStage)		.YoungerThan(TuronianStage));
	const OKR PredefinedOKR::TuronianStage		(89.3-1.0,	93.6+0.8,	"TuronianStage",		OKRLinkArray().OlderThan(ConiacianStage)		.YoungerThan(CenomanianStage));
	const OKR PredefinedOKR::CenomanianStage	(93.6-0.8,	99.6+0.9,	"CenomanianStage",		OKRLinkArray().OlderThan(TuronianStage)			.YoungerThan(AlbianStage));
	const OKR PredefinedOKR::AlbianStage		(99.6-0.9,	112.0+1.0,	"AlbianStage",			OKRLinkArray().OlderThan(CenomanianStage)		.YoungerThan(AptianStage));
	const OKR PredefinedOKR::AptianStage		(112.0-1.0,	125.0+1.0,	"AptianStage",			OKRLinkArray().OlderThan(AlbianStage)			.YoungerThan(BarremianStage));
	const OKR PredefinedOKR::BarremianStage		(125.0-1.0,	130.0+1.5,	"BarremianStage",		OKRLinkArray().OlderThan(AptianStage)			.YoungerThan(HauterivianStage));
	const OKR PredefinedOKR::HauterivianStage	(130.0-1.5,	133.9+2.0,	"HauterivianStage",		OKRLinkArray().OlderThan(BarremianStage)		.YoungerThan(ValanginianStage));
	const OKR PredefinedOKR::ValanginianStage	(133.9-2.0,	140.2+3.0,	"ValanginianStage",		OKRLinkArray().OlderThan(HauterivianStage)		.YoungerThan(BerriasianStage));
	const OKR PredefinedOKR::BerriasianStage	(140.2-3.0,	145.5+4.0,	"BerriasianStage",		OKRLinkArray().OlderThan(ValanginianStage)		.YoungerThan(TithonianStage));
	const OKR PredefinedOKR::TithonianStage		(145.5-4.0,	150.8+4.0,	"TithonianStage",		OKRLinkArray().OlderThan(BerriasianStage)		.YoungerThan(KimmeridgianStage));
	const OKR PredefinedOKR::KimmeridgianStage	(150.8-4.0,	155.6+4.0,	"KimmeridgianStage",	OKRLinkArray().OlderThan(TithonianStage)		.YoungerThan(OxfordianStage));
	const OKR PredefinedOKR::OxfordianStage		(155.6-4.0,	161.2+4.0,	"OxfordianStage",		OKRLinkArray().OlderThan(KimmeridgianStage)		.YoungerThan(CallovianStage));
	const OKR PredefinedOKR::CallovianStage		(161.2-4.0,	164.7+4.0,	"CallovianStage",		OKRLinkArray().OlderThan(OxfordianStage)		.YoungerThan(BathonianStage));
	const OKR PredefinedOKR::BathonianStage		(164.7-4.0,	167.7+3.5,	"BathonianStage",		OKRLinkArray().OlderThan(CallovianStage)		.YoungerThan(BajocianStage));
	const OKR PredefinedOKR::BajocianStage		(167.7-3.5,	171.6+3.0,	"BajocianStage",		OKRLinkArray().OlderThan(BathonianStage)		.YoungerThan(AalenianStage));
	const OKR PredefinedOKR::AalenianStage		(171.6-3.0,	175.6+2.0,	"AalenianStage",		OKRLinkArray().OlderThan(BajocianStage)			.YoungerThan(ToarcianStage));
	const OKR PredefinedOKR::ToarcianStage		(175.6-2.0,	183.0+1.5,	"ToarcianStage",		OKRLinkArray().OlderThan(AalenianStage)			.YoungerThan(PliensbachianStage));
	const OKR PredefinedOKR::PliensbachianStage	(183.0-1.5,	189.6+1.5,	"PliensbachianStage",	OKRLinkArray().OlderThan(ToarcianStage)			.YoungerThan(SinemurianStage));
	const OKR PredefinedOKR::SinemurianStage	(189.6-1.5, 196.5+1.0,	"SinemurianStage",		OKRLinkArray().OlderThan(PliensbachianStage)	.YoungerThan(HettangianStage));
	const OKR PredefinedOKR::HettangianStage	(196.5-1.0,	199.6+0.6,	"HettangianStage",		OKRLinkArray().OlderThan(SinemurianStage)		.YoungerThan(RhaetianStage));
	const OKR PredefinedOKR::RhaetianStage		(199.6-0.6, 203.6+1.5,	"RhaetianStage",		OKRLinkArray().OlderThan(HettangianStage)		.YoungerThan(NorianStage));
	const OKR PredefinedOKR::NorianStage		(203.6-1.5, 216.5+2.0,	"NorianStage",			OKRLinkArray().OlderThan(RhaetianStage)			.YoungerThan(CarnianStage));
	const OKR PredefinedOKR::CarnianStage		(216.5-2.0,	228.7+2.0,	"CarnianStage",			OKRLinkArray().OlderThan(NorianStage)			.YoungerThan(LadinianStage));
	const OKR PredefinedOKR::LadinianStage		(228.7-2.0,	237.0+2.0,	"LadinianStage",		OKRLinkArray().OlderThan(CarnianStage)			.YoungerThan(AnisianStage));
	const OKR PredefinedOKR::AnisianStage		(237.0-2.0,	245.0+1.5,	"AnisianStage",			OKRLinkArray().OlderThan(LadinianStage)			.YoungerThan(OlenekianStage));
	const OKR PredefinedOKR::OlenekianStage		(245.0-1.5,	249.5+0.7,	"OlenekianStage",		OKRLinkArray().OlderThan(AnisianStage)			.YoungerThan(InduanStage));
	const OKR PredefinedOKR::InduanStage		(249.5-0.7, 251.0+0.4,	"InduanStage",			OKRLinkArray().OlderThan(OlenekianStage)		.YoungerThan(ChanghsingianStage));
	const OKR PredefinedOKR::ChanghsingianStage	(251.0-0.4,	253.8+0.7,	"ChanghsingianStage",	OKRLinkArray().OlderThan(InduanStage)			.YoungerThan(WuchiapingianStage));
	const OKR PredefinedOKR::WuchiapingianStage	(253.8-0.7, 260.4+0.7,	"WuchiapingianStage",	OKRLinkArray().OlderThan(ChanghsingianStage)	.YoungerThan(CapitanianStage));
	const OKR PredefinedOKR::CapitanianStage	(260.4-0.7, 265.8+0.7,	"CapitanianStage",		OKRLinkArray().OlderThan(WuchiapingianStage)	.YoungerThan(WordianStage));
	const OKR PredefinedOKR::WordianStage		(265.8-0.7, 268.0+0.7,	"WordianStage",			OKRLinkArray().OlderThan(CapitanianStage)		.YoungerThan(RoadianStage));
	const OKR PredefinedOKR::RoadianStage		(268.0-0.7, 270.6+0.7,	"RoadianStage",			OKRLinkArray().OlderThan(WordianStage)			.YoungerThan(KungurianStage));
	const OKR PredefinedOKR::KungurianStage		(270.6-0.7, 275.6+0.7,	"KungurianStage",		OKRLinkArray().OlderThan(RoadianStage)			.YoungerThan(ArtinskianStage));
	const OKR PredefinedOKR::ArtinskianStage	(275.6-0.7, 284.4+0.7,	"ArtinskianStage",		OKRLinkArray().OlderThan(KungurianStage)		.YoungerThan(SakmarianStage));
	const OKR PredefinedOKR::SakmarianStage		(284.4-0.7, 294.6+0.8,	"SakmarianStage",		OKRLinkArray().OlderThan(ArtinskianStage)		.YoungerThan(AsselianStage));
	const OKR PredefinedOKR::AsselianStage		(294.6-0.8, 299.0+0.8,	"AsselianStage",		OKRLinkArray().OlderThan(SakmarianStage)		.YoungerThan(GzhelianStage));
	const OKR PredefinedOKR::GzhelianStage		(299.0-0.8,	303.4+0.9,	"GzhelianStage",		OKRLinkArray().OlderThan(AsselianStage)			.YoungerThan(KasimovianStage));
	const OKR PredefinedOKR::KasimovianStage	(303.4-0.9, 307.2+1.0,	"KasimovianStage",		OKRLinkArray().OlderThan(GzhelianStage)			.YoungerThan(MoscovianStage));
	const OKR PredefinedOKR::MoscovianStage		(307.2-1.0,	311.7+1.1,	"MoscovianStage",		OKRLinkArray().OlderThan(KasimovianStage)		.YoungerThan(BashkirianStage));
	const OKR PredefinedOKR::BashkirianStage	(311.7-1.1, 318.1+1.3,	"BashkirianStage",		OKRLinkArray().OlderThan(MoscovianStage)		.YoungerThan(SerpukhovianStage));
	const OKR PredefinedOKR::SerpukhovianStage	(318.1-1.3, 328.3+1.6,	"SerpukhovianStage",	OKRLinkArray().OlderThan(BashkirianStage)		.YoungerThan(ViseanStage));
	const OKR PredefinedOKR::ViseanStage		(328.3-1.6, 345.3+2.1,	"ViseanStage",			OKRLinkArray().OlderThan(SerpukhovianStage)		.YoungerThan(TournaisianStage));
	const OKR PredefinedOKR::TournaisianStage	(345.3-2.1, 359.2+2.5,	"TournaisianStage",		OKRLinkArray().OlderThan(ViseanStage)			.YoungerThan(FamennianStage));
	const OKR PredefinedOKR::FamennianStage		(359.2-2.5, 374.5+2.6,	"FamennianStage",		OKRLinkArray().OlderThan(TournaisianStage)		.YoungerThan(FrasnianStage));
	const OKR PredefinedOKR::FrasnianStage		(374.5-2.6, 385.3+2.6,	"FrasnianStage",		OKRLinkArray().OlderThan(FamennianStage	)		.YoungerThan(GivetianStage));
	const OKR PredefinedOKR::GivetianStage		(385.3-2.6, 391.8+2.7,	"GivetianStage",		OKRLinkArray().OlderThan(FrasnianStage)			.YoungerThan(EifelianStage));
	const OKR PredefinedOKR::EifelianStage		(391.8-2.7, 397.5+2.7,	"EifelianStage",		OKRLinkArray().OlderThan(GivetianStage)			.YoungerThan(EmsianStage));
	const OKR PredefinedOKR::EmsianStage		(397.5-2.7, 407.0+2.8,	"EmsianStage",			OKRLinkArray().OlderThan(EifelianStage)			.YoungerThan(PragianStage));
	const OKR PredefinedOKR::PragianStage		(407.0-2.8,	411.2+2.8,	"PragianStage",			OKRLinkArray().OlderThan(EmsianStage)			.YoungerThan(LochkovianStage));
	const OKR PredefinedOKR::LochkovianStage	(411.2-2.8, 416.0+2.8,	"LochkovianStage",		OKRLinkArray().OlderThan(PragianStage)			.YoungerThan(PridoliSeries));
	const OKR PredefinedOKR::PridoliSeries		(416.0-2.8,	418.7+2.7,	"PridoliSeries",		OKRLinkArray().OlderThan(LochkovianStage)		.YoungerThan(LudfordianStage));
	const OKR PredefinedOKR::LudfordianStage	(418.7-2.7, 421.3+2.6,	"LudfordianStage",		OKRLinkArray().OlderThan(PridoliSeries)			.YoungerThan(GorstianStage));
	const OKR PredefinedOKR::GorstianStage		(421.3-2.6, 422.9+2.5,	"GorstianStage",		OKRLinkArray().OlderThan(LudfordianStage)		.YoungerThan(HomerianStage));
	const OKR PredefinedOKR::HomerianStage		(422.9-2.5, 426.2+2.4,	"HomerianStage",		OKRLinkArray().OlderThan(GorstianStage)			.YoungerThan(SheinwoodianStage));
	const OKR PredefinedOKR::SheinwoodianStage	(426.2-2.4, 428.2+2.3,	"SheinwoodianStage",	OKRLinkArray().OlderThan(HomerianStage)			.YoungerThan(TelychianStage));
	const OKR PredefinedOKR::TelychianStage		(428.2-2.3, 436.0+1.9,	"TelychianStage",		OKRLinkArray().OlderThan(SheinwoodianStage)		.YoungerThan(AeronianStage));
	const OKR PredefinedOKR::AeronianStage		(436.0-1.9,	439.0+1.8,	"AeronianStage",		OKRLinkArray().OlderThan(TelychianStage)		.YoungerThan(RhuddanianStage));
	const OKR PredefinedOKR::RhuddanianStage	(439.0-1.8,	443.7+1.5,	"RhuddanianStage",		OKRLinkArray().OlderThan(AeronianStage)			.YoungerThan(HirnantianStage));
	const OKR PredefinedOKR::HirnantianStage	(443.7-1.5, 445.6+1.5,	"HirnantianStage",		OKRLinkArray().OlderThan(RhuddanianStage)		.YoungerThan(KatianStage));
	const OKR PredefinedOKR::KatianStage		(445.6-1.5, 455.8+1.6,	"KatianStage",			OKRLinkArray().OlderThan(HirnantianStage)		.YoungerThan(SandbianStage));
	const OKR PredefinedOKR::SandbianStage		(455.8-1.6, 460.9+1.6,	"SandbianStage",		OKRLinkArray().OlderThan(KatianStage)			.YoungerThan(DarriwilianStage));
	const OKR PredefinedOKR::DarriwilianStage	(460.9-1.6, 468.1+1.6,	"DarriwilianStage",		OKRLinkArray().OlderThan(SandbianStage)			.YoungerThan(DapingianStage));
	const OKR PredefinedOKR::DapingianStage		(468.1-1.6, 471.8+1.6,	"DapingianStage",		OKRLinkArray().OlderThan(DarriwilianStage)		.YoungerThan(FloianStage));
	const OKR PredefinedOKR::FloianStage		(471.8-1.6, 478.6+1.7,	"FloianStage",			OKRLinkArray().OlderThan(DapingianStage)		.YoungerThan(TremadocianStage));
	const OKR PredefinedOKR::TremadocianStage	(478.6-1.7, 488.3+1.7,	"TremadocianStage",		OKRLinkArray().OlderThan(FloianStage)			.YoungerThan(Stage10));
	const OKR PredefinedOKR::Stage10			(488.3-1.7, 492.0,		"Stage10",				OKRLinkArray().OlderThan(TremadocianStage)		.YoungerThan(Stage9));
	const OKR PredefinedOKR::Stage9				(492.0,		496.0,		"Stage9",				OKRLinkArray().OlderThan(Stage10)				.YoungerThan(PaibianStage));
	const OKR PredefinedOKR::PaibianStage		(496.0,		499+2.0,	"PaibianStage",			OKRLinkArray().OlderThan(Stage9)				.YoungerThan(GuzhangianStage));
	const OKR PredefinedOKR::GuzhangianStage	(499-2.0,	503.0,		"GuzhangianStage",		OKRLinkArray().OlderThan(PaibianStage)			.YoungerThan(DrumianStage));
	const OKR PredefinedOKR::DrumianStage		(503.0,		506.5,		"DrumianStage",			OKRLinkArray().OlderThan(GuzhangianStage)		.YoungerThan(Stage5));
	const OKR PredefinedOKR::Stage5				(506.5,		510.0,		"Stage5",				OKRLinkArray().OlderThan(DrumianStage)			.YoungerThan(Stage4));
	const OKR PredefinedOKR::Stage4				(510.0,		517.0,		"Stage4",				OKRLinkArray().OlderThan(Stage5)				.YoungerThan(Stage3));
	const OKR PredefinedOKR::Stage3				(517.0,		521.0,		"Stage3",				OKRLinkArray().OlderThan(Stage4)				.YoungerThan(Stage2));
	const OKR PredefinedOKR::Stage2				(521.0,		528.0,		"Stage2",				OKRLinkArray().OlderThan(Stage3)				.YoungerThan(FortunianStage));
	const OKR PredefinedOKR::FortunianStage		(528.0,		542+1.0,	"FortunianStage",		OKRLinkArray().OlderThan(Stage2)				.YoungerThan(EdiacaranSystem));
	const OKR PredefinedOKR::EdiacaranSystem	(542-1.0,	635.0,		"EdiacaranSystem",		OKRLinkArray().OlderThan(FortunianStage)		.YoungerThan(CryogenianSystem));
	const OKR PredefinedOKR::CryogenianSystem	(635.0,		850.0,		"CryogenianSystem",		OKRLinkArray().OlderThan(EdiacaranSystem)		.YoungerThan(TonianSystem));
	const OKR PredefinedOKR::TonianSystem		(850.0,		1000.0,		"TonianSystem",			OKRLinkArray().OlderThan(CryogenianSystem)		.YoungerThan(StenianSystem));
	const OKR PredefinedOKR::StenianSystem		(1000.0,	1200.0,		"StenianSystem",		OKRLinkArray().OlderThan(TonianSystem)			.YoungerThan(EctasianSystem));
	const OKR PredefinedOKR::EctasianSystem		(1200.0,	1400.0,		"EctasianSystem",		OKRLinkArray().OlderThan(StenianSystem)			.YoungerThan(CalymmianSystem));
	const OKR PredefinedOKR::CalymmianSystem	(1400.0,	1600.0,		"CalymmianSystem",		OKRLinkArray().OlderThan(EctasianSystem)		.YoungerThan(StatherianSystem));
	const OKR PredefinedOKR::StatherianSystem	(1600.0,	1800.0,		"StatherianSystem",		OKRLinkArray().OlderThan(CalymmianSystem)		.YoungerThan(OrosirianSystem));
	const OKR PredefinedOKR::OrosirianSystem	(1800.0,	2050.0,		"OrosirianSystem",		OKRLinkArray().OlderThan(StatherianSystem)		.YoungerThan(RhyacianSystem));
	const OKR PredefinedOKR::RhyacianSystem		(2050.0,	2300.0,		"RhyacianSystem",		OKRLinkArray().OlderThan(OrosirianSystem)		.YoungerThan(SiderianSystem));
	const OKR PredefinedOKR::SiderianSystem		(2300.0,	2500.0,		"SiderianSystem",		OKRLinkArray().OlderThan(RhyacianSystem)		.YoungerThan(NeoarcheanEra));
	const OKR PredefinedOKR::NeoarcheanEra		(2500.0,	2800.0,		"NeoarcheanEra",		OKRLinkArray().OlderThan(SiderianSystem)		.YoungerThan(MesoarcheanEra));
	const OKR PredefinedOKR::MesoarcheanEra		(2800.0,	3200.0,		"MesoarcheanEra",		OKRLinkArray().OlderThan(NeoarcheanEra)			.YoungerThan(PaleoarcheanEra));
	const OKR PredefinedOKR::PaleoarcheanEra	(3200.0,	3600.0,		"PaleoarcheanEra",		OKRLinkArray().OlderThan(MesoarcheanEra)		.YoungerThan(EoarcheanEra));
	const OKR PredefinedOKR::EoarcheanEra		(3600.0,	4000.0,		"EoarcheanEra",			OKRLinkArray().OlderThan(PaleoarcheanEra)		.YoungerThan(HeadanEon));
	const OKR PredefinedOKR::HeadanEon			(4000.0,	4600.0,		"HeadanEon",			OKRLinkArray().OlderThan(EoarcheanEra));
