/*	Taxa.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Taxa.h"
#include "../include/Utils.h"

IMPLEMENT_CLASS(Taxon, OKR);
DEFINE_EVENT_TYPE(EVT_TAXON_SELECT);

ExposureString DoubleToString(double num){return(ExposureString(wxString::Format("%f",num)));}
double StringToDouble(ExposureString text){double ret; (text.AsStringType()).ToDouble(&ret); return(ret);}

DECLARE_EXPOSURE_TEXT_TYPE(double,DoubleToString,StringToDouble)

BEGIN_EXPOSURE_TABLE(Taxon,"A single Taxon")->AlignLeft()->Expand()->UpdateEntireInterface()
	EXPOSURE_GROUP("Name")
		EXPOSE_TEXT("Name","", &Taxon::Name, &Taxon::Name, "The name of this taxon")
	EXPOSURE_END_GROUP()
	EXPOSURE_ROW()
	EXPOSURE_GROUP("Oldest Known Record")
		EXPOSURE_GROUP()
			EXPOSURE_ENABLE_GROUP("Grouped", "Grouped", &OKR::IsSameAs, &OKR::IsSameAs, "")
				EXPOSURE_ROW()
				EXPOSE_STATIC_TEXT("Same OKR As:")
				EXPOSE_CHOICE("SameAsTaxon","",&Taxon::SameAsTaxon, &Taxon::SameAsTaxon,"The Taxon with the same OKR as this one")->Dynamic(&Taxon::GetTaxaChanged2,&Taxon::GetTaxaCountWithBlank,
					&Taxon::_GetTaxaNamesWithBlank,&Taxon::_GetTaxaNamesWithBlank,NULL)
				EXPOSE_BUTTON("GoToTaxon",">",&Taxon::SelectSameAsTaxon,"Select the 'grouped' taxon to see its data")->ExactFit()
			EXPOSURE_END_ENABLE_GROUP()
		EXPOSURE_END_GROUP()
		EXPOSURE_ROW()
		EXPOSURE_GROUP()
			EXPOSURE_ENABLE_GROUP("NotGrouped", "Individual", &OKR::IsNotSameAs, &OKR::IsNotSameAs, "")
				EXPOSURE_ROW()
				EXPOSE_STATIC_TEXT("Youngest (mya):")
				EXPOSURE_ROW()
				EXPOSE_TEXT("MinOKR","",&OKR::ExposureMinOKR,&OKR::MinOKR,"The minimum possible age for the oldest known record for this taxon")
				EXPOSURE_ROW()
				EXPOSE_STATIC_TEXT("Oldest (mya):")
				EXPOSURE_ROW()
				EXPOSE_TEXT("MaxOKR","",&OKR::ExposureMaxOKR,&OKR::MaxOKR,"The maximum possible age for the oldest known record for this taxon")
			EXPOSURE_END_ENABLE_GROUP()
		EXPOSURE_END_GROUP()
	EXPOSURE_END_GROUP()
END_EXPOSURE_TABLE()

TaxaNameHash Taxon::sm_TaxaNameHash;
TaxaIDHash Taxon::sm_TaxaIDHash;
bool Taxon::sm_TaxaChanged1 = false;
bool Taxon::sm_TaxaChanged2 = false;
int Taxon::sm_NextExportID = 1;
wxCommandProcessor *Taxon::sm_CommandProcessor = NULL;

void TaxaNameHash::Destroy()
{
	TaxaNameHash::iterator it = this->begin(), end = this->end();
	for(; it != end; ++it)
	{
		wxDELETE(it->second);
	}
	this->clear();
}
void TaxaIDHash::Destroy()
{
	this->clear();
}

void Taxon::ChangeName(wxString newName)
{
	if (this->sm_TaxaNameHash.find(newName) == this->sm_TaxaNameHash.end())
	{
		if (!this->m_Name.IsEmpty())
		{
			//Remove this item from the taxa hash
			if (this->sm_TaxaNameHash.find(this->m_Name) != this->sm_TaxaNameHash.end())
			{
				this->sm_TaxaNameHash.erase(this->m_Name);
			}
		}
		this->m_Name = newName;
		this->sm_TaxaNameHash[newName] = this;
		Taxon::sm_TaxaChanged1 = Taxon::sm_TaxaChanged2 = true;
	}
}

void Taxon::ImportID(int newID)
{
	if (this->m_ImportID == newID){return;}
	int ID = newID;
	
	if (newID < 0 || (this->sm_TaxaIDHash.find(newID) != this->sm_TaxaIDHash.end()))
	{
		//Find the first empty ID
		ID = 0;
		while(this->sm_TaxaIDHash.find(ID) != this->sm_TaxaIDHash.end()){ID++;}
	}
	//Remove this item from the taxa hash
	if (this->sm_TaxaIDHash.find(this->m_ImportID) != this->sm_TaxaIDHash.end())
	{
		this->sm_TaxaIDHash.erase(this->m_ImportID);
	}
		
	this->m_ImportID = ID;
	this->sm_TaxaIDHash[ID] = this;
}

Taxon *Taxon::FindTaxonByName(wxString Name)
{
	Name.EndsWith("*",&Name);
	TaxaNameHash::iterator Results = Taxon::sm_TaxaNameHash.find(Name);
	if (Results != Taxon::sm_TaxaNameHash.end())
	{
		return(Results->second);
	}
	return(NULL);
}

Taxon *Taxon::FindTaxonByImportID(int ID)
{
	TaxaIDHash::iterator Results = Taxon::sm_TaxaIDHash.find(ID);
	if (Results != Taxon::sm_TaxaIDHash.end())
	{
		return(Results->second);
	}
	return(NULL);
}

Taxon *Taxon::FindTaxonByExportID(int ExportID)
{
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		if (it->second->m_ExportID == ExportID){return(it->second);}
	}
	return(NULL);
}

bool Taxon::CheckTaxaAges(wxString *ErrorMessage, wxString *WarningMessage, bool ShowMaxAgeWarning)
{
	bool Result = true;
	wxArrayString ErrorResultArray, WarningResultArray;
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		if (it->second->IsNotSameAs() && it->second->MinOKR() > it->second->MaxOKR()){Result = false; ErrorResultArray.Add(it->first + " has a minimum age larger than its maximum age");}
		if (ShowMaxAgeWarning && it->second->IsNotSameAs() && it->second->MaxOKR() < 0.00000001 && !it->second->AcceptableOKR()){WarningResultArray.Add(it->first + " has max age set to 0.0");}
		if (it->second->IsSameAs() && it->second->SameAsTaxon().IsEmpty()){Result = false; ErrorResultArray.Add(it->first + " is marked as grouped, but no group membership specified");}
	}
	if (ErrorMessage || WarningMessage)
	{
		//Done like this so we can show errors grouped by taxon and alphabetically
		if (!ErrorResultArray.IsEmpty() && ErrorMessage)
		{
			wxString ErrorResultString;
			ErrorResultArray.Sort();
			for(unsigned int index = 0, size = ErrorResultArray.size(); index < size; ++index){ErrorResultString += ErrorResultArray[index] + "\n";}
			*ErrorMessage = ErrorResultString;
		}
		if (!WarningResultArray.IsEmpty() && WarningMessage)
		{
			wxString WarningResultString;
			WarningResultArray.Sort();
			for(unsigned int index = 0, size = WarningResultArray.size(); index < size; ++index){WarningResultString += WarningResultArray[index] + "\n";}
			*WarningMessage = WarningResultString;
		}
	}
	return(Result);
}

wxArrayString Taxon::GetTaxaNames(bool AddStarToNames)
{
	wxArrayString Names;
	wxString Current;
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		Current = it->first;
		if (AddStarToNames && !it->second->IsSameAs() && it->second->MaxOKR() <= 0.00000000 && !it->second->AcceptableOKR()){Current += "*";}
		Names.Add(Current);
	}
	Names.Sort();
	return(Names);
}

wxString *Taxon::_GetTaxaNames()
{
	wxString *Result = new wxString[Taxon::sm_TaxaNameHash.size()];
	wxArrayString Names;
	wxString Current;
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		Current = it->first;
		if (!it->second->IsSameAs() && it->second->MaxOKR() <= 0.00000000 && !it->second->AcceptableOKR()){Current += "*";}
		Names.Add(Current);
	}
	Names.Sort();
	for(unsigned int index = 0, size = Names.size(); index < size; ++index)
	{
		Result[index] = Names[index];
	}
	return(Result);
}

wxString *Taxon::_GetTaxaNamesWithBlank()
{
	wxString *Result = new wxString[Taxon::sm_TaxaNameHash.size()+1];
		Result[0] = wxEmptyString;
	wxArrayString Names;
	wxString Current;
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		Current = it->first;
		if (!it->second->IsSameAs() && it->second->MaxOKR() <= 0.00000000 && !it->second->AcceptableOKR()){Current += "*";}
		Names.Add(Current);
	}
	Names.Sort();
	for(unsigned int index = 0, size = Names.size(); index < size; ++index)
	{
		Result[index+1] = Names[index];
	}
	return(Result);
}

unsigned int Taxon::GetTaxaCount()
{
	return(Taxon::sm_TaxaNameHash.size());
}

unsigned int Taxon::GetTaxaCountWithBlank()
{
	return(Taxon::sm_TaxaNameHash.size()+1);
}

void Taxon::ResetExportIDs()
{
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		it->second->m_ExportID = -1;
	}
	Taxon::sm_NextExportID = 1;
}
void Taxon::RemoveAutomaticLinks()
{
	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
	for(;it != end; ++it)
	{
		OKRLinkArray &LinkArray = it->second->m_OKRLinkArray;
		for(unsigned int link = 0; link < LinkArray.size(); ++link)
		{
			if (!LinkArray[link].LinkedOKR()->Owner())
			{
				LinkArray.RemoveAt(link);
				link--;
			}
		}
		it->second->m_ExportID = -1;
	}
}
wxDataInputStream& Taxon::Load(wxDataInputStream& strm, wxString Name)
{
	wxInt32 ImportID, ExportID;

	strm >> ImportID >> ExportID;

	this->ChangeName(Name); this->ImportID(ImportID); this->m_ExportID = ExportID;
	OKR::Load(strm);
	return(strm);
}

wxXmlNode *Taxon::Save()
{
	wxXmlNode *TaxonNode = new wxXmlNode(wxXML_ELEMENT_NODE, "Taxon");
	TaxonNode->AddChild(CreateXmlContentNode("Name",this->m_Name));
	TaxonNode->AddChild(OKR::Save());
	return(TaxonNode);
}

wxDataInputStream& Taxon::LoadAllTaxa(wxDataInputStream& strm)
{
	wxUint32 size;
	size = strm.Read32();
	Taxon *NewTaxon = NULL;
	wxString name;

	for(unsigned int index = 0; index < size; ++index)
	{
		strm >> name;
		NewTaxon = Taxon::FindTaxonByName(name);
		if (!NewTaxon){NewTaxon = new Taxon();}
		NewTaxon->Load(strm,name);
	}
	return(strm);
}
//wxTextInputStream& Taxon::LoadAllTaxa(wxTextInputStream& strm)
//{
//	unsigned long size = 0;
//	wxString line;
//	strm >> line; line.AfterFirst('=').ToULong(&size);
//	Taxon *NewTaxon = NULL;
//	wxString name;
//
//	for(unsigned int index = 0; index < size; ++index)
//	{
//		strm >> name;
//		NewTaxon = Taxon::FindTaxonByName(name);
//		if (!NewTaxon){NewTaxon = new Taxon();}
//		NewTaxon->Load(strm,name);
//	}
//	return(strm);
//}
bool Taxon::Load(wxXmlNode* CurrentNode)
{
	if (!CurrentNode || CurrentNode->GetName() != "Taxon"){return(false);}
	Taxon *NewTaxon = new Taxon();
	bool Result = true;
	for(wxXmlNode *Current = CurrentNode->GetChildren(); Current != NULL; Current = Current->GetNext())
	{
		if (Current->GetName() == "Name")
		{
			wxString name = Current->GetNodeContent();
			if (name.IsEmpty()){wxMessageBox("Name is empty");}
			Taxon *FoundTaxon = Taxon::FindTaxonByName(name);
			if (FoundTaxon){wxDELETE(NewTaxon); NewTaxon = FoundTaxon;}
			else {NewTaxon->ChangeName(name);}
		}
		else if (Current->GetName() == "OKR")
		{
			Result &= NewTaxon->OKR::Load(Current);
		}
	}
	return(Result);
}
bool Taxon::LoadAllTaxa(wxXmlNode* Root)
{
	if (Root->GetName() != "Taxa"){return(false);}
	bool Result = true;
	wxXmlNode *CurrentNode = Root->GetChildren();
	while(CurrentNode)
	{
		Result &= Taxon::Load(CurrentNode);
		CurrentNode = CurrentNode->GetNext();
	}
	return(Result);
}
//wxDataOutputStream& Taxon::SaveAllTaxa(wxDataOutputStream& strm)
//{
//	strm << (wxUint32)Taxon::sm_TaxaNameHash.size();
//	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
//
//	for(;it != end; ++it)
//	{
//		it->second->Save(strm);
//	}
//	return(strm);
//}
//wxTextOutputStream& Taxon::SaveAllTaxa(wxTextOutputStream& strm)
//{
//	strm << "Count = " << (wxUint32)Taxon::sm_TaxaNameHash.size() << "\n";
//	TaxaNameHash::iterator it = Taxon::sm_TaxaNameHash.begin(), end = Taxon::sm_TaxaNameHash.end();
//	for(;it != end; ++it)
//	{
//		it->second->Save(strm);
//	}
//	return(strm);
//}
wxXmlNode* Taxon::SaveAllTaxa()
{
	wxXmlNode *TaxaData = new wxXmlNode(wxXML_ELEMENT_NODE,"Taxa");
	wxArrayString TaxaNames = Taxon::GetTaxaNames(false);
	for(unsigned int index = 0, size = TaxaNames.size(); index < size; ++index)
	{
		Taxon *data = Taxon::FindTaxonByName(TaxaNames[index]);
		if (data){TaxaData->AddChild(data->Save());}
	}
	return(TaxaData);
}