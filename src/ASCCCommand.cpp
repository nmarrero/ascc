/*	ASCCCommand.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/ASCCCommand.h"
#include <wx/wupdlock.h>

DEFINE_EVENT_TYPE(EVT_TAXA_RENAME);
DEFINE_EVENT_TYPE(EVT_TAXA_DELETE);
DEFINE_EVENT_TYPE(EVT_TAXA_ADD);
DEFINE_EVENT_TYPE(EVT_OKR_CHANGED);
DEFINE_EVENT_TYPE(EVT_PHYLO_LEAF);
DEFINE_EVENT_TYPE(EVT_COMMAND_SUBMITTED);

IMPLEMENT_CLASS(ASCCCommandProcessor,wxCommandProcessor);
IMPLEMENT_CLASS(ASCCCommand, wxCommand);
IMPLEMENT_CLASS(ASCCCommandGroup, ASCCCommand);
IMPLEMENT_CLASS(TC_Base, ASCCCommand);
IMPLEMENT_CLASS(TC_Add, TC_Base);
IMPLEMENT_CLASS(TC_Delete, TC_Base);
IMPLEMENT_CLASS(TC_Rename, TC_Base);
IMPLEMENT_CLASS(TC_SetMinOKR, TC_Base);
IMPLEMENT_CLASS(TC_SetMaxOKR, TC_Base);
IMPLEMENT_CLASS(TC_Group, TC_Base);
IMPLEMENT_CLASS(TC_CopyOKR, TC_Base);
IMPLEMENT_CLASS(TC_LinkOKR, TC_Base);
IMPLEMENT_CLASS(TC_UnlinkOKR, TC_Base);
IMPLEMENT_CLASS(TC_RelinkOKR, TC_Base);
IMPLEMENT_CLASS(PTC_Base, ASCCCommand);
IMPLEMENT_CLASS(PTC_AddNode, PTC_Base);
IMPLEMENT_CLASS(PTC_DeleteNode, PTC_Base);
IMPLEMENT_CLASS(PTC_RenameNode, PTC_Base);
IMPLEMENT_CLASS(PTC_MoveNode, PTC_Base);
IMPLEMENT_CLASS(PTC_AddTree, PTC_Base);
IMPLEMENT_CLASS(PTC_DeleteTree, PTC_Base);

#include "../include/Taxa.h"
//TC_Base public functions
TC_Base::TC_Base(wxString CommandName):ASCCCommand(CommandName),m_Taxon(NULL),
	m_MinOKR(0.0),m_MaxOKR(0.0),m_SameAs(wxEmptyString),m_TaxonName(wxEmptyString),m_TaxonNameLookup(wxEmptyString),m_TaxonOwned(false),m_OKR(NULL),m_LinkIndex(0){}
TC_Base::TC_Base(const TC_Base& other):ASCCCommand(other),m_Taxon(other.m_Taxon),
	m_MinOKR(other.m_MinOKR),m_MaxOKR(other.m_MaxOKR),m_SameAs(other.m_SameAs),m_TaxonName(other.m_TaxonName),m_TaxonNameLookup(other.m_TaxonNameLookup),
	m_TaxonOwned(other.m_TaxonOwned),m_OKR(other.m_OKR?new OKR(*other.m_OKR):NULL),m_Link(other.m_Link),m_LinkIndex(other.m_LinkIndex){}
TC_Base::~TC_Base(){if (this->m_TaxonOwned){wxDELETE(this->m_Taxon);} wxDELETE(this->m_OKR);}

void TC_Base::SetTaxon(Taxon *newTaxon){this->m_Taxon = newTaxon;}
void TC_Base::SetTaxon(wxString TaxonName)
{
	this->m_TaxonNameLookup = TaxonName;
}
Taxon *TC_Base::GetTaxon()
{
	if (!this->m_Taxon && !this->m_TaxonNameLookup.IsEmpty())
	{
		return(Taxon::FindTaxonByName(this->m_TaxonNameLookup));
	}
	return(this->m_Taxon);
}

void TC_Base::SetOKR(const OKR& OKRToCopy){this->m_OKR = new OKR(OKRToCopy);}
OKR *TC_Base::GetOKR(){return(this->m_OKR);}

void TC_Base::SetMinOKR(double minOKR){this->m_MinOKR = minOKR;}
double TC_Base::GetMinOKR(){return(this->m_MinOKR);}

void TC_Base::SetMaxOKR(double maxOKR){this->m_MaxOKR = maxOKR;}
double TC_Base::GetMaxOKR(){return(this->m_MaxOKR);}

void TC_Base::SetSameAs(wxString SameAs){this->m_SameAs = SameAs;}
wxString TC_Base::GetSameAs(){return(this->m_SameAs);}

void TC_Base::SetTaxonName(wxString TaxonName){this->m_TaxonName = TaxonName;}
wxString TC_Base::GetTaxonName(){return(this->m_TaxonName);}

void TC_Base::SetLink(OKRLink& Link){this->m_Link = Link;}
OKRLink& TC_Base::GetLink(){return(this->m_Link);}

void TC_Base::SetLinkIndex(unsigned int LinkIndex){this->m_LinkIndex = LinkIndex;}
unsigned int TC_Base::GetLinkIndex(){return(this->m_LinkIndex);}

//TC_Base protected functions
bool TC_Base::DoAddTaxon()
{
	Taxon *TaxonToAdd = this->GetTaxon();
	this->m_TaxonOwned = false;
	if (!TaxonToAdd && this->m_TaxonName.IsEmpty()){return(false);}
	else if (!this->m_TaxonName.IsEmpty() && Taxon::FindTaxonByName(this->m_TaxonName)){return(false);}
	
	if (!TaxonToAdd){TaxonToAdd = new Taxon();}
	else {this->m_TaxonName = TaxonToAdd->Name();}
			
	TaxonToAdd->ChangeName(this->m_TaxonName); 
	if (TaxonToAdd->ImportID() != -1){TaxonToAdd->ImportID(TaxonToAdd->ImportID());} 
	this->SetTaxon(TaxonToAdd); 
	
	wxCommandEvent evt(EVT_TAXA_ADD);
	evt.SetString(this->m_TaxonName);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoDeleteTaxon()
{
	Taxon *TaxonToDelete = this->GetTaxon();	
	//Remove the taxon from the list
	TaxaNameHash::iterator nameit = Taxon::sm_TaxaNameHash.find(TaxonToDelete->Name());
	if (nameit != Taxon::sm_TaxaNameHash.end()){Taxon::sm_TaxaNameHash.erase(nameit);}

	TaxaIDHash::iterator idit = Taxon::sm_TaxaIDHash.find(TaxonToDelete->ImportID());
	if (idit != Taxon::sm_TaxaIDHash.end()){Taxon::sm_TaxaIDHash.erase(idit);}

	Taxon::sm_TaxaChanged1 = Taxon::sm_TaxaChanged2 = true;
	this->SetTaxon(TaxonToDelete);

	this->m_TaxonOwned = true;

	wxCommandEvent evt(EVT_TAXA_DELETE);
	evt.SetString(TaxonToDelete->Name());
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoRenameTaxon()
{
	Taxon *TaxonToRename = this->GetTaxon();
	if (!TaxonToRename){return(false);}
	wxString OldName = TaxonToRename->Name();
	TaxonToRename->ChangeName(this->m_TaxonName);
	
	wxCommandEvent evt(EVT_TAXA_RENAME);
	evt.SetString(OldName + ";" + this->m_TaxonName);
	this->DispatchEvent(evt);

	this->m_TaxonName = OldName;
	this->SetTaxon(TaxonToRename);
	
	return(true);
}
bool TC_Base::DoSetMinOKR()
{
	Taxon *TaxonToChange = this->GetTaxon();
	if (!TaxonToChange){return(false);}
	double OldMinOKR = TaxonToChange->MinOKR();
	TaxonToChange->OKR::MinOKR(this->m_MinOKR);
	this->m_MinOKR = OldMinOKR;
	this->SetTaxon(TaxonToChange);

	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoSetMaxOKR()
{
	Taxon *TaxonToChange = this->GetTaxon();
	if (!TaxonToChange){return(false);}
	double OldMaxOKR = TaxonToChange->MaxOKR();
	TaxonToChange->OKR::MaxOKR(this->m_MaxOKR);
	this->m_MaxOKR = OldMaxOKR;
	this->SetTaxon(TaxonToChange);

	if ((OldMaxOKR == 0.00 && TaxonToChange->MaxOKR() != 0.00) ||
		(OldMaxOKR != 0.00 && TaxonToChange->MaxOKR() == 0.00))
	{Taxon::sm_TaxaChanged1 = Taxon::sm_TaxaChanged2 = true;}  

	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoSetSameAs()
{
	Taxon *TaxonToChange = this->GetTaxon();
	if (!TaxonToChange || TaxonToChange->Name() == this->m_SameAs){return(false);}
	double OldMaxOKR = TaxonToChange->MaxOKR();
	wxString OldSameAsTaxon = TaxonToChange->SameAsTaxon();
	TaxonToChange->OKR::SameAsTaxon(this->m_SameAs);
	this->m_SameAs = OldSameAsTaxon;
	this->SetTaxon(TaxonToChange);

	if ((OldMaxOKR == 0.00 && TaxonToChange->MaxOKR() != 0.00) ||
		(OldMaxOKR != 0.00 && TaxonToChange->MaxOKR() == 0.00))
	{Taxon::sm_TaxaChanged1 = Taxon::sm_TaxaChanged2 = true;} 

	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoCopyOKR()
{
	Taxon *TaxonToChange = this->GetTaxon();
	OKR *OKRToCopy = this->GetOKR();
	if (!TaxonToChange || !OKRToCopy){return(false);}
	bool OldAcceptableOKR = TaxonToChange->AcceptableOKR();
	double OldMaxOKR = TaxonToChange->MaxOKR();
	OKR Temp(*TaxonToChange);
	TaxonToChange->OKR::operator=(*OKRToCopy);
	OKRToCopy->OKR::operator=(Temp);
	this->SetTaxon(TaxonToChange);
	
	if ((OldMaxOKR == 0.00 && TaxonToChange->MaxOKR() != 0.00) ||
		(OldMaxOKR != 0.00 && TaxonToChange->MaxOKR() == 0.00) ||
		(OldAcceptableOKR != TaxonToChange->AcceptableOKR()))
	{Taxon::sm_TaxaChanged1 = Taxon::sm_TaxaChanged2 = true;} 


	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoLinkOKR()
{
	Taxon *TaxonToChange = this->GetTaxon();
	if (!TaxonToChange){return(false);}

	if (this->m_Link.Type() == OLDER_THAN)
	{
		TaxonToChange->OlderThan(this->m_Link.LinkedOKR());
	}
	else
	{
		TaxonToChange->YoungerThan(this->m_Link.LinkedOKR());
	}
	this->m_LinkIndex = TaxonToChange->OKRLinks()-1;

	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoUnlinkOKR()
{
	Taxon *TaxonToChange = this->GetTaxon();
	if (!TaxonToChange){return(false);}
	if (this->m_LinkIndex >= TaxonToChange->OKRLinks()){return(false);}

	this->m_Link = TaxonToChange->Link(this->m_LinkIndex);
	TaxonToChange->RemoveLink(this->m_LinkIndex);
	
	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}
bool TC_Base::DoRelinkOKR()
{
	Taxon *TaxonToChange = this->GetTaxon();
	if (!TaxonToChange){return(false);}
	if (this->m_LinkIndex >= TaxonToChange->OKRLinks()){return(false);}

	OKRLink OldLink = TaxonToChange->Link(this->m_LinkIndex);
	
	TaxonToChange->Link(this->m_LinkIndex).LinkedOKR(this->m_Link.LinkedOKR());
	TaxonToChange->Link(this->m_LinkIndex).Type(this->m_Link.Type());

	this->m_Link.LinkedOKR(OldLink.LinkedOKR());
	this->m_Link.Type(OldLink.Type());

	wxCommandEvent evt(EVT_OKR_CHANGED);
	this->DispatchEvent(evt);
	return(true);
}

#include "../include/PhylogeneticTree.h"
//PTC_Base Public Fxns
PTC_Base::PTC_Base(wxString CommandName):ASCCCommand(CommandName),m_Ctrl(NULL),m_Node(NULL),m_ParentNode(NULL),
	m_NodeIndex(-1), m_NodeLookupIndex(-1), m_ParentNodeLookupIndex(-1), m_NodeOwned(false), 
	m_LeafLabel(wxEmptyString),m_LargestTextExtent(wxSize(1,1)),m_Relayout(true)
{}
PTC_Base::PTC_Base(const PTC_Base& other):ASCCCommand(other),m_Ctrl(other.m_Ctrl),m_Node(other.m_Node),m_ParentNode(other.m_ParentNode),
	m_NodeIndex(other.m_NodeIndex), m_NodeLookupIndex(other.m_NodeLookupIndex), m_ParentNodeLookupIndex(other.m_ParentNodeLookupIndex), m_NodeOwned(other.m_NodeOwned), 
	m_LeafLabel(other.m_LeafLabel),m_LargestTextExtent(other.m_LargestTextExtent),m_Relayout(other.m_Relayout)
{}
PTC_Base::~PTC_Base(){if (this->m_NodeOwned){wxDELETE(this->m_Node);}}

void PTC_Base::SetCtrl(PTreeCtrl *Ctrl){this->m_Ctrl = Ctrl;}
PTreeCtrl *PTC_Base::GetCtrl(){return(this->m_Ctrl);}

void PTC_Base::SetNode(PTreeNode *Node){this->m_Node = Node; this->m_NodeLookupIndex = -1;}
void PTC_Base::SetNode(PTreeNode *Parent, int index){this->m_Node = Parent; this->m_NodeLookupIndex = index;}
PTreeNode *PTC_Base::GetNode()
{
	if (this->m_Node && this->m_NodeLookupIndex == -1){return(this->m_Node);}
	else if (this->m_Node){return(this->m_Node->Child(this->m_NodeLookupIndex));}
	return(NULL);
}

void PTC_Base::SetParent(PTreeNode *Parent){this->m_ParentNode = Parent; this->m_ParentNodeLookupIndex = -1;}
void PTC_Base::SetParent(PTreeNode *Grandparent, int index){this->m_ParentNode = Grandparent; this->m_ParentNodeLookupIndex = index;}
PTreeNode *PTC_Base::GetParent()
{
	if (this->m_ParentNode && this->m_ParentNodeLookupIndex == -1){return(this->m_ParentNode);}
	else if (this->m_ParentNode){return(this->m_ParentNode->Child(this->m_ParentNodeLookupIndex));}
	return(NULL);
}

void PTC_Base::SetLeafLabel(wxString LeafLabel){this->m_LeafLabel = LeafLabel;}
wxString PTC_Base::GetLeafLabel(){return(this->m_LeafLabel);}

void PTC_Base::SetNodeIndex(int NodeIndex){this->m_NodeIndex = NodeIndex;}
int PTC_Base::GetNodeIndex(){return(this->m_NodeIndex);}

PTC_Base& PTC_Base::Relayout(bool relayout){this->m_Relayout = relayout; return(*this);}
bool PTC_Base::Relayout(){return(this->m_Relayout);}

//PTC_Base Protected Fxns
bool PTC_Base::DoMoveNode()
{
	PTreeNode *Node = this->GetNode();
	PTreeNode *Parent = this->GetParent();
	if (Parent && Node && Node->GetParent() && Node->GetParent()->IsKindOf(CLASSINFO(PTreeNode)))
	{
		//Get the old attributes
		PTreeNode *TempParent = (PTreeNode *)Node->GetParent();
		int TempIndex = TempParent->FindChild(Node);
			
		//Move the node to the new parent
		TempParent->RemoveChild(Node);
		Parent->DoAddChild(Node,this->m_NodeIndex);
		Node->Reparent(Parent);
			
		//Check for errors
		TempParent->CheckForErrors();
		Parent->CheckForErrors();

		//Save the old attributes
		this->SetParent(TempParent);
		this->SetNode(Node);
		this->m_NodeIndex = TempIndex;

		//Relayout
		if (this->Relayout()){Parent->SendLayoutEvent(); Parent->SendRefreshEvent();}

		return(true);
	} 
	return(false);
}
bool PTC_Base::DoRenameNode()
{
	PTreeNode *Node = this->GetNode();
	if (!Node){return(false);}
	//Swap the node name
	wxString Temp = Node->GetLabel();
	Node->SetLabel(this->m_LeafLabel);
	this->m_LeafLabel = Temp;
	Node->Refresh();
	if (Node->IsRoot()){((PTreeCtrl*)Node->GetParent())->UpdateSelector();}
	return(true);
}
bool PTC_Base::DoAddNode()
{
	PTreeNode *Node = this->GetNode();
	PTreeNode *Parent = this->GetParent();
	if (!Parent)
	{
		return(false);
	}
	if (this->m_LargestTextExtent.GetWidth() == 1)
	{
		this->m_LargestTextExtent = PTreeNode::sm_LargestTextExtent;
	}
	PTreeNode::sm_LargestTextExtent.SetWidth(this->m_LargestTextExtent.GetWidth());

	if (Node){Parent->DoAddChild(Node,this->m_NodeIndex); Node->Reparent(Parent);}
	else if (this->m_LeafLabel.IsEmpty()){Node = Parent->AddChild(this->m_NodeIndex);}
	else 
	{
		Node = Parent->AddLeaf(this->m_LeafLabel, this->m_NodeIndex);
		wxCommandEvent evt(EVT_PHYLO_LEAF);
		evt.SetString(this->m_LeafLabel);
		this->DispatchEvent(evt);
	}
		
	this->m_NodeIndex = Parent->FindChild(Node);
	this->m_NodeOwned = false; //I no longer own the node
	Node->Show(true);
	this->SetNode(Node);
	if (!Node->GetLabel().IsEmpty()){Node->SetLabel(Node->GetLabel());}
	this->SetParent(Parent);
	Parent->CheckForErrors();
	if (this->Relayout()){Parent->SendLayoutEvent(); Parent->SendRefreshEvent();}
		

	return(true);
}
bool PTC_Base::DoDeleteNode()
{
	PTreeNode *Node = this->GetNode();
	if (!Node)
	{
		return(false);
	}
	if (Node->GetParent() && Node->GetParent()->IsKindOf(CLASSINFO(PTreeNode)))
	{
		this->SetParent((PTreeNode *)this->m_Node->GetParent());
		this->SetNode(Node);
		this->SetNodeIndex(((PTreeNode *)this->m_Node->GetParent())->FindChild(Node));
		this->m_ParentNode->RemoveChild(Node);
		Node->Show(false);
		this->m_NodeOwned = true; //I need to delete the node
		if (this->m_LargestTextExtent.GetWidth() == 1)
		{
			PTreeNode *current = this->m_ParentNode;
			while(current->GetParent()->IsKindOf(CLASSINFO(PTreeNode))){current = (PTreeNode*)current->GetParent();}
			if (current->GetParent()->IsKindOf(CLASSINFO(PTreeCtrl)))
			{
				PTreeCtrl *Ctrl = (PTreeCtrl*)current->GetParent();
				wxArrayString Leaves; Ctrl->GetLeaves(Leaves);
				wxClientDC dc(this->m_ParentNode); wxSize TempSize;
				for(unsigned int index = 0, size = Leaves.size(); index < size; ++index)
				{
					TempSize = dc.GetTextExtent(Leaves[index]); TempSize.IncBy(6,0);
					if (TempSize.GetWidth() > this->m_LargestTextExtent.GetWidth()){this->m_LargestTextExtent.SetWidth(TempSize.GetWidth());}
					if (TempSize.GetHeight() > this->m_LargestTextExtent.GetHeight()){this->m_LargestTextExtent.SetHeight(TempSize.GetHeight());}
				}
			}
		}
		PTreeNode::sm_LargestTextExtent.SetWidth(this->m_LargestTextExtent.GetWidth());

		this->m_ParentNode->CheckForErrors();
		if (this->Relayout()){this->m_ParentNode->SendLayoutEvent(); this->m_ParentNode->SendRefreshEvent();}
		return(true);
	}
	else if (Node->GetParent())
	{
		Node->GetParent()->RemoveChild(Node);
		this->SetNode(Node);
		Node->Show(false);
		this->m_NodeOwned = true; //I need to delete the node
		return(false);
	}
	else
	{
		//No parent, nothing to do
		this->SetNode(Node);
		Node->Show(false);
		this->m_NodeOwned = true; //I need to delete the node
		return(false);
	}
}
bool PTC_Base::DoAddTree()
{
	PTreeCtrl *Ctrl = this->GetCtrl();
	PTreeNode *Node = this->GetNode();
	if (!Ctrl)
	{
		return(false);
	}
	wxWindowUpdateLocker lock(Ctrl);

	if (!Node)
	{
		Node = new PTreeNode(Ctrl,wxID_ANY);
		Node->SetLabel(this->m_LeafLabel);
		Node->SetRoot(true);
	}
	else {Node->Reparent(Ctrl); Node->Show(true);}
	if (this->m_NodeIndex < 0){Ctrl->m_Trees.Add(Node);}
	else{Ctrl->m_Trees.Insert(Node,this->m_NodeIndex);}
	this->m_NodeOwned = false;

	this->SetNode(Node);

	Ctrl->CheckForErrors();
	Ctrl->UpdateSelector();
	return(true);
}
bool PTC_Base::DoDeleteTree()
{
	PTreeCtrl *Ctrl = this->GetCtrl();
	PTreeNode *Node = this->GetNode();
	if (!Ctrl && Node){Ctrl = (PTreeCtrl*)Node->GetParent();}
	if (!Ctrl || !Node)
	{
		return(false);
	}
	wxWindowUpdateLocker lock(Ctrl);

	Ctrl->RemoveChild(Node);
	Node->Show(false);
	this->m_NodeOwned = true;
	this->SetNode(Node);
	this->SetCtrl(Ctrl);
	
	Ctrl->CheckForErrors();
	Ctrl->UpdateSelector();
	return(true);
}

