/*	Utils.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../include/Utils.h"

wxXmlNode *CreateXmlContentNode(wxString Name, wxString Content)
{
	wxXmlNode *Element = new wxXmlNode(wxXML_ELEMENT_NODE,Name);
	Element->AddChild(new wxXmlNode(wxXML_TEXT_NODE,Content,Content));
	return(Element);
}