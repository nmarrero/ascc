/*	PhylogeneticTree.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file PhylogeneticTree.cpp
 * @author Nico Marrero
 * @date September, 2012
 * @brief This file contains the data structures used for dynamic and predefined Oldest Known Records
 *
 */
#include "../include/PhylogeneticTree.h"
#include "../include/Taxa.h"
#include "../include/ASCCCommand.h"
#include "../include/Utils.h"

#include <wx/regex.h>
#include <wx/arrimpl.cpp>
#include <wx/artprov.h>
#include <wx/wupdlock.h>
#include <wx/debug.h>

#include <cmath>

#define TEXT_BORDER 3
#define NODE_CIRCLE_RADIUS 5

wxWindowID PTreeNode::sm_NextID = wxID_HIGHEST;
wxSize PTreeNode::sm_LargestTextExtent = wxSize(1,1);
wxCommandProcessor *PTreeCtrl::sm_CommandProcessor = NULL;

BEGIN_EVENT_TABLE(PTreeNode, wxControl)
	EVT_PAINT(PTreeNode::OnPaint)
	EVT_LEFT_DOWN(PTreeNode::OnMouseLeftDown)
	EVT_LEFT_UP(PTreeNode::OnMouseLeftUp)
	EVT_MOTION(PTreeNode::OnMouseMove)
	EVT_CONTEXT_MENU(PTreeNode::OnContextMenu)
	EVT_ENTER_WINDOW(PTreeNode::OnMouseEnter)
	EVT_LEAVE_WINDOW(PTreeNode::OnMouseLeave)
	EVT_MENU(wxID_HELP,PTreeNode::OnReportError)
	EVT_MENU(wxID_ADD,PTreeNode::OnAddLeaf)
	EVT_MENU(wxID_NEW,PTreeNode::OnAddChild)
	EVT_MENU(wxID_REPLACE,PTreeNode::OnRename)
	EVT_MENU(wxID_DELETE,PTreeNode::OnRemove)
	EVT_MENU(wxID_COPY,PTreeNode::OnCopy)
	EVT_MENU(wxID_CUT,PTreeNode::OnCut)
	EVT_MENU(wxID_PASTE,PTreeNode::OnPaste)
	EVT_MENU(MENUID_MERGE,PTreeNode::OnMergeWithParent)
	EVT_MENU(MENUID_INSERT,PTreeNode::OnInsertNodeAbove)
	EVT_MENU(wxID_DUPLICATE,PTreeNode::OnAddSisterTaxonToNode)
	EVT_MENU(MENUID_ADDSISTERTOSUBTREE,PTreeNode::OnAddSisterTaxaToSubtree)
	EVT_MENU(MENUID_LADDERIZE_LEFT,PTreeNode::OnLadderizeLeft)
	EVT_MENU(MENUID_LADDERIZE_RIGHT,PTreeNode::OnLadderizeRight)
	EVT_MENU(MENUID_ADDANCHOR,PTreeNode::OnAddAnchorTaxon)
	EVT_MENU(MENUID_SHIFT,PTreeNode::OnShiftBranches)
	EVT_MENU(MENUID_RESTRUCTURE,PTreeNode::OnRestructure)
END_EVENT_TABLE()

BEGIN_EVENT_TABLE(PTreeCtrl, wxScrolled<wxWindow>)
	EVT_SIZE(PTreeCtrl::OnSize)
	EVT_COMMAND(wxID_ANY,EVT_TREE_LAYOUT,PTreeCtrl::OnLayoutChildren)
	EVT_COMMAND(wxID_ANY,EVT_TREE_REFRESH,PTreeCtrl::OnRefresh)
	EVT_LEFT_DOWN(PTreeCtrl::OnMouseLeftDown)
	EVT_CONTEXT_MENU(PTreeCtrl::OnContextMenu)
	EVT_CHOICE(wxID_ANY,PTreeCtrl::OnTreeSelectorUpdate)
	EVT_MENU(wxID_NEW, PTreeCtrl::OnNewTree)
	EVT_MENU(wxID_PASTE, PTreeCtrl::OnPaste)
	EVT_MENU(MENUID_CHECKFORERRORS, PTreeCtrl::OnCheckForErrors)
	EVT_MENU(MENUID_LADDERIZE_LEFT,PTreeCtrl::OnLadderizeLeft)
	EVT_MENU(MENUID_LADDERIZE_RIGHT,PTreeCtrl::OnLadderizeRight)
	EVT_MENU(MENUID_TOIMAGE, PTreeCtrl::OnSaveDisplayAsImage)
END_EVENT_TABLE()

WX_DECLARE_OBJARRAY(SizeInfo,SizeInfoArray);
WX_DEFINE_OBJARRAY(SizeInfoArray);
struct NodeCount
{
	unsigned int Count;
	PTreeNode *Child;
};
WX_DECLARE_OBJARRAY(NodeCount,_NodeCountArray);
WX_DEFINE_OBJARRAY(_NodeCountArray);
class NodeCountArray : public _NodeCountArray
{
public:
	void AddNodeCount(unsigned int count, PTreeNode *child){NodeCount nc; nc.Count = count; nc.Child = child; this->Add(nc);}
};

DEFINE_EVENT_TYPE(EVT_TREE_LAYOUT);
DEFINE_EVENT_TYPE(EVT_TREE_SELECT_NODE);
DEFINE_EVENT_TYPE(EVT_TREE_ENTER_NODE);
DEFINE_EVENT_TYPE(EVT_TREE_LEAVE_NODE);
DEFINE_EVENT_TYPE(EVT_TREE_RESTRUCTURE);
DEFINE_EVENT_TYPE(EVT_TREE_REFRESH);

//PTreeNode
PTreeNode::PTreeNode():m_Selected(false),m_IsLeaf(false),m_IsRoot(false),m_MouseDown(false),m_Error(0),m_NodePosition(wxPoint(0,0)),
	m_MIGAge(0.0), m_MIGDisagreement(0.0), m_MIGDisagreementTotal(0.0), m_MinDivergenceAge(0.0), m_MinGhostLineage(0.0),
	m_MaxDivergenceAge(0.0),m_MaxGhostLineage(0.0)
{}
PTreeNode::PTreeNode(wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
	:m_Selected(false),m_IsLeaf(false),m_IsRoot(false),m_MouseDown(false),m_Error(0),m_NodePosition(wxPoint(0,0)),
	m_MIGAge(0.0), m_MIGDisagreement(0.0), m_MIGDisagreementTotal(0.0), m_MinDivergenceAge(0.0), m_MinGhostLineage(0.0),
	m_MaxDivergenceAge(0.0),m_MaxGhostLineage(0.0)
{ this->Create(parent, id, pos, size, style); }
bool PTreeNode::Create(wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
	if (id == wxID_ANY){id = this->sm_NextID++;}
	if (!wxControl::Create(parent, id, pos, size, style)){return(false);}
	wxControl::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	wxControl::SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	wxControl::SetInitialSize(size);
	wxControl::SetLabel(wxEmptyString);
	wxControl::SetDropTarget(new PTreeNodeDropTarget(this));
	return(true);
}
PTreeNode::~PTreeNode(){}

PTC_Base *PTreeNode::AddAnchorTaxon()
{
	if (this->m_Children.size() == 0){return(NULL);}
	PTC_Base *Result = NULL;
	wxString NewLabel = ::wxGetTextFromUser("Enter taxon name:","New Name",wxEmptyString);

	PTreeNode *Node = NULL;
	unsigned int NodeIndex;
	if ((this->m_Children.size() > 1 && this->m_Children[0]->IsLeaf() == this->m_Children[1]->IsLeaf()) || this->m_Children.size() == 1){Node = this->m_Children[0]; NodeIndex = 0;}
	else {Node = (this->m_Children[0]->IsLeaf()?this->m_Children[1]:this->m_Children[0]); NodeIndex = (this->m_Children[0]->IsLeaf()?1:0);}

	if (!NewLabel.IsEmpty())
	{
		Result = new PTC_AddNode(this,NodeIndex);
		Result->Link(PTC_AddNode(this,NodeIndex,NewLabel)); Result->Link(TC_Add(NewLabel));
		Result->Link(PTC_MoveNode(Node,this,NodeIndex,-1));
	}
	return(Result);
}
PTreeNode *PTreeNode::AddChild(int index)
{
	PTreeNode *newChild = new PTreeNode(this,this->sm_NextID++); 
	return(this->DoAddChild(newChild,index));
}
PTreeNode *PTreeNode::AddLeaf(wxString Label, int index)
{
	PTreeNode *newChild = new PTreeNode(this,this->sm_NextID++); 
	newChild->SetLabel(Label);
	newChild->m_IsLeaf = true;
	return(this->DoAddChild(newChild,index));
}
PTC_Base *PTreeNode::AddSisterTaxa()
{
	unsigned int NumChildren = this->m_Children.size();
	PTC_Base *ChildResult = NULL, *Result = NULL;
	if (NumChildren > 0)
	{
		for(unsigned int index = 0; index < NumChildren; ++index)
		{
			ChildResult = this->m_Children[index]->AddSisterTaxa();
			if (ChildResult && Result){Result->Link(ChildResult);}
			else if (ChildResult){Result = ChildResult;}
		}
	}
	else if (this->m_IsLeaf)
	{
		Result = this->AddSisterTaxon();
	}
	return(Result);
}
PTC_Base *PTreeNode::AddSisterTaxon()
{
	PTC_Base *Result = NULL;
	wxString NewLabel;
	if (!this->GetParent()->IsKindOf(CLASSINFO(PTreeNode))){return(NULL);}
	if (this->m_IsLeaf)
	{
		NewLabel = this->GetLabel();
		wxString Suffix = NewLabel.AfterLast('_');
		long count;
		if (Suffix.ToLong(&count))
		{
			NewLabel = NewLabel.BeforeLast('_') + wxString::Format("_%d",count+1);
		}
		else
		{
			NewLabel.Append("_1");
		}
	}
	else {NewLabel = ::wxGetTextFromUser("Enter taxon name:","New Name",wxEmptyString);}

	PTreeNode *Parent = (PTreeNode*)this->GetParent();
	unsigned int MyIndex = Parent->FindChild(this);
	if (!NewLabel.IsEmpty())
	{
		Result = new PTC_AddNode(Parent,MyIndex);
		Result->Link(PTC_MoveNode(this,Parent,MyIndex,-1));
		Result->Link(PTC_AddNode(Parent,MyIndex,NewLabel)); Result->Link(TC_Add(NewLabel));
	}
	return(Result);
}
double PTreeNode::CalculateMIG()
{
	if (this->Children() == 0)
	{
		wxString Label = this->GetLabel(); //Using this instead of GetLabelText() since there are no menu codes in the label and it takes longer to call
		Taxon *MyTaxon = Taxon::FindTaxonByName(Label);
		wxASSERT_MSG(MyTaxon != NULL, "PTreeNode::CalculateMIG - Taxon '" + Label + "' not found");
		this->m_MIGAge = MyTaxon->CurrentOKR();
		this->m_MIGDisagreement = this->m_MIGDisagreementTotal = 0.0;
		return(this->m_MIGDisagreement);
	}
	else
	{
		this->m_MIGAge = 0.0;
		this->m_MIGDisagreement = 0.0;
		double DisagreementTotal = 0.0;
		//Tell the kids to calculate their MIG and get me back the oldest of the kids' ages
		for(unsigned int index=0, size = this->Children(); index < size; ++index)
		{
			PTreeNode *Child = this->Child(index);
			DisagreementTotal += Child->CalculateMIG();
			if (Child->m_MIGAge > this->m_MIGAge){this->m_MIGAge = Child->m_MIGAge;}
		}
		//wxASSERT(DisagreementTotal >= 0.0);
		//wxASSERT(this->m_MIGAge > 0.0);
		//Using a for loop even though there should only be two children (for just in case)
		for(unsigned int index=1, size = this->Children(); index < size; ++index)
		{
			PTreeNode *Child0 = this->Child(index-1);
			PTreeNode *Child1 = this->Child(index);
			this->m_MIGDisagreement = abs(Child0->m_MIGAge - Child1->m_MIGAge);
			wxASSERT(this->m_MIGDisagreement >= 0.0000000);
		}
		this->m_MIGDisagreementTotal = DisagreementTotal + this->m_MIGDisagreement;
		return(this->m_MIGDisagreementTotal);
	}
}
unsigned int PTreeNode::CheckForErrors()
{
	unsigned int SubtreeError = this->m_Error = 0;
	unsigned int NumChildren = this->m_Children.size();
	if (this->IsLeaf())
	{
		if (NumChildren > 0){this->m_Error |= ERROR_LEAF_WITH_CHILDREN;}
		if (this->GetLabel().IsEmpty()){this->m_Error |= ERROR_LEAF_WITHOUT_LABEL;}
	}
	else
	{
		if (NumChildren > 2){this->m_Error |= ERROR_TOO_MANY_CHILDREN;}
		if (NumChildren < 2){this->m_Error |= ERROR_TOO_FEW_CHILDREN;}
	}

	SubtreeError = this->m_Error;
	for(unsigned int index = 0; index < NumChildren; ++index)
	{
		SubtreeError |= this->m_Children[index]->CheckForErrors();
	}
	return(SubtreeError);
}
PTreeNode *PTreeNode::Child(unsigned int index)
{
	if (index < this->m_Children.size()){return(this->m_Children[index]);} 
	return(NULL);
}
unsigned int PTreeNode::Children(){return(this->m_Children.size());}
PTC_Base *PTreeNode::DeleteNodes(wxString NodeLabelToDelete)
{
	if (this->GetLabel() == NodeLabelToDelete)
	{
		return(new PTC_DeleteNode(this));
	}
	else
	{
		PTC_Base *Result = NULL, *ChildResult = NULL;
		for(unsigned int index = 0; index < this->m_Children.size(); ++index)
		{
			if (this->m_Children[index]->GetLabel() == NodeLabelToDelete)
			{
				if (Result){Result->Link(new PTC_DeleteNode(this->m_Children[index]));}
				else {Result = new PTC_DeleteNode(this->m_Children[index]);}
			}
			else
			{
				ChildResult = this->m_Children[index]->DeleteNodes(NodeLabelToDelete);
				if (Result){Result->Link(ChildResult);}
				else {Result = ChildResult;}
			}
		}
		return(Result);
	}
}
PTreeNode *PTreeNode::DoAddChild(PTreeNode *newChild, int index)
{
	if (index < 0){this->m_Children.Add(newChild);}
	else {this->m_Children.Insert(newChild,(unsigned int)index);}
	this->CheckForErrors();

	return(newChild);
}
void PTreeNode::Draw(wxDC& dc, wxCoord ParentOriginX, wxCoord ParentOriginY)
{
	this->OnDraw(dc,ParentOriginX + this->GetPosition().x, ParentOriginY + this->GetPosition().y);
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		this->m_Children[index]->Draw(dc, ParentOriginX + this->GetPosition().x, ParentOriginY + this->GetPosition().y);
	}
}
int PTreeNode::FindChild(wxWindowBase *child)
{
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		if (this->m_Children[index] == child){return((int)index);}
	}
	return(-1);
}
double PTreeNode::FindOldestChildAge()
{
	double Max = -1.0;
	Taxon *CurrentTaxon = NULL;
	if (this->IsLeaf()){CurrentTaxon = Taxon::FindTaxonByName(this->GetLabel()); if (CurrentTaxon){Max = CurrentTaxon->MaxOKR();}}
	else
	{
		for(unsigned int index = 0, size = this->Children(); index < size; ++index)
		{
			CurrentTaxon = Taxon::FindTaxonByName(this->m_Children[index]->GetLabel());
			if (CurrentTaxon && CurrentTaxon->MaxOKR() > Max){Max = CurrentTaxon->MaxOKR();}
		}
	}
	return(Max);
}
void PTreeNode::RestructurePolytomies(bool Chronologically, bool undoable, bool relayout)
{
	unsigned int NumChildren = this->Children();
	for(unsigned int index = 0, size = NumChildren; index < size; ++index)
	{
		this->m_Children[index]->RestructurePolytomies(Chronologically,undoable,relayout);
	}
	if (NumChildren > 2)
	{
		double *OldestAge = new double[NumChildren]; 
		int *OldestIndex = new int[NumChildren];
		PTreeNode **Children = new PTreeNode*[NumChildren];

		for(unsigned int index = 0, size = NumChildren; index < size; ++index)
		{
			OldestAge[index] = this->m_Children[index]->FindOldestChildAge();
			OldestIndex[index] = index;
			Children[index] = this->m_Children[index];
		}

		//sort
		for(unsigned int index = 0, size = NumChildren; index < size; ++index)
		{
			for(unsigned int test = index+1; test < size; ++test)
			{
				if (!Chronologically && OldestAge[test] < OldestAge[index])
				{
					double tempage = OldestAge[test];
					int tempindex = OldestIndex[test];
					OldestAge[test] = OldestAge[index];
					OldestIndex[test] = OldestIndex[index];
					OldestAge[index] = tempage;
					OldestIndex[index] = tempindex;
				}
				else if (Chronologically && OldestAge[test] > OldestAge[index])
				{
					double tempage = OldestAge[test];
					int tempindex = OldestIndex[test];
					OldestAge[test] = OldestAge[index];
					OldestIndex[test] = OldestIndex[index];
					OldestAge[index] = tempage;
					OldestIndex[index] = tempindex;
				}
			}
		}

		PTreeNode *CurrentParent = this;
		for(unsigned int index = 1, size = NumChildren-1; index < size; ++index)
		{
			PTreeCtrl::SubmitCommand(
				ASCCCommandGroup(wxString::Format("Restructure Polytomies %d/%d",index,size))
				.Link(PTC_AddNode(CurrentParent,0).Relayout(false))
				.Link(PTC_MoveNode(Children[OldestIndex[index]],CurrentParent,0,0).Relayout(false)),
				undoable);
			CurrentParent = CurrentParent->Child(0);
		}
		PTreeCtrl::SubmitCommand(
				ASCCCommandGroup(wxString::Format("Restructure Polytomies %d/%d",NumChildren-1,NumChildren-1))
				.Link(PTC_MoveNode(Children[OldestIndex[NumChildren-1]],CurrentParent,0).Relayout(relayout)),
				undoable);
		delete [] OldestAge;
		delete [] OldestIndex;
		delete [] Children;
	}
}
void PTreeNode::GetLeaves(wxArrayString& Result)
{
	if (this->m_Leaves.size() > 0)
	{
		PTreeNodeHash::iterator it;
		for(it = this->m_Leaves.begin(); it != this->m_Leaves.end(); ++it)
		{
			Result.Add(it->first);
		}
		return;
	}
	/*wxMessageBox("Failed");
	//Slow recursive get if hash is not populated
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		this->m_Children[index]->GetLeaves(Result);
	}
	if (this->m_IsLeaf){Result.Add(this->GetLabel());}*/
}
SizeInfo PTreeNode::GetSizeInfo()
{
	SizeInfo MySizeInfo;
	MySizeInfo.TreeDepth = 1;
	if (this->m_IsLeaf)
	{
		MySizeInfo.MinSize = this->sm_LargestTextExtent;
		MySizeInfo.NumChildren = 0;
	}
	else
	{
		SizeInfo ChildSizeInfo;
		MySizeInfo.NumChildren = this->m_Children.size();
		if (this->m_Children.size() > 0)
		{
			for(unsigned int index = 0, size = MySizeInfo.NumChildren; index < size; ++index)
			{
				ChildSizeInfo = this->m_Children[index]->GetSizeInfo();
				MySizeInfo.SetWidth(max(MySizeInfo.GetWidth(), ChildSizeInfo.GetWidth()));
				MySizeInfo.SetHeight(MySizeInfo.GetHeight() + ChildSizeInfo.GetHeight());
				MySizeInfo.TreeDepth = max(MySizeInfo.TreeDepth, ChildSizeInfo.TreeDepth+1);
			}
		}
		else
		{
			MySizeInfo.SetWidth(this->sm_LargestTextExtent.GetWidth());
		}
		if (MySizeInfo.GetHeight() < this->sm_LargestTextExtent.GetHeight()*2)
		{
			MySizeInfo.SetHeight(this->sm_LargestTextExtent.GetHeight()*2);
		}
	}
	this->m_SizeInfo = MySizeInfo;
	return(MySizeInfo);
}
wxString PTreeNode::GetTreeData()
{
	wxString TreeData;
	if (this->m_IsLeaf)
	{
		Taxon *Data = Taxon::FindTaxonByName(this->GetLabelText());
		if (!Data){Data = new Taxon(); Data->Name(this->GetLabelText());}
		TreeData = wxString::Format("%d ",Data->ExportID());
	}
	else
	{
		TreeData = "(";
		for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
		{
			TreeData += this->m_Children[index]->GetTreeData();
		}
		TreeData += ")";
	}
	return(TreeData);
}
wxString PTreeNode::GetTreeNameData()
{
	wxString TreeData;
	if (this->m_IsLeaf)
	{
		Taxon *Data = Taxon::FindTaxonByName(this->GetLabelText());
		if (!Data){Data = new Taxon(); Data->Name(this->GetLabelText());}
		TreeData = Data->Name();
	}
	else
	{
		TreeData = "(";
		for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
		{
			TreeData += this->m_Children[index]->GetTreeNameData() + " ";
		}
		TreeData += ")";
	}
	return(TreeData);
}
void PTreeNode::InsertNodeAboveChild(PTreeNode *child)
{
	int ChildIndex = this->FindChild(child); if (ChildIndex == -1){return;}
	PTreeCtrl::SubmitCommand(PTC_Group("Insert Above").Link(PTC_AddNode(this,ChildIndex)).Link(PTC_MoveNode(child,this,ChildIndex,-1)));
}
void PTreeNode::ShiftBranches(ASCCCommand *Command)
{
	unsigned int NumChildren = this->m_Children.size();
	if (NumChildren > 1)
	{
		if (Command)
		{
			Command->Link(PTC_MoveNode(this->m_Children[0],this,NumChildren-1));
		}
	}
}
unsigned int PTreeNode::Ladderize(LadderizeType Type,ASCCCommand *Command)
{
	unsigned int NumChildren = this->m_Children.size();
	if (NumChildren == 0){return(1);}
	else
	{
		unsigned int TotalNodes = 1;	//start with one node (for me)
		NodeCountArray Nodes;
		for(unsigned int index = 0, size = NumChildren; index < size; ++index)
		{
			Nodes.AddNodeCount(this->m_Children[index]->Ladderize(Type,Command), this->m_Children[index]);
			TotalNodes += Nodes[index].Count;
		}
		//sort according to node count
		if (Command)
		{
			for(unsigned int index = 0, size = NumChildren; index < size; ++index)
			{
				for(unsigned int subindex = index + 1; subindex < size; ++subindex)
				{
					if ((Type == LADDERIZE_LEFT && Nodes[index].Count < Nodes[subindex].Count) ||
						(Type == LADDERIZE_RIGHT && Nodes[index].Count > Nodes[subindex].Count))
					{
						NodeCount Temp = Nodes[index]; 
						Nodes[index] = Nodes[subindex];
						Nodes[subindex] = Temp;
					}
				}
			}
			for(unsigned int index = 0, size = Nodes.size(); index < size; ++index)
			{
				Command->Link(PTC_MoveNode(Nodes[index].Child,this,index));
			}
		}
		return(TotalNodes);
	}
}
void PTreeNode::ComputeDivergences()
{
	PTreeNodeArray& Array = this->m_Children;
	unsigned int NumChildren = Array.size();
	if (NumChildren == 0)
	{
		//Find the divergence age of the taxon associated with this leaf node (if we can)
		Taxon *ThisTaxon = Taxon::FindTaxonByName(this->GetLabelText());
		if (ThisTaxon)
		{
			this->m_MinDivergenceAge = ThisTaxon->MinOKR();
			this->m_MaxDivergenceAge = ThisTaxon->MaxOKR();
		}
		return;
	}
	
	//Clear some vars that will be populated with info from children
	this->m_MinDivergenceAge = this->m_MaxDivergenceAge = 0.0;
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		Array[index]->ComputeDivergences();

		this->m_MinDivergenceAge = max(this->m_MinDivergenceAge, Array[index]->MinDivergenceAge());
		this->m_MaxDivergenceAge = max(this->m_MaxDivergenceAge, Array[index]->MaxDivergenceAge());
	}
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		Array[index]->m_MinGhostLineage = this->m_MinDivergenceAge - Array[index]->MinDivergenceAge();
		Array[index]->m_MaxGhostLineage = this->m_MaxDivergenceAge - Array[index]->MaxDivergenceAge();
	}
}
void PTreeNode::DistributeGhostLineages()
{
	//This calculates and distributes the ghost lineages for a node and its children
	PTreeNodeArray& Array = this->m_Children;
	if (Array.size() == 0){return;}
	int oldest_leaf_index = -1, oldest_node_index = -1;
	double oldest_leaf_min_divergence_age = -10000.00, oldest_node_min_divergence_age = -10000.00;
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		if (Array[index]->IsLeaf())
		{
			if (Array[index]->m_MinDivergenceAge > oldest_leaf_min_divergence_age)
			{
				oldest_leaf_min_divergence_age = Array[index]->m_MinDivergenceAge;
				oldest_leaf_index = index;
			}
		}
		else
		{
			if (Array[index]->m_MinDivergenceAge > oldest_node_min_divergence_age)
			{
				oldest_node_min_divergence_age = Array[index]->m_MinDivergenceAge;
				oldest_node_index = index;
			}
		}
	}
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		Array[index]->m_CalculatedMinGhostLineage = Array[index]->m_MinGhostLineage; 
		Array[index]->m_CalculatedMaxGhostLineage = Array[index]->m_MaxGhostLineage;
	
		if (oldest_leaf_index == index || (oldest_leaf_index == -1 && oldest_node_index == index))
		{
			if (this->m_CalculatedMinGhostLineage > 0.00){Array[index]->m_CalculatedMinGhostLineage += this->m_CalculatedMinGhostLineage;}
			if (this->m_CalculatedMaxGhostLineage > 0.00){Array[index]->m_CalculatedMaxGhostLineage += this->m_CalculatedMaxGhostLineage;}
		}
		Array[index]->DistributeGhostLineages();
	}
}
void PTreeNode::LayoutChildren()
{
	PTreeNodeArray& Array = this->m_Children;
	unsigned int NumChildren = Array.size();
	this->m_Leaves.clear();
	
	wxString Tooltip = wxString::Format("FAD: \n\tmin - %f\n\tmax - %f\n",this->m_MinDivergenceAge,this->m_MaxDivergenceAge);
	Tooltip += wxString::Format("Ghost Lineage: \n\tmin - %f\n\tmax - %f\n",this->m_MinGhostLineage,this->m_MaxGhostLineage);
	Tooltip += wxString::Format("Distributed Ghost Lineage: \n\tmin - %f\n\tmax - %f\n",this->m_CalculatedMinGhostLineage,this->m_CalculatedMaxGhostLineage);

	if (NumChildren == 0)
	{
		this->m_NodePosition = wxPoint(0,this->GetSize().GetHeight()/2);
		//Update our hash to include just ourselves
		this->m_Leaves[this->GetLabel()] = this;
		Tooltip += wxString::Format("\nFinal FAD:\n\tmin - %f\n\tmax - %f\n",this->m_MinDivergenceAge+this->m_CalculatedMinGhostLineage, this->m_MaxDivergenceAge+this->m_CalculatedMaxGhostLineage);
		this->SetToolTip(Tooltip);
		return;
	}
	this->SetToolTip(Tooltip);
	wxSize MySize = this->GetSize();
	wxSize ChildSize = this->GetSize(); ChildSize.SetHeight(ChildSize.GetHeight()/NumChildren);
	wxSize ChildMinSize(0,0);
	int MaxDepth = 1;
	
	SizeInfoArray SizeArray;
	for(unsigned int index = 0; index < NumChildren; ++index)
	{
		SizeArray.Add(Array[index]->GetSizeInfo());
		ChildMinSize.SetHeight(ChildMinSize.GetHeight() + SizeArray.Last().MinSize.GetHeight());
		MaxDepth = max(MaxDepth,SizeArray.Last().TreeDepth);
	}
	
	wxCoord CurrentHeight = 0, CurrentWidth = 0, TotalHeight = 0, ChildPos = 0;
	if(!this->GetLabel().IsEmpty()){wxClientDC dc(this); CurrentHeight = dc.GetTextExtent(this->GetLabel()).GetHeight();}
	wxPoint Heights(this->GetSize().GetHeight(),0);
	
	if (!this->IsRoot()){ChildPos = (MySize.GetWidth()-this->sm_LargestTextExtent.GetWidth())/(MaxDepth+1);}
	else {ChildPos = 5;}
	
	//Clear some vars that will be populated with info from children
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		TotalHeight += CurrentHeight;
		CurrentHeight = SizeArray[index].GetHeight();
		Array[index]->SetPosition(wxPoint(ChildPos,TotalHeight));
		Array[index]->SetSize(max(MySize.GetWidth()-Array[index]->GetPosition().x,SizeArray[index].GetWidth()), CurrentHeight);
		Array[index]->LayoutChildren();

		//The following all has to be executed after calling LayoutChildren on our children
		Heights.x = min(Heights.x,Array[index]->NodePosition().y+Array[index]->GetPosition().y);
		Heights.y = max(Heights.y,Array[index]->NodePosition().y+Array[index]->GetPosition().y);
	
		this->m_Leaves.Update(Array[index]->m_Leaves);
	}
	
	this->m_NodePosition = wxPoint(0,(Heights.x+Heights.y)/2);
}
wxDataInputStream& PTreeNode::Load(wxDataInputStream& strm)
{
	wxInt32 height, width, depth;
	wxUint32 children;
	wxInt16 leaf, root;
	wxString label;

	strm >> height >> width >> depth;
	strm >> leaf >> root >> label >> children;
	
	this->m_SizeInfo.SetHeight(height); this->m_SizeInfo.SetWidth(width); this->m_SizeInfo.TreeDepth = depth; this->m_SizeInfo.NumChildren = children;
	this->m_IsLeaf = (leaf==1); this->m_IsRoot = (root==1);
	this->SetLabel(label);
	label = this->GetLabel();
	for(unsigned int index = 0; index < children; ++index)
	{
		this->AddChild()->Load(strm);
	}
	return(strm);
}
PTreeNode *PTreeNode::FindLeaf(wxString LabelToFind)
{
	//Check our leaf hash
	PTreeNodeHash::iterator Result = this->m_Leaves.find(LabelToFind);
	if (Result != this->m_Leaves.end()){return(Result->second);}
	/*PTreeNodeHash::iterator begin = this->m_Leaves.begin(), end = this->m_Leaves.end(), it;
	wxArrayString Leaves;
	wxString LeavesInHash;
	for (it = begin; it != end; it++)
	{
		Leaves.Add(it->first);
	}
	Leaves.Sort();
	for(unsigned int index = 0, size = Leaves.size(); index < size; ++index)
	{
		LeavesInHash += "\n\t" + Leaves[index];
	}
	wxMessageBox(wxString::Format("Failed to find %s in:\n\n%s",LabelToFind,LeavesInHash));
	//If for some reason it is not in there, resort to a slow recursive search
	if (this->GetLabel() == LabelToFind && this->m_Children.size() == 0){return(this);}
	else
	{
		PTreeNode *Result = NULL;
		for(unsigned int index = 0, children = this->m_Children.size(); index < children; index++)
		{
			Result = this->m_Children[index]->FindLeaf(LabelToFind);
			if (Result){return(Result);}
		}
	}*/
	return(NULL);
}
void PTreeNode::UpdateLeafHash()
{
	//This is also performed during LayoutChildren
	this->m_Leaves.clear();
	if (this->m_Children.size() == 0)
	{
		//Update our hash to include just ourselves
		this->m_Leaves[this->GetLabelText()] = this;
		return;
	}
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		this->m_Children[index]->UpdateLeafHash();
		this->m_Leaves.Update(this->m_Children[index]->m_Leaves);
	}
}
//wxTextInputStream& PTreeNode::Load(wxTextInputStream& strm)
//{	
//	wxString line;
//	unsigned long height, width, depth, children;
//	bool leaf = false, root = false;
//	wxString label, type;
//
//	strm >> line; label = line.AfterFirst('=').Trim(true).Trim(false);
//	strm >> line; line.AfterFirst('=').ToULong(&height);
//	strm >> line; line.AfterFirst('=').ToULong(&width);
//	strm >> line; line.AfterFirst('=').ToULong(&depth);
//	strm >> line; type = line.AfterFirst('=').Trim(true).Trim(false);
//	if (type == "Leaf"){leaf = true;}
//	else if (type == "Root"){root = true;}
//	strm >> line; line.AfterFirst('=').ToULong(&children);
//	
//	this->m_SizeInfo.SetHeight(height); this->m_SizeInfo.SetWidth(width); this->m_SizeInfo.TreeDepth = depth; this->m_SizeInfo.NumChildren = children;
//	this->m_IsLeaf = leaf; this->m_IsRoot = root;
//	this->SetLabel(label);
//
//	for(unsigned int index = 0; index < children; ++index)
//	{
//		this->AddChild()->Load(strm);
//	}
//	return(strm);
//}
PTC_Base *PTreeNode::MergeWithChild(PTreeNode *child)
{
	int ChildIndex = this->FindChild(child); if (ChildIndex == -1){return(NULL);}

	PTC_Base *Command = NULL;
	if (child->Children() > 0)
	{
		Command = new PTC_MoveNode(child->Child(0),this,ChildIndex++);
		for(unsigned int index = 1, size = child->Children(); index < size; ++index)
		{
			Command->Link(PTC_MoveNode(child->Child(index),this,ChildIndex++));
		}
		Command->Link(PTC_DeleteNode(child));
	}
	else{Command = new PTC_DeleteNode(child);}

	return(Command);
}
double PTreeNode::MinDivergenceAge()
{
	return(this->m_MinDivergenceAge);
}
double PTreeNode::MaxDivergenceAge()
{
	return(this->m_MaxDivergenceAge);
}
double PTreeNode::MinGhostLineage()
{
	return(this->m_MinGhostLineage);
}
double PTreeNode::MaxGhostLineage()
{
	return(this->m_MaxGhostLineage);
}
double PTreeNode::CalculatedMinGhostLineage()
{
	return(this->m_CalculatedMinGhostLineage);
}
double PTreeNode::CalculatedMaxGhostLineage()
{
	return(this->m_CalculatedMaxGhostLineage);
}
void PTreeNode::OnAddAnchorTaxon(wxCommandEvent& event)
{
	PTC_Base *Result = this->AddAnchorTaxon(); 
	if (Result)
	{
		PTreeCtrl::SubmitCommand(PTC_Group("Add Anchor Taxon").Link(Result));
	}
}
void PTreeNode::OnAddChild(wxCommandEvent& event)
{
	PTreeCtrl::SubmitCommand(PTC_AddNode(this));
}
void PTreeNode::OnAddLeaf(wxCommandEvent& event)
{
	wxString Label = ::wxGetTextFromUser("Enter new name:","New Name",wxEmptyString); 
	if (!Label.IsEmpty())
	{
		PTreeCtrl::SubmitCommand(PTC_AddNode(this,Label).Link(TC_Add(Label)));
	}
}
void PTreeNode::OnAddSisterTaxaToSubtree(wxCommandEvent& event)
{
	PTC_Base *Result = this->AddSisterTaxa();
	if (Result)
	{
		PTreeCtrl::SubmitCommand(PTC_Group("Add Sister Taxa (Subtree)").Link(Result));
	}
}
void PTreeNode::OnAddSisterTaxonToNode(wxCommandEvent& event)
{
	PTC_Base *Result = this->AddSisterTaxon(); 
	if (Result)
	{
		PTreeCtrl::SubmitCommand(PTC_Group("Add Sister Taxon").Link(Result));
	}
}
void PTreeNode::OnBeginDrag()
{
	PTreeNodeData Data(this);

	wxDropSource dragSource(this);
	dragSource.SetData(Data);
	dragSource.SetCursor(wxDragNone,wxCursor(wxCURSOR_NO_ENTRY)); 
	wxDragResult Result = dragSource.DoDragDrop(wxDrag_AllowMove);
	//Do nothing with the result since I am being moved
}
void PTreeNode::OnContextMenu(wxContextMenuEvent& event)
{
	wxMenu *ContextMenu = new wxMenu();
	wxMenuItem *MenuItem = NULL;
		if (this->HasError())
		{
			MenuItem = new wxMenuItem(ContextMenu,wxID_HELP,"Report Error", "Describes the error currently affecting this item"); 
				MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_HELP)); ContextMenu->Append(MenuItem);
			ContextMenu->AppendSeparator();
		}
		if (!this->IsLeaf())
		{
			if (!this->IsRoot() || this->m_Children.size() == 0)
			{
				ContextMenu->Append(wxID_ADD, "Add Taxon", "Adds a taxon as a child to this item");
				ContextMenu->Append(wxID_NEW, "Add Node", "Adds a node as a child to this item");
			}
			wxMenu *SisterMenu = new wxMenu();
				SisterMenu->Append(wxID_DUPLICATE,"To Node","Adds a sister taxon to this node");
				SisterMenu->Append(MENUID_ADDSISTERTOSUBTREE,"To Subtree","Adds sister taxa to each leaf in the subtree");
				
			ContextMenu->AppendSubMenu(SisterMenu,"Add Sister Taxon");
			ContextMenu->Append(MENUID_ADDANCHOR,"Add Anchor Taxon","Adds an anchor taxon to this node");
			ContextMenu->AppendSeparator();
			if (!this->IsRoot() && !((PTreeNode*)this->GetParent())->IsRoot()){ContextMenu->Append(MENUID_MERGE, "Merge with Parent", "Merges this node with the parent node");}
		}
		else
		{
			ContextMenu->Append(wxID_DUPLICATE,"Add Sister Taxon", "Adds a sister taxon to this taxon");
		}
		if (!this->IsRoot())
		{
			MenuItem = new wxMenuItem(ContextMenu, MENUID_INSERT, "Insert Node Above", "Inserts a new node as the parent of this one");
				MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_GO_TO_PARENT)); ContextMenu->Append(MenuItem);
			ContextMenu->AppendSeparator();
		}
		ContextMenu->Append(MENUID_RESTRUCTURE,"Restructure Polytomies","Restructures any polytomies found in this node or below");
		ContextMenu->Append(MENUID_LADDERIZE_RIGHT,"Ladderize");
		if (this->m_Children.size() > 1){ContextMenu->Append(MENUID_SHIFT,"Shift Branches","Shifts and wraps the children of this node up by one");}
		ContextMenu->AppendSeparator();
		if (wxTheClipboard->IsOpened() || wxTheClipboard->Open())
		{
			MenuItem = new wxMenuItem(ContextMenu, wxID_COPY, "Copy","Copies the subtree starting with this node to the clipboard");
				MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_COPY)); ContextMenu->Append(MenuItem);
			MenuItem = new wxMenuItem(ContextMenu, wxID_CUT, "Cut","Copies the subtree starting with this node to the clipboard, deletes the original upon pasting");
				MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_CUT)); ContextMenu->Append(MenuItem);
		
			MenuItem = new wxMenuItem(ContextMenu, wxID_PASTE, "Paste","Pastes any subtree copied to the clipboard as a child of this node");
				MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_PASTE)); 
				ContextMenu->Append(MenuItem);
				if (!wxTheClipboard->IsSupported(wxDataFormat("PTreeNode"))){MenuItem->Enable(false);}
			ContextMenu->AppendSeparator();
			wxTheClipboard->Close();
		}
		if (!this->GetLabel().IsEmpty()){ContextMenu->Append(wxID_REPLACE,"Rename", "Change the name of this node");}
		MenuItem = new wxMenuItem(ContextMenu, wxID_DELETE,"Delete","Delete this item and all children");
				MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_DELETE)); ContextMenu->Append(MenuItem);
		ContextMenu->AppendSeparator();
		ContextMenu->Append(MENUID_TOIMAGE,"Save As Image", "Saves the current tree display as an image");
		this->PopupMenu(ContextMenu);
		wxDELETE(ContextMenu);
}
void PTreeNode::OnDraw(wxDC& dc, wxCoord OriginX, wxCoord OriginY)
{
	wxCoord Radius = NODE_CIRCLE_RADIUS;
	wxPoint MyPos = this->m_NodePosition;
	unsigned int NumChildren = this->m_Children.size();
	dc.SetBrush(*wxBLUE_BRUSH);
	if (this->HasError()){dc.SetBrush(*wxRED_BRUSH);}
	if (this->m_IsRoot)
	{
		dc.DrawText(this->GetLabel(),wxPoint(TEXT_BORDER+OriginX,OriginY)); return;
	}
	wxString Label = this->GetLabel();
	if (this->m_IsLeaf) //leaf - no kids
	{
		if (!Label.IsEmpty())
		{
			wxSize MySize = this->GetSize(); 
			int TextStart = MySize.GetWidth() - this->sm_LargestTextExtent.GetWidth();
			dc.DrawText(Label,wxPoint(TEXT_BORDER+OriginX+TextStart,OriginY));
			if (this->m_Selected)
			{
				if (!this->HasError()){dc.SetPen(*wxGREEN_PEN);}
				else {dc.SetPen(*wxRED_PEN);}
				dc.SetBrush(*wxTRANSPARENT_BRUSH);
				dc.DrawRectangle(wxPoint(TextStart+OriginX,OriginY),this->sm_LargestTextExtent);
				dc.DrawLine(OriginX,MySize.GetHeight()/2+OriginY,TextStart+OriginX,MySize.GetHeight()/2+OriginY);
				dc.SetBrush(wxNullBrush);
				dc.SetPen(wxNullPen);
			}
			else
			{
				dc.DrawLine(OriginX,MySize.GetHeight()/2+OriginY,TextStart+OriginX,MySize.GetHeight()/2+OriginY);
			}
		}
	}
	else //node - has children
	{
		//Draw lines to children
		wxPoint ChildPos = wxPoint(this->GetSize().GetWidth() - 2*Radius,0);
		MyPos.x += Radius;
		for(unsigned int index = 0; index < NumChildren; ++index)
		{
			ChildPos = this->m_Children[index]->GetPosition() + this->m_Children[index]->NodePosition(); //ChildPos.y += this->m_Children[index]->GetSize().GetHeight()/2;
			dc.DrawLine(ChildPos.x-2*Radius+OriginX,MyPos.y+OriginY,ChildPos.x-2*Radius+OriginX,ChildPos.y+OriginY);
			if (this->m_Children[index]->IsSelected()){dc.SetPen(*wxGREEN_PEN);}
			dc.DrawLine(ChildPos.x-2*Radius+OriginX,ChildPos.y+OriginY,ChildPos.x+Radius+OriginX,ChildPos.y+OriginY);
			dc.SetPen(*wxBLACK_PEN);
		}
		
		if (this->m_Selected)
		{
			dc.SetBrush(*wxGREEN_BRUSH);
			dc.SetPen(*wxGREEN_PEN);
		}
		dc.DrawLine(ChildPos.x-2*Radius+OriginX,MyPos.y+OriginY,-Radius+OriginX,MyPos.y+OriginY);
		if (this->HasError()){dc.SetPen(*wxRED_PEN);}
		dc.DrawCircle(wxPoint(ChildPos.x-2*Radius+OriginX,MyPos.y+OriginY),Radius);
		if (!Label.IsEmpty())
		{
			dc.DrawText(Label,OriginX,OriginY);
		}
	}
	
	dc.SetPen(*wxBLACK_PEN);
	dc.SetBrush(wxNullBrush);
}
void PTreeNode::OnInsertNodeAbove(wxCommandEvent& event)
{
	((PTreeNode *)this->GetParent())->InsertNodeAboveChild(this);
}
void PTreeNode::OnLadderizeLeft(wxCommandEvent& event)
{
	PTC_Group Command("Ladderize Left");
	this->Ladderize(LADDERIZE_LEFT,&Command);
	if (Command.Link())
	{
		PTreeCtrl::SubmitCommand(Command);
	}
}
void PTreeNode::OnRestructure(wxCommandEvent& event)
{
	wxCommandEvent evt(EVT_TREE_RESTRUCTURE);
	evt.SetClientData(this);
	this->AddPendingEvent(evt);
}
void PTreeNode::OnShiftBranches(wxCommandEvent& event)
{
	PTC_Group Command("Branch Shift");
	this->ShiftBranches(&Command);
	if (Command.Link())
	{
		PTreeCtrl::SubmitCommand(Command);
	}
}
void PTreeNode::OnCopy(wxCommandEvent& event)
{
	if (wxTheClipboard->IsOpened() || wxTheClipboard->Open())
	{
		wxTheClipboard->SetData(new PTreeNodeData(this,false));
		wxTheClipboard->Close();
	}
}
void PTreeNode::OnCut(wxCommandEvent& event)
{
	if (wxTheClipboard->IsOpened() || wxTheClipboard->Open())
	{
		wxTheClipboard->SetData(new PTreeNodeData(this,true));
		wxTheClipboard->Close();
	}
}
void PTreeNode::OnPaste(wxCommandEvent& event)
{
	if (wxTheClipboard->IsOpened() || wxTheClipboard->Open())
	{
		if (wxTheClipboard->IsSupported( wxDataFormat("PTreeNode") ))
		{
			PTreeNodeData data;
			if (wxTheClipboard->GetData(data))
			{
				if (this->DoPaste(&data,"Paste") && data.IsCut())
				{
					//Replace cut data with copy data for multiple pastes
					wxTheClipboard->SetData(new PTreeNodeData(data.GetNode(),false));
				}
			}
		}  
		wxTheClipboard->Close();
	}
}
bool PTreeNode::DoPaste(PTreeNodeData *data, wxString CommandTitle)
{
	PTreeNode *Node = data->GetNode();
	PTreeNode *Owner = this;
	PTreeNode *Parent = (PTreeNode*)this->GetParent();
	unsigned int OwnerIndex = Parent->FindChild(Owner);
	if (!Node || Owner == Node || OwnerIndex == -1){return(false);}

	//Make sure the new owner is not already a child of the child being moved
	PTreeNode *CheckNode = Owner;
	while(CheckNode && CheckNode->GetParent()->IsKindOf(CLASSINFO(PTreeNode)))
	{
		CheckNode = (PTreeNode *)CheckNode->GetParent();
		if (CheckNode == Node){return(false);}
	}

	//Make sure we are not pasting a node onto its own parent
	if (Owner == Node->GetParent()){return(false);}

	//Base command group
	PTC_Group Command(CommandTitle); 
	
	//do the actual paste
	if (data->IsCut())
	{
		Command.Link(PTC_AddNode(Parent,OwnerIndex)).Link(PTC_MoveNode(Owner,Parent,OwnerIndex,wxID_ANY)).Link(PTC_MoveNode(Node,Parent,OwnerIndex,wxID_ANY));
	}
	else
	{
		Command.Link(PTC_AddNode(Parent,OwnerIndex)).Link(PTC_MoveNode(Owner,Parent,OwnerIndex,wxID_ANY)).Link(PTC_AddNode(Parent,OwnerIndex,wxID_ANY,new PTreeNode(Node,Owner)));
	}

	//merge parent if it will result in a node with only one child
	if (data->IsCut() && Node->GetParent()->GetParent()->IsKindOf(CLASSINFO(PTreeNode))) //Check if grandparent is phylo node
	{
		PTC_Group Merge("Merge");
		PTreeNode *NodeParent = (PTreeNode *)Node->GetParent();
		PTreeNode *NodeGrandParent = (PTreeNode *)NodeParent->GetParent();
		if (NodeParent->Children() == 2)
		{
			unsigned int NodeIndex = NodeGrandParent->FindChild(NodeParent);
			for(unsigned int index = 0, size = NodeParent->Children(); index < size; ++index)
			{
				if (NodeParent->Child(index) != Node){Merge.Link(PTC_MoveNode(NodeParent->Child(index),NodeGrandParent,NodeIndex++));}
			}
			Merge.Link(PTC_DeleteNode(NodeParent));
		}
		PTreeCtrl::SubmitCommand(Command.Link(Merge));
	}
	else
	{
		PTreeCtrl::SubmitCommand(Command);
	}
	return(true);
}
void PTreeNode::OnLadderizeRight(wxCommandEvent& event)
{
	PTC_Group Command("Ladderize Right");
	this->Ladderize(LADDERIZE_RIGHT,&Command);
	if (Command.Link())
	{
		PTreeCtrl::SubmitCommand(Command);
	}
}
void PTreeNode::OnMergeWithParent(wxCommandEvent& event)
{
	ASCCCommand *Result = ((PTreeNode *)this->GetParent())->MergeWithChild(this);
	if (Result)
	{
		PTreeCtrl::SubmitCommand(PTC_Group("Merge Node").Link(Result));
	}
}
void PTreeNode::OnMouseEnter(wxMouseEvent& event)
{
	this->Selected(true);
	wxString Label = (this->GetLabel().IsEmpty()?wxString::Format("Node %d",this->GetId()):this->GetLabel());
	wxCommandEvent evt(EVT_TREE_ENTER_NODE);
	evt.SetId(this->GetId());
	evt.SetString(Label);
	this->AddPendingEvent(evt);
}
void PTreeNode::OnMouseMove(wxMouseEvent& event)
{
	if (event.Dragging()){this->OnBeginDrag();}
}
void PTreeNode::OnMouseLeave(wxMouseEvent& event)
{
		this->Selected(false);

		wxString Label = (this->GetLabel().IsEmpty()?wxString::Format("Node %d",this->GetId()):this->GetLabel());
		wxCommandEvent evt(EVT_TREE_LEAVE_NODE);
		evt.SetId(this->GetId());
		evt.SetString(Label);
		this->AddPendingEvent(evt);
}
void PTreeNode::OnMouseLeftDown(wxMouseEvent& event)
{
	this->m_MouseDown = true;
	//Send a taxon selection event if we clicked on a taxon
	if (this->Children() == 0)
	{
		int MouseX = event.GetPosition().x, MouseY = event.GetPosition().y;
		int TextStartX = this->GetSize().GetWidth() - PTreeNode::sm_LargestTextExtent.GetWidth();
		int TextEndY = PTreeNode::sm_LargestTextExtent.GetHeight();
		if (MouseX >= TextStartX && MouseY <= TextEndY)
		{
			wxString Label = (this->GetLabel().IsEmpty()?wxString::Format("Node %d",this->GetId()):this->GetLabel());
			wxCommandEvent evt(EVT_TREE_SELECT_NODE);
			evt.SetId(this->GetId());
			evt.SetString(Label);
			this->AddPendingEvent(evt);
		}
	}
	this->SetFocus();
	//event.Skip();
}
void PTreeNode::OnMouseLeftUp(wxMouseEvent& event)
{
	if (!this->m_MouseDown){return;}
	this->m_MouseDown = false;

	//event.Skip();
}
void PTreeNode::OnPaint(wxPaintEvent& event)
{
	wxAutoBufferedPaintDC dc(this);
	dc.SetBackground(wxBrush(this->GetBackgroundColour()));
	dc.SetFont(wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT));
	dc.Clear();
	this->OnDraw(dc);
}
void PTreeNode::OnRemove(wxCommandEvent& event)
{
	this->SendLayoutEvent(); 
	if (this->IsRoot()){PTreeCtrl::SubmitCommand(PTC_DeleteTree(this,((PTreeCtrl*)this->GetParent())->FindTree(this)));}
	else{PTreeCtrl::SubmitCommand(PTC_DeleteNode(this));}
}
void PTreeNode::OnRename(wxCommandEvent& event)
{
	wxString Label = ::wxGetTextFromUser("Enter new name:","New Name",this->GetLabel()); 
	if (!Label.IsEmpty())
	{
		if (this->IsRoot())
		{
			PTreeCtrl::SubmitCommand(PTC_RenameNode(this,Label));
		}
		else
		{
			PTreeCtrl::SubmitCommand(PTC_RenameNode(this,Label).Link(TC_Rename(this->GetLabel(),Label)));
		}
		this->SendLayoutEvent();
	}
}	
void PTreeNode::OnReportError(wxCommandEvent& event)
{
	wxString Error = this->ReportError();
	if (!Error.IsEmpty()){wxMessageBox(Error);}
}
wxString PTreeNode::Print(int indent)
{
	wxString Result; Result.Pad(indent,'-',false);
	if (this->GetLabel().IsEmpty()){Result += wxString::Format("Node%d",this->GetId()) + "\n";}
	else {Result += "LABEL: " + this->GetLabel() + "\n";}
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		Result += this->m_Children[index]->Print(indent+1);
	}
	return(Result);
}
void PTreeNode::RemoveChild(wxWindowBase *child)
{
	int index = this->FindChild(child); 
	if (index != -1)
	{
		this->m_Children.RemoveAt((unsigned int)index);
	}
	wxWindowBase::RemoveChild(child);
	this->CheckForErrors();
}
wxString PTreeNode::ReportError()
{
	wxString ReturnString;
	this->CheckForErrors();
	wxString Label = (this->GetLabel().IsEmpty()?wxString::Format("Node %d",this->GetId()):this->GetLabel());
	unsigned int NumChildren = this->m_Children.size();
	
	if (this->m_Error & ERROR_LEAF_WITH_CHILDREN){ReturnString += wxString::Format("Node '%s' is a leaf, but has %d children\n",Label,NumChildren);} 
	if (this->m_Error & ERROR_LEAF_WITHOUT_LABEL){ReturnString += wxString::Format("Node '%s' is a leaf, but has an empty label\n",Label);} 
	if (this->m_Error & ERROR_TOO_MANY_CHILDREN){ReturnString += wxString::Format("Node '%s' has more than 2 children\n",Label);} 
	if (this->m_Error & ERROR_TOO_FEW_CHILDREN){ReturnString += wxString::Format("Node '%s' has less than 2 children\n",Label);} 

	return(ReturnString);
}
//wxDataOutputStream& PTreeNode::Save(wxDataOutputStream& strm)
//{
//	strm << (wxInt32)this->m_SizeInfo.GetHeight() << (wxInt32)this->m_SizeInfo.GetWidth() << (wxInt32)this->m_SizeInfo.TreeDepth;
//	strm << (wxInt16)this->m_IsLeaf << (wxInt16)this->m_IsRoot << this->GetLabel() << (wxUint32)this->m_Children.size();
//	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
//	{
//		this->m_Children[index]->Save(strm);
//	}
//	return(strm);
//}
bool PTreeNode::Load(wxXmlNode *CurrentNode)
{
	if (!CurrentNode || CurrentNode->GetName() != "TreeNode"){return(false);}
	bool Result = true;
	bool HasLabel = false, HasHeight = false, HasWidth = false, HasChildren = false;
	for(wxXmlNode *Current = CurrentNode->GetChildren(); Current != NULL; Current = Current->GetNext())
	{
		if (Current->GetName() == "Label"){this->SetLabel(Current->GetNodeContent()); HasLabel = true;}
		if (Current->GetName() == "TreeNode")
		{
			Result &= this->AddChild()->Load(Current);
			HasChildren = true;
		}
	}
	if (!HasChildren){this->m_IsLeaf = true;}
	if (!HasHeight || !HasWidth){Result = false;}
	if (!HasLabel){Result = false;}
	return(Result);
}
wxXmlNode* PTreeNode::Save()
{
	wxXmlNode *TreeNode = new wxXmlNode(wxXML_ELEMENT_NODE,"TreeNode");
	if (!this->GetLabel().IsEmpty()){TreeNode->AddChild(CreateXmlContentNode("Label",this->GetLabel()));}
	
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		TreeNode->AddChild(this->m_Children[index]->Save());
	}
	return(TreeNode);
}
wxTextOutputStream& PTreeNode::Save(wxTextOutputStream& strm, unsigned int indent)
{
	wxString data;

	data.Append('\t',indent); data << "Label = " << this->GetLabel() << "\n";
	data.Append('\t',indent); data << "Height = " << this->m_SizeInfo.GetHeight() << "\n";
	data.Append('\t',indent); data << "Width = " << this->m_SizeInfo.GetWidth() << "\n"; 
	data.Append('\t',indent); data << "Depth = " << this->m_SizeInfo.TreeDepth << "\n";
	if (this->m_IsLeaf){data.Append('\t',indent); data << "Type = Leaf\n";}
	else if (this->m_IsRoot){data.Append('\t',indent); data << "Type = Root\n";}
	else {data.Append('\t',indent); data << "Type = Internal\n";}
	data.Append('\t',indent); data << "ChildrenCount = " << this->m_Children.size() << "\n";
	strm << data;

	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		this->m_Children[index]->Save(strm,indent+1);
	}
	return(strm);
}
void PTreeNode::SendRefreshEvent()
{
	if (!this->GetParent()){return;}
	wxCommandEvent evt(EVT_TREE_REFRESH);
	this->GetParent()->wxEvtHandler::AddPendingEvent(evt);
}
void PTreeNode::SendLayoutEvent()
{
	if (!this->GetParent()){return;}
	wxCommandEvent evt(EVT_TREE_LAYOUT);
	this->GetParent()->wxEvtHandler::AddPendingEvent(evt);
}
void PTreeNode::SetFocus()
{
	this->GetParent()->SetFocus();
}
void PTreeNode::SetLabel(const wxString& label)
{
	if (!this->IsRoot())
	{
		wxClientDC dc(this); wxSize TempSize = dc.GetTextExtent(label); TempSize.IncBy(2*TEXT_BORDER,0);
		if (TempSize.GetWidth() > this->sm_LargestTextExtent.GetWidth()){this->sm_LargestTextExtent.SetWidth(TempSize.GetWidth());}
		if (TempSize.GetHeight() > this->sm_LargestTextExtent.GetHeight()){this->sm_LargestTextExtent.SetHeight(TempSize.GetHeight());}
	}
	wxControl::SetLabel(label);
	this->SetMinSize(this->sm_LargestTextExtent);
}
void PTreeNode::SetRoot(bool isRoot){this->m_IsRoot = isRoot;}
void PTreeNode::UpdateLabel(wxString OldName, wxString NewName)
{
	if ((this->IsLeaf() || this->IsRoot()) && this->GetLabel() == OldName)
	{
		this->SetLabel(NewName);
		this->Refresh();
		return;
	}
	for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
	{
		this->m_Children[index]->UpdateLabel(OldName,NewName);
	}
}











//PTreeCtrl
unsigned int PTreeCtrl::CheckForErrors()
{
	unsigned int TotalErrors = 0;
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		TotalErrors |= this->CheckForErrors(index);
	}
	this->Refresh();
	return(TotalErrors);
}
unsigned int PTreeCtrl::CheckForErrors(unsigned int index)
{
	if (index < this->Trees() && this->m_Trees[index]->Children() > 0)
	{
		return(this->m_Trees[index]->Child(0)->CheckForErrors());
	}
	return(0);
}
void PTreeCtrl::RestructurePolytomies(bool Chronologically)
{
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		this->m_Trees[index]->RestructurePolytomies(Chronologically);
	}
}
wxArrayString PTreeCtrl::GetTreeData()
{
	wxArrayString Results;
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		Results.Add(this->GetTreeData(index));
	}
	return(Results);
}
wxArrayString PTreeCtrl::GetTreeNameData()
{
	wxArrayString Results;
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		Results.Add(this->GetTreeNameData(index));
	}
	return(Results);
}
wxString PTreeCtrl::GetTreeData(unsigned int index)
{
	if (index >= this->m_Trees.size() || this->m_Trees[index]->Children() == 0){return(wxEmptyString);}
	return(wxString::Format("%s = (0 %s)",this->m_Trees[index]->GetLabel(),this->m_Trees[index]->Child(0)->GetTreeData()));
}
wxString PTreeCtrl::GetTreeNameData(unsigned int index)
{
	if (index >= this->m_Trees.size() || this->m_Trees[index]->Children() == 0){return(wxEmptyString);}
	return(wxString::Format("%s = (0 %s)",this->m_Trees[index]->GetLabel(),this->m_Trees[index]->Child(0)->GetTreeNameData()));
}
void PTreeCtrl::OnNewTree(wxCommandEvent& event)
{
	unsigned int count = this->m_Trees.Count();
	wxString NewLabel = wxString::Format("Tree_%d",count);
	this->SubmitCommand(PTC_AddTree(this,NewLabel));
	if (this->m_Trees.size() > 1)
	{
		this->m_TreeSelector->SetStringSelection("All Trees");
		this->UpdateTreeDisplay(this->m_Trees.size());
	}
}
void PTreeCtrl::AddTree(wxArrayString Data)
{
	for(unsigned int index = 0, size = Data.size(); index < size; index += 2)
	{
		this->AddTree(Data[index],Data[index+1]);
	}
}
void PTreeCtrl::AddTree(wxString TreeName, wxString TreeData)
{
	if (TreeName.IsEmpty())
	{
		TreeName = wxString::Format("Root%d",this->Trees());
	}
	wxWindowUpdateLocker lock(this);
	PTreeNode *Subtree = new PTreeNode(this,wxID_ANY);
		Subtree->SetLabel(TreeName);
		Subtree->SetRoot(true);
		this->m_Trees.Add(Subtree);

	if (TreeData.IsEmpty()){return;}

	//Correct the data
	int Return = 0;
	wxRegEx AddSpace; if (!AddSpace.Compile("\\d+",wxRE_ADVANCED|wxRE_ICASE)){wxMessageBox("AddSpace Compilation failed");}
	wxRegEx RemoveZero; if (!RemoveZero.Compile("^\\(0 (.*)\\)",wxRE_ADVANCED|wxRE_ICASE)){wxMessageBox("RemoveZero Compilation failed");}
	TreeData.Replace(","," ",true);
	Return = AddSpace.ReplaceAll(&TreeData,"\\0 ");
	TreeData.Replace("  "," ");
	RemoveZero.ReplaceAll(&TreeData,"\\1");

	//Parse the data - ( means adding child (.) and push on stack, ' ' means adding sibling, ) means pop current stack
	int TotalTaxa = 0;
	PTreeNodeArray Stack;
	Stack.Push(Subtree);
	size_t size = TreeData.length();
	for(size_t index = 0; index < size; ++index)
	{
		char test = (char)TreeData[index];
		switch(test)
		{
		case('('): {Stack.Push(Stack.Top()->AddChild()); break;}
		case(' '): {/*don't care*/ break;}
		case(')'): {Stack.Pop(); break;}
		default:
			{
				long num = 0;
				wxString Sub = TreeData.Mid(index);
				Sub = Sub.BeforeFirst(' ').BeforeFirst(')').BeforeFirst('(');
				if (Sub.ToLong(&num))
				{
					Taxon *Node = Taxon::FindTaxonByImportID(num);
					if (!Node){Node = new Taxon(); Node->ImportID(num); Node->ChangeName(wxString::Format("Node %d",num));}
					Stack.Top()->AddLeaf(Node->Name());
					index += Sub.length()-1;
				}
				else
				{
					//Tree might have taxon name, not ID
					Taxon *Node = Taxon::FindTaxonByName(Sub);
					if (!Node){Node = new Taxon(); Node->ChangeName(Sub);}
					Stack.Top()->AddLeaf(Node->Name());
					index += Sub.length()-1;
				}
				++TotalTaxa;
			}
		}
	}
	Stack.Pop();
	this->LayoutChildren();
	this->CheckForErrors();
	this->UpdateSelector();
	if (this->m_Trees.size() > 1)
	{
		this->m_TreeSelector->SetStringSelection("All Trees");
		this->UpdateTreeDisplay(this->m_Trees.size());
	}
}
void PTreeCtrl::DistributeGhostLineages()
{
	PTreeNodeArray& Array = this->m_Trees;
	unsigned int NumChildren = Array.size();
	if (NumChildren == 0){return;}
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		Array[index]->ComputeDivergences();
		Array[index]->DistributeGhostLineages();
	}
}
void PTreeCtrl::LayoutChildren()
{
	wxWindowUpdateLocker lock(this);

	PTreeNodeArray& Array = this->m_Trees;
	unsigned int NumChildren = Array.size();
	if (NumChildren == 0){return;}
	wxSize MySize = this->GetClientSize();
	wxPoint Zero = this->CalcUnscrolledPosition(wxPoint(0,0));

	SizeInfoArray SizeArray;
	for(unsigned int index = 0; index < NumChildren; ++index)
	{
		SizeArray.Add(Array[index]->GetSizeInfo());
	}
		
	wxClientDC dc(this);
	wxCoord CurrentHeight = 0, TotalHeight = 35;
	for(unsigned int index = 0, size = Array.size(); index < size; ++index)
	{
		if (Array[index]->IsShown())
		{
			TotalHeight += CurrentHeight;
			CurrentHeight = SizeArray[index].GetHeight() + dc.GetTextExtent(Array[index]->GetLabel()).GetHeight();
			Array[index]->SetPosition(wxPoint(0-Zero.x,TotalHeight-Zero.y));
			Array[index]->SetSize(max(MySize.GetWidth(),SizeArray[index].GetWidth()), CurrentHeight);
		}
		Array[index]->ComputeDivergences();
		Array[index]->DistributeGhostLineages();
		Array[index]->LayoutChildren();
	}
	TotalHeight += CurrentHeight + 10;
	this->SetVirtualSize(MySize.GetWidth(),TotalHeight);
}
wxDataInputStream& PTreeCtrl::Load(wxDataInputStream& strm)
{
	wxWindowUpdateLocker lock(this);
	wxUint32 children;
	strm >> children;
	
	PTreeNode *Subtree = NULL;
	for(unsigned int index = 0; index < children; ++index)
	{
		Subtree = new PTreeNode(this,wxID_ANY);
		this->m_Trees.Add(Subtree);
		Subtree->Load(strm);
	}
	this->UpdateSelector();
	if (this->m_Trees.size() > 1)
	{
		this->m_TreeSelector->SetStringSelection("All Trees");
		this->UpdateTreeDisplay(this->m_Trees.size());
	}
	return(strm);
}
//wxTextInputStream& PTreeCtrl::Load(wxTextInputStream& strm)
//{
//	wxWindowUpdateLocker lock(this);
//	wxString line;
//	unsigned long children;
//	strm >> line; line.AfterFirst('=').ToULong(&children);
//	
//	PTreeNode *Subtree = NULL;
//	for(unsigned int index = 0; index < children; ++index)
//	{
//		Subtree = new PTreeNode(this,wxID_ANY);
//		this->m_Trees.Add(Subtree);
//		Subtree->Load(strm);
//	}
//	this->UpdateSelector();
//	if (this->m_Trees.size() > 1)
//	{
//		this->m_TreeSelector->SetStringSelection("All Trees");
//		this->UpdateTreeDisplay(this->m_Trees.size());
//	}
//	return(strm);
//}
void PTreeCtrl::Ladderize()
{
	//Default is ladderize right
	PTC_Group Command("Ladderize");
	this->Ladderize(LADDERIZE_RIGHT,&Command);
	if (Command.Link())
	{
		PTreeCtrl::SubmitCommand(Command);
	}
}
void PTreeCtrl::Ladderize(LadderizeType Type, ASCCCommand *Command)
{
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		this->m_Trees[index]->Ladderize(Type,Command);
	}
}
void PTreeCtrl::OnLadderizeLeft(wxCommandEvent& event)
{
	PTC_Group Command("Ladderize Left");
	this->Ladderize(LADDERIZE_LEFT,&Command);
	if (Command.Link())
	{
		PTreeCtrl::SubmitCommand(Command);
	}
}
void PTreeCtrl::OnLadderizeRight(wxCommandEvent& event)
{
	PTC_Group Command("Ladderize Right");
	this->Ladderize(LADDERIZE_RIGHT,&Command);
	if (Command.Link())
	{
		PTreeCtrl::SubmitCommand(Command);
	}
}
void PTreeCtrl::OnPaste(wxCommandEvent& event)
{
	if (wxTheClipboard->IsOpened() || wxTheClipboard->Open())
	{
		if (wxTheClipboard->IsSupported( wxDataFormat("PTreeNode") ))
		{
			PTreeNodeData data;
			if (wxTheClipboard->GetData(data))
			{
				//Start a new tree
				unsigned int count = this->m_Trees.Count();
				wxString NewLabel = wxString::Format("Tree_%d",count);
				PTreeNode* NewTree = new PTreeNode(data.GetNode(),this);
				NewTree->SetLabel(NewLabel);
				PTC_Group cmd("Paste Tree");
				cmd.Link(PTC_AddTree(this,NewLabel,wxID_ANY,NewTree));
				if (data.IsCut())
				{
					cmd.Link(PTC_DeleteNode(data.GetNode()));
					if (data.GetNode()->GetParent()->GetParent()->IsKindOf(CLASSINFO(PTreeNode))) //Check if grandparent is phylo node
					{
						PTC_Group Merge("Merge");
						PTreeNode *NodeParent = (PTreeNode *)data.GetNode()->GetParent();
						PTreeNode *NodeGrandParent = (PTreeNode *)NodeParent->GetParent();
						if (NodeParent->Children() == 2)
						{
							unsigned int NodeIndex = NodeGrandParent->FindChild(NodeParent);
							for(unsigned int index = 0, size = NodeParent->Children(); index < size; ++index)
							{
								if (NodeParent->Child(index) != data.GetNode()){Merge.Link(PTC_MoveNode(NodeParent->Child(index),NodeGrandParent,NodeIndex++));}
							}
							Merge.Link(PTC_DeleteNode(NodeParent));
						}
						cmd.Link(Merge);
					}
					//Replace cut data with copy data for multiple pastes
					wxTheClipboard->SetData(new PTreeNodeData(data.GetNode(),false));
				}
				this->SubmitCommand(cmd);

				if (this->m_Trees.size() > 1)
				{
					this->m_TreeSelector->SetStringSelection("All Trees");
					this->UpdateTreeDisplay(this->m_Trees.size());
				}
			}
		}  
		wxTheClipboard->Close();
	}
}
//wxDataOutputStream& PTreeCtrl::Save(wxDataOutputStream& strm)
//{
//	strm << (wxUint32)this->m_Trees.size();
//	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
//	{
//		this->m_Trees[index]->Save(strm);
//	}
//	return(strm);
//}
//wxTextOutputStream& PTreeCtrl::Save(wxTextOutputStream& strm)
//{
//	strm << "TreeCount = " << this->m_Trees.size() << "\n";
//	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
//	{
//		this->m_Trees[index]->Save(strm);
//	}
//	return(strm);
//}
bool PTreeCtrl::Load(wxXmlNode* CurrentNode)
{
	wxWindowUpdateLocker lock(this);
	if (!CurrentNode || CurrentNode->GetName() != "Trees"){return(false);}
	bool Result = true;
	for(wxXmlNode *Current = CurrentNode->GetChildren(); Current != NULL; Current = Current->GetNext())
	{
		if (Current->GetName() == "TreeNode")
		{
			PTreeNode *Subtree = new PTreeNode(this,wxID_ANY);
			Subtree->SetRoot(true);
			this->m_Trees.Add(Subtree);
			Result &= Subtree->Load(Current);
		}
	}
	this->UpdateSelector();
	if (this->m_Trees.size() > 1)
	{
		this->m_TreeSelector->SetStringSelection("All Trees");
		this->UpdateTreeDisplay(this->m_Trees.size());
	}
	return(Result);
}
wxXmlNode* PTreeCtrl::Save()
{
	wxXmlNode *TreeData = new wxXmlNode(wxXML_ELEMENT_NODE,"Trees");
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		TreeData->AddChild(this->m_Trees[index]->Save());
	}
	return(TreeData);
}
void PTreeCtrl::UpdateLabel(wxString OldName, wxString NewName)
{
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		if (this->m_Trees[index]->Children() > 0){this->m_Trees[index]->Child(0)->UpdateLabel(OldName,NewName);}
	}
	this->UpdateSelector();
}
void PTreeCtrl::SaveDisplayAsImage(wxString Filename)
{
	if (Filename.IsEmpty())
	{
		Filename = wxFileSelector("Select save file: ",::wxGetCwd(),wxEmptyString,wxEmptyString,"Image Files" + wxImage::GetImageExtWildcard(),wxFD_SAVE|wxFD_OVERWRITE_PROMPT,this);
		if (Filename.IsEmpty()){return;}
	}
	
	wxCoord width = this->GetVirtualSize().GetWidth(), height = this->GetVirtualSize().GetHeight()-30;
	wxBitmap bmp(width,height);
	wxMemoryDC dc(bmp);
	dc.SetBackground(wxBrush(this->GetBackgroundColour()));
	dc.SetFont(wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT));
	dc.Clear();
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		if (this->m_Trees[index]->IsEnabled())
		{
			this->m_Trees[index]->Draw(dc, 0,-30);
		}
	}
	//dc.Blit(0,0,width,height-30,&client,0,30);
	bmp.ConvertToImage().SaveFile(Filename);
}
bool PTreeCtrl::SubmitCommand(const ASCCCommand& Cmd, bool undoable)
{
	ASCCCommand *Clone = Cmd.Clone();
	if (PTreeCtrl::sm_CommandProcessor && undoable){return(PTreeCtrl::sm_CommandProcessor->Submit(Clone));} 
	else {Clone->Do(); wxDELETE(Clone); return(false);}
}
PTC_Base *PTreeCtrl::DeleteNodes(wxString NodeLabelToDelete)
{
	PTC_Base *TreeResult = NULL, *Result = NULL;
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
	{
		TreeResult = this->m_Trees[index]->DeleteNodes(NodeLabelToDelete);
		if (Result){Result->Link(TreeResult);}
		else {Result = TreeResult;}
	}
	return(Result);
}
void PTreeCtrl::OnDraw(wxDC& dc)
{
	dc.Clear();
	dc.SetBrush(wxBrush(wxSystemSettings::GetColour(wxSYS_COLOUR_3DFACE)));
	dc.DrawRectangle(wxPoint(0,0),wxSize(this->GetClientSize().GetWidth(),30));
	dc.SetFont(wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT));
	dc.SetTextForeground(*wxBLACK);
	dc.DrawText("Tree to display: ",10,8);
}
void PTreeCtrl::UpdateLeafHashes()
{
	for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index){this->m_Trees[index]->UpdateLeafHash();}
}
void PTreeCtrl::UpdateTreeDisplay(int TreeToDisplay)
{
	wxWindowUpdateLocker lock(this);
	if (TreeToDisplay == this->m_Trees.size())
	{
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index){this->m_Trees[index]->Show(true);}
	}
	else
	{
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
		{
			this->m_Trees[index]->Show(false);
		}
		this->m_Trees[TreeToDisplay]->Show(true);
	}
	this->LayoutChildren();
}












wxDragResult PTreeNodeDropTarget::OnData(wxCoord x, wxCoord y, wxDragResult def)
{
	if (!this->GetData() || !this->m_Owner || !this->m_Owner->GetParent()->IsKindOf(CLASSINFO(PTreeNode))){return(wxDragNone);}
	if (!this->m_Owner->DoPaste(((PTreeNodeData *)this->GetDataObject()),"Drag")){return(wxDragNone);}
	this->m_Owner->Selected(false);
	return(wxDragMove);
}