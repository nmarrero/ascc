/*	TaxaDataWindow.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file TaxaDataWindow.cpp
 * @author Nico Marrero
 * @date September, 2012
 * @brief This file contains the code used for the Taxa Data Window and its associated Predefined OKR Window
 *
 */
#include "../include/TaxaDataWindow.h"
#include "../include/Taxa.h"
#include "../include/ASCCCommand.h"
#include <wx/wupdlock.h>
#include <wx/tooltip.h>

wxCommandProcessor *TaxaDataWindow::sm_CommandProcessor = NULL;
DEFINE_EVENT_TYPE(EVT_CHANGE_LINK);

BEGIN_EVENT_TABLE(TaxaAgeLinkWindow, wxPanel)
	EVT_CHOICE(wxID_ANY,TaxaAgeLinkWindow::OnUpdate)
END_EVENT_TABLE()

BEGIN_EVENT_TABLE(TaxaDataWindow,wxScrolledWindow)
	EVT_BUTTON(wxID_NEW,TaxaDataWindow::OnAddLink)
	EVT_BUTTON(wxID_DELETE, TaxaDataWindow::OnDeleteLink)
	EVT_COMMAND(wxID_ANY,EVT_CHANGE_LINK,TaxaDataWindow::OnChangeLink)
END_EVENT_TABLE()


bool TaxaAgeLinkWindow::TransferDataToWindow()
{
	if (!this->m_Link || !this->m_Link->LinkedOKR()){return(false);}
	if (this->m_Link->LinkedOKR()->Owner())
	{
		this->m_TaxaSelection->SetStringSelection(this->m_Link->LinkedOKR()->Owner()->Name());
	}
	else
	{
		this->m_TaxaSelection->SetStringSelection(this->m_Link->LinkedOKR()->PredefinedName());		
	}
	this->m_AgeSelection->SetSelection(this->m_Link->Type() == OLDER_THAN?0:1);
	return(true);
}

void TaxaAgeLinkWindow::UpdateTaxa()
{
	wxArrayString Names = Taxon::GetTaxaNames(false);
	//Add the predefined names
	Names.Add("HoloceneStage");Names.Add("TarantianStage");Names.Add("IonianStage");Names.Add("CalabrianStage");		
	Names.Add("GelasianStage");Names.Add("PiacenzianStage");Names.Add("ZancleanStage");Names.Add("MessinianStage");		
	Names.Add("TortonianStage");Names.Add("SerravallianStage");Names.Add("LanghianStage");Names.Add("BurdigalianStage");
	Names.Add("AquitanianStage");Names.Add("ChattianStage");Names.Add("RupelianStage");Names.Add("PriabonianStage");
	Names.Add("BartonianStage");Names.Add("LutetianStage");Names.Add("YpresianStage");Names.Add("ThanetianStage");		
	Names.Add("SelandianStage");Names.Add("DanianStage");Names.Add("MaastrichtianStage");Names.Add("CampanianStage");		
	Names.Add("SantonianStage");Names.Add("ConiacianStage");Names.Add("TuronianStage");Names.Add("CenomanianStage");		
	Names.Add("AlbianStage");Names.Add("AptianStage");Names.Add("BarremianStage");Names.Add("HauterivianStage");		
	Names.Add("ValanginianStage");Names.Add("BerriasianStage");Names.Add("TithonianStage");Names.Add("KimmeridgianStage");	
	Names.Add("OxfordianStage");Names.Add("CallovianStage");Names.Add("BathonianStage");Names.Add("BajocianStage");		
	Names.Add("AalenianStage");Names.Add("ToarcianStage");Names.Add("PliensbachianStage");Names.Add("SinemurianStage");		
	Names.Add("HettangianStage");Names.Add("RhaetianStage");Names.Add("NorianStage");Names.Add("CarnianStage");			
	Names.Add("LadinianStage");Names.Add("AnisianStage");Names.Add("OlenekianStage");Names.Add("InduanStage");			
	Names.Add("ChanghsingianStage");Names.Add("WuchiapingianStage");Names.Add("CapitanianStage");Names.Add("WordianStage");			
	Names.Add("RoadianStage");Names.Add("KungurianStage");Names.Add("ArtinskianStage");Names.Add("SakmarianStage");
	Names.Add("AsselianStage");Names.Add("GzhelianStage");Names.Add("KasimovianStage");Names.Add("MoscovianStage");
	Names.Add("BashkirianStage");Names.Add("SerpukhovianStage");Names.Add("ViseanStage");Names.Add("TournaisianStage");
	Names.Add("FamennianStage");Names.Add("FrasnianStage");Names.Add("GivetianStage");Names.Add("EifelianStage");		
	Names.Add("EmsianStage");Names.Add("PragianStage");Names.Add("LochkovianStage");Names.Add("PridoliSeries");		
	Names.Add("LudfordianStage");Names.Add("GorstianStage");Names.Add("HomerianStage");Names.Add("SheinwoodianStage");	
	Names.Add("TelychianStage");Names.Add("AeronianStage");Names.Add("RhuddanianStage");Names.Add("HirnantianStage");		
	Names.Add("KatianStage");Names.Add("SandbianStage");Names.Add("DarriwilianStage");Names.Add("DapingianStage");		
	Names.Add("FloianStage");Names.Add("TremadocianStage");Names.Add("Stage10");Names.Add("Stage9");Names.Add("PaibianStage");			
	Names.Add("GuzhangianStage");Names.Add("DrumianStage");Names.Add("Stage5");Names.Add("Stage4");Names.Add("Stage3");				
	Names.Add("Stage2");Names.Add("FortunianStage");Names.Add("EdiacaranSystem");Names.Add("CryogenianSystem");		
	Names.Add("TonianSystem");Names.Add("StenianSystem");Names.Add("EctasianSystem");Names.Add("CalymmianSystem");		
	Names.Add("StatherianSystem");Names.Add("OrosirianSystem");Names.Add("RhyacianSystem");Names.Add("SiderianSystem");		
	Names.Add("NeoarcheanEra");Names.Add("MesoarcheanEra");Names.Add("PaleoarcheanEra");Names.Add("EoarcheanEra");			
	Names.Add("HeadanEon");	
	this->m_TaxaSelection->Set(Names);
}

bool TaxaAgeLinkWindow::Create(wxWindow *parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
{
	bool Result = wxPanel::Create(parent, winid, pos, size, style, name);

	wxArrayString Names = Taxon::GetTaxaNames(false);
	//Add the predefined names
	Names.Add("HoloceneStage");Names.Add("TarantianStage");Names.Add("IonianStage");Names.Add("CalabrianStage");		
	Names.Add("GelasianStage");Names.Add("PiacenzianStage");Names.Add("ZancleanStage");Names.Add("MessinianStage");		
	Names.Add("TortonianStage");Names.Add("SerravallianStage");Names.Add("LanghianStage");Names.Add("BurdigalianStage");
	Names.Add("AquitanianStage");Names.Add("ChattianStage");Names.Add("RupelianStage");Names.Add("PriabonianStage");
	Names.Add("BartonianStage");Names.Add("LutetianStage");Names.Add("YpresianStage");Names.Add("ThanetianStage");		
	Names.Add("SelandianStage");Names.Add("DanianStage");Names.Add("MaastrichtianStage");Names.Add("CampanianStage");		
	Names.Add("SantonianStage");Names.Add("ConiacianStage");Names.Add("TuronianStage");Names.Add("CenomanianStage");		
	Names.Add("AlbianStage");Names.Add("AptianStage");Names.Add("BarremianStage");Names.Add("HauterivianStage");		
	Names.Add("ValanginianStage");Names.Add("BerriasianStage");Names.Add("TithonianStage");Names.Add("KimmeridgianStage");	
	Names.Add("OxfordianStage");Names.Add("CallovianStage");Names.Add("BathonianStage");Names.Add("BajocianStage");		
	Names.Add("AalenianStage");Names.Add("ToarcianStage");Names.Add("PliensbachianStage");Names.Add("SinemurianStage");		
	Names.Add("HettangianStage");Names.Add("RhaetianStage");Names.Add("NorianStage");Names.Add("CarnianStage");			
	Names.Add("LadinianStage");Names.Add("AnisianStage");Names.Add("OlenekianStage");Names.Add("InduanStage");			
	Names.Add("ChanghsingianStage");Names.Add("WuchiapingianStage");Names.Add("CapitanianStage");Names.Add("WordianStage");			
	Names.Add("RoadianStage");Names.Add("KungurianStage");Names.Add("ArtinskianStage");Names.Add("SakmarianStage");
	Names.Add("AsselianStage");Names.Add("GzhelianStage");Names.Add("KasimovianStage");Names.Add("MoscovianStage");
	Names.Add("BashkirianStage");Names.Add("SerpukhovianStage");Names.Add("ViseanStage");Names.Add("TournaisianStage");
	Names.Add("FamennianStage");Names.Add("FrasnianStage");Names.Add("GivetianStage");Names.Add("EifelianStage");		
	Names.Add("EmsianStage");Names.Add("PragianStage");Names.Add("LochkovianStage");Names.Add("PridoliSeries");		
	Names.Add("LudfordianStage");Names.Add("GorstianStage");Names.Add("HomerianStage");Names.Add("SheinwoodianStage");	
	Names.Add("TelychianStage");Names.Add("AeronianStage");Names.Add("RhuddanianStage");Names.Add("HirnantianStage");		
	Names.Add("KatianStage");Names.Add("SandbianStage");Names.Add("DarriwilianStage");Names.Add("DapingianStage");		
	Names.Add("FloianStage");Names.Add("TremadocianStage");Names.Add("Stage10");Names.Add("Stage9");Names.Add("PaibianStage");			
	Names.Add("GuzhangianStage");Names.Add("DrumianStage");Names.Add("Stage5");Names.Add("Stage4");Names.Add("Stage3");				
	Names.Add("Stage2");Names.Add("FortunianStage");Names.Add("EdiacaranSystem");Names.Add("CryogenianSystem");		
	Names.Add("TonianSystem");Names.Add("StenianSystem");Names.Add("EctasianSystem");Names.Add("CalymmianSystem");		
	Names.Add("StatherianSystem");Names.Add("OrosirianSystem");Names.Add("RhyacianSystem");Names.Add("SiderianSystem");		
	Names.Add("NeoarcheanEra");Names.Add("MesoarcheanEra");Names.Add("PaleoarcheanEra");Names.Add("EoarcheanEra");			
	Names.Add("HeadanEon");			 

	this->m_TaxaSelection = new wxChoice(this,wxID_ANY,wxDefaultPosition, wxDefaultSize, Names);
	wxArrayString Choices; Choices.Add("older than"); Choices.Add("younger than"); 
	this->m_AgeSelection = new wxChoice(this,wxID_ANY,wxDefaultPosition, wxDefaultSize, Choices);

	wxBoxSizer *TopSizer = new wxBoxSizer(wxHORIZONTAL);
	TopSizer->Add(this->m_AgeSelection);
	TopSizer->AddSpacer(5);
	TopSizer->Add(this->m_TaxaSelection);
	this->SetSizer(TopSizer);
	this->SendSizeEvent();

	return(Result);
}
void TaxaDataWindow::Update()
{
	wxWindowUpdateLocker lock(this);
	if (this->m_TaxaData){this->m_TaxaData->Update();}
	if (this->m_CurrentTaxon)
	{
		unsigned int TotalLinks = this->m_CurrentTaxon->OKRLinks();
		/*for(unsigned int index = 0, size = this->m_CurrentTaxon->OKRLinks(); index < size; ++index)
		{
			if (this->m_CurrentTaxon->Link(index).LinkedOKR()->Owner()){++TotalLinks;}
		}*/
		while(this->m_LinkWindows.size() < TotalLinks)
		{
			//Add extra link windows
			this->AddLink();
		}
		while(this->m_LinkWindows.size() > TotalLinks)
		{
			//Remove extra link windows
			this->DeleteLink();
		}

		//Go through each link and update the window
		bool LinksActive = !this->m_CurrentTaxon->IsSameAs() || !this->m_CurrentTaxon->SameAs();
		unsigned int link = 0;
		for(unsigned int index = 0, size = TotalLinks; index < size; ++index,++link)
		{
			//while (!this->m_CurrentTaxon->Link(link).LinkedOKR()->Owner()){++link;}
			OKRLink& linkedokr = this->m_CurrentTaxon->Link(link);
			this->m_LinkWindows[index]->Link(link,&this->m_CurrentTaxon->Link(link));
			this->m_LinkWindows[index]->Enable(LinksActive);
		}
	}
	if(this->m_TaxaData){this->m_TaxaData->UpdateInterface();}
	this->SendSizeEvent();
}
void TaxaDataWindow::ChangeObject(Taxon *NewTaxon)
{
	if (this->m_TaxaData)
	{
		this->m_TaxaData->ChangeObject(NewTaxon);
	}
	this->m_CurrentTaxon = NewTaxon;

	if (!NewTaxon)
	{
		this->SetSizer(NULL); 
		this->DestroyChildren(); 
		this->m_LinkWindowsSizer = NULL; 
		this->m_TaxaData = NULL;
		this->m_LinkWindows.Empty(); 
		return;
	}

	//Make sure the number of links is correct
	wxWindowUpdateLocker lock(this);

	if (!this->m_LinkWindowsSizer)
	{
		if (!this->m_TaxaData){this->m_TaxaData = new wxExposurePanel(this,NewTaxon,wxID_ANY); this->m_TaxaData->SetScrollRate(0,0);}
		wxBoxSizer *TopSizer = new wxBoxSizer(wxVERTICAL);
		TopSizer->Add(this->m_TaxaData,1,wxALIGN_CENTER|wxEXPAND|wxALL,5);

		wxStaticBoxSizer *ConstraintsSizer = new wxStaticBoxSizer(wxVERTICAL,this,"Age Constraints");
		this->m_LinkWindowsSizer = new wxBoxSizer(wxVERTICAL);
		ConstraintsSizer->Add(this->m_LinkWindowsSizer);
		wxBoxSizer *ButtonSizer = new wxBoxSizer(wxHORIZONTAL);
		wxButton *AddButton = new wxButton(this,wxID_NEW,"+",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT); AddButton->SetToolTip("Add an additional age constraint");
		wxButton *DeleteButton = new wxButton(this,wxID_DELETE,"-",wxDefaultPosition,wxDefaultSize,wxBU_EXACTFIT); DeleteButton->SetToolTip("Remove the last age constraint");
		ButtonSizer->Add(AddButton);
		ButtonSizer->AddSpacer(10);
		ButtonSizer->Add(DeleteButton);
		ConstraintsSizer->Add(ButtonSizer,0,wxALIGN_CENTER|wxTOP,5);

		TopSizer->Add(ConstraintsSizer,0,wxLEFT|wxRIGHT|wxALIGN_CENTER,5);
		this->SetSizer(TopSizer);
	}
	this->FitInside();
	this->Update();
	this->SendSizeEvent();
}

void TaxaDataWindow::AddLink()
{
	this->m_LinkWindows.Add(new TaxaAgeLinkWindow(this)); 
	this->m_LinkWindowsSizer->Add(this->m_LinkWindows.Last());
}
void TaxaDataWindow::DeleteLink()
{
	if (this->m_LinkWindows.size() > 0)
	{
		TaxaAgeLinkWindow *win = this->m_LinkWindows.Last(); 
		this->m_LinkWindowsSizer->Detach(win);
		this->m_LinkWindows.RemoveAt(this->m_LinkWindows.size()-1);
		wxDELETE(win);
	}
}

void TaxaDataWindow::OnChangeLink(wxCommandEvent& event)
{
	//No changing links if this is marked "Same As"
	if (this->m_CurrentTaxon->IsSameAs() && this->m_CurrentTaxon->SameAs()){return;}
	long temp;
	if (event.GetString().BeforeFirst(' ').ToLong(&temp))
	{
		wxString LinkName = event.GetString().AfterFirst(' ');
		Taxon *NewLinkTaxon = Taxon::FindTaxonByName(LinkName);
		if (NewLinkTaxon)
		{
			//Taxon Link
			this->SubmitCommand(TC_RelinkOKR(this->m_CurrentTaxon,event.GetInt(),OKRLink((temp == 0)?OLDER_THAN:YOUNGER_THAN,NewLinkTaxon)));
		}
		else
		{
			//Predefined OKR Link
			const OKR *NewOKRLink = OKR::FindPredefinedOKR(LinkName);
			this->SubmitCommand(TC_RelinkOKR(this->m_CurrentTaxon,event.GetInt(),OKRLink((temp == 0)?OLDER_THAN:YOUNGER_THAN,NewOKRLink)));
		}
	}
}
void TaxaDataWindow::OnAddLink(wxCommandEvent& event)
{
	//No adding links if this is marked "Same As"
	if (this->m_CurrentTaxon->IsSameAs() && this->m_CurrentTaxon->SameAs()){return;}
	wxWindowUpdateLocker lock(this);
	//Add a new link to the taxon
	TC_LinkOKR cmd(this->m_CurrentTaxon,OKRLink(OLDER_THAN,Taxon::FindTaxonByName(Taxon::GetTaxaNames()[0])));
	this->SubmitCommand(cmd);
}
void TaxaDataWindow::OnDeleteLink(wxCommandEvent& event)
{
	//No deleting links if this is marked "Same As"
	if (this->m_CurrentTaxon->IsSameAs() && this->m_CurrentTaxon->SameAs()){return;}
	wxWindowUpdateLocker lock(this); 
	unsigned int lastindex = 0;
	for(unsigned int index = 0, size = this->m_CurrentTaxon->OKRLinks(); index < size; ++index)
	{
		if (this->m_CurrentTaxon->Link(index).LinkedOKR()->Owner()){lastindex = index;}
	}
	TC_UnlinkOKR cmd(this->m_CurrentTaxon,lastindex);
	this->SubmitCommand(cmd);
}

bool TaxaDataWindow::SubmitCommand(const ASCCCommand& Cmd)
{
	ASCCCommand *Clone = Cmd.Clone();
	if (TaxaDataWindow::sm_CommandProcessor){return(TaxaDataWindow::sm_CommandProcessor->Submit(Clone));} 
	else {Clone->Do(); wxDELETE(Clone); return(false);}
}


