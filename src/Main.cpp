/*	Main.cpp
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <wx/wx.h>
#include <wx/aui/aui.h>
#include <wx/artprov.h>
#include <wx/aboutdlg.h>
#include <wx/generic/aboutdlgg.h>
#include <wx/statline.h>
#include <wx/config.h>
#include <wx/docview.h>
#include <wx/cmdline.h>
#include <wx/datetime.h>
#include <exposure/wx/wxExposure.h>
#include "../include/ASCCData.h"
#include "../include/ASCCDataWindow.h"
#include "../include/PhylogeneticTree.h"
#include "../include/ASCCCommand.h"
#include "../include/TaxaDataWindow.h"
#include "../include/wxLibraries.h"
#include "../include/Utils.h"

#ifndef NDEBUG
#pragma comment(lib,"wxExposure_d.lib")
#else
#pragma comment(lib,"wxExposure.lib")
#endif

#define ASCC_VERSION "4.0.7"

wxString AUTOSAVE(wxString Filename) {wxFileName file(Filename); file.SetExt("autosave."+file.GetExt()); return(file.GetFullPath());}

class ASCCApp : public wxApp {public: ~ASCCApp(); bool OnInit();};
enum {MENUID_IMPORT = wxID_HIGHEST, MENUID_EXPORT, MENUID_REMOVEAUTOMATICCONSTRAINTS, MENUID_SHOWCONSOLE, MENUID_ANALYZESCORES, MENUID_CALCULATESCORES, MENUID_CHECKTAXA, ID_DATA, ID_AGEBINS, ID_TAXA, ID_TAXALIST, ID_AUTOSAVE};

static const wxCmdLineEntryDesc g_cmdLineDesc[] =
{
	{wxCMD_LINE_PARAM, NULL, NULL, "input file", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL},
	{wxCMD_LINE_NONE}
};

class ASCCMainFrame : public wxFrame, virtual public Exposure
{
public:
	ASCCMainFrame():wxFrame(NULL,wxID_ANY,wxString("ASCC ") + ASCC_VERSION,wxDefaultPosition, wxSize(1024,768),
		wxBORDER_THEME | wxCLIP_CHILDREN | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxRESIZE_BORDER | wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX),
		m_Tree(new PTreeCtrl(this,wxID_ANY)), m_Config("ASCC"),m_FileHistory(5),
		m_Timer(this,ID_AUTOSAVE),m_TaxaList(new wxExposurePanel(this,this,ID_TAXALIST)),m_DataPanel(new TaxaDataWindow(this)),
		m_OKRTree(new OKRSelectionTree(this,wxID_ANY)), m_CommandProcessor()
	{
		//Tell the aui manager to manage this window
		this->m_AuiManager.SetManagedWindow(this);

		//Create a status bar
		this->CreateStatusBar();

		//Load the file history
		this->m_FileHistory.Load(this->m_Config);

		//Menu stuff
		this->CreateMenu();

		//Layout
			this->m_AuiManager.AddPane(this->m_OKRTree,wxAuiPaneInfo().Name("OKRTree").Caption("Geologic Time Scale").Left().Position(0).CloseButton(true).MinSize(200,100));
			this->m_AuiManager.AddPane(this->m_TaxaList,wxAuiPaneInfo().Name("TaxaList").Caption("Taxa").Left().Position(0).Row(1).CloseButton(true).TopDockable(false).BottomDockable(false).MinSize(200,200));
			this->m_AuiManager.AddPane(this->m_DataPanel,wxAuiPaneInfo().Name("TaxaData").Caption("Taxon Data").Left().Position(1).Row(1).CloseButton(true).TopDockable(false).BottomDockable(false).MinSize(315,300));
			this->m_AuiManager.AddPane(this->m_Tree,wxAuiPaneInfo().Name("PTreeCtrl").Caption("Tree Data").Center().CloseButton(false));
			this->m_AuiManager.Update();
			this->m_OKRTree->ScrollToEdge();
			this->m_DefaultPerspective = this->m_AuiManager.SavePerspective();
			this->m_AuiManager.LoadPerspective(this->m_Config.Read("AUIManager/Perspective",wxEmptyString));
			
		//Autosave timer
			this->m_Timer.Start(this->m_Config.Read("/Options/AutosaveInterval",300000)); //Default 5 min autosave interval

		//Set up the command processor
			this->m_Tree->SetCommandProcessor(&this->m_CommandProcessor);
			this->m_CommandProcessor.Initialize();
			Taxon::SetCommandProcessor(&this->m_CommandProcessor);
			this->m_DataPanel->SetCommandProcessor(&this->m_CommandProcessor);

		//Misc
			wxFrame::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
			this->SetStatusText("Ready");
	}
	~ASCCMainFrame(){this->m_FileHistory.Save(this->m_Config); this->m_Config.Write("AUIManager/Perspective",this->m_AuiManager.SavePerspective()); this->m_AuiManager.UnInit();}
	void SetFilename(wxString Filename)
	{
		this->m_CurrentFilename = Filename;
		if (Filename.IsEmpty()){this->SetTitle(wxString("ASCC ") + ASCC_VERSION);}
		else {this->SetTitle(wxString("ASCC ") + ASCC_VERSION + " - " + wxFileName(Filename).GetFullName());}
		this->UpdateFilenameStatus();
		if (!Filename.IsEmpty())
		{
			this->m_FileHistory.AddFileToHistory(Filename);
			this->CreateMenu();
		}
	}
	void UpdateFilenameStatus()
	{
		wxString Filename = this->GetTitle();
		if (!Filename.Contains(" - ")){return;}
		
		if (Filename.Contains("*")){Filename = Filename.BeforeLast('*');}
		this->SetTitle(Filename + (this->m_CommandProcessor.IsDirty()?"*":""));
	}
	void Load(wxString Filename = wxEmptyString)
	{
		if (Filename.IsEmpty())
		{
			Filename = wxFileSelector("Select file to load:",wxEmptyString,wxEmptyString,"ASCC Files|*.ascc;*.asccdata","ASCC Files (*.ascc;*.asccdata)|*.ascc;*.asccdata|All Files (*.*)|*.*",wxFD_OPEN,this);
			if (Filename.IsEmpty()){return;}
		}
		wxString Extension = Filename.AfterLast('.');
		if (Extension == "ascc")
		{
			this->LoadXML(Filename);
		}
		else if (Extension == "asccdata")
		{
			ASCCDataAnalysis *data = ASCCDataAnalysis::Load(Filename);
			if (data)
			{
				ASCCDataAnalysisWindow *win = new ASCCDataAnalysisWindow(data);
				win->Show();
			}
		}
		else {wxMessageBox("Unable to load file '" + Filename + "': Unrecognized extension (file is not .ascc or .asccdata)");}
		return;
	}
	void LoadBinary(wxString Filename = wxEmptyString)
	{
		if (Filename.IsEmpty())
		{
			Filename = wxFileSelector("Select file to load:",wxEmptyString,wxEmptyString,"ASCC Files|*.ascc","ASCC Files (*.ascc)|*.ascc|All Files (*.*)|*.*",wxFD_OPEN,this);
			if (Filename.IsEmpty()){return;}
		}
		wxString FileToOpen = Filename;
		if (wxFileExists(AUTOSAVE(Filename)))
		{
			if (wxMessageBox("An backup version of this file was found. Would you like to recover the data from this backup?", "Backup file found", wxYES_NO, this) == wxYES)
			{
				FileToOpen = AUTOSAVE(Filename);
			}
			else
			{
				wxRemoveFile(AUTOSAVE(Filename));
			}
		}
		wxFileInputStream InputStream(FileToOpen);
		if (!InputStream.IsOk())
		{
			for(unsigned int index = 0; index < this->m_FileHistory.GetCount(); ++index)
			{
				if (this->m_FileHistory.GetHistoryFile(index) == Filename){this->m_FileHistory.RemoveFileFromHistory(index); this->CreateMenu(); --index;}
			}
			return;
		}

		//Before continuing, make sure the user knows we're gonna clear out the data
		if (!Taxon::GetTaxaNames().IsEmpty() || this->m_Tree->Trees() > 0)
		{
			if(wxMessageBox("Continuing will clear all data and cannot be undone. Continue?","Confirmation Needed",wxYES_NO|wxICON_QUESTION,this) == wxNO){return;}
			this->ClearAllData();
		}

		//Ok, continue...
		wxDataInputStream DataStream(InputStream);
		wxString Section;
	
		while(!InputStream.Eof())
		{
			DataStream >> Section;
			if (Section == "[TAXALIST]")
			{
				unsigned int size = 0;
				DataStream >> size;
				wxString name; Taxon *NewTaxon;
				for(unsigned int index = 0; index < size; ++index)
				{
					DataStream >> name;
					NewTaxon = Taxon::FindTaxonByName(name);
					if (!NewTaxon){NewTaxon = new Taxon(); NewTaxon->ChangeName(name);}
				}
			}
			else if (Section == "[TAXA]"){Taxon::LoadAllTaxa(DataStream);}
			else if (Section == "[TREES]"){this->m_Tree->Load(DataStream);}
		}
		this->m_Tree->CheckForErrors();
		this->SetFilename(Filename);
		this->SetCurrentTaxon(Taxon::GetTaxaNames()[0]);
		ExposureInterface::UpdateAllInterfaces();
		this->SendSizeEvent();
		this->m_OKRTree->ScrollToEdge();
		this->CreateMenu();
	}
	//void Save(wxString Filename = wxEmptyString)
	//{
	//	if (Filename.IsEmpty()){Filename = this->m_CurrentFilename;}
	//	if (Filename.IsEmpty()){this->SaveAs();}
	//	else
	//	{
	//		wxFileOutputStream OutputStream(Filename); if (!OutputStream.IsOk()){return;}
	//		wxTextOutputStream DataStream(OutputStream);
	//		//wxDataOutputStream DataStream(OutputStream);
	//		
	//		//Commit any remaining changes in the Taxa Data window
	//		this->m_DataPanel->Commit();

	//		//Save the list of taxa names
	//		DataStream << "[TAXALIST]\n";
	//			wxArrayString TaxaNames = Taxon::GetTaxaNames();
	//			DataStream << "Taxa = " << TaxaNames.size() << "\n";
	//			for(unsigned int index = 0; index < TaxaNames.size(); ++index)
	//			{
	//				if (TaxaNames[index].EndsWith("*")){DataStream << TaxaNames[index].BeforeLast('*') << "\n";}
	//				else {DataStream << TaxaNames[index] << "\n";}
	//			}
	//		//Save the taxa data
	//		DataStream << "[TAXA]\n";
	//			Taxon::SaveAllTaxa(DataStream);
	//		//Save the tree data
	//		DataStream << "[TREES]\n";
	//			this->m_Tree->Save(DataStream);

	//		if (wxFileExists(AUTOSAVE(this->m_CurrentFilename))){wxRemoveFile(AUTOSAVE(this->m_CurrentFilename));}
	//		this->m_CommandProcessor.MarkAsSaved();
	//		this->UpdateFilenameStatus();
	//	}
	//}
	void LoadXML(wxString Filename = wxEmptyString)
	{
		if (Filename.IsEmpty())
		{
			Filename = wxFileSelector("Select file to load:",wxEmptyString,wxEmptyString,"ASCC Files|*.ascc","ASCC Files (*.ascc)|*.ascc|All Files (*.*)|*.*",wxFD_OPEN,this);
			if (Filename.IsEmpty()){return;}
		}
		wxString FileToOpen = Filename;
		if (wxFileExists(AUTOSAVE(Filename)))
		{
			if (wxMessageBox("An backup version of this file was found. Would you like to recover the data from this backup?", "Backup file found", wxYES_NO, this) == wxYES)
			{
				FileToOpen = AUTOSAVE(Filename);
			}
			else
			{
				wxRemoveFile(AUTOSAVE(Filename));
			}
		}

		//Before continuing, make sure the user knows we're gonna clear out the data
		if (!Taxon::GetTaxaNames().IsEmpty() || this->m_Tree->Trees() > 0)
		{
			if(wxMessageBox("Continuing will clear all data and cannot be undone. Continue?","Confirmation Needed",wxYES_NO|wxICON_QUESTION,this) == wxNO){return;}
			this->ClearAllData();
		}
		
		wxXmlDocument Data;
		bool LoadSuccess = true;
		{
			wxLogNull logno;
			if (LoadSuccess && !wxFileExists(Filename))
			{
				wxMessageBox("File '" + Filename + "' not found. Please check that the file exists and try loading it again.");
				LoadSuccess = false;
			}
			if (LoadSuccess && !Data.Load(Filename))
			{
				wxMessageBox("Error loading file '" + Filename + "'. \n"
							"- If this is an ASCC XML file, it is not valid. Please check it and try again. \n"
							"- If the file is not in the ASCC XML format and is the old binary ascc format, you can load the file using Import in the File menu."); 
				LoadSuccess = false;
			}
			if (LoadSuccess && Data.GetRoot()->GetName() != "ASCC")
			{
				wxMessageBox("Error loading file '" + Filename + "'. This file does not appear to be in the ASCC XML format."); 
				LoadSuccess = false;
			}
		}
		if (!LoadSuccess)
		{
			for(unsigned int index = 0; index < this->m_FileHistory.GetCount(); ++index)
			{
				if (this->m_FileHistory.GetHistoryFile(index) == Filename){this->m_FileHistory.RemoveFileFromHistory(index); this->CreateMenu(); --index;}
			}
			return;
		}
		
		
		for(wxXmlNode *CurrentNode = Data.GetRoot()->GetChildren(); CurrentNode != NULL; CurrentNode = CurrentNode->GetNext())
		{
			if (CurrentNode->GetName() == "Taxa"){LoadSuccess &= Taxon::LoadAllTaxa(CurrentNode);}
			if (CurrentNode->GetName() == "Trees"){LoadSuccess &= this->m_Tree->Load(CurrentNode);}
		}
		this->m_Tree->CheckForErrors();
		this->SetFilename(Filename);
		this->SetCurrentTaxon(Taxon::GetTaxaNames()[0]);
		ExposureInterface::UpdateAllInterfaces();
		this->SendSizeEvent();
		this->m_OKRTree->ScrollToEdge();
		this->CreateMenu();
	}
	void SaveXML(wxString Filename = wxEmptyString)
	{
		if (Filename.IsEmpty()){Filename = this->m_CurrentFilename;}
		if (Filename.IsEmpty()){this->SaveAs();}
		else
		{
			//Commit any remaining changes in the Taxa Data window
			this->m_DataPanel->Commit();

			wxXmlDocument SaveFile;
			wxXmlNode *ASCCNode = new wxXmlNode(wxXML_ELEMENT_NODE, "ASCC");
			//Save the taxa data
				ASCCNode->AddChild(Taxon::SaveAllTaxa());
			//Save the tree data
				ASCCNode->AddChild(this->m_Tree->Save());
			SaveFile.SetRoot(ASCCNode);
			SaveFile.Save(Filename);

			if (wxFileExists(AUTOSAVE(this->m_CurrentFilename))){wxRemoveFile(AUTOSAVE(this->m_CurrentFilename));}
			this->m_CommandProcessor.MarkAsSaved();
			this->UpdateFilenameStatus();
		}
	}
	void SaveAs()
	{
		wxString Filename = wxFileSelector("Select save file:",wxEmptyString,this->m_CurrentFilename,"ASCC Files|*.ascc","ASCC Files (*.ascc)|*.ascc|All Files (*.*)|*.*",wxFD_SAVE|wxFD_OVERWRITE_PROMPT,this);
		if (Filename.IsEmpty()){return;}
		this->SetFilename(Filename);
		this->SaveXML();
	}
	void ClearAllData()
	{
		this->m_Tree->ClearTreeData();
		this->m_DataPanel->ChangeObject(NULL);
		Taxon::ClearTaxonData();
		this->SetFilename(wxEmptyString);
		this->m_OKRTree->ResetAgeSelection();
		ExposureInterface::UpdateAllInterfaces();
		this->CreateMenu();
		this->SendSizeEvent();
	}
protected:
	void OnNew(wxCommandEvent& event)
	{
		if (!Taxon::GetTaxaNames().IsEmpty() || this->m_Tree->Trees() > 0)
		{
			if(wxMessageBox("Continuing will clear all data and cannot be undone. Continue?","Confirmation Needed",wxYES_NO|wxICON_QUESTION,this) == wxNO){return;}
		}
		this->ClearAllData();
	}
	void OnAutoSave(wxTimerEvent& event){if (!this->m_CurrentFilename.IsEmpty()){this->SaveXML(AUTOSAVE(this->m_CurrentFilename));}}
	void OnSave(wxCommandEvent& event){this->SaveXML();}	
	void OnSaveAs(wxCommandEvent& event){this->SaveAs();}
	void OnLoad(wxCommandEvent& event){this->Load();}
	/*void OnExport(wxCommandEvent& event)
	{
		this->Save();
		unsigned int Error = this->m_Tree->CheckForErrors();
		Taxon::ResetExportIDs();
		if (Error != 0)
		{
			//Check for fixable errors
			if (Error & PTreeNode::ERROR_TOO_MANY_CHILDREN)
			{
				ASCC::ASCCRestructureDialog dlg(this,ASCC::ASCCRestructureDialog::ALLOW_COMPOLY,"One or more polytomies were found. How should they be restructured before exporting?");
				int RestructureType = dlg.ShowModal();
				if (RestructureType == wxID_CANCEL){return;}

				if (RestructureType == ASCC::NO_RESTRUCTURE)
				{
					this->m_Tree->RestructurePolytomies(true);
					wxArrayString ChronologicalData = this->m_Tree->GetTreeData();
					while(this->m_CommandProcessor.CanUndo() && this->m_CommandProcessor.IsDirty()){this->m_CommandProcessor.Undo();}

					this->m_Tree->RestructurePolytomies(false);
					wxArrayString ReverseChronologicalData = this->m_Tree->GetTreeData();
					while(this->m_CommandProcessor.CanUndo() && this->m_CommandProcessor.IsDirty()){this->m_CommandProcessor.Undo();}

					if (ChronologicalData.size() != ReverseChronologicalData.size()){wxLogError("OnExport: Tree data array sizes must match");return;}
					for(unsigned int index = 0, size = ChronologicalData.size(); index < size; ++index)
					{
						ChronologicalData[index] = ChronologicalData[index].BeforeFirst('=').Trim() + "Chronological =" + ChronologicalData[index].AfterFirst('=');
						ReverseChronologicalData[index] = ReverseChronologicalData[index].BeforeFirst('=').Trim() + "ReverseChronological =" + ReverseChronologicalData[index].AfterFirst('=');
					}
					if (ASCC::ExportToTNT(ChronologicalData,ReverseChronologicalData)){wxMessageBox("Export Successful","Export Results",wxOK,this);}
					return;
				}
				else
				{
					this->m_Tree->RestructurePolytomies(RestructureType == ASCC::RESTRUCTURE_CHRONOLOGICALLY);
				
					if (this->m_Tree->CheckForErrors() != 0)
					{
						if (wxMessageBox("More errors were found in the tree data shown. Export anyway?","Continue with export?",wxYES_NO|wxICON_EXCLAMATION,this) == wxNO){return;}
					}
				}
			}
			else
			{
				if (wxMessageBox("Errors were encountered in the tree data shown. Export anyway?","Continue with export?",wxYES_NO|wxICON_EXCLAMATION,this) == wxNO){return;}
			}
		}
		wxString TaxaAgeResults;
		if (!Taxon::CheckTaxaAges(&TaxaAgeResults))
		{
			if (wxMessageBox("The following errors were found in the taxa data:\n" + TaxaAgeResults + "\n Export anyway?","Continue with export?",wxYES_NO|wxICON_EXCLAMATION,this) == wxNO){return;}
		}
		wxArrayString Data = this->m_Tree->GetTreeData();
		if (ASCC::ExportToTNT(Data)){wxMessageBox("Export Successful","Export Results",wxOK,this);}
	}*/
	void OnExport(wxCommandEvent& event)
	{
		wxString Filename = wxFileSelector("Select file to export:",wxEmptyString,wxEmptyString,"Taxa/FAD CSV File (*.csv)|*.csv","Taxa/FAD CSV File (*.csv)|*.csv|All Files(*.*)|*.*",wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
		if (Filename.IsEmpty()){return;}
		this->ExportFAD(Filename);
	}
	void OnImport(wxCommandEvent& event)
	{
		wxString Filename = wxFileSelector("Select file to parse:",wxEmptyString,wxEmptyString,"Taxa/FAD CSV File (Taxa Only) (*.csv; *.txt)|*.csv;*.txt","Taxa/FAD CSV File (Taxa Only) (*.csv; *.txt)|*.csv;*.txt|TRE, NEX, TNT files (*.tre;*.nex;*.tnt)|*.tre;*.nex;*.tnt|ASCC Binary (*.ascc)|*.ascc|All Files(*.*)|*.*",wxFD_OPEN);
		if (Filename.IsEmpty()){return;}

		//Before continuing, make sure the ask the user if they want data cleared
		if (!Taxon::GetTaxaNames().IsEmpty() || this->m_Tree->Trees() > 0)
		{
			int Result = wxMessageBox("Would you like to clear the current data (taxa and trees) before importing new data?","Confirmation Needed",wxYES|wxNO|wxCANCEL|wxICON_QUESTION,this);
			if(Result == wxYES)
			{
				this->ClearAllData();
			}
			else if (Result == wxCANCEL)
			{
				return;
			}
		}
		wxString ext = Filename.AfterLast('.').Lower();
		if (ext == "ascc"){this->LoadBinary(Filename); }
		else if (ext == "txt" || ext == "csv"){this->ImportFAD(Filename); }
		else if (ext == "tre" || ext == "nex" || ext == "tnt"){this->ImportTRENEX(Filename); }
		else 
		{
			wxMessageBox("Unable to import files of type '" + ext + "'.\nUnderstood file types:\n\tASCC Binary: *.ascc\n\tFAD/Taxa data: *.csv;*.txt\n\tTRE/NEX data: *.tre;*.nex");
			return;
		}
		this->m_Tree->CheckForErrors();
		ExposureInterface::UpdateAllInterfaces();
		if (this->GetCurrentTaxon().IsEmpty() || !Taxon::FindTaxonByName(this->GetCurrentTaxon())){this->SetCurrentTaxon(Taxon::GetTaxaNames()[0]);}
		this->SendSizeEvent();
		this->m_OKRTree->ScrollToEdge();
		this->CreateMenu();
	}
	void ImportFAD(wxString Filename)
	{
		if (Filename.IsEmpty()){wxMessageBox("No filename given to import"); return;}
		
		bool OverwriteData = true;
		if (Taxon::GetTaxaCount() > 0)
		{
			int Result = wxMessageBox("Overwrite FAD dates with imported data?","Confirmation Needed",wxYES|wxNO|wxCANCEL|wxICON_QUESTION,this);
			if(Result == wxCANCEL){return;}
			OverwriteData = (Result == wxYES);
		}

		wxFileInputStream InputStream(Filename); wxTextInputStream Data(InputStream);
		if (!InputStream.IsOk()){wxMessageBox("Unable to open '" + Filename + "' to import data."); return;}

		wxString Token;
		unsigned int line=0, TaxaChanged = 0;
		wxArrayString Errors, TaxaAdded;
		wxString NextLine;
		while(!InputStream.Eof() && (NextLine = Data.ReadLine()) != "Tree Data")
		{
			if (NextLine.IsEmpty()){continue;}
			if (NextLine == "Taxa"){continue;}
			line++;
			Taxon *FirstTaxon = NULL, *SecondTaxon = NULL;
			bool NewFirstTaxon = false;
			double MinAge = 0.0000, MaxAge = 0.0000;
			
			//Initial Cell is Empty
			Token = NextLine.BeforeFirst(',');
			NextLine = NextLine.AfterFirst(',');

			//First token is taxon name
			Token = NextLine.BeforeFirst(',').Trim(true).Trim(false); NextLine = NextLine.AfterFirst(',');
			if (Token.IsEmpty() || NextLine.IsEmpty()){continue;}

			FirstTaxon = Taxon::FindTaxonByName(Token);
			if(!FirstTaxon){FirstTaxon = new Taxon(); FirstTaxon->Name(Token); NewFirstTaxon = true; TaxaAdded.Add(Token);}

			//Second token is either another taxon name (done) or empty - grab third and fourth tokens for age
			Token = NextLine.BeforeFirst(',').Trim(true).Trim(false); NextLine = NextLine.AfterFirst(',');
			if (Token.IsEmpty()){Token = NextLine.BeforeFirst(',').Trim(true).Trim(false); NextLine = NextLine.AfterFirst(',');}
			//Now we should have *something* for our age - either a same-as or actual ages
			if (Token.IsEmpty()){Errors.Add(wxString::Format("Line %d: %s: Expected to find an age in cell, but cell is empty",line,FirstTaxon->Name())); continue;}
			if (Token.ToDouble(&MinAge))
			{
				//We have a number, so it must mean we have two ages to import
				if (NewFirstTaxon || OverwriteData){FirstTaxon->MinOKR(MinAge); TaxaChanged++;} //Set the first age since it is good
				Token = NextLine.BeforeFirst(',').Trim(true).Trim(false); NextLine = NextLine.AfterFirst(',');
				if (Token.IsEmpty()){Errors.Add(wxString::Format("Line %d: %s: Expected a second age but nothing found",line,FirstTaxon->Name())); continue;}
				if (!Token.ToDouble(&MaxAge))
				{
					Errors.Add(wxString::Format("Line %d: %s: Found Minimum FAD, but Maximum FAD missing or not a number",line,FirstTaxon->Name()));
				}
				//Looks good, set the second age
				else if (NewFirstTaxon || OverwriteData){FirstTaxon->MaxOKR(MaxAge);}
			}
			else
			{
				//We have a name, we must need to group this taxon
				SecondTaxon = Taxon::FindTaxonByName(Token);
				if (!SecondTaxon)
				{
					//The grouped taxon doesn't exist, so let's make it
					SecondTaxon = new Taxon(); SecondTaxon->Name(Token); TaxaAdded.Add(Token);
				}
				if (NewFirstTaxon || OverwriteData){FirstTaxon->SameAsTaxon(Token); TaxaChanged++;}
			}
		}
		wxString ReturnString = wxString::Format("Import Complete: %d Taxa Updated\n\nErrors: %d",(int)(TaxaChanged - TaxaAdded.GetCount()),(unsigned int)Errors.GetCount());
		for (unsigned int index = 0, size = Errors.GetCount(); index < size; ++index)
		{
			ReturnString += "\n" + Errors.Item(index);
		}
		ReturnString += wxString::Format("\n\nTaxa Added: %d",(unsigned int)TaxaAdded.GetCount());
		for (unsigned int index = 0, size = TaxaAdded.GetCount(); index < size; ++index)
		{
			ReturnString += "\n" + TaxaAdded.Item(index);
		}
		
		wxMessageBox(ReturnString,"Import Results",wxOK|wxICON_INFORMATION);
		wxLogStatus("Finished importing " + Filename);
	}
	void ImportTRENEX(wxString Filename)
	{
		if (Filename.IsEmpty()){wxMessageBox("No filename given to import"); return;}
		int Result = wxMessageBox("Remove taxa not found in a tree?","Confirmation Needed",wxYES|wxNO|wxCANCEL|wxICON_QUESTION,this);
		if(Result == wxCANCEL){return;}
		bool RemoveTaxa = (Result == wxYES);
		wxString RemovedTaxa;

		wxArrayString PreimportTaxa = Taxon::GetTaxaNames();
		wxString WithoutAsterisk;
		for(unsigned int index = 0, size = PreimportTaxa.size(); index < size; ++index)
		{
			if (PreimportTaxa[index].EndsWith("*",&WithoutAsterisk)){PreimportTaxa[index] = WithoutAsterisk;}
		}

		wxArrayString Data = ASCCImport(Filename,true);
		wxArrayString Taxa = Taxon::GetTaxaNames();
		if (!Data.IsEmpty())
		{
			this->m_Tree->AddTree(Data);
			wxArrayString Leaves; this->m_Tree->GetLeaves(Leaves);
			for(unsigned int index = 0, size = Taxa.size(); index < size; ++index)
			{
				if (Taxa[index].EndsWith("*",&WithoutAsterisk)){Taxa[index] = WithoutAsterisk;}
				if (Leaves.Index(Taxa[index]) == wxNOT_FOUND && PreimportTaxa.Index(Taxa[index]) == wxNOT_FOUND && RemoveTaxa)
				{
					RemovedTaxa += Taxa[index] + "\n";
					Taxon::FindTaxonByName(Taxa[index])->Destroy();
				}
			}
			Taxa = Taxon::GetTaxaNames();
		}
		if (!this->m_DataPanel->GetObject() && Taxa.size() > 0)
		{
			this->SetCurrentTaxon(Taxa[0]);
		}
		if (!RemovedTaxa.IsEmpty()){wxMessageBox(wxString("Removed Taxa:\n") + RemovedTaxa);}
		wxLogStatus("Finished importing " + Filename);
	}
	void ExportFAD(wxString Filename)
	{
		if (Filename.IsEmpty()){wxMessageBox("No filename given to import"); return;}
		wxFileOutputStream OutputStream(Filename); wxTextOutputStream Data(OutputStream);
		wxSortedArrayString TaxaNames = Taxon::GetTaxaNames(false);
		Taxon *CurrentTaxon = NULL;
		wxString CurrentName;
		Data << "\nTaxa\n";
		for (unsigned int index = 0, size = TaxaNames.GetCount(); index < size; index++)
		{
			CurrentName = TaxaNames.Item(index);
			CurrentTaxon = Taxon::FindTaxonByName(CurrentName);
			Data << "," << CurrentName << ",";
			if (CurrentTaxon->IsSameAs())
			{
				Data << CurrentTaxon->SameAsTaxon();
			}
			Data << "," << CurrentTaxon->MinOKR() << ",";
			Data << CurrentTaxon->MaxOKR() << "\n";
		}
		Data << "\nTree Data\n";
		wxString TreeName, TreeData;
		wxArrayString TreeNameData = this->m_Tree->GetTreeNameData(), TreeNames = this->m_Tree->TreeNames();
		for (unsigned int index = 0, size = this->m_Tree->Trees(); index < size; index++)
		{
			TreeName = TreeNames[index];
			TreeData = TreeNameData[index];
			Data << "," << TreeName << "," << TreeData.AfterFirst('=') << "\n";
		}
		wxLogStatus("Taxa/FAD data successfully written to " + Filename);
	}
	void OnExit(wxCommandEvent& event)
	{
		this->Close();
	}
	void OnClose(wxCloseEvent& event)
	{
		if (this->m_CommandProcessor.IsDirty())
		{
			int Result = wxMessageBox("Would you like to save this file before exiting?", "Save?", wxYES|wxNO|(event.CanVeto()?wxCANCEL:0), this);
			if (Result == wxYES){this->SaveXML();}
			else if (Result == wxCANCEL){event.Veto(); return;}
			else if (!this->m_CurrentFilename.IsEmpty() && wxFileExists(AUTOSAVE(this->m_CurrentFilename))){wxRemoveFile(AUTOSAVE(this->m_CurrentFilename));}
		}
		this->Destroy();
	}
	void OnUndo(wxCommandEvent& event){this->m_CommandProcessor.Undo(); this->UpdateFilenameStatus();}
	void OnRedo(wxCommandEvent& event){this->m_CommandProcessor.Redo(); this->UpdateFilenameStatus();}
	void OnViewWindow(wxCommandEvent& event)
	{
		switch(event.GetId())
		{
		case(wxID_VIEW_LIST):{this->m_AuiManager.GetPane("TaxaList").Show(true); this->m_AuiManager.Update(); break;}
		case(wxID_VIEW_DETAILS):{this->m_AuiManager.GetPane("TaxaData").Show(true); this->m_AuiManager.Update(); break;}
		case(wxID_VIEW_SORTDATE):{this->m_AuiManager.GetPane("OKRTree").Show(true); this->m_AuiManager.Update(); break;}
		case(wxID_VIEW_SORTTYPE):{this->m_AuiManager.GetPane("PTreeCtrl").Show(true); this->m_AuiManager.Update(); break;}
		}
	}
	void OnDefaultLayout(wxCommandEvent& event){this->m_AuiManager.LoadPerspective(this->m_DefaultPerspective); this->m_OKRTree->ScrollToEdge();}
	void OnAnalyzeScores(wxCommandEvent& event)
	{
		wxString Result = AnalyzeTNTScores();
		if (!Result.IsEmpty()){wxMessageBox(Result,"TNT Score Analysis Complete",wxOK,this);}
	}
	void Restructure(PTreeNode* Node = NULL)
	{
		ASCCRestructureDialog dlg(this, ASCCRestructureDialog::DENY_COMPOLY);
		int ReturnValue = dlg.ShowModal();
		if (ReturnValue == wxID_CANCEL){return;}
		if (!Node){this->m_Tree->RestructurePolytomies(ReturnValue == RESTRUCTURE_CHRONOLOGICALLY);}
		else {Node->RestructurePolytomies(ReturnValue == RESTRUCTURE_CHRONOLOGICALLY);}

		if (this->m_Config.Read("/Options/LadderizeAfterRestructuring",true))
		{
			if (!Node){this->m_Tree->Ladderize();}
		}
	}
	void OnNodeRestructure(wxCommandEvent& event)
	{
		this->Restructure(((PTreeNode*)event.GetClientData()));
	}
	void OnRestructure(wxCommandEvent& event)
	{
		if (this->m_Tree->Trees() == 0){return;}

		this->Restructure();
	}
	void OnCheckTaxa(wxCommandEvent& event)
	{
		wxString TaxaAgeErrors, TaxaAgeWarnings;
		if (!Taxon::CheckTaxaAges(&TaxaAgeErrors, &TaxaAgeWarnings))
		{
			wxMessageBox("The following errors were found in the taxa data:\n\n" + TaxaAgeErrors,"Taxa Errors",wxICON_EXCLAMATION|wxOK,this);
		}
		else if (!TaxaAgeWarnings.IsEmpty())
		{
			wxMessageBox("The following warnings were found in the taxa data:\n\n" + TaxaAgeWarnings,"Taxa Warnings",wxOK,this);
		}
		else
		{
			wxMessageBox("No errors found");
		}
	}
	void OnTaxaSelect(wxCommandEvent& event)
	{
		this->SetCurrentTaxon(event.GetString());
	}
	void OnCommandSubmitted(wxCommandEvent& event){this->UpdateFilenameStatus();}
	void NewTaxon()
	{
		wxString TaxonName = ::wxGetTextFromUser("New Taxon Name:");
		if (TaxonName.IsEmpty()){return;}
		if (Taxon::FindTaxonByName(TaxonName)){wxMessageBox("A Taxon with the name '" + TaxonName + "' already exists"); return;} 
		this->m_CommandProcessor.Submit(new TC_Add(TaxonName));
		ExposureInterface::UpdateAllInterfaces();		
	}
	wxString GetCurrentTaxon()
	{
		if (this->m_DataPanel)
		{
			Taxon *Obj = dynamic_cast<Taxon*>(this->m_DataPanel->GetObject());
			if (Obj)
			{
				wxString Name = Obj->Name();
				if (Obj->MaxOKR() <= 0.0000000 && !Obj->AcceptableOKR()){Name += "*";}
				return(Name);
			}
		}
		return(wxEmptyString);
	}
	void SetCurrentTaxon(wxString NewTaxon)
	{
		Taxon *obj = Taxon::FindTaxonByName(NewTaxon); 
		if(obj)
		{
			this->ChangeObject(obj);
		}
	}
	void ChangeObject(Taxon *newObject)
	{
		this->m_DataPanel->ChangeObject(newObject);
		if (newObject)
		{
			this->m_OKRTree->MarkAgeSelection(newObject->MinOKR(),newObject->MaxOKR(),true);
			this->m_TaxaList->UpdateInterface();
		}
	}
	void OnTaxaChange(wxCommandEvent& event)
	{
		this->m_TaxaList->Update();//Interface();// this->m_TaxaList->Refresh();
		this->m_DataPanel->UpdateTaxa();  //this->m_DataPanel->Refresh();
		this->SendSizeEvent();
	}
	void DeleteTaxon()
	{
		Taxon *Obj = dynamic_cast<Taxon*>(this->m_DataPanel->GetObject()); 
		if (Obj)
		{
			if (Taxon::GetTaxaNames().size() > 1)
			{
				wxArrayString Taxa  = Taxon::GetTaxaNames();
				wxString TopTaxon = Taxa[0];
				if (TopTaxon.EndsWith("*")){TopTaxon = TopTaxon.BeforeLast('*');}
				this->SetCurrentTaxon(Obj->Name() != TopTaxon?Taxa[0]:Taxa[1]);
			}
			else
			{
				this->SetCurrentTaxon(wxEmptyString);
			}
			
			ASCCCommand *Command = new TC_Delete(Obj);
			ASCCCommand *Result = NULL;
			if (!Obj->Name().IsEmpty() && this->m_Tree)
			{
				Result = this->m_Tree->DeleteNodes(Obj->Name());
				if (Result){Command->Link(Result);}
			}
			this->m_CommandProcessor.Submit(Command);
		}
	}
	void OnTaxaRename(wxCommandEvent& event)
	{
		wxString Name  = event.GetString().BeforeFirst(';');
		wxString NewName = event.GetString().AfterFirst(';');
		if (!Name.IsEmpty() && !NewName.IsEmpty())
		{
			this->m_Tree->UpdateLabel(Name,NewName);
			this->m_TaxaList->UpdateInterface();
			this->m_DataPanel->UpdateTaxa();
		}
	}
	void OnOKRSelection(OKREvent& event)
	{
		Taxon *Obj = this->m_DataPanel->GetObject(); 
		if (Obj)
		{
			this->m_CommandProcessor.Submit(new TC_CopyOKR(Obj,*event.GetOKR()));
		}
	}
	void OnNewLeaf(wxCommandEvent& event){this->m_TaxaList->Update();}
	void OnOKRChanged(wxCommandEvent& event)
	{
		this->m_TaxaList->Update();
		this->m_DataPanel->Update();
		Taxon *Obj = dynamic_cast<Taxon*>(this->m_DataPanel->GetObject()); 
		if (Obj)
		{
			this->m_OKRTree->MarkAgeSelection(Obj->MinOKR(),Obj->MaxOKR(),true);
		}
	}
	void OnAbout(wxCommandEvent& event)
	{
		wxAboutDialogInfo info;
		info.SetName("ASCC");
		info.SetVersion(ASCC_VERSION);
		info.SetCopyright(wxString::Format("Copyright %d Clint Boyd",wxDateTime::GetCurrentYear()));
		info.SetWebSite("http://stratfit.org/ASCC/","The ASCC Program Suite");
		info.SetDescription("ASCC - Assistance with Stratigraphic Consistency Calculations\n"	\
							"\n"	\
							"Written By Nico Marrero\n"	\
							"Citation:\n"	\
							"  Boyd, C.A., Cleland, T. P., Marrero, N. L., and Clarke, J. A. in review.\n"	\
							"  Exploring the Effects of Phylogenetic Uncertainty and Consensus Trees\n"	\
							"  on Stratigraphic Consistency Scores: a New Program and a\n"	\
							"  Standardized Method Proposed. Cladistics.\n" \
							"\n" \
							"ASCC uses a modified version of the wxMathPlot library from \n" \
							"  http://wxmathplot.sourceforge.net/ by David Schalig and Davide Rondini. "); 
		wxGenericAboutBox(info);
	}
	void OnOpenFAQ(wxCommandEvent& event)
	{
		if (!::wxLaunchDefaultBrowser("http://www.stratfit.org/ASCC/FAQ.shtml",wxBROWSER_NEW_WINDOW))
		{
			wxMessageBox("Unable to launch default browser to view ASCC FAQ.\n\nThe ASCC FAQ can be found at http://www.stratfit.org/ASCC/FAQ.shtml");
		}
	}
	void OnRemoveAutomaticConstraints(wxCommandEvent& event)
	{
		Taxon::RemoveAutomaticLinks();
		this->m_DataPanel->UpdateTaxa(); this->m_DataPanel->Refresh();
		this->SendSizeEvent();
	}
	void OnRecentFile(wxCommandEvent& event)
	{
		wxString Filename(this->m_FileHistory.GetHistoryFile(event.GetId() - wxID_FILE1));
		if (!Filename.IsEmpty()){this->LoadXML(Filename);}
	}
	void OnCalculateScores(wxCommandEvent& event)
	{
		ASCCDataAnalysis *data = new ASCCDataAnalysis();
		data->Replicates(this->m_Config.Read("/Options/DefaultReplicates",1000));
		data->ShowMaxAgeWarning(this->m_Config.ReadBool("/Options/ShowMaxAgeWarnings",true));
		data->SetScoreMIG(this->m_Config.ReadBool("/Options/UseMIG",true));
		data->SetScoreMSM(this->m_Config.ReadBool("/Options/UseMSM",true));
		data->SetScoreGER(this->m_Config.ReadBool("/Options/UseGER",true));
		data->SetScoredAGL(this->m_Config.ReadBool("/Options/UsedAGL",true));
		wxExposureDialog dlg(this,data,"ASCC Data Analysis Setup");
		dlg.CentreOnParent();
		int Result;
		while((Result = dlg.ShowModal()) != wxID_CANCEL && !data->GetScoreMIG() && !data->GetScoreMSM() && !data->GetScoreGER() && !data->GetScoredAGL()){wxMessageBox("At least one metric must be selected to continue, otherwise press cancel to exit score calculation","Score Calculation Configuration Error");}
		if (Result == wxID_CANCEL){wxDELETE(data); return;}
		this->m_Config.Write("/Options/DefaultReplicates",data->Replicates());
		this->m_Config.Write("/Options/ShowMaxAgeWarnings",data->ShowMaxAgeWarning());
		this->m_Config.Write("/Options/UseMIG",data->GetScoreMIG());
		this->m_Config.Write("/Options/UseMSM",data->GetScoreMSM());
		this->m_Config.Write("/Options/UseGER",data->GetScoreGER());
		this->m_Config.Write("/Options/UsedAGL",data->GetScoredAGL());

		wxString TaxaAgeErrors, TaxaAgeWarnings;
		if (!Taxon::CheckTaxaAges(&TaxaAgeErrors,&TaxaAgeWarnings,data->ShowMaxAgeWarning()))
		{
			wxMessageBox("The following errors were found in the taxa data:\n" + TaxaAgeErrors + "\n They must be fixed before proceeding\n","Errors in Taxa");
			return;
		}
		if (!TaxaAgeWarnings.IsEmpty())
		{
			if (wxMessageBox("The following warnings were found in the taxa data:\n" + TaxaAgeWarnings + "\n Continue anyway?\n","Taxa Warnings",wxYES | wxNO | wxCENTRE) == wxNO)
			{
				return;
			}
		}
		bool Finished = true;
		unsigned int Error = this->m_Tree->CheckForErrors();
		
		if (Error != 0)
		{
			//Check for fixable errors
			if (Error & PTreeNode::ERROR_TOO_MANY_CHILDREN)
			{
				ASCCRestructureDialog dlg(this,ASCCRestructureDialog::ALLOW_COMPOLY,"One or more polytomies were found. How should they be restructured?");
				int RestructureType = dlg.ShowModal();
				if (RestructureType == wxID_CANCEL){wxDELETE(data); return;}

				
				//To do this, we get the tree data and then enter it into a new PTreectrl
				wxWindowDisabler disable;
				wxBusyInfo *busy = new wxBusyInfo("Restructuring trees...",this);

				PTreeCtrl *dummy = new PTreeCtrl(this,wxID_ANY);
				PTreeNode *TempNode = NULL, *RevChronNode = NULL;
				for(unsigned int index = 0, size = this->m_Tree->Trees(); index < size; ++index)
				{
					TempNode = new PTreeNode(this->m_Tree->GetTree(index),dummy);
					if (TempNode->Child(0)->CheckForErrors() & PTreeNode::ERROR_TOO_MANY_CHILDREN)
					{
						//This is one of the nodes we are restructuring
						if (RestructureType != RESTRUCTURE_REVERSE_CHRONOLOGICALLY)
						{
							TempNode->RestructurePolytomies(true,false,false); 
							if (RestructureType == RESTRUCTURE_CHRONOLOGICALLY) {TempNode->SetLabel("Chronological " + TempNode->GetLabel());}
							else {TempNode->SetLabel("__compoly_chron " + TempNode->GetLabel());}
							dummy->AddTree(TempNode);
							TempNode = NULL;
						}
						if (RestructureType != RESTRUCTURE_CHRONOLOGICALLY)
						{
							if (!TempNode){TempNode = new PTreeNode(this->m_Tree->GetTree(index),dummy);}
							TempNode->RestructurePolytomies(false,false,false);
							if (RestructureType == RESTRUCTURE_REVERSE_CHRONOLOGICALLY) {TempNode->SetLabel("Reverse Chronological " + TempNode->GetLabel());}
							else {TempNode->SetLabel("__compoly_reverse_chron " + TempNode->GetLabel());}
							dummy->AddTree(TempNode);
						}
					}
					else {dummy->AddTree(TempNode);} //Don't have to call layoutchildren() as these have already been layed out
				}
				wxDELETE(busy);
				Finished = data->CalculateScores(dummy);
				wxDELETE(dummy);
			}
			else
			{
				wxMessageBox("Errors were encountered in the tree data shown. They must be fixed before proceeding","Errors in Tree");
				return;
			}
		}
		else {Finished = data->CalculateScores(this->m_Tree);}
		
		if (Finished)
		{
			ASCCDataAnalysisWindow *win = new ASCCDataAnalysisWindow(data);
			win->Show();
		}
		else {wxDELETE(data);}
	}
	void CreateMenu()
	{
		wxMenuItem *MenuItem = NULL;
		wxMenu *FileMenu = new wxMenu();
		MenuItem = new wxMenuItem(FileMenu,wxID_NEW); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_NEW)); FileMenu->Append(MenuItem);
		MenuItem = new wxMenuItem(FileMenu,wxID_OPEN); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_FILE_OPEN)); FileMenu->Append(MenuItem);
		MenuItem = new wxMenuItem(FileMenu,wxID_SAVE); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_FILE_SAVE)); FileMenu->Append(MenuItem);
		MenuItem = new wxMenuItem(FileMenu,wxID_SAVEAS); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_FILE_SAVE_AS)); FileMenu->Append(MenuItem);
		FileMenu->AppendSeparator();
		FileMenu->Append(MENUID_IMPORT,"&Import\tCtrl+I","Import data (Tre/Nex, Taxa/FAD CSV, Old Binary ASCC Files)");
		FileMenu->Append(MENUID_EXPORT,"&Export Data\tCtrl+E","Export Taxa/FAD/Tree data to CSV");
		FileMenu->AppendSeparator();
		if (this->m_FileHistory.GetCount() > 0)
		{
			wxMenu *RecentFiles = new wxMenu();
			this->m_FileHistory.AddFilesToMenu(RecentFiles);
			FileMenu->AppendSubMenu(RecentFiles,"Recent Files");
			FileMenu->AppendSeparator();
		}
		MenuItem = new wxMenuItem(FileMenu,wxID_EXIT,"E&xit\tCTRL+X"); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_QUIT)); FileMenu->Append(MenuItem);
		
		wxMenu *ToolsMenu = new wxMenu();
				MenuItem = new wxMenuItem(ToolsMenu,MENUID_ANALYZESCORES,"Analyze TNT Scores","Analyzes scores from TNT outputs reporting the min-max of each"); ToolsMenu->Append(MenuItem);
				bool ToolsEnabled = Taxon::GetTaxaCount() > 0 && this->m_Tree->Trees() > 0;
				MenuItem = new wxMenuItem(ToolsMenu,MENUID_CHECKTAXA,"Check Taxa for Errors","Looks for any errors in the taxa data"); ToolsMenu->Append(MenuItem);
					MenuItem->Enable(ToolsEnabled);
				MenuItem = new wxMenuItem(ToolsMenu,MENUID_RESTRUCTURE,"Restructure All Polytomies","Restructures all polytomies found in the tree data"); ToolsMenu->Append(MenuItem);
					MenuItem->Enable(ToolsEnabled);
				MenuItem = new wxMenuItem(ToolsMenu,MENUID_REMOVEAUTOMATICCONSTRAINTS,"Remove Automatic Constraints","Removes age links based on the time scale (not other taxa)"); ToolsMenu->Append(MenuItem);
					MenuItem->Enable(ToolsEnabled);
				ToolsMenu->AppendSeparator();
				MenuItem = new wxMenuItem(ToolsMenu,MENUID_CALCULATESCORES,"Calculate Scores","Calculates various scores for the current tree data");
					MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_FIND)); 
					ToolsMenu->Append(MenuItem);
					MenuItem->Enable(ToolsEnabled);

		wxMenuBar *Menu = this->GetMenuBar();
		if (!Menu)
		{		
			wxMenu *HelpMenu = new wxMenu();
				MenuItem = new wxMenuItem(HelpMenu,wxID_ABOUT); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_HELP,wxART_MENU)); HelpMenu->Append(MenuItem);
				MenuItem = new wxMenuItem(HelpMenu,wxID_HELP_INDEX,"Web FAQ","Open ASCC's faq (requires an internet connection)"); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_HELP_BOOK,wxART_MENU)); HelpMenu->Append(MenuItem);
		
			wxMenu *EditMenu = new wxMenu();
				MenuItem = new wxMenuItem(EditMenu,wxID_UNDO); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_UNDO)); EditMenu->Append(MenuItem);
				MenuItem = new wxMenuItem(EditMenu,wxID_REDO); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_REDO)); EditMenu->Append(MenuItem);
				this->m_CommandProcessor.SetEditMenu(EditMenu);

			wxMenu *ViewMenu = new wxMenu();
				MenuItem = new wxMenuItem(ViewMenu,wxID_VIEW_LIST,"Taxa List"); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_LIST_VIEW)); ViewMenu->Append(MenuItem);
				MenuItem = new wxMenuItem(ViewMenu,wxID_VIEW_DETAILS,"Taxa Data"); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_REPORT_VIEW)); ViewMenu->Append(MenuItem);
				MenuItem = new wxMenuItem(ViewMenu,wxID_VIEW_SORTDATE,"Geologic Time Scale"); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_HELP_SIDE_PANEL)); ViewMenu->Append(MenuItem);
				MenuItem = new wxMenuItem(ViewMenu,wxID_VIEW_SORTTYPE,"Tree Data"); MenuItem->SetBitmap(wxArtProvider::GetBitmap(wxART_HELP_PAGE)); ViewMenu->Append(MenuItem);
				ViewMenu->AppendSeparator();
				ViewMenu->Append(wxID_RESET, "Default Layout", "Returns the current layout to the default");

			Menu = new wxMenuBar(); this->SetMenuBar(Menu);
			Menu->Append(FileMenu,"File");
			Menu->Append(EditMenu,"Edit");
			Menu->Append(ViewMenu,"View");
			Menu->Append(ToolsMenu,"Tools");
			Menu->Append(HelpMenu,"Help");
		}
		else
		{
			delete Menu->Replace(0,FileMenu,"File");
			delete Menu->Replace(3,ToolsMenu,"Tools");
		}
	}

	wxConfig m_Config;
	wxFileHistory m_FileHistory;
	TaxaDataWindow *m_DataPanel;
	wxExposurePanel *m_TaxaList;
	OKRSelectionTree *m_OKRTree;
	PTreeCtrl *m_Tree;
	wxString m_CurrentFilename;
	wxAuiManager m_AuiManager;
	wxString m_DefaultPerspective;
	wxTimer m_Timer;
	ASCCCommandProcessor m_CommandProcessor;

	DECLARE_EVENT_TABLE();
	DECLARE_EXPOSURE_TABLE();
};

BEGIN_EVENT_TABLE(ASCCMainFrame,wxFrame)
	EVT_MENU(wxID_NEW, ASCCMainFrame::OnNew)
	EVT_MENU(wxID_SAVE, ASCCMainFrame::OnSave)
	EVT_MENU(wxID_SAVEAS, ASCCMainFrame::OnSaveAs)
	EVT_MENU(wxID_OPEN, ASCCMainFrame::OnLoad)
	EVT_MENU(MENUID_IMPORT, ASCCMainFrame::OnImport)
	EVT_MENU(MENUID_EXPORT, ASCCMainFrame::OnExport)
	EVT_MENU(wxID_EXIT, ASCCMainFrame::OnExit)
	EVT_CLOSE(ASCCMainFrame::OnClose)
	EVT_MENU(wxID_ABOUT, ASCCMainFrame::OnAbout)
	EVT_MENU(wxID_HELP_INDEX, ASCCMainFrame::OnOpenFAQ)
	EVT_MENU(MENUID_ANALYZESCORES, ASCCMainFrame::OnAnalyzeScores)
	EVT_MENU_RANGE(wxID_FILE1, wxID_FILE9, ASCCMainFrame::OnRecentFile)
	EVT_MENU(wxID_UNDO,ASCCMainFrame::OnUndo)
	EVT_MENU(wxID_REDO,ASCCMainFrame::OnRedo)
	EVT_MENU(wxID_VIEW_LIST,ASCCMainFrame::OnViewWindow)
	EVT_MENU(wxID_VIEW_DETAILS,ASCCMainFrame::OnViewWindow)
	EVT_MENU(wxID_VIEW_SORTDATE,ASCCMainFrame::OnViewWindow)
	EVT_MENU(wxID_VIEW_SORTTYPE,ASCCMainFrame::OnViewWindow)
	EVT_MENU(wxID_RESET,ASCCMainFrame::OnDefaultLayout)
	EVT_MENU(MENUID_RESTRUCTURE,ASCCMainFrame::OnRestructure)
	EVT_MENU(MENUID_CHECKTAXA,ASCCMainFrame::OnCheckTaxa)
	EVT_MENU(MENUID_CALCULATESCORES, ASCCMainFrame::OnCalculateScores)
	EVT_MENU(MENUID_REMOVEAUTOMATICCONSTRAINTS, ASCCMainFrame::OnRemoveAutomaticConstraints)
	EVT_COMMAND(wxID_ANY,EVT_TREE_SELECT_NODE,ASCCMainFrame::OnTaxaSelect)
	EVT_COMMAND(wxID_ANY,EVT_TAXON_SELECT,ASCCMainFrame::OnTaxaSelect)
	EVT_COMMAND(wxID_ANY,EVT_TAXA_RENAME,ASCCMainFrame::OnTaxaRename)
	EVT_COMMAND(wxID_ANY,EVT_TAXA_DELETE,ASCCMainFrame::OnTaxaChange)
	EVT_COMMAND(wxID_ANY,EVT_TAXA_ADD,ASCCMainFrame::OnTaxaChange)
	EVT_COMMAND(wxID_ANY,EVT_OKR_CHANGED,ASCCMainFrame::OnOKRChanged)
	EVT_COMMAND(wxID_ANY,EVT_PHYLO_LEAF,ASCCMainFrame::OnNewLeaf)
	EVT_COMMAND(wxID_ANY,EVT_COMMAND_SUBMITTED,ASCCMainFrame::OnCommandSubmitted)
	EVT_COMMAND(wxID_ANY,EVT_TREE_RESTRUCTURE,ASCCMainFrame::OnNodeRestructure)
	EVT_OKR_SELECTION(wxID_ANY,ASCCMainFrame::OnOKRSelection)
	EVT_TIMER(ID_AUTOSAVE,ASCCMainFrame::OnAutoSave)
END_EVENT_TABLE()

BEGIN_EXPOSURE_TABLE(ASCCMainFrame,"The list of taxa")
	//EXPOSE_DYNAMIC_LIST(Taxa, "", &ASCCMainFrame::GetCurrentTaxon, &ASCCMainFrame::SetCurrentTaxon, &Taxon::GetTaxaNames, 
	//				&Taxon::GetTaxaDescriptions, &Taxon::GetTaxaChanged1, "")
	EXPOSE_LIST("Taxa","",&ASCCMainFrame::GetCurrentTaxon, &ASCCMainFrame::SetCurrentTaxon,"")->Dynamic(&Taxon::GetTaxaChanged1,
					&Taxon::GetTaxaCount,&Taxon::_GetTaxaNames,&Taxon::_GetTaxaNames,NULL)->AlignCenter()
	EXPOSURE_ROW()
	EXPOSURE_GROUP()
		EXPOSE_BUTTON("AddTaxon","New Taxon",&ASCCMainFrame::NewTaxon,"Adds a new Taxon to the Taxa List")
		EXPOSE_BUTTON("DeleteTaxon","Delete Taxon",&ASCCMainFrame::DeleteTaxon,"Deletes this Taxon from the Taxa List")
	EXPOSURE_END_GROUP()
END_EXPOSURE_TABLE()

DECLARE_APP(ASCCApp);
IMPLEMENT_APP(ASCCApp)
bool ASCCApp::OnInit()
{
	//Parse command line
	wxString FilenameToOpen;
	wxCmdLineParser cmdParser(g_cmdLineDesc, argc, argv);
	cmdParser.Parse(false);
	if (cmdParser.GetParamCount() > 0)
	{
		wxFileName fName(cmdParser.GetParam(0));
		fName.Normalize(wxPATH_NORM_LONG|wxPATH_NORM_DOTS|wxPATH_NORM_TILDE|wxPATH_NORM_ABSOLUTE);
		FilenameToOpen = fName.GetFullPath();
	}
	//Create main app window
	::wxInitAllImageHandlers();
	ASCCMainFrame *frame = new ASCCMainFrame();
	wxIcon icon(ASCCIcon_xpm);
	frame->SetIcon(icon);
	frame->Show(true);
	if (!FilenameToOpen.IsEmpty()){frame->Load(FilenameToOpen);}
	return(true);
}

ASCCApp::~ASCCApp()
{

}

