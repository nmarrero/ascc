__ASCC requires the MS visual c++ runtime to be installed. This can be downloaded from 
[http://www.microsoft.com/en-us/download/details.aspx?id=30679](http://www.microsoft.com/en-us/download/details.aspx?id=30679)__


While stratigraphic consistency metrics can be extremely useful for comparing phylogenetic hypotheses 
to the stratigraphic record of first appearances (and to alternative phylogenetic hypotheses), the 
process of calculating these metrics has become increasingly tedious as authors have improved upon 
their performance. Creating the necessary files was difficult, especially for researchers unfamiliar 
with the programming language used by TNT (Tree analysis using New Technology; Goloboff et al., 2008). 

We have sought to simplify the calculation of the GER range, MIG range, and MSM\* range as well as the 
all new delta AGL range metrics in hope of encouraging more researchers to calculate these informative 
statistics.

Version 4.0.0 of ASCC incorporates performing the calculations of the metrics above directly and displays 
histograms of pairwise differences when multiple trees are entered. Progress is slowly being made towards 
cleaning up the code and getting it fully commented so others can expand on what we've started building here. 
We're not done yet, so while you can still access the source code here, you do so at your own eye-bleeding risk :-)

Note: When performing calculations, depending on the number of replicates used, ASCC can use a lot of RAM. 
One test performed with one million replicates using 4 trees consumed over 4 gigs of RAM. 32-bit users should 
definitely keep this in mind as using this much RAM will cause windows to have to use virtual memory and that 
will slow down the program (on top of the time it takes to run tons of replicates).

ASCC has precompiled binaries for Windows and we have recently gotten ask building and running in Linux. We are
still testing the Linux builds but when they are ready for users to run we will upload a Makefile for building in Linux
(and a Visual Studio solution for building in Windows).

Developers Note: ASCC uses the Exposure GUI rapid-prototyping library 
([http://bitbucket.org/nmarrero/exposure](http://bitbucket.org/nmarrero/exposure)).
A copy of Exposure is required for anyone wanting to build ASCC.