/*	ASCCCommand.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ASCC_COMMAND_HEADER__
#define __ASCC_COMMAND_HEADER__

#include <wx/wx.h>
#include <wx/cmdproc.h>
#include "OKR.h"

DECLARE_EVENT_TYPE(EVT_TAXA_RENAME,-1);		//One of the taxa have been renamed
DECLARE_EVENT_TYPE(EVT_TAXA_DELETE,-1);
DECLARE_EVENT_TYPE(EVT_TAXA_ADD,-1);
DECLARE_EVENT_TYPE(EVT_OKR_CHANGED,-1);
DECLARE_EVENT_TYPE(EVT_PHYLO_LEAF,-1);
DECLARE_EVENT_TYPE(EVT_COMMAND_SUBMITTED,-1);

class ASCCCommandProcessor : public wxCommandProcessor
{
public:
	ASCCCommandProcessor(int maxCommands = -1):wxCommandProcessor(maxCommands){}
	virtual bool Submit(wxCommand *command, bool storeIt = true)
	{
		if (wxCommandProcessor::Submit(command,storeIt))
		{
			wxCommandEvent evt(EVT_COMMAND_SUBMITTED);
			wxTheApp->GetTopWindow()->wxEvtHandler::AddPendingEvent(evt);
			return(true);
		}
		return(false);
	}
	virtual void SetMenuStrings()
	{
	#if wxUSE_MENUS
		if (m_commandEditMenu)
		{
			wxString undoLabel = "Undo";//GetUndoMenuLabel();
			wxString redoLabel = "Redo";//GetRedoMenuLabel();

			if (m_currentCommand)
			{
				wxCommand *command = (wxCommand *)m_currentCommand->GetData();
				undoLabel = "Undo " + command->GetName();

				if (m_currentCommand->GetNext())
				{
					wxCommand *redoCommand = (wxCommand *)m_currentCommand->GetNext()->GetData();
					redoLabel = "Redo " + redoCommand->GetName();
				}
			}

			m_commandEditMenu->SetHelpString(wxID_UNDO, undoLabel);
			m_commandEditMenu->Enable(wxID_UNDO, CanUndo());

			m_commandEditMenu->SetHelpString(wxID_REDO, redoLabel);
			m_commandEditMenu->Enable(wxID_REDO, CanRedo());
		}
	#endif // wxUSE_MENUS
	}
	virtual bool IsDirty() const
    {
        return ((m_currentCommand && (m_lastSavedCommand != m_currentCommand)) || (!m_currentCommand && m_lastSavedCommand));
    }
	DECLARE_CLASS(ASCCCommandProcessor);
};

class ASCCCommand : public wxCommand
{
public:
	ASCCCommand(wxString CommandName):wxCommand(true,CommandName),m_LinkedNode(NULL){}
	ASCCCommand(const ASCCCommand& other):wxCommand(other.CanUndo(),other.GetName()),m_LinkedNode(other.m_LinkedNode?other.m_LinkedNode->Clone():NULL){}
	virtual ~ASCCCommand(){wxDELETE(this->m_LinkedNode);}

	virtual ASCCCommand *Clone() const = 0;
	virtual bool Do()
	{
		bool Result = this->OnDo();
		if (this->m_LinkedNode && !this->m_LinkedNode->Do())
		{
			ASCCCommand *Node = this->m_LinkedNode; this->m_LinkedNode = Node->Link(); Node->m_LinkedNode = NULL; delete Node;
		}
		return(Result);
	}
	virtual bool Undo()
	{
		bool Result = true;
		if (this->m_LinkedNode){Result &= this->m_LinkedNode->Undo();}
		Result &= this->OnUndo();
		return(Result);
	}
	virtual ASCCCommand& Link(const ASCCCommand& linkedNode)
	{
		ASCCCommand *Node = this;
		while(Node->m_LinkedNode){Node = Node->m_LinkedNode;}
		Node->m_LinkedNode = linkedNode.Clone();
		return(*this);
	}
	virtual ASCCCommand& Link(ASCCCommand *linkedNode)
	{
		ASCCCommand *Node = this;
		while(Node->m_LinkedNode){Node = Node->m_LinkedNode;}
		Node->m_LinkedNode = linkedNode;
		return(*this);
	}
	ASCCCommand *Link() const {return(this->m_LinkedNode);}
	bool IsKindOf(const wxClassInfo *info) const
	{
		const ASCCCommand *Cmd = this;
		while(Cmd->Link())
		{
			Cmd = Cmd->Link();
			if (Cmd->IsKindOf(info)){return(true);}
		}
		return(wxObject::IsKindOf(info));
	}
protected:
	void DispatchEvent(wxCommandEvent& evt){wxTheApp->GetTopWindow()->wxEvtHandler::AddPendingEvent(evt);}
	virtual bool OnDo() = 0;
	virtual bool OnUndo() = 0;
	
protected:
	ASCCCommand *m_LinkedNode;

	DECLARE_CLASS(ASCCCommand);
};

class ASCCCommandGroup : public ASCCCommand
{
public:
	ASCCCommandGroup(wxString CommandName):ASCCCommand(CommandName),m_NumLinks(0){}
	ASCCCommandGroup(const ASCCCommandGroup& other):ASCCCommand(other){}
	virtual ASCCCommand *Clone() const {return(new ASCCCommandGroup(*this));}
	virtual ASCCCommand& Link(const ASCCCommand& linkedNode)
	{
		this->m_NumLinks++;
		return(ASCCCommand::Link(linkedNode));
	}
	virtual ASCCCommand& Link(ASCCCommand *linkedNode)
	{
		this->m_NumLinks++;
		return(ASCCCommand::Link(linkedNode));
	}
	ASCCCommand *Link() const {return(this->m_LinkedNode);}

protected:
	unsigned int m_NumLinks;
	virtual bool OnDo(){return(true);}
	virtual bool OnUndo(){return(true);}

	DECLARE_CLASS(ASCCCommandGroup);
};

//Taxa commands - Add, Delete, Rename, Min OKR, Max OKR, Group
class Taxon;
class OKR;
class OKRLink;
class TC_Base : public ASCCCommand
{
public:
	TC_Base(wxString CommandName);
	TC_Base(const TC_Base& other);
	virtual ~TC_Base();

	void SetTaxon(Taxon *newTaxon);
	void SetTaxon(wxString TaxonName);
	Taxon *GetTaxon();

	void SetOKR(const OKR& OKRToCopy);
	OKR *GetOKR();

	void SetMinOKR(double minOKR);
	double GetMinOKR();

	void SetMaxOKR(double maxOKR);
	double GetMaxOKR();

	void SetSameAs(wxString SameAs);
	wxString GetSameAs();

	void SetTaxonName(wxString TaxonName);
	wxString GetTaxonName();

	void SetLink(OKRLink& Link);
	OKRLink& GetLink();

	void SetLinkIndex(unsigned int LinkIndex);
	unsigned int GetLinkIndex();
protected:
	bool DoAddTaxon();
	bool DoDeleteTaxon();
	bool DoRenameTaxon();
	bool DoSetMinOKR();
	bool DoSetMaxOKR();
	bool DoSetSameAs();
	bool DoCopyOKR();
	bool DoLinkOKR();
	bool DoUnlinkOKR();
	bool DoRelinkOKR();
protected:
	Taxon *m_Taxon;
	OKR *m_OKR;
	double m_MinOKR;
	double m_MaxOKR;
	wxString m_SameAs;
	wxString m_TaxonName;
	wxString m_TaxonNameLookup;
	OKRLink m_Link;
	unsigned int m_LinkIndex;
	bool m_TaxonOwned;

	DECLARE_CLASS(TC_Base);
};

class TC_Add : public TC_Base
{
public:
	TC_Add(Taxon *TaxonToAdd):TC_Base("Add Taxon"){this->SetTaxon(TaxonToAdd);}
	TC_Add(wxString NewTaxonName):TC_Base("Add Taxon"){this->SetTaxonName(NewTaxonName);}
	
	TC_Add(const TC_Add& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_Add(*this));}
protected:
	virtual bool OnDo(){return(this->DoAddTaxon());}
	virtual bool OnUndo(){return(this->DoDeleteTaxon());}

	DECLARE_CLASS(TC_Add);
};

class TC_Delete : public TC_Base
{
public:
	TC_Delete(Taxon *TaxonToDelete):TC_Base("Delete Taxon"){this->SetTaxon(TaxonToDelete);}
	TC_Delete(wxString TaxonName):TC_Base("Delete Taxon"){this->SetTaxon(TaxonName);}
	
	TC_Delete(const TC_Delete& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_Delete(*this));}
protected:
	virtual bool OnDo(){return(this->DoDeleteTaxon());}
	virtual bool OnUndo(){return(this->DoAddTaxon());}

	DECLARE_CLASS(TC_Delete);
};

class TC_Rename : public TC_Base
{
public:
	TC_Rename(Taxon *TaxonToRename, wxString NewName):TC_Base("Rename Taxon"){this->SetTaxon(TaxonToRename); this->SetTaxonName(NewName);}
	TC_Rename(wxString TaxonName, wxString NewName):TC_Base("Rename Taxon"){this->SetTaxon(TaxonName); this->SetTaxonName(NewName);}
	
	TC_Rename(const TC_Rename& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_Rename(*this));}
protected:
	virtual bool OnDo(){return(this->DoRenameTaxon());}
	virtual bool OnUndo(){return(this->DoRenameTaxon());}

	DECLARE_CLASS(TC_Rename);
};

class TC_SetMinOKR : public TC_Base
{
public:
	TC_SetMinOKR(Taxon *TaxonToChange, double MinOKR):TC_Base("Set Min OKR"){this->SetTaxon(TaxonToChange); this->SetMinOKR(MinOKR);}
	TC_SetMinOKR(wxString TaxonName, double MinOKR):TC_Base("Set Min OKR"){this->SetTaxon(TaxonName); this->SetMinOKR(MinOKR);}
	
	TC_SetMinOKR(const TC_SetMinOKR& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_SetMinOKR(*this));}
protected:
	virtual bool OnDo(){return(this->DoSetMinOKR());}
	virtual bool OnUndo(){return(this->DoSetMinOKR());}

	DECLARE_CLASS(TC_SetMinOKR);
};

class TC_SetMaxOKR : public TC_Base
{
public:
	TC_SetMaxOKR(Taxon *TaxonToChange, double MaxOKR):TC_Base("Set Max OKR"){this->SetTaxon(TaxonToChange); this->SetMaxOKR(MaxOKR);}
	TC_SetMaxOKR(wxString TaxonName, double MaxOKR):TC_Base("Set Max OKR"){this->SetTaxon(TaxonName); this->SetMaxOKR(MaxOKR);}
	
	TC_SetMaxOKR(const TC_SetMaxOKR& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_SetMaxOKR(*this));}
protected:
	virtual bool OnDo(){return(this->DoSetMaxOKR());}
	virtual bool OnUndo(){return(this->DoSetMaxOKR());}

	DECLARE_CLASS(TC_SetMaxOKR);
};

class TC_Group : public TC_Base
{
public:
	TC_Group(Taxon *TaxonToChange, wxString SameAsTaxon):TC_Base("Taxon Group"){this->SetTaxon(TaxonToChange); this->SetSameAs(SameAsTaxon);}
	TC_Group(wxString TaxonName, wxString SameAsTaxon):TC_Base("Taxon Group"){this->SetTaxon(TaxonName); this->SetSameAs(SameAsTaxon);}
	
	TC_Group(const TC_Group& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_Group(*this));}
protected:
	virtual bool OnDo(){return(this->DoSetSameAs());}
	virtual bool OnUndo(){return(this->DoSetSameAs());}

	DECLARE_CLASS(TC_Group);
};

class TC_CopyOKR : public TC_Base
{
public:
	TC_CopyOKR(Taxon *TaxonToChange, const OKR& OKRToCopy):TC_Base("Set OKR"){this->SetTaxon(TaxonToChange); this->SetOKR(OKRToCopy);}
	TC_CopyOKR(wxString TaxonName, const OKR& OKRToCopy):TC_Base("Set OKR"){this->SetTaxon(TaxonName); this->SetOKR(OKRToCopy);}
	
	TC_CopyOKR(const TC_CopyOKR& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_CopyOKR(*this));}
protected:
	virtual bool OnDo(){return(this->DoCopyOKR());}
	virtual bool OnUndo(){return(this->DoCopyOKR());}

	DECLARE_CLASS(TC_CopyOKR);
};

class TC_LinkOKR : public TC_Base
{
public:
	TC_LinkOKR(Taxon *TaxonToChange, OKRLink NewLink):TC_Base("Link OKR"){this->SetTaxon(TaxonToChange); this->SetLink(NewLink);}
	TC_LinkOKR(wxString TaxonName, OKRLink NewLink):TC_Base("Link OKR"){this->SetTaxon(TaxonName); this->SetLink(NewLink);}
	
	TC_LinkOKR(const TC_LinkOKR& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_LinkOKR(*this));}
protected:
	virtual bool OnDo(){return(this->DoLinkOKR());}
	virtual bool OnUndo(){return(this->DoUnlinkOKR());}

	DECLARE_CLASS(TC_LinkOKR);
};

class TC_UnlinkOKR : public TC_Base
{
public:
	TC_UnlinkOKR(Taxon *TaxonToChange, unsigned int LinkIndex):TC_Base("Unlink OKR"){this->SetTaxon(TaxonToChange); this->SetLinkIndex(LinkIndex);}
	TC_UnlinkOKR(wxString TaxonName, unsigned int LinkIndex):TC_Base("Unlink OKR"){this->SetTaxon(TaxonName); this->SetLinkIndex(LinkIndex);}
	
	TC_UnlinkOKR(const TC_UnlinkOKR& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_UnlinkOKR(*this));}
protected:
	virtual bool OnDo(){return(this->DoUnlinkOKR());}
	virtual bool OnUndo(){return(this->DoLinkOKR());}

	DECLARE_CLASS(TC_UnlinkOKR);
};

class TC_RelinkOKR : public TC_Base
{
public:
	TC_RelinkOKR(Taxon *TaxonToChange, unsigned int LinkIndex, OKRLink NewLink):TC_Base("Change OKR Link"){this->SetTaxon(TaxonToChange); this->SetLinkIndex(LinkIndex); this->SetLink(NewLink);}
	TC_RelinkOKR(wxString TaxonName, unsigned int LinkIndex, OKRLink NewLink):TC_Base("Change OKR Link"){this->SetTaxon(TaxonName); this->SetLinkIndex(LinkIndex); this->SetLink(NewLink);}
	
	TC_RelinkOKR(const TC_RelinkOKR& other):TC_Base(other){}
	virtual ASCCCommand *Clone() const {return(new TC_RelinkOKR(*this));}
protected:
	virtual bool OnDo(){return(this->DoRelinkOKR());}
	virtual bool OnUndo(){return(this->DoRelinkOKR());}

	DECLARE_CLASS(TC_RelinkOKR);
};

//PTreeCommands - AddNode, DeleteNode, RenameNode, MoveNode
class PTreeNode;
class PTreeCtrl;
typedef ASCCCommandGroup PTC_Group;
class PTC_Base : public ASCCCommand
{
public:
	PTC_Base(wxString CommandName);
	PTC_Base(const PTC_Base& other);
	virtual ~PTC_Base();

	void SetCtrl(PTreeCtrl *Ctrl);
	PTreeCtrl *GetCtrl();

	void SetNode(PTreeNode *Node);
	void SetNode(PTreeNode *Parent, int index);
	PTreeNode *GetNode();

	void SetParent(PTreeNode *Parent);
	void SetParent(PTreeNode *Grandparent, int index);
	PTreeNode *GetParent();

	void SetLeafLabel(wxString LeafLabel);
	wxString GetLeafLabel();

	void SetNodeIndex(int NodeIndex);
	int GetNodeIndex();

	PTC_Base& Relayout(bool relayout);
	bool Relayout();

protected:
	bool DoMoveNode();
	bool DoRenameNode();
	bool DoAddNode();
	bool DoDeleteNode();
	bool DoAddTree();
	bool DoDeleteTree();
	
protected:
	PTreeCtrl *m_Ctrl;
	PTreeNode *m_ParentNode;
	PTreeNode *m_Node;
	int m_NodeIndex, m_NodeLookupIndex, m_ParentNodeLookupIndex;
	bool m_NodeOwned, m_Relayout;
	wxString m_LeafLabel;
	wxSize m_LargestTextExtent;

	DECLARE_CLASS(PTC_Base);
};
class PTC_AddNode : public PTC_Base
{
public:
	//Constructor 1
	PTC_AddNode(PTreeNode *Parent, int IndexForNewNode = -1, PTreeNode *Node = NULL):PTC_Base("Add Node"){if (Parent){this->SetParent(Parent);} if (Node){this->SetNode(Node);} this->SetNodeIndex(IndexForNewNode);}
	PTC_AddNode(PTreeNode *Grandparent, int ParentLookupIndex, int IndexForNewNode, PTreeNode *Node = NULL):PTC_Base("Add Node"){if (Grandparent){this->SetParent(Grandparent,ParentLookupIndex);} if (Node){this->SetNode(Node);} this->SetNodeIndex(IndexForNewNode);}
	PTC_AddNode(PTreeNode *Parent, int NodeLookupIndex, int IndexForNewNode):PTC_Base("Add Node"){if (Parent){this->SetParent(Parent); this->SetNode(Parent, NodeLookupIndex);} this->SetNodeIndex(IndexForNewNode);}
	//Constructor 2
	PTC_AddNode(PTreeNode *Parent, wxString LeafLabel):PTC_Base("Add Leaf"){if (Parent){this->SetParent(Parent);} this->SetLeafLabel(LeafLabel);}
	PTC_AddNode(PTreeNode *Grandparent, int ParentLookupIndex, wxString LeafLabel):PTC_Base("Add Leaf"){if (Grandparent){this->SetParent(Grandparent,ParentLookupIndex);} this->SetLeafLabel(LeafLabel);}

	PTC_AddNode(const PTC_AddNode& other):PTC_Base(other){}
	virtual PTC_Base *Clone() const {return(new PTC_AddNode(*this));}
protected:
	virtual bool OnDo(){return(this->DoAddNode());}
	virtual bool OnUndo(){return(this->DoDeleteNode());}

	DECLARE_CLASS(PTC_AddNode);
};
class PTC_DeleteNode : public PTC_Base
{
public:
	PTC_DeleteNode(PTreeNode *Node):PTC_Base("Delete Node"){if (Node){this->SetNode(Node);}}
	PTC_DeleteNode(PTreeNode *Parent, int NodeLookupIndex):PTC_Base("Delete Node"){if (Parent){this->SetNode(Parent,NodeLookupIndex);}}

	PTC_DeleteNode(const PTC_DeleteNode& other):PTC_Base(other){}
	virtual PTC_Base *Clone() const {return(new PTC_DeleteNode(*this));}
protected:
	virtual bool OnDo(){return(this->DoDeleteNode());}
	virtual bool OnUndo(){return(this->DoAddNode());}

	DECLARE_CLASS(PTC_DeleteNode);
};
class PTC_RenameNode : public PTC_Base
{
public:
	PTC_RenameNode(PTreeNode *Node, wxString NewLabel):PTC_Base("Rename Node"){if (Node){this->SetNode(Node);} this->SetLeafLabel(NewLabel);}
	PTC_RenameNode(PTreeNode *Parent, int NodeLookupIndex, wxString NewLabel):PTC_Base("Rename Node"){if (Parent){this->SetNode(Parent,NodeLookupIndex);} this->SetLeafLabel(NewLabel);}

	PTC_RenameNode(const PTC_RenameNode& other):PTC_Base(other){}
	virtual PTC_Base *Clone() const {return(new PTC_RenameNode(*this));}
protected:
	virtual bool OnDo(){return(this->DoRenameNode());}
	virtual bool OnUndo(){return(this->DoRenameNode());}

	DECLARE_CLASS(PTC_RenameNode);
};
class PTC_MoveNode : public PTC_Base
{
public:
	PTC_MoveNode(PTreeNode *Node, PTreeNode *NewParent):PTC_Base("Move Node"){if (Node){this->SetNode(Node);} if (NewParent){this->SetParent(NewParent);} this->SetNodeIndex(-1);}
	PTC_MoveNode(PTreeNode *Node, PTreeNode *NewParent, int NewIndex):PTC_Base("Move Node"){if (Node){this->SetNode(Node);} if (NewParent){this->SetParent(NewParent);} this->SetNodeIndex(NewIndex);}
	PTC_MoveNode(PTreeNode *Parent, int NodeLookupIndex, PTreeNode *NewParent, int NewIndex = -1):PTC_Base("Move Node"){if (Parent){this->SetNode(Parent,NodeLookupIndex);} if (NewParent){this->SetParent(NewParent);} this->SetNodeIndex(NewIndex);}
	PTC_MoveNode(PTreeNode *Node, PTreeNode *NewGrandparent, int ParentLookupIndex, int NewIndex):PTC_Base("Move Node"){if (Node){this->SetNode(Node);} if (NewGrandparent){this->SetParent(NewGrandparent,ParentLookupIndex);} this->SetNodeIndex(NewIndex);}
	PTC_MoveNode(PTreeNode *Parent, int NodeLookupIndex, PTreeNode *NewGrandparent, int ParentLookupIndex, int NewIndex = -1):PTC_Base("Move Node"){if (Parent){this->SetNode(Parent,NodeLookupIndex);} if (NewGrandparent){this->SetParent(NewGrandparent,ParentLookupIndex);} this->SetNodeIndex(NewIndex);}

	PTC_MoveNode(const PTC_MoveNode& other):PTC_Base(other){}
	virtual PTC_Base *Clone() const {return(new PTC_MoveNode(*this));}
protected:
	virtual bool OnDo(){return(this->DoMoveNode());}
	virtual bool OnUndo(){return(this->DoMoveNode());}

	DECLARE_CLASS(PTC_MoveNode);
};
class PTC_AddTree : public PTC_Base
{
public:
	PTC_AddTree(PTreeCtrl *Ctrl, wxString Label = wxEmptyString, int Index = -1, PTreeNode* Node = NULL):PTC_Base("Add Tree"){if (Ctrl){this->SetCtrl(Ctrl);} this->SetNode(Node); this->SetLeafLabel(Label); this->SetNodeIndex(Index);}
	
	PTC_AddTree(const PTC_AddTree& other):PTC_Base(other){}
	virtual PTC_Base *Clone() const {return(new PTC_AddTree(*this));}
protected:
	virtual bool OnDo(){return(this->DoAddTree());}
	virtual bool OnUndo(){return(this->DoDeleteTree());}

	DECLARE_CLASS(PTC_AddTree);
};
class PTC_DeleteTree : public PTC_Base
{
public:
	PTC_DeleteTree(PTreeNode *Node, int Index):PTC_Base("Delete Tree"){this->SetNode(Node); this->SetNodeIndex(Index);}

	PTC_DeleteTree(const PTC_DeleteTree& other):PTC_Base(other){}
	virtual PTC_Base *Clone() const {return(new PTC_DeleteTree(*this));}
protected:
	virtual bool OnDo(){return(this->DoDeleteTree());}
	virtual bool OnUndo(){return(this->DoAddTree());}

	DECLARE_CLASS(PTC_DeleteTree);
};
#endif

