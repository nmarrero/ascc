/*	Utils.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __UTILS_HEADER__
#define __UTILS_HEADER__

#include <wx/wx.h>
#include <wx/xml/xml.h>

template<typename T>
T min(T a, T b){return (a<=b?a:b);}
template<typename T>
T max(T a, T b){return (a>=b?a:b);}

wxXmlNode *CreateXmlContentNode(wxString Name, wxString Content);


#endif

