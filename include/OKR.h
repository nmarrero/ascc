/*	OKR.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file OKR.h
 * @author Nico Marrero
 * @date September, 2012
 * @brief This file contains the data structures used for dynamic and predefined Oldest Known Records
 *
 */
/* Why are OKRs separate from Taxa? Because of the Predefined OKRs based on geologic ages */
#ifndef __OLDEST_KNOWN_RECORD_HEADER__
#define __OLDEST_KNOWN_RECORD_HEADER__

#include <wx/wx.h>
#include <wx/datstrm.h>
#include <wx/dcbuffer.h>
#include <wx/xml/xml.h>
#include <exposure/wx/wxExposure.h>

//Declare the two main classes here so they can use each other interoperably (Taxon is included from Taxa.h in OKR.cpp)
class OKR;
class Taxon;

/** 
 * @class OKRLink
 * @brief This represents an age constraint between two Taxa or a Taxon and predefined OKR (geologic time slice)
 */
typedef enum {YOUNGER_THAN, OLDER_THAN} OKRLinkType;
class OKRLink : virtual public Exposure
{
public:
	/** 
	 * @param link The type of link (YOUNGER_THAN or OLDER_THAN)
	 * @param okr The okr that constrains the owner of this OKRLink
	 */
	OKRLink(OKRLinkType link, const OKR *okr):m_LinkType(link),m_LinkedOKR(okr){}
	/** The LinkType defaults to YOUNGER_THAN and the LinkedOKR is set to NULL */
	OKRLink():m_LinkType(YOUNGER_THAN),m_LinkedOKR(NULL){}
	/**
	 * @name Accessors & Mutators
	 * @{
	 */
	/** Returns the type of this link (YOUNGER_THAN or OLDER_THAN) */
	OKRLinkType Type(){return(this->m_LinkType);}
	/** 
	 * Sets the type of this link (YOUNGER_THAN or OLDER_THAN).
	 * YOUNGER_THAN means that the OKR set by @ref LinkedOKR is 
	 *	younger than the owner of this OKRLink. OLDER_THAN means
	 *	it is older than the owner of this OKRLink.
	 */
	void Type(OKRLinkType newType){this->m_LinkType = newType;}
	/** Returns the OKR that constrains the owner of this OKRLink */
	const OKR *LinkedOKR(){return(this->m_LinkedOKR);}
	/** Sets the OKR that constrains the owner of this OKRLink */
	void LinkedOKR(const OKR* newOKR){this->m_LinkedOKR = newOKR;}
	/** @} */

	/** Compares two OKRLinks to see if they have the same type and link to the same OKR */
	bool operator==(const OKRLink& other);

	/**
	 * @name Save/Load Functions
	 * @{
	 */
	/** 
	 * This is called to load a single OKRLink from an ASCC binary file, 
	 *	but it is not meant to be called directly
	 */
	wxDataInputStream& Load(wxDataInputStream& strm);
	/** 
	 * This is called to load a single OKRLink from an ASCC XML file,
	 *	but it is not meant to be called directly
	 */
	bool Load(wxXmlNode* Current);
	/** 
	 * This is called to write a single OKRLink to an ASCC XML file,
	 *	but it is not meant to be called directly
	 */
	wxXmlNode* Save();
	/** @} */
private:
	OKRLinkType m_LinkType;
	const OKR *m_LinkedOKR;

	DECLARE_EXPOSURE_TABLE();
};

/** 
 * @class OKRLinkArray
 * An array of OKRLinks based on a wxWidgets Object Array
 */
WX_DECLARE_OBJARRAY(OKRLink,_OKRLinkArray);
class OKRLinkArray : public _OKRLinkArray
{
public:
	/** Adds a new OKRLink with type YOUNGER_THAN and OKR LinkedOKR */
	OKRLinkArray& YoungerThan(const OKR& LinkedOKR)
	{
		this->Add(OKRLink(YOUNGER_THAN,&LinkedOKR));
		return(*this);
	}
	/** Adds a new OKRLink with type OLDER_THAN and OKR LinkedOKR */
	OKRLinkArray& OlderThan(const OKR& LinkedOKR)
	{
		this->Add(OKRLink(OLDER_THAN,&LinkedOKR));
		return(*this);
	}
	/** Appends another OKRLinkArray and ensures no duplicates */
	void Append(const OKRLinkArray& other)
	{
		bool FoundLink = false;
		for(unsigned int link = 0, size = other.size(); link < size; ++link)
		{
			FoundLink = false;
			for(unsigned int mylink = 0, mysize = this->size(); mylink < mysize; ++mylink)
			{
				if (this->Item(mylink) == other[link]){FoundLink = true;}
			}
			if (!FoundLink){this->Add(other[link]);}
		}
	}
};


/**
 * @class OKREvent
 * This is calls that is fired when a predefined OKR is selected in the Predefined OKR window
 */
DECLARE_EVENT_TYPE(EVT_OKR_PREDEFINED_SELECTION,-1);
class OKREvent : public wxCommandEvent
{
public:
	OKREvent(wxEventType commandType, int winid = 0):wxCommandEvent(commandType,winid){}
	OKREvent(const OKREvent& other):wxCommandEvent(other),m_OKR(other.m_OKR){}

	void SetOKR(const OKR* pOKR){this->m_OKR = pOKR;}
	const OKR* GetOKR() const {return(this->m_OKR);}

	wxEvent *Clone(void) const { return new OKREvent(*this); }
protected:
	const OKR *m_OKR;
};
typedef void (wxEvtHandler::*OKREventFunction)(OKREvent&);

#define EVT_OKR_SELECTION(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( EVT_OKR_PREDEFINED_SELECTION, id, -1, \
    (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) \
    wxStaticCastEvent( OKREventFunction, & fn ), (wxObject *) NULL ),

typedef const OKR *predefOKR;
WX_DECLARE_STRING_HASH_MAP(predefOKR,PredefinedOKRHash);

/**
 * @class OKR
 * This class represents an Oldest Known Record
 */
class OKR : public wxObject, virtual public Exposure
{
public:
	OKR():m_Owner(NULL),m_SameAsLink(NULL),m_MinOKR(0),m_MaxOKR(0),m_IsSameAs(false),m_AcceptableOKR(false){}
	OKR(Taxon *Owner):m_Owner(Owner),m_SameAsLink(NULL),m_MinOKR(0),m_MaxOKR(0),m_IsSameAs(false),m_AcceptableOKR(false){}
	OKR(Taxon *Owner, OKR *SameAs):m_Owner(Owner),m_SameAsLink(SameAs),m_MinOKR(0),m_MaxOKR(0),m_IsSameAs(true),m_AcceptableOKR(false){}
	OKR(double Min, double Max):m_Owner(NULL),m_SameAsLink(NULL),m_MinOKR(Min),m_MaxOKR(Max),m_IsSameAs(false),m_AcceptableOKR(false){}
	OKR(double Min, double Max, wxString Name, OKRLinkArray Links)
		:m_Owner(NULL),m_SameAsLink(NULL),m_MinOKR(Min),m_MaxOKR(Max),m_IsSameAs(false),m_Name(Name),m_OKRLinkArray(Links),m_AcceptableOKR(false)
	{
		if (Name != "No Fossil Record"){OKR::sm_PredefinedOKRHash[Name] = this;}
		this->m_AcceptableOKR = true;
	}
	OKR(const OKR& other):m_Owner(NULL){this->operator=(other);}
	OKR(Taxon *Owner, const OKR& other)
	{
		this->operator=(other);
		this->m_Owner = Owner;
	}
	
	OKR& operator=(const OKR& other)
	{
		if (other.m_Owner){this->m_Owner = other.m_Owner;}
		this->m_SameAsLink = other.m_SameAsLink;
		this->m_IsSameAs = other.m_IsSameAs;
		this->m_MinOKR = other.m_MinOKR;
		this->m_MaxOKR = other.m_MaxOKR;
		this->m_OKRLinkArray.Empty();
		this->m_OKRLinkArray.Append(other.m_OKRLinkArray);
		this->m_AcceptableOKR = other.m_AcceptableOKR;
		return(*this);
	}

	Taxon *Owner(void) const {return(this->m_Owner);}
	wxString PredefinedName(void) const {return(this->m_Name);}
	OKR *SameAs(void) const {return(this->m_SameAsLink);}
	wxString SameAsTaxon(); //can't be const because of Exposure
	bool IsSameAs(void){return(this->m_IsSameAs);} //can't be const because of Exposure
	bool IsNotSameAs(void){return(!this->m_IsSameAs);} //can't be const because of Exposure
	double ExposureMinOKR(void){return(this->MinOKR());}
	double ExposureMaxOKR(void){return(this->MaxOKR());}
	double MinOKR(void) const {if (this->m_IsSameAs && this->m_SameAsLink){return(this->m_SameAsLink->MinOKR());} return(this->m_MinOKR);}
	double MaxOKR(void) const {if (this->m_IsSameAs && this->m_SameAsLink){return(this->m_SameAsLink->MaxOKR());} return(this->m_MaxOKR);}
	double CurrentOKR(void) const {if (this->m_IsSameAs && this->m_SameAsLink){return(this->m_SameAsLink->CurrentOKR());} return(this->m_CurrentOKR);}
	bool AcceptableOKR() const {return(this->m_AcceptableOKR);}
	//double CurrentOKR(void) const {return(this->m_CurrentOKR);}
	unsigned int OKRLinks() const {if (this->m_IsSameAs && this->m_SameAsLink){return(this->m_SameAsLink->OKRLinks());} return(this->m_OKRLinkArray.size());}
	OKRLink& Link(unsigned int index) const 
	{
		if (this->m_IsSameAs && this->m_SameAsLink){return(this->m_SameAsLink->Link(index));} 
		wxASSERT_MSG(index < this->m_OKRLinkArray.size(),"Index given to OKR::OKRLink larger than array size"); return(this->m_OKRLinkArray[index]);
	}

	void Owner(Taxon *newOwner){this->m_Owner = newOwner;}
	virtual void SameAs(OKR *sameOKR){this->m_SameAsLink = sameOKR; this->IsSameAs(true);}
	virtual void SameAsTaxon(wxString TaxonName);
	void IsSameAs(bool TorF){this->m_IsSameAs = TorF;}
	void IsNotSameAs(bool TorF){this->m_IsSameAs = !TorF;}
	virtual void MinOKR(double min){this->m_MinOKR = min;}
	virtual void MaxOKR(double max){this->m_MaxOKR = max;}
	void CurrentOKR(double current){this->m_CurrentOKR = current;}
	void AcceptableOKR(bool acceptable){this->m_AcceptableOKR = acceptable;}
	void RemoveLink(unsigned int index){if (index < this->m_OKRLinkArray.size()){this->m_OKRLinkArray.RemoveAt(index);}}
	OKR& YoungerThan(const OKR *older)
	{
		if (!older){return(*this);} 
		//if (this->m_IsSameAs && this->m_SameAsLink){this->m_SameAsLink->YoungerThan(older);}
		this->m_OKRLinkArray.Add(OKRLink(YOUNGER_THAN,older));
		return(*this);
	}
	OKR& OlderThan(const OKR *younger)
	{
		if (!younger){return(*this);} 
		//if (this->m_IsSameAs && this->m_SameAsLink){this->m_SameAsLink->OlderThan(younger);}
		this->m_OKRLinkArray.Add(OKRLink(OLDER_THAN,younger));
		return(*this);
	}

	wxDataInputStream& Load(wxDataInputStream& strm);
	wxTextInputStream& Load(wxTextInputStream& strm);
	bool Load(wxXmlNode* Current);
	wxDataOutputStream& Save(wxDataOutputStream& strm);
	wxTextOutputStream& Save(wxTextOutputStream& strm);
	wxXmlNode* Save();

	bool operator==(const OKR& other) const
	{
		bool Result = true;
		Result &= this->MinOKR() == other.MinOKR();
		Result &= this->MaxOKR() == other.MaxOKR();
		if (Result && (this->m_IsSameAs || other.m_IsSameAs))
		{
			const OKR *MySameAs = this, *OtherSameAs = &other;
			while(MySameAs->SameAs()){MySameAs = MySameAs->SameAs();}
			while(OtherSameAs->SameAs()){OtherSameAs = OtherSameAs->SameAs();}
			Result &= MySameAs == OtherSameAs;
		}
		return(Result);
	}
	static const OKR *FindPredefinedOKR(wxString Name) {if (OKR::sm_PredefinedOKRHash.find(Name) != OKR::sm_PredefinedOKRHash.end()) {return(OKR::sm_PredefinedOKRHash[Name]);} return(NULL);}
protected:
	Taxon *m_Owner;
	OKR *m_SameAsLink;
	bool m_IsSameAs, m_AcceptableOKR;
	double m_MinOKR;
	double m_MaxOKR;
	double m_CurrentOKR;
	wxString m_Name;
	OKRLinkArray m_OKRLinkArray;

	static PredefinedOKRHash sm_PredefinedOKRHash;
	DECLARE_CLASS(OKR);
	DECLARE_EXPOSURE_TABLE();
};

namespace PredefinedOKR
{
	extern const OKR NoFossilRecord;
	extern const OKR HoloceneStage;
	extern const OKR TarantianStage;
	extern const OKR IonianStage;
	extern const OKR CalabrianStage;
	extern const OKR GelasianStage;
	extern const OKR PiacenzianStage;
	extern const OKR ZancleanStage;
	extern const OKR MessinianStage;
	extern const OKR TortonianStage;
	extern const OKR SerravallianStage;
	extern const OKR LanghianStage;
	extern const OKR BurdigalianStage;
	extern const OKR AquitanianStage;
	extern const OKR ChattianStage;
	extern const OKR RupelianStage;
	extern const OKR PriabonianStage;
	extern const OKR BartonianStage;
	extern const OKR LutetianStage;
	extern const OKR YpresianStage;
	extern const OKR ThanetianStage;
	extern const OKR SelandianStage;
	extern const OKR DanianStage;
	extern const OKR MaastrichtianStage;
	extern const OKR CampanianStage;
	extern const OKR SantonianStage;
	extern const OKR ConiacianStage;
	extern const OKR TuronianStage;
	extern const OKR CenomanianStage;
	extern const OKR AlbianStage;
	extern const OKR AptianStage;
	extern const OKR BarremianStage;
	extern const OKR HauterivianStage;
	extern const OKR ValanginianStage;
	extern const OKR BerriasianStage;
	extern const OKR TithonianStage;
	extern const OKR KimmeridgianStage;
	extern const OKR OxfordianStage;
	extern const OKR CallovianStage;
	extern const OKR BathonianStage;
	extern const OKR BajocianStage;
	extern const OKR AalenianStage;
	extern const OKR ToarcianStage;
	extern const OKR PliensbachianStage;
	extern const OKR SinemurianStage;
	extern const OKR HettangianStage;
	extern const OKR RhaetianStage;
	extern const OKR NorianStage;
	extern const OKR CarnianStage;
	extern const OKR LadinianStage;
	extern const OKR AnisianStage;
	extern const OKR OlenekianStage;
	extern const OKR InduanStage;
	extern const OKR ChanghsingianStage;
	extern const OKR WuchiapingianStage;
	extern const OKR CapitanianStage;
	extern const OKR WordianStage;
	extern const OKR RoadianStage;
	extern const OKR KungurianStage;
	extern const OKR ArtinskianStage;
	extern const OKR SakmarianStage;
	extern const OKR AsselianStage;
	extern const OKR GzhelianStage;
	extern const OKR KasimovianStage;
	extern const OKR MoscovianStage;
	extern const OKR BashkirianStage;
	extern const OKR SerpukhovianStage;
	extern const OKR ViseanStage;
	extern const OKR TournaisianStage;
	extern const OKR FamennianStage;	
	extern const OKR FrasnianStage;
	extern const OKR GivetianStage;
	extern const OKR EifelianStage;
	extern const OKR EmsianStage;
	extern const OKR PragianStage;
	extern const OKR LochkovianStage;
	extern const OKR PridoliSeries;
	extern const OKR LudfordianStage;
	extern const OKR GorstianStage;
	extern const OKR HomerianStage;
	extern const OKR SheinwoodianStage;
	extern const OKR TelychianStage;
	extern const OKR AeronianStage;
	extern const OKR RhuddanianStage;
	extern const OKR HirnantianStage;
	extern const OKR KatianStage;
	extern const OKR SandbianStage;
	extern const OKR DarriwilianStage;
	extern const OKR DapingianStage;
	extern const OKR FloianStage;
	extern const OKR TremadocianStage;
	extern const OKR Stage10;
	extern const OKR Stage9;
	extern const OKR PaibianStage;
	extern const OKR GuzhangianStage;
	extern const OKR DrumianStage;
	extern const OKR Stage5;
	extern const OKR Stage4;
	extern const OKR Stage3;
	extern const OKR Stage2;
	extern const OKR FortunianStage;
	extern const OKR EdiacaranSystem;
	extern const OKR CryogenianSystem;
	extern const OKR TonianSystem;
	extern const OKR StenianSystem;
	extern const OKR EctasianSystem;
	extern const OKR CalymmianSystem;
	extern const OKR StatherianSystem;
	extern const OKR OrosirianSystem;
	extern const OKR RhyacianSystem;
	extern const OKR SiderianSystem;
	extern const OKR NeoarcheanEra;
	extern const OKR MesoarcheanEra;
	extern const OKR PaleoarcheanEra;
	extern const OKR EoarcheanEra;
	extern const OKR HeadanEon;
};

//Selectable text node
class OKRNode;
class OKRSelectionTree;
WX_DEFINE_ARRAY(OKRNode*,OKRNodeArray);
class OKRNode : public wxControl
{
public:
	OKRNode(wxString Name, wxString SecondaryName, wxColour Colour, wxWindow *parent, 
		wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxBORDER_NONE)
		:wxControl(parent, id, pos, size, style),m_Name(Name),m_SecondaryName(SecondaryName),m_Colour(Colour)
	{
		if (Colour.IsOk()){wxControl::SetBackgroundColour(Colour);}
		wxControl::SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	}
	void AddChild(OKRNode *node)
	{
		if (node)
		{
			this->m_Children.Add(node);
		}
	}
	virtual void ResetMinSelection(){for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index){this->m_Children[index]->ResetMinSelection();}}
	virtual void ResetMaxSelection(){for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index){this->m_Children[index]->ResetMaxSelection();}}
	virtual wxPoint MarkMinSelection(double AgeMin)
	{
		bool FoundMin = false;
		wxPoint MinPos = wxPoint(0,0);
		for(int index = this->m_Children.size()-1, size = 0; index >= size; --index)
		{
			if (!FoundMin)
			{
				if ((MinPos = this->m_Children[index]->MarkMinSelection(AgeMin)) != wxPoint(0,0))
				{
					MinPos += this->GetPosition();
					FoundMin = true; 
				}
			}
			else {this->m_Children[index]->ResetMinSelection();}
		}
		return(MinPos);
	}
	virtual wxPoint MarkMaxSelection(double AgeMax)
	{
		bool FoundMax = false;
		wxPoint MaxPos = wxPoint(0,0);
		for(unsigned int index = 0, size = this->m_Children.size(); index < size; ++index)
		{
			if (!FoundMax)
			{
				if((MaxPos = this->m_Children[index]->MarkMaxSelection(AgeMax)) != wxPoint(0,0)){FoundMax = true; MaxPos += this->GetPosition();}
			}
			else {this->m_Children[index]->ResetMaxSelection();}
		}
		return(MaxPos);
	}
	unsigned int Children(){return(this->m_Children.size());}
	void LayoutChildren();
	void ColourChildren(wxColour Colour1, wxColour Colour2);
	void OnPaint(wxPaintEvent& event){wxAutoBufferedPaintDC dc(this); dc.Clear(); this->DoPaint(dc);}
	virtual void RemoveChild(wxWindowBase *child);
protected:
	OKRNodeArray m_Children;
	wxString m_Name;
	wxString m_SecondaryName;
	wxColour m_Colour;

	virtual void DoPaint(wxDC& dc);

	DECLARE_CLASS(OKRNode);
	DECLARE_EVENT_TABLE();
};

class OKRSelectableNode : public OKRNode
{
public:
	OKRSelectableNode(wxString Name, wxString SecondaryName, wxColour Colour, const OKR *OKRSelection,OKRNode *parent, wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxBORDER_NONE)
		:OKRNode(Name, SecondaryName, Colour, parent, id, pos, size, style),m_OKRSelection(OKRSelection),m_HasAgeMin(false),m_HasAgeMax(false)
	{
		this->SetCursor(wxCursor(wxCURSOR_HAND));
		wxControl::SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	}
	void OnMouseLeftUp(wxMouseEvent& event)
	{
		OKREvent evt(EVT_OKR_PREDEFINED_SELECTION,this->GetId());
		evt.SetString(this->m_Name);
		evt.SetOKR(this->m_OKRSelection);
		this->AddPendingEvent(evt);
	}
	virtual void ResetMinSelection(){if (this->m_HasAgeMin){this->m_HasAgeMin = false; this->Refresh();}}
	virtual void ResetMaxSelection(){if (this->m_HasAgeMax){this->m_HasAgeMax = false; this->Refresh();}}
	virtual wxPoint MarkMinSelection(double AgeMin)
	{
		if (AgeMin >= this->m_OKRSelection->MinOKR() && AgeMin <= this->m_OKRSelection->MaxOKR())
		{
			this->m_HasAgeMin = true;
			this->m_AgeMinLocation = (AgeMin - this->m_OKRSelection->MinOKR())/(this->m_OKRSelection->MaxOKR() - this->m_OKRSelection->MinOKR());
			this->Refresh();
		}
		else {this->ResetMinSelection();}
		return(this->m_HasAgeMin?this->GetPosition():wxPoint(0,0));
	}
	virtual wxPoint MarkMaxSelection(double AgeMax)
	{
		if (AgeMax >= this->m_OKRSelection->MinOKR() && AgeMax <= this->m_OKRSelection->MaxOKR())
		{
			this->m_HasAgeMax = true;
			this->m_AgeMaxLocation = (AgeMax - this->m_OKRSelection->MinOKR())/(this->m_OKRSelection->MaxOKR() - this->m_OKRSelection->MinOKR());
			this->Refresh();
		}
		else {this->ResetMaxSelection();}
		return(this->m_HasAgeMax?this->GetPosition():wxPoint(0,0));
	}
	void OnPaint(wxPaintEvent& event){wxAutoBufferedPaintDC dc(this); this->DoPaint(dc);}

	
protected:
	const OKR *m_OKRSelection;
	bool m_HasAgeMin, m_HasAgeMax;
	double m_AgeMinLocation, m_AgeMaxLocation;

	virtual void DoPaint(wxDC& dc);

	DECLARE_CLASS(OKRSelectableNode);
	DECLARE_EVENT_TABLE();
};

typedef enum {EON, ERA, SYSTEM, SERIES, STAGE} GeologicTimePeriod;
struct GeologicScaleData
{
	wxString Name;
	GeologicTimePeriod Type;
	const OKR *PredefinedOKR;
	const wxColour Colour;
	const wxColour EndColour;
};

extern GeologicScaleData GeologicScale[];

class OKRSelectionTree : public wxScrolledWindow
{
public:
	OKRSelectionTree():m_Tree(NULL){}
	OKRSelectionTree(wxWindow *parent, wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxBORDER_SIMPLE)
		:m_Tree(NULL)
	{this->Create(parent,id,pos,size,style);}
	bool Create(wxWindow *parent, wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxBORDER_SIMPLE);

	void OnSize(wxSizeEvent& event)
	{
		this->LayoutChildren();
		this->Refresh();
	}
	void LayoutChildren()
	{
		if (this->m_Tree)
		{
			this->m_Tree->LayoutChildren();
			this->SetVirtualSize(this->m_Tree->GetSize());
		}
	}
	void ResetAgeSelection()
	{
		if (this->m_Tree)
		{
			this->m_Tree->ResetMinSelection();
			this->m_Tree->ResetMaxSelection();
			this->Scroll(this->GetScrollLines(wxHORIZONTAL),0);
		}
	}
	void MarkAgeSelection(double MinAge, double MaxAge, bool ScrollToSelection = false)
	{
		if (this->m_Tree)
		{
			wxPoint MinPos = this->CalcUnscrolledPosition(this->m_Tree->MarkMinSelection(MinAge));
			wxPoint MaxPos = this->CalcUnscrolledPosition(this->m_Tree->MarkMaxSelection(MaxAge));
			wxSize ClientSize = this->GetClientSize();
			wxPoint ViewStart = this->GetViewStart();
			int ScrollX, ScrollY; this->GetScrollPixelsPerUnit(&ScrollX,&ScrollY);
			ViewStart.x *= ScrollX;
			ViewStart.y *= ScrollY;

			if (ScrollToSelection &&
				(MinPos.y < ViewStart.y || MinPos.y > ViewStart.y + this->GetClientSize().GetHeight() ||
				MaxPos.y < ViewStart.y && MaxPos.y > ViewStart.y + this->GetClientSize().GetHeight()))
			{
				float MinPosPercent = float(MinPos.y)/float(this->GetVirtualSize().GetHeight());
				this->Scroll(-1,int(float(this->GetScrollLines(wxVERTICAL))*MinPosPercent));
			}
		}
	}
	void ScrollToEdge()
	{
		this->Scroll(this->GetScrollLines(wxHORIZONTAL),-1);
	}
protected:
	OKRNode *m_Tree;

	DECLARE_CLASS(OKRSelectionTree);
	DECLARE_EVENT_TABLE();
};

#endif

