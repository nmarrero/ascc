/*	ASCCDataWindow.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <wx/wx.h>
#include <wx/notebook.h>
#include <wx/dcgraph.h>
#include <wx/dcmemory.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include "ASCCData.h"
#include "mathplot.h"

WX_DECLARE_HASH_MAP(long, unsigned long, wxIntegerHash, wxIntegerEqual,DifferenceHash);
extern wxString LayerNames[];

class ASCCHistogramWindow: public mpWindow
{
	
public:
    ASCCHistogramWindow( wxWindow *parent, wxWindowID id,
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long flags = 0);
protected:
	void OnChar(wxKeyEvent& event);
	DECLARE_EVENT_TABLE();
};

class ASCCInfoLegend: public mpInfoLegend
{
public:
    /** Default constructor */
	ASCCInfoLegend();

    /** Complete constructor, setting initial rectangle and background brush.
        @param rect The initial bounding rectangle.
        @param brush The wxBrush to be used for box background: default is transparent
        @sa mpInfoLayer::mpInfoLayer */
	ASCCInfoLegend(wxRect rect, const wxBrush* brush = wxTRANSPARENT_BRUSH, wxString HelpString = wxEmptyString);

    /**  Default destructor */
	~ASCCInfoLegend();

    /** Plot method.
        @param dc the device content where to plot
        @param w the window to plot
        @sa mpLayer::Plot */
    virtual void   Plot(wxDC & dc, mpWindow & w);

protected:
    wxString m_HelpString;
};
class ASCCFXYVector: public mpFXYVector
{
public:
	ASCCFXYVector(wxString Name);
	virtual void Plot(wxDC & dc, mpWindow & w);
};

class ASCCDataAnalysisWindow: public wxFrame
{
public:
	ASCCDataAnalysisWindow();
	ASCCDataAnalysisWindow(ASCCDataAnalysis *data);

	void LoadData(ASCCDataAnalysis *data);
	void UnloadData();
	~ASCCDataAnalysisWindow();
protected:
	void BuildGrid();
	void BuildHistograms();
	void ExportGrid(wxString Filename = wxEmptyString);
	void ExportFAD(wxString Filename = wxEmptyString);
	void GenerateMenu();
	void OnContextMenu(wxContextMenuEvent& event);
	void ShowContextMenu();
	void OnCopy(wxCommandEvent& event);
	void OnGridRightClick(wxGridEvent& event);
	void OnSelectAll(wxCommandEvent& event);
	void OnViewDetails(wxCommandEvent& event);
	void OnExportGrid(wxCommandEvent& event);
	void OnExportFAD(wxCommandEvent& event);
	void OnSave(wxCommandEvent& event);
	void OnLoad(wxCommandEvent& event);

	wxArrayString m_TreeNames;
	wxGrid *m_Grid;
	wxNotebook *m_Notebook;
	bool m_GER, m_MSM, m_MIG, m_dAGL;
	unsigned int m_Replicates;
	unsigned int m_Seed;
	ASCCDataAnalysis *m_Data;
	bool m_ShowReplicates;
	double *m_MinMIG, *m_MinGER, *m_MinMSM, *m_MindAGL, *m_MinFAD, *m_FADMinChosenAges;
	double *m_MaxMIG, *m_MaxGER, *m_MaxMSM, *m_MaxdAGL, *m_MaxFAD, *m_FADMaxChosenAges;

	DECLARE_EVENT_TABLE();
};


