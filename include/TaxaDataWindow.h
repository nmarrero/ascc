/*	TaxaDataWindow.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __TAXA_DATA_WINDOW__
#define __TAXA_DATA_WINDOW__

#include <exposure/wx/wxExposure.h>
#include <wx/wx.h>
#include <wx/cmdproc.h>

class Taxon;
class OKRLink;
class ASCCCommand;

DECLARE_EVENT_TYPE(EVT_CHANGE_LINK,-1);

class TaxaAgeLinkWindow : public wxPanel
{
public:
	TaxaAgeLinkWindow(){this->Init();}
	TaxaAgeLinkWindow(wxWindow *parent,
            wxWindowID winid = wxID_ANY,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = wxTAB_TRAVERSAL | wxNO_BORDER,
            const wxString& name = wxPanelNameStr)
	{
		this->Init();
		this->Create(parent, winid, pos, size, style, name);
	}
	virtual bool TransferDataToWindow();
	
	OKRLink *Link(){return(this->m_Link);}
	unsigned int LinkIndex(){return(this->m_LinkIndex);}
	void Link(unsigned int LinkIndex, OKRLink *Link){this->m_LinkIndex = LinkIndex; this->m_Link = Link; this->TransferDataToWindow();}

	void UpdateTaxa();
protected:
	void Init(){this->m_Link = NULL; m_TaxaSelection = NULL; m_AgeSelection = NULL;}
	bool Create(wxWindow *parent,
            wxWindowID winid = wxID_ANY,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = wxTAB_TRAVERSAL | wxNO_BORDER,
            const wxString& name = wxPanelNameStr);
	void OnUpdate(wxCommandEvent& event)
	{
		wxCommandEvent evt(EVT_CHANGE_LINK);
		evt.SetInt(this->m_LinkIndex); 
		evt.SetString(wxString::Format("%d %s",this->m_AgeSelection->GetSelection(),this->m_TaxaSelection->GetStringSelection())); 
		this->AddPendingEvent(evt);
	}
	OKRLink *m_Link;
	unsigned int m_LinkIndex;

	//Widgets (ptrs here for easy access without lookups)
	wxChoice *m_TaxaSelection;
	wxChoice *m_AgeSelection;

	DECLARE_EVENT_TABLE();
};

WX_DEFINE_ARRAY(TaxaAgeLinkWindow*,TaxaAgeLinkWindowArray);
class TaxaDataWindow : public wxScrolledWindow
{
	friend class TaxaAgeLinkWindow;
public:
	TaxaDataWindow(){this->Init();}
	TaxaDataWindow(wxWindow *parent,
            wxWindowID winid = wxID_ANY,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = wxScrolledWindowStyle,
            const wxString& name = wxPanelNameStr)
	{
		this->Init();
		this->Create(parent, winid, pos, size, style, name);
	}
	~TaxaDataWindow(){}

	void ChangeObject(Taxon *NewTaxon);
	Taxon *GetObject(){return(this->m_CurrentTaxon);}
	void Update();
	void Commit(){this->m_TaxaData->CommitInterface();}
	void UpdateTaxa()
	{
		for(unsigned int index = 0, size = this->m_LinkWindows.size(); index < size; ++index)
		{
			this->m_LinkWindows[index]->UpdateTaxa();
		}
	}

	static void SetCommandProcessor(wxCommandProcessor *Processor){TaxaDataWindow::sm_CommandProcessor = Processor;}
protected:
	void Init(){this->m_TaxaData = NULL; this->m_CurrentTaxon = NULL; this->m_LinkWindowsSizer = NULL;}
	bool Create(wxWindow *parent,
            wxWindowID winid = wxID_ANY,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = wxScrolledWindowStyle,
            const wxString& name = wxPanelNameStr)
	{
		bool Result = wxScrolledWindow::Create(parent, winid, pos, size, style, name);
		this->SetScrollRate(0,10);
		if (!this->m_TaxaData){this->m_TaxaData = new wxExposurePanel(this,NULL,wxID_ANY); this->m_TaxaData->SetScrollRate(0,0);}
		this->Fit();
		return(Result);
	}
	void AddLink();
	void DeleteLink();

	void OnChangeLink(wxCommandEvent& event);
	void OnAddLink(wxCommandEvent& event);
	void OnDeleteLink(wxCommandEvent& event);

	static bool SubmitCommand(const ASCCCommand& Cmd);

	wxExposurePanel *m_TaxaData;
	Taxon *m_CurrentTaxon;
	TaxaAgeLinkWindowArray m_LinkWindows;
	wxSizer *m_LinkWindowsSizer;
	static wxCommandProcessor *sm_CommandProcessor;

	DECLARE_EVENT_TABLE();
};

#endif

