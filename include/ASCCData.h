/*	ASCCData.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ASCC_DATA_HEADER__
#define __ASCC_DATA_HEADER__

#include "Taxa.h"
#include "PhylogeneticTree.h"

#include <wx/wx.h>
#include <wx/regex.h>
#include <wx/txtstrm.h>
#include <wx/wfstream.h>
#include <wx/grid.h>
#include <wx/busyinfo.h>
#include <wx/dirdlg.h>
#include <wx/arrimpl.cpp>
#include <wx/tokenzr.h>
#include <wx/progdlg.h>
#include <wx/debug.h>
#include <wx/numdlg.h>
#include <wx/clipbrd.h>
#include <wx/wupdlock.h>
#include <time.h>
#include <cstdlib>


WX_DEFINE_ARRAY(Taxon*,_TaxonArray);
class TaxonArray : public _TaxonArray
{
public:
	Taxon *Find(unsigned int ExportID) const;
	static int Compare(Taxon** first, Taxon** second);
};

WX_DEFINE_ARRAY_INT(int, IndexArray);
class Agebin;
class AgebinArray;
WX_DEFINE_ARRAY_PTR(Agebin*,_AgebinArray);
class AgebinArray : public _AgebinArray
{
	public:~AgebinArray();
			Agebin *Find(unsigned int Index);
};
class Agebin
{
public:
	Agebin();
	static AgebinArray& GetArray(){return(Agebin::sm_AgebinArray);}
	static void ResetArray();

	void Reset(){this->m_CurrentSet = false;}
	void Index(int index){this->m_Index = index;}
	void MaxValue(double max){this->m_MaxValue = max;}
	void MinValue(double min){this->m_MinValue = min;}
		
	int Index(){return(this->m_Index);}
	bool CurrentSet(){return(this->m_CurrentSet);}
	double CurrentValue(){return(this->m_CurrentValue);}
	double MaxValue(){return(this->m_MaxValue);}
	double MinValue(){return(this->m_MinValue);}
	void OlderThan(int linkindex);
	void YoungerThan(int linkindex);
	bool GenerateValue();
protected:
	int m_Index;
	double m_MaxValue, m_MinValue;
	bool m_CurrentSet;
	double m_CurrentValue;
	IndexArray m_OlderThan;
	IndexArray m_YoungerThan;

	static AgebinArray sm_AgebinArray;
};
int CompareAgebinRange(Agebin **first, Agebin **second);
	
void GenerateAgebinArray(TaxonArray& Taxa);
class ASCCDataAnalysisResult
{
public:
	ASCCDataAnalysisResult(unsigned int trees, unsigned int replicates);
	ASCCDataAnalysisResult(ASCCDataAnalysisResult &other);
	~ASCCDataAnalysisResult();
	unsigned int Size(){return(this->m_Size);}
	unsigned int Replicates(){return(this->m_Replicates);}
	void Taxa(unsigned int Taxa)
	{
		this->m_Taxa = Taxa; 
		if (this->m_FAD){wxDELETEA(this->m_FAD);} this->m_FAD = new double[this->m_Replicates*this->m_Size*Taxa];
		if (this->m_FADAges){wxDELETEA(this->m_FADAges);} this->m_FADAges = new double[this->m_Replicates*Taxa];
		if (this->m_MaxGhostLineages){wxDELETEA(this->m_MaxGhostLineages);} this->m_MaxGhostLineages = new double[this->m_Size * Taxa];
		if (this->m_MinGhostLineages){wxDELETEA(this->m_MinGhostLineages);} this->m_MinGhostLineages = new double[this->m_Size * Taxa];
	}
	unsigned int Taxa(){return(this->m_Taxa);}
	double MIG(unsigned int replicate, unsigned int tree){return(this->m_MIG[replicate*this->m_Size+tree]);}
	double GER(unsigned int replicate, unsigned int tree){return(this->m_GER[replicate*this->m_Size+tree]);}
	double MSM(unsigned int replicate, unsigned int tree){return(this->m_MSM[replicate*this->m_Size+tree]);}
	double dAGL(unsigned int replicate, unsigned int tree){return(this->m_dAGL[replicate*this->m_Size+tree]);}
	double FAD(unsigned int replicate, unsigned int tree, unsigned int taxon){return(this->m_FAD[replicate*this->m_Size*this->m_Taxa+tree*this->m_Taxa+taxon]);}
	double FADAge(unsigned int replicate, unsigned int taxon){return(this->m_FADAges[replicate*this->m_Taxa+taxon]);}
	double MinGhostLineage(unsigned int tree, unsigned int taxon){return(this->m_MinGhostLineages[tree*this->m_Taxa + taxon]);}
	double MaxGhostLineage(unsigned int tree, unsigned int taxon){return(this->m_MaxGhostLineages[tree*this->m_Taxa + taxon]);}
	double Lo(unsigned int replicate){return(this->m_Lo[replicate]);}
	double Lm(unsigned int replicate){return(this->m_Lm[replicate]);}
	double MIG(unsigned int replicate, unsigned int tree, double value){this->m_MIG[replicate*this->m_Size+tree] = value; return(value);}
	double GER(unsigned int replicate, unsigned int tree, double value){this->m_GER[replicate*this->m_Size+tree] = value; return(value);}
	double MSM(unsigned int replicate, unsigned int tree, double value){this->m_MSM[replicate*this->m_Size+tree] = value; return(value);}
	double dAGL(unsigned int replicate, unsigned int tree, double value){this->m_dAGL[replicate*this->m_Size+tree] = value; return(value);}
	double FAD(unsigned int replicate, unsigned int tree, unsigned int taxon, double value){this->m_FAD[replicate*this->m_Size*this->m_Taxa+tree*this->m_Taxa+taxon] = value; return(value);}
	double FADAge(unsigned int replicate, unsigned int taxon, double value){this->m_FADAges[replicate*this->m_Taxa+taxon] = value; return(value);}
	double MinGhostLineage(unsigned int tree, unsigned int taxon, double value){this->m_MinGhostLineages[tree*this->m_Taxa + taxon] = value; return(value);}
	double MaxGhostLineage(unsigned int tree, unsigned int taxon, double value){this->m_MaxGhostLineages[tree*this->m_Taxa + taxon] = value; return(value);}
	double Lo(unsigned int replicate, double value){this->m_Lo[replicate] = value; return(value);}
	double Lm(unsigned int replicate, double value){this->m_Lm[replicate] = value; return(value);}
protected:	
	double *m_MIG, *m_GER, *m_MSM, *m_dAGL, *m_FAD, *m_FADAges, *m_MinGhostLineages, *m_MaxGhostLineages;
	double *m_Lo, *m_Lm;
	unsigned int m_Size, m_Replicates, m_Taxa;
};

class ASCCDataAnalysis: virtual public Exposure
{
public:
	ASCCDataAnalysis();
	~ASCCDataAnalysis();
	void ShowMaxAgeWarning(bool show){this->m_ShowMaxAgeWarning = show;}
	bool ShowMaxAgeWarning(void){return(this->m_ShowMaxAgeWarning);}
	void Replicates(unsigned int Replicates){this->m_Replicates = Replicates;}
	unsigned int Replicates(){return(this->m_Replicates);}
	void Seed(unsigned int seed){this->m_Seed = seed; srand(this->m_Seed);}
	unsigned int Seed(){return(this->m_Seed);}
	void SetScoreMIG(bool use_mig = true){this->m_MIG = use_mig;}
	void SetScoreMSM(bool use_msm = true){this->m_MSM = use_msm;}
	void SetScoreGER(bool use_ger = true){this->m_GER = use_ger;}
	void SetScoredAGL(bool use_dagl = true){this->m_dAGL = use_dagl;}
	bool GetScoreMIG(void){return(this->m_MIG);}
	bool GetScoreMSM(void){return(this->m_MSM);}
	bool GetScoreGER(void){return(this->m_GER);}
	bool GetScoredAGL(void){return(this->m_dAGL);}
	bool CalculateScores(PTreeCtrl *ctrl);
	void Save(wxString Filename = wxEmptyString);
	const wxArrayString& TreeNames(){return(this->m_TreeNames);}
	const wxArrayString& TreeData(){return(this->m_TreeData);}
	const TaxonArray& Taxa(){return(this->m_TaxonArray);}
	ASCCDataAnalysisResult *Result(){return(this->m_Result);}
	static ASCCDataAnalysis *Load(wxString Filename = wxEmptyString);
protected:
	void NewReplicate();
	double CalculateMIG(PTreeCtrl *ctrl, unsigned int TreeIndex);
	double CalculateLo();
	double CalculateLm();
	double CalculateMSM(double Lo, double MIG);
	double CalculateGER(double Lo, double Lm, double MIG);
	double CalculatedAGL(double MIG);
	
	unsigned long m_Replicates, m_CurrentReplicate;
	unsigned int m_Seed;
	bool m_GER, m_MSM, m_MIG, m_dAGL;
	bool m_ShowMaxAgeWarning;
	ASCCDataAnalysisResult *m_Result;
	TaxonArray m_TaxonArray;
	wxArrayString m_TreeNames, m_TreeData;

	DECLARE_EXPOSURE_TABLE();
};

	wxString AnalyzeTNTScores(wxString ScoreLocation = wxEmptyString);
	wxArrayString ASCCImport(wxString Filename = wxEmptyString, bool ShowResults = false);

	typedef enum {RESTRUCTURE_CHRONOLOGICALLY, RESTRUCTURE_REVERSE_CHRONOLOGICALLY, NO_RESTRUCTURE, CANCELLED = wxID_CANCEL} RestructureOptions; 
	class ASCCRestructureDialog : public wxDialog
	{
	public:
		typedef enum {ALLOW_COMPOLY, DENY_COMPOLY} ComPolyOption;
		ASCCRestructureDialog();
		ASCCRestructureDialog(wxWindow *parent, 
			 ComPolyOption AllowComPolyMethod = DENY_COMPOLY,
			 wxString Caption = "How should the polytomies be restructured?",
			 wxWindowID id = wxID_ANY,
             const wxPoint& pos = wxDefaultPosition,
             const wxSize& size = wxDefaultSize,
             long style = wxDEFAULT_DIALOG_STYLE,
             const wxString& name = wxDialogNameStr);
		bool Create(wxWindow *parent, 
			 ComPolyOption AllowComPolyMethod = DENY_COMPOLY, 
			 wxString Caption = "How should the polytomies be restructured?",
			 wxWindowID id = wxID_ANY,
             const wxPoint& pos = wxDefaultPosition,
             const wxSize& size = wxDefaultSize,
             long style = wxDEFAULT_DIALOG_STYLE,
             const wxString& name = wxDialogNameStr);
	protected:
		void OnButton(wxCommandEvent& event);
		ComPolyOption m_AllowComPoly;

		DECLARE_CLASS(ASCCRestructureDialog);
		DECLARE_EVENT_TABLE();
	};

	static const char *ASCCIcon_xpm[] = {
/* columns rows colors chars-per-pixel */
	"32 32 95 2",
	"   c #855D0F",
	".  c #855F14",
	"X  c #8B610F",
	"o  c #8C6313",
	"O  c #86611A",
	"+  c #8B651B",
	"@  c #8F681E",
	"#  c #916715",
	"$  c #936815",
	"%  c #996B14",
	"&  c #936B1D",
	"*  c #986E1C",
	"=  c #9F731F",
	"-  c #A17217",
	";  c #856424",
	":  c #8A6722",
	">  c #8D6A25",
	",  c #85672B",
	"<  c #8C6B2B",
	"1  c #936D25",
	"2  c #916E2B",
	"3  c #967027",
	"4  c #9A7225",
	"5  c #95712B",
	"6  c #9B752B",
	"7  c #9F782C",
	"8  c #866A34",
	"9  c #8D6E32",
	"0  c #8F733B",
	"q  c #947334",
	"w  c #997634",
	"e  c #9D7933",
	"r  c #93743A",
	"t  c #97783B",
	"y  c #9C7B3B",
	"u  c #A07727",
	"i  c #A07729",
	"p  c #A1792D",
	"a  c #A27D35",
	"s  c #A17E3C",
	"d  c #9E7E42",
	"f  c #A07F40",
	"g  c #AD822F",
	"h  c #B68424",
	"j  c #A68035",
	"k  c #AA8234",
	"l  c #AC863D",
	"z  c #AF883C",
	"x  c #B18633",
	"c  c #BA8D37",
	"v  c #B68D3D",
	"b  c #9F834B",
	"n  c #A38244",
	"m  c #A98643",
	"M  c #A3844A",
	"N  c #A6884E",
	"B  c #AC8B4E",
	"V  c #B38E46",
	"C  c #B6934F",
	"Z  c #A28752",
	"A  c #A78C55",
	"S  c #AB8E56",
	"D  c #AE9057",
	"F  c #AD925F",
	"G  c #BB9A5A",
	"H  c #B19661",
	"J  c #B49A67",
	"K  c #BB9D63",
	"L  c #B49B6C",
	"P  c #BCA26E",
	"I  c #BBA475",
	"U  c #D3A347",
	"Y  c #C7A96D",
	"T  c #C4AB79",
	"R  c #C3AD82",
	"E  c #C7B186",
	"W  c #CAB384",
	"Q  c #C6B28B",
	"!  c #CAB58C",
	"~  c #CCBA96",
	"^  c #CDBB98",
	"/  c #D0BC94",
	"(  c #D1BF9B",
	")  c #D2C19E",
	"_  c #D4C4A2",
	"`  c #DDCAA5",
	"'  c #DACBAD",
	"]  c #DBCDB1",
	"[  c #DFD2B7",
	"{  c #DFD2B8",
	"}  c #E2D4B7",
	"|  c #E1D4B9",
	" . c #E6DAC3",
	".. c #ECE3CF",
	"X. c #F2EAD9",
	/* pixels */
	"r r q f n l y y y w r n s s t t y y y U U 7 9 w e t q Z Y V z z ",
	"t r 9 0 y s l y q q r r r r t t t y y e v c t 9 w y ( X.~ C l l ",
	"5 5 1 > > 3 1 1 1 1 $ $ # 1 : * * 1 4 * = h c % o / } a & 1 g g ",
	"+ & > > @ @ : 1 1 1 # # o > > : 1 1 5 3 * = z c ! [ 7 4 1 1 1 g ",
	"; : 1 < > > > : q 9 9 < < < < < < q q q w w y G X.n < 5 4 n t 2 ",
	"; ; : 2 < < > > < q 9 < < > @ + @ : 1 1 1 3 3 W ` x 6 + a ..D 2 ",
	"O O O O O < : + < : > > 9 > > > > > : q R S 3 _ T p 5 5 ) ..6 5 ",
	"; ; ; O O : + + < 1 1 1 9 9 > > > > 5 { ^ 3 6 X.T k p R X.H 3 3 ",
	", , 8 9 < < 1 > < < 2 1 q 9 > + > > Q ~ > 1 2 X.{ P ' X.W 5 6 6 ",
	". . . , < < : 1 > + < & & 2 > + + F | > : > y H ~ ~ R m 7 y q w ",
	". . . ; ; + @ : 1 X > & & 5 > + y  .q > : ~ J 1 1 > > 2 7 7 7 5 ",
	"> > : > 2 6 p 1 1 1 > > 2 w 6 < ' J > > y X.w 2 2 5 > > 7 7 7 7 ",
	"; > > > > 2 6 p p p 2 1 1 1 6 B ..+ + > ^ ' + : > 2 2 > > 7 7 7 ",
	"; ; , , : > > ; 9 > > < > 1 2 L _ @ + d ..A > > : 2 5 1 > > 4 4 ",
	"+ + > < > > 1 1 5 ; ; q _ M 2 _ R 6 q [ ~ < < < > < 2 1 3 9 9 e ",
	"6 6 6 5 5 2 > 3 5 6 q { ..R 5 ' _ S | ( > : 1 1 > > : $ 1 1 > > ",
	"+ * > * 5 5 1 > 6 6 | ~ ~ I < _ X. .L 6 5 < : : < > > > 2 5 3 3 ",
	"& + 6 o $ 1 # # > K  .i M X X > # - & & % 4 1 : > > > > > 5 5 3 ",
	"2 2 > 5 q F > > 9 _ ! 5 5 < + , , + 2 q 1 3 4 3 1 1 > > > > 5 5 ",
	"< < > > I X.M > 9 _ X.| X.N + < , : : q 5 1 4 4 4 1 1 9 < > 1 3 ",
	"q q q 5 E X.' + + > < < | R & < > + O , < 5 5 3 4 4 1 1 > > 1 3 ",
	"1 1 1 5 ] | X.y + @ < 5  .H 5 r 9 < , , , 9 0 q w 6 6 5 2 < y w ",
	"6 6 6 6 | W X.P P  .@ N ..6 * 5 3 3 * X   . : > e 5 6 6 5 1 1 4 ",
	"6 2 2 2 X.L ..X...d 2 { I 5 1 1 3 5 5 6 > ; ; : < 5 5 7 6 1 1 1 ",
	"1 6 y ..X.X.X.X.N 1 ' Q : > 1 1 1 1 3 3 6 > ; O < < 7 7 e e w w ",
	"2 2 s I X...J ..L w 1 > : > > 1 1 1 1 3 3 6 > + O + > p p p 7 6 ",
	"2 2 2 I X.H + T  .0 5 1 : ; : > 1 1 1 1 5 1 w < , O O > u i p 7 ",
	"2 2 2 ^ X.< @ w ..< < 1 1 + o > 1 1 1 + < & & 6 5 + X : : 6 6 6 ",
	"2 b _ ....r 9 M [ e k e w 5 : < < 2 q > < + & & w q > : : < e e ",
	"2 q Q X.! 5 2 q @ < e k e w 1 : : : > > < < + & 5 q q 1 + + < e ",
	"o o m X.1 1 1 < @ > : 4 j e 3 1 : O + + < < < + 2 2 2 q 1 & 1 4 ",
	". o o d > > > < @ > > : e e 4 1 1 + O O , < < + > 2 2 q 1 & 1 1 "
	};



#endif

