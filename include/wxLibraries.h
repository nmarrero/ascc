/*	wxLibraries.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef NDEBUG
	#pragma comment(lib, "wxmsw30ud_core.lib")
	#pragma comment(lib, "wxmsw30ud_adv.lib")
	#pragma comment(lib, "wxmsw30ud_aui.lib")
	#pragma comment(lib, "wxbase30ud.lib")
	#pragma comment(lib, "wxbase30ud_xml.lib")
	#pragma comment(lib, "wxregexud.lib")
	#pragma comment(lib, "wxexpatd.lib")
	#pragma comment(lib, "wxzlibd.lib")
	#pragma comment(lib, "wxjpegd.lib")
	#pragma comment(lib, "wxtiffd.lib")
	#pragma comment(lib, "wxpngd.lib")

#else
	#pragma comment(lib, "wxmsw30u_core.lib")
	#pragma comment(lib, "wxmsw30u_adv.lib")
	#pragma comment(lib, "wxmsw30u_aui.lib")
	#pragma comment(lib, "wxbase30u.lib")
	#pragma comment(lib, "wxbase30u_xml.lib")
	#pragma comment(lib, "wxregexu.lib")
	#pragma comment(lib, "wxexpat.lib")
	#pragma comment(lib, "wxzlib.lib")
	#pragma comment(lib, "wxjpeg.lib")
	#pragma comment(lib, "wxtiff.lib")
	#pragma comment(lib, "wxpng.lib")
#endif

#pragma comment(lib,"comctl32.lib")
#pragma comment(lib,"rpcrt4.lib")
