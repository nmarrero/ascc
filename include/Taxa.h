/*	Taxa.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __TAXA_HEADER__
#define __TAXA_HEADER__

#include <wx/wx.h>
#include <wx/datstrm.h>
#include <wx/txtstrm.h>
#include <exposure/wx/wxExposure.h>
#include "OKR.h"
#include "ASCCCommand.h"

class Taxon;

wxString DoubleTowxString(double num);
double wxStringToDouble(wxString text);

DECLARE_EVENT_TYPE(EVT_TAXON_SELECT,-1);

WX_DECLARE_STRING_HASH_MAP(Taxon*,_TaxaNameHash);
WX_DECLARE_HASH_MAP(int, Taxon*, wxIntegerHash, wxIntegerEqual, _TaxaIDHash);
class TaxaNameHash : public _TaxaNameHash {public: ~TaxaNameHash(){this->Destroy();} void Destroy();};
class TaxaIDHash : public _TaxaIDHash {public: ~TaxaIDHash(){this->Destroy();} void Destroy();};
class TC_Base;
class Taxon : public OKR, virtual public Exposure
{
friend class TC_Base;
public:
	Taxon():OKR(this),m_ImportID(-1),m_ExportID(-1){}
	Taxon(const Taxon& other):OKR(this,other),m_ImportID(-1),m_ExportID(-1){}

	~Taxon(){}
	wxString Destroy()
	{
		TaxaNameHash::iterator nameit = this->sm_TaxaNameHash.find(this->m_Name);
		if (nameit != this->sm_TaxaNameHash.end()){this->sm_TaxaNameHash.erase(nameit);}

		TaxaIDHash::iterator idit = this->sm_TaxaIDHash.find(this->m_ImportID);
		if (idit != this->sm_TaxaIDHash.end()){this->sm_TaxaIDHash.erase(idit);}

		this->sm_TaxaChanged1 = this->sm_TaxaChanged2 =true;

		delete this;
		return(wxEmptyString);
	}
	wxString Name(){return(this->m_Name);}
	int ImportID(){return(this->m_ImportID);}
	int ExportID(){if (this->m_ExportID == -1){this->m_ExportID = Taxon::sm_NextExportID++;}return(this->m_ExportID);}
	wxString SameAsTaxon(){return(OKR::SameAsTaxon());}
	double MinOKR() const {return(this->OKR::MinOKR());}
	double MaxOKR() const {return(this->OKR::MaxOKR());}
	double CurrentOKR(void) const {return(this->OKR::CurrentOKR());}
	void Name(wxString newName)
	{
		if (newName != this->m_Name)
		{
			this->SubmitCommand(TC_Rename(this,newName));
		}
	}
	void SelectSameAsTaxon()
	{
		wxString Taxon = this->SameAsTaxon();
		if (!Taxon.IsEmpty())
		{
			wxCommandEvent evt(EVT_TAXON_SELECT);
			evt.SetString(Taxon);
			wxTheApp->GetTopWindow()->wxEvtHandler::AddPendingEvent(evt);
		}
	}
	void ChangeName(wxString NewName);
	void ImportID(int newID);
	void ExportID(int newExportID){this->m_ExportID = newExportID;}
	void SameAsTaxon(wxString TaxonName)
	{
		if (TaxonName.IsEmpty()){return;}
		if (TaxonName.EndsWith("*")){TaxonName = TaxonName.BeforeLast('*');}
		if (TaxonName != this->SameAsTaxon())
		{
			if (TaxonName == this->Name()){wxMessageBox(this->Name() + " cannot be grouped with itself");}
			//Check to make sure that setting this as our SameAs won't cause a cycle
			Taxon *CurrentTaxon = Taxon::FindTaxonByName(TaxonName);
			while (CurrentTaxon && !CurrentTaxon->SameAsTaxon().IsEmpty())
			{
				if (CurrentTaxon->SameAsTaxon() == this->Name()){wxMessageBox("Grouping " + this->Name() + " with " + TaxonName + " would cause a cycle as " + CurrentTaxon->Name() + " points back to " + this->Name()); return;}
				CurrentTaxon = Taxon::FindTaxonByName(CurrentTaxon->SameAsTaxon());
			}
			this->SubmitCommand(TC_Group(this,TaxonName));
		}
	}
	void MinOKR(double min){double Threshold = 0.0000000000001; if (min > this->m_MinOKR + Threshold || min < this->m_MinOKR - Threshold){this->SubmitCommand(TC_SetMinOKR(this,min));}}
	void MaxOKR(double max)
	{
		double Threshold = 0.0000000000001; 
		if (max > this->m_MaxOKR + Threshold || max < this->m_MaxOKR - Threshold)
		{
			this->SubmitCommand(TC_SetMaxOKR(this,max));
		}
	}
	void CurrentOKR(double current){this->OKR::CurrentOKR(current);}

	wxDataInputStream& Load(wxDataInputStream& strm, wxString Name);
	wxTextInputStream& Load(wxTextInputStream& strm, wxString Name);
	wxDataOutputStream& Save(wxDataOutputStream& strm);
	wxTextOutputStream& Save(wxTextOutputStream& strm);
	wxXmlNode* Save();

	static void ClearTaxonData(){Taxon::sm_TaxaIDHash.Destroy(); Taxon::sm_TaxaNameHash.Destroy();  Taxon::sm_NextExportID = 1; Taxon::sm_TaxaChanged1 = Taxon::sm_TaxaChanged1 = true;}
	static Taxon *FindTaxonByName(wxString Name);
	static Taxon *FindTaxonByImportID(int ID);
	static Taxon *FindTaxonByExportID(int ExportID);
	static bool CheckTaxaAges(wxString *ErrorMessage, wxString *WarningMessage, bool ShowMaxAgeWarning=true);
	static wxArrayString GetTaxaNames(bool AddStarToNames = true);
	static wxArrayString GetTaxaNamesWithBlank(bool AddStarToNames = true){wxArrayString Names = GetTaxaNames(AddStarToNames); Names.Insert(wxEmptyString,0); return(Names);}
	static wxString *_GetTaxaNames();
	static wxString *_GetTaxaNamesWithBlank();
	static unsigned int GetTaxaCount();
	static unsigned int GetTaxaCountWithBlank();
	static bool GetTaxaChanged1(){bool changed = Taxon::sm_TaxaChanged1; Taxon::sm_TaxaChanged1 = false; return(changed);}
	static bool GetTaxaChanged2(){bool changed = Taxon::sm_TaxaChanged2; Taxon::sm_TaxaChanged2 = false; return(changed);}
	static int MaxExportID(){return(Taxon::sm_NextExportID-1);}
	static void ResetExportIDs();
	static void RemoveAutomaticLinks();
	static void SetCommandProcessor(wxCommandProcessor *CmdProc){Taxon::sm_CommandProcessor = CmdProc;}
	static wxDataInputStream& LoadAllTaxa(wxDataInputStream& strm);
	//static wxTextInputStream& LoadAllTaxa(wxTextInputStream& strm);
	static bool LoadAllTaxa(wxXmlNode *node);
	static bool Load(wxXmlNode *node);
	//static wxDataOutputStream& SaveAllTaxa(wxDataOutputStream& strm);
	//static wxTextOutputStream& SaveAllTaxa(wxTextOutputStream& strm);
	static wxXmlNode* SaveAllTaxa();
	
protected:
	bool SubmitCommand(const ASCCCommand& Cmd)
	{
		ASCCCommand *Clone = Cmd.Clone();
		if (Taxon::sm_CommandProcessor){return(Taxon::sm_CommandProcessor->Submit(Clone));}
		else {Clone->Do(); wxDELETE(Clone); return(false);}
	}
	int m_ImportID;
	int m_ExportID;

	static TaxaNameHash sm_TaxaNameHash;
	static TaxaIDHash sm_TaxaIDHash;
	static bool sm_TaxaChanged1;
	static bool sm_TaxaChanged2;
	static int sm_NextExportID;
	static wxCommandProcessor *sm_CommandProcessor;

	DECLARE_CLASS(Taxon);
	DECLARE_EXPOSURE_TABLE();
};

#endif

