/*	PhylogeneticTree.h
	Author: Nico Marrero
	Copyright 2012,2013 Clint Boyd
	
	This file is part of ASCC.

    ASCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ASCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ASCC.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __PHYLOGENETIC_TREE_HEADER__
#define __PHYLOGENETIC_TREE_HEADER__

#include <wx/wx.h>
#include <wx/datstrm.h>
#include <wx/cmdproc.h>
#include <wx/mstream.h>
#include <wx/dnd.h>
#include <wx/statline.h>
#include <wx/xml/xml.h>
#include <wx/clipbrd.h>

DECLARE_EVENT_TYPE(EVT_TREE_LAYOUT,-1);
DECLARE_EVENT_TYPE(EVT_TREE_SELECT_NODE,-1);
DECLARE_EVENT_TYPE(EVT_TREE_ENTER_NODE,-1);
DECLARE_EVENT_TYPE(EVT_TREE_LEAVE_NODE,-1);
DECLARE_EVENT_TYPE(EVT_TREE_RESTRUCTURE,-1);
DECLARE_EVENT_TYPE(EVT_TREE_REFRESH,-1);

typedef enum {LADDERIZE_LEFT, LADDERIZE_RIGHT} LadderizeType;
enum {MENUID_INSERT, MENUID_TOIMAGE, MENUID_RESTRUCTURE, MENUID_MERGE, MENUID_CHECKFORERRORS, MENUID_LADDERIZE_LEFT, MENUID_LADDERIZE_RIGHT, MENUID_ADDSISTERTOSUBTREE, MENUID_ADDANCHOR, MENUID_SHIFT};

class PTreeNode;
WX_DEFINE_ARRAY(PTreeNode*,_PTreeNodeArray);
class PTreeNodeArray : public _PTreeNodeArray
{
public:
	void Push(PTreeNode *newTop){this->Add(newTop);}
	PTreeNode *Pop(){PTreeNode *oldTop = this->Top(); this->RemoveAt(this->size()-1); return(oldTop);}
	PTreeNode *Top(){return(this->Item(this->size()-1));}
};
WX_DECLARE_STRING_HASH_MAP(PTreeNode*,_PTreeNodeHash);
class PTreeNodeHash : public _PTreeNodeHash
{
public:
	void Update(const PTreeNodeHash &NewHash)
	{
		PTreeNodeHash::const_iterator it;
		for(it = NewHash.begin(); it != NewHash.end(); ++it )
		{
			
			PTreeNodeHash::value_type val(it->first,it->second);
			this->insert(val);
		}
	}
};
struct SizeInfo
{
	wxSize MinSize;
	int NumChildren;
	int TreeDepth;
	SizeInfo():MinSize(wxSize(0,0)),NumChildren(0),TreeDepth(0){}
	int GetWidth(){return(MinSize.GetWidth());}
	void SetWidth(int width){MinSize.SetWidth(width);}
	int GetHeight(){return(MinSize.GetHeight());}
	void SetHeight(int height){MinSize.SetHeight(height);}
};
//Commands (found in ASCCCommand.h)
class ASCCCommand;
class PTC_Base;
class PTC_AddNode;
class PTC_DeleteNode;
class PTC_RenameNode;
class PTC_MoveNode;
class PTreeCtrl;
class PTreeNodeData;
class PTreeNode : public wxControl
{
	friend class PTreeCtrl;
	friend class PTC_Base;
public:
	PTreeNode();
	PTreeNode(PTreeNode *other, wxWindow *parent)
	{
		this->Create(parent,wxID_ANY,other->GetPosition(),other->GetSize(),other->GetWindowStyle());
		this->SetLabel(other->GetLabel());
		this->m_SizeInfo = other->m_SizeInfo;
		this->m_NodePosition = other->m_NodePosition;
		this->m_Selected = other->m_Selected;
		this->m_IsLeaf = other->m_IsLeaf;
		this->m_IsRoot = other->m_IsRoot;
		this->m_MouseDown = other->m_MouseDown;
		this->m_MIGAge = other->m_MIGAge;
		this->m_MIGDisagreement = other->m_MIGDisagreement;
		this->m_MIGDisagreementTotal = other->m_MIGDisagreementTotal;
		this->m_MinDivergenceAge = other->m_MinDivergenceAge;
		this->m_MinGhostLineage = other->m_MinGhostLineage;
		this->m_MaxDivergenceAge = other->m_MaxDivergenceAge;
		this->m_MaxGhostLineage = other->m_MaxGhostLineage;
		this->m_Error = other->m_Error;
		for(unsigned int child = 0, size = other->m_Children.size(); child < size; ++child)
		{
			this->m_Children.Add(new PTreeNode(other->m_Children[child], this));
		}
		this->m_Leaves.Update(other->m_Leaves);
	}
	PTreeNode(wxWindow *parent, wxWindowID id,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxBORDER_NONE);	//Changing this default style requires a complete rebuild -- FYI

	bool Create(wxWindow *parent, wxWindowID id,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxBORDER_NONE); //Changing this default style requires a complete rebuild -- FYI
	virtual ~PTreeNode();

	PTreeNode *Child(unsigned int index);
	unsigned int Children();
	virtual wxString Print(int indent = 0);
	void Draw(wxDC &dc, wxCoord ParentOriginX = 0, wxCoord ParentOriginY = 0);
	void SetLabel(const wxString& label);
	void SetFocus();
	SizeInfo GetSizeInfo();
	wxString GetTreeData();
	wxString GetTreeNameData();
	void SetRoot(bool isRoot);
	bool IsRoot(){return(this->m_IsRoot);}
	bool IsLeaf(){return(this->m_IsLeaf);}
	wxPoint NodePosition(){return(this->m_NodePosition);}
	virtual void RemoveChild(wxWindowBase *child);
	PTreeNode *AddChild(int index = -1);
	PTreeNode *AddLeaf(wxString Label, int index = -1);
	unsigned int CheckForErrors();
	wxString ReportError();
	void RestructurePolytomies(bool Chronologically, bool undoable = true, bool relayout = true); //undoable = false means the restructure won't appear in the undo stack, relayout = true means the subtree display will be recalculated
	double FindOldestChildAge();
	int FindChild(wxWindowBase *child);
	PTreeNode *DoAddChild(PTreeNode *newChild, int index = -1);
	PTC_Base *AddSisterTaxa();
	PTC_Base *AddSisterTaxon();
	PTC_Base *AddAnchorTaxon();
	void Selected(bool TorF){if (this->m_Selected != TorF){this->m_Selected = TorF; if (this->GetParent()){this->GetParent()->Refresh();}}}
	bool IsSelected(){return(this->m_Selected);}
	bool HasError(){return(this->m_Error != 0);}
	unsigned int Error(){return(this->m_Error);}
	PTreeNode *FindLeaf(wxString LabelToFind);
	void UpdateLabel(wxString OldName, wxString NewName);
	void UpdateLeafHash();
	void LayoutChildren();
	void ComputeDivergences();
	void DistributeGhostLineages();
	void InsertNodeAboveChild(PTreeNode *child);
	PTC_Base *MergeWithChild(PTreeNode *child);
	PTC_Base *DeleteNodes(wxString NodeLabelToDelete);
	void GetLeaves(wxArrayString& Result);
	unsigned int Ladderize(LadderizeType Type, ASCCCommand *Command);
	void ShiftBranches(ASCCCommand *Command);
	double CalculateMIG();
	double FAD(){return(this->m_MIGAge);}
	double MaxDivergenceAge();
	double MinDivergenceAge();
	double MaxGhostLineage();
	double MinGhostLineage();
	double CalculatedMaxGhostLineage();
	double CalculatedMinGhostLineage();
	void OnAddSisterTaxaToSubtree(wxCommandEvent& event);
	void OnAddSisterTaxonToNode(wxCommandEvent& event);
	void OnAddAnchorTaxon(wxCommandEvent& event);
	void OnAddChild(wxCommandEvent& event);
	void OnBeginDrag();
	void OnDraw(wxDC& dc, wxCoord OriginX = 0, wxCoord OriginY = 0);
	void OnAddLeaf(wxCommandEvent& event);
	void OnRename(wxCommandEvent& event);
	void OnRemove(wxCommandEvent& event);
	void OnReportError(wxCommandEvent& event);
	void OnDuplicate(wxCommandEvent& event);
	void OnMergeWithParent(wxCommandEvent& event);
	void OnInsertNodeAbove(wxCommandEvent& event);
	void OnPaint(wxPaintEvent& event);
	void OnMouseLeftDown(wxMouseEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	void OnMouseLeftUp(wxMouseEvent& event);
	void OnContextMenu(wxContextMenuEvent& event);
	void OnMouseEnter(wxMouseEvent& event);
	void OnMouseLeave(wxMouseEvent& event);
	void OnMenuHighlight(wxMenuEvent& event);
	void OnLadderizeLeft(wxCommandEvent& event);
	void OnLadderizeRight(wxCommandEvent& event);
	void OnRestructure(wxCommandEvent& event);
	void OnShiftBranches(wxCommandEvent& event);
	void OnCopy(wxCommandEvent& event);
	void OnCut(wxCommandEvent& event);
	void OnPaste(wxCommandEvent& event);
	bool DoPaste(PTreeNodeData *data, wxString CommandTitle);
	void SendLayoutEvent();
	void SendRefreshEvent();

	wxDataInputStream& Load(wxDataInputStream& strm); //Read this out of a datainputstream
	wxTextInputStream& Load(wxTextInputStream& strm); //Read this out of a datainputstream
	wxDataOutputStream& Save(wxDataOutputStream& strm); //Write this into a dataoutputstream
	wxTextOutputStream& Save(wxTextOutputStream& strm, unsigned int indent = 1); //Write this into a dataoutputstream
	bool Load(wxXmlNode *Current); //Read this node from an xml file and return the next node to process
	wxXmlNode* Save(); //Write this into an XML file

	static void ResetIDs(){PTreeNode::sm_NextID = wxID_HIGHEST;}
protected:
	PTreeNodeArray m_Children;
	PTreeNodeHash m_Leaves;
	SizeInfo m_SizeInfo;
	wxPoint m_NodePosition;
	bool m_Selected;
	bool m_IsLeaf;
	bool m_IsRoot;
	bool m_MouseDown;
	double m_MIGAge, m_MIGDisagreement, m_MIGDisagreementTotal;
	double m_MinDivergenceAge, m_MaxDivergenceAge;
	double m_MinGhostLineage, m_MaxGhostLineage, m_CalculatedMinGhostLineage, m_CalculatedMaxGhostLineage;
	unsigned int m_Error;

	static wxWindowID sm_NextID;
	static wxSize sm_LargestTextExtent;

	DECLARE_EVENT_TABLE();

public:
	typedef enum {
		ERROR_LEAF_WITH_CHILDREN = 1<<0,
		ERROR_LEAF_WITHOUT_LABEL = 1<<1,
		ERROR_TOO_MANY_CHILDREN = 1<<2,
		ERROR_TOO_FEW_CHILDREN = 1<<3
	} PhylogeneticError;
};

class PTreeCtrl : public wxScrolled<wxWindow>
{
	friend class PTC_Base;
public:
	PTreeCtrl(){}
	PTreeCtrl(wxWindow *parent, wxWindowID id,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxVSCROLL|wxBORDER_SIMPLE)
	{ this->Create(parent, id, pos, size, style); }

	bool Create(wxWindow *parent, wxWindowID id,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxVSCROLL|wxBORDER_SIMPLE)
	{
		if (!wxScrolled<wxWindow>::Create(parent, id, pos, size, style)){return(false);}
		wxScrolled<wxWindow>::SetScrollRate(0,10);
		wxScrolled<wxWindow>::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
		wxScrolled<wxWindow>::SetInitialSize(size);
		this->m_TreeSelector = new wxChoice(this,wxID_ANY,wxPoint(100,5),wxDefaultSize,0,NULL,wxBORDER_THEME);
		return(true);
	}
	~PTreeCtrl(){}
	
	void SaveDisplayAsImage(wxString Filename = wxEmptyString);
	wxString GetTreeData(unsigned int TreeIndex);
	wxString GetTreeNameData(unsigned int TreeIndex);
	wxArrayString GetTreeData();
	wxArrayString GetTreeNameData();
	void AddTree(wxString TreeName = wxEmptyString, wxString TreeData = wxEmptyString);
	void AddTree(wxArrayString Data);
	unsigned int CheckForErrors();
	unsigned int CheckForErrors(unsigned int index);
	void RestructurePolytomies(bool Chronologically);
	void UpdateLabel(wxString OldName, wxString NewName);
	void OnNewTree(wxCommandEvent& event);
	PTC_Base *DeleteNodes(wxString NodeLabelToDelete);
	void UpdateSelector()
	{
		bool AllTreesSelected = (this->m_TreeSelector->GetStringSelection() == "All Trees");
		//Collect the tree names
		wxArrayString Names;
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
		{
			Names.Add(this->m_Trees[index]->GetLabel());
		}
		if (Names.size() > 1){Names.Add("All Trees");}
		this->m_TreeSelector->Set(Names);
		if (AllTreesSelected && Names.size() > 1){this->m_TreeSelector->SetStringSelection("All Trees");}
		else if (this->m_TreeSelector->GetStringSelection().IsEmpty() && Names.size() > 0){this->m_TreeSelector->SetStringSelection(Names[0]); this->UpdateTreeDisplay(0);}
	}
	void OnTreeSelectorUpdate(wxCommandEvent& event)
	{
		this->UpdateTreeDisplay(event.GetInt());
	}
	void UpdateLeafHashes();
	void DistributeGhostLineages();
	void UpdateTreeDisplay(int TreeToDisplay);
	virtual void RemoveChild(wxWindowBase *child)
	{
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
		{
			if (this->m_Trees[index] == child)
			{
				this->m_Trees.RemoveAt(index);
				break;
			}
		}
		wxScrolled<wxWindow>::RemoveChild(child);
		this->UpdateSelector();
	}
	unsigned int Trees(){return(this->m_Trees.size());}
	wxArrayString TreeNames()
	{
		wxArrayString Result;
		for(unsigned int index = 0, size = this->Trees(); index < size; ++index)
		{
			Result.Add(this->m_Trees[index]->GetLabel());
		}
		return(Result);
	}
	void AddTree(PTreeNode *Tree, int index = -1)
	{
		if (index == -1){this->m_Trees.Add(Tree);}
		else {this->m_Trees.Insert(Tree,index);}
	}
	PTreeNode *GetTree(unsigned int index)
	{
		if (index < this->Trees())
		{
			return(this->m_Trees[index]);
		}
		return(NULL);
	}
	void OnRefresh(wxCommandEvent& event)
	{
		this->Refresh();
	}
	void OnSize(wxSizeEvent& event)
	{
		this->LayoutChildren();
		this->Refresh();
	}
	void OnLayoutChildren(wxCommandEvent& event)
	{
		this->LayoutChildren();
	}
	void OnSaveDisplayAsImage(wxCommandEvent& event)
	{
		this->SaveDisplayAsImage();
	}
	void OnContextMenu(wxContextMenuEvent& event)
	{
		wxMenu *ContextMenu = new wxMenu();
			ContextMenu->Append(wxID_NEW,"Add Tree", "Add a new tree");
			if (wxTheClipboard->IsOpened() || wxTheClipboard->Open())
			{
				if (wxTheClipboard->IsSupported(wxDataFormat("PTreeNode")))
				{
					ContextMenu->Append(wxID_PASTE,"Paste","Pastes the currently copied node as a new tree");
				}
				wxTheClipboard->Close();
			}
			wxMenu *LadderizeMenu = new wxMenu();
				LadderizeMenu->Append(MENUID_LADDERIZE_LEFT,"Left/Up");
				LadderizeMenu->Append(MENUID_LADDERIZE_RIGHT,"Right/Down");
			ContextMenu->AppendSubMenu(LadderizeMenu,"Ladderize");
			ContextMenu->AppendSeparator();
			ContextMenu->Append(MENUID_CHECKFORERRORS, "Check for Errors", "Checks the trees for errors");
			ContextMenu->AppendSeparator();
			ContextMenu->Append(MENUID_TOIMAGE, "Save As Image", "Saves the current tree display as an image");
			this->PopupMenu(ContextMenu);
			wxDELETE(ContextMenu);
	}
	void OnCheckForErrors(wxCommandEvent& event)
	{
		this->CheckForErrors();
	}
	void OnMouseLeftDown(wxMouseEvent& event)
	{
		if (this->FindFocus() != this)
		{
			this->SetFocus();
		}
	}
	double CalculateMIG(unsigned int TreeIndex)
	{
		if (TreeIndex >= this->m_Trees.size()){return(0.0);}
		return(this->m_Trees[TreeIndex]->CalculateMIG());
	}
	void ClearTreeData()
	{
		while(this->m_Trees.size() > 0)
		{
			PTreeNode *Child = this->m_Trees[0];
			this->RemoveChild(Child);
			wxDELETE(Child);
		}
		PTreeNode::sm_LargestTextExtent = wxSize(1,1);
	}
	void GetLeaves(unsigned int tree, wxArrayString& Result)
	{
		if (tree < this->Trees() && tree >= 0){this->m_Trees[tree]->GetLeaves(Result);}
	}
	void GetLeaves(wxArrayString& Result)
	{
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
		{
			this->GetLeaves(index,Result);
		}
	}
	int FindTree(wxWindowBase *child)
	{
		if (!child){return(-1);}
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
		{
			if (this->m_Trees[index] == child){return((int)index);}
		}
		return(-1);
	}
	PTreeNode *FindLeaf(wxString LabelToFind)
	{
		if (LabelToFind.IsEmpty()){return(NULL);}
		PTreeNode *Result = NULL;
		for(unsigned int index = 0, size = this->m_Trees.size(); index < size; ++index)
		{
			if ((Result = this->m_Trees[index]->FindLeaf(LabelToFind)) != NULL){return(Result);}
		}
		return(NULL);
	}
	void OnDraw(wxDC& dc);
	void OnLadderizeLeft(wxCommandEvent& event);
	void OnLadderizeRight(wxCommandEvent& event);
	void OnPaste(wxCommandEvent& event);
	void Ladderize(LadderizeType Type, ASCCCommand *Command);
	void Ladderize();
	wxDataInputStream& Load(wxDataInputStream& strm); //Read this out of a datainputstream
	wxTextInputStream& Load(wxTextInputStream& strm); //Read this out of a datainputstream
	wxDataOutputStream& Save(wxDataOutputStream& strm); //Write this into a dataoutputstream
	wxTextOutputStream& Save(wxTextOutputStream& strm); //Write this into a dataoutputstream
	bool Load(wxXmlNode* Current); 
	wxXmlNode* Save();

	static void SetCommandProcessor(wxCommandProcessor *CmdProc){PTreeCtrl::sm_CommandProcessor = CmdProc;}
	static bool SubmitCommand(const ASCCCommand& Cmd, bool undoable = true);

protected:
	PTreeNodeArray m_Trees;
	wxChoice *m_TreeSelector;
	static wxCommandProcessor *sm_CommandProcessor;
	void LayoutChildren();
private:
	DECLARE_EVENT_TABLE()
};


class PTreeNodeData : public wxDataObjectSimple
{
public:
	PTreeNodeData(PTreeNode *Node = NULL, bool cut=true):m_Node(Node),m_Cut(cut)
	{
		this->m_Format.SetId("PTreeNode");
		this->SetFormat(this->m_Format);
	}
	PTreeNode *GetNode(){return(this->m_Node);}
		 // get the size of our data
    virtual size_t GetDataSize() const
    { 
		return sizeof(_PTreeNodeData);
	}
	bool IsCut(){return(this->m_Cut);}
	bool IsCopy(){return(!this->m_Cut);}

    // copy our data to the buffer
    virtual bool GetDataHere(void *buf) const
    {
		_PTreeNodeData data(this->m_Node,this->m_Cut);
		memcpy(buf,&data,sizeof(_PTreeNodeData));
		return(true);
	}

    // copy data from buffer to our data
    virtual bool SetData(size_t len, const void *buf)
    {
		_PTreeNodeData data;
		memcpy(&data,buf,len);
		this->m_Node = data.m_Node;
		this->m_Cut = data.m_Cut;
		return(true);
	}
protected:
	wxDataFormat m_Format;
	PTreeNode *m_Node;
	bool m_Cut;
	struct _PTreeNodeData
	{
		_PTreeNodeData():m_Node(NULL),m_Cut(false){}
		_PTreeNodeData(PTreeNode *node, bool cut):m_Node(node),m_Cut(cut){}
		PTreeNode *m_Node;
		bool m_Cut;
	};
};
class PTreeNodeDropTarget : public wxDropTarget
{
public:
	PTreeNodeDropTarget(PTreeNode *Owner):m_Owner(Owner){this->SetDataObject(new PTreeNodeData);}
	virtual wxDragResult OnData(wxCoord x, wxCoord y, wxDragResult def);
	virtual wxDragResult OnEnter(wxCoord x, wxCoord y, wxDragResult def)
	{
		this->m_Owner->Selected(true);
		return(def);
	}
	virtual void OnLeave()
	{
		this->m_Owner->Selected(false);
	}
protected:
	PTreeNode *m_Owner;
};
#endif

